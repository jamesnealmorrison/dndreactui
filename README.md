Build instructions:

1. `npm install`
1. set env variable `PUBLIC_PATH` to be wherever all the assets will be hosted (S3 bucket)
1. `npm run build`
1. statically serve up the HTML file, and drop everything else in the S3 bucket, we should figure out a way to automate this
1. if you aren't going to static serve the html, just make sure whatever is generating the HTML looks something like the output html
1. right now build generates a manifest.json, favicon, etc. and a lot of placeholder icon files, we might need all that if this is going to be launched as a webview from within another app
