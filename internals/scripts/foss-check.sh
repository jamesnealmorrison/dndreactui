# !/bin/bash -xfe

 # install against the public registry
npm install --registry https://registry.npmjs.org/

if [[ -f packages.txt ]]; then rm packages.txt; fi
if [[ -f buildOutput.txt ]]; then rm buildOutput.txt; fi

 # pipe all of the packages into a file with no duplicates
npm ls | sed 's/[└─┬├│ \`\|\+]//g' | sed 's/\-\-//g' | sed 's/deduped//g' | sed '/UNMETPEERDEPENDENCY/g' | sed '/^$/d' | sort -u > packages.txt

# make sure we are logged into the ehi registry
npm login --registry https://artifactory.ehi.com/artifactory/api/npm/remote-npm-repos/

count=$(cat packages.txt | wc -l | sed 's/^ *//g')
 # iterate through the packages.txt file and try to build each
 # log errors to buildOutput.txt

 while read p; do
     current=$((current+1));
     echo "\rchecking $current of $count - $p";
     npm install $p --registry https://artifactory.ehi.com/artifactory/api/npm/remote-npm-repos/ --dry-run --no-build --fetch-retries 0 --loglevel silly \
     2>&1 | grep "npm ERR! 404" \
     | tee -a buildOutput.txt;
 done < packages.txt

 cat buildOutput.txt | sort -u

 if (( $(cat buildOutput.txt | wc -l) > 0 )); then
     echo "Failed to find dependences in artifactory."
     exit 1
 fi
