import ConnectedTextfield from 'App/shared/containers/ConnectedTextfield'

export const getFieldByName = (form, name) =>
  form
    .find(ConnectedTextfield)
    .filterWhere(field => field.prop('name') === name)
