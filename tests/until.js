/* eslint-disable */
import { ShallowWrapper } from 'enzyme'

function shallowRecursively(wrapper, selector) {
  // Do not try to shallow render empty nodes and host elements
  // (a.k.a primitives). Simply return the wrapper in that case.
  if (wrapper.isEmptyRender() || typeof wrapper.getElement().type === 'string')
    return wrapper

  const nextWrapper = wrapper.dive()

  return selector && wrapper.is(selector)
    ? nextWrapper
    : shallowRecursively(nextWrapper, selector)
}

function until(selector) {
  return this.single('until', () => shallowRecursively(this, selector))
}

ShallowWrapper.prototype.until = until
