/* eslint-disable import/first */
import 'babel-polyfill'

import 'jest-enzyme'

import './raf-polyfill'
import './until'

import { FormattedMessage } from 'react-intl'
import { is } from 'immutable'
import React from 'react'
import isMatch from 'lodash/fp/isMatch'

import Adapter from 'enzyme-adapter-react-16'
import Enzyme, { mount, shallow } from 'enzyme'

import StorageShim from './StorageShim'

Enzyme.configure({ adapter: new Adapter() })

window.localStorage = new StorageShim()
window.localStorage.setItem('apiToken', 'token')
global.shallowComponent = (Component, opts) => (props = {}) =>
  shallow(<Component {...props} />, opts)
global.mountComponent = (Component, opts) => (props = {}) =>
  mount(<Component {...props} />, opts)

const toString = obj =>
  obj && typeof obj.toString === 'function' ? obj.toString() : obj

function textRecursively(wrapper) {
  return wrapper
    .children()
    .map(child => {
      if (!child.children().length) {
        if (child.getElement()) {
          return '' // empty element with no children
        }
        return child.text() // text content
      }
      return textRecursively(child) // element has child nodes
    })
    .join('')
}

const matchers = {
  toEqualImmutable() {
    return {
      compare(actual, expected) {
        const pass = is(actual, expected)
        const message = () => `Expected
${toString(actual)}
${pass ? ' not' : ''} to equal
${toString(expected)}
`
        return {
          pass,
          message,
        }
      },
    }
  },
  toHaveMessage() {
    return {
      compare(actual, expected) {
        try {
          const messageProps = actual.find(FormattedMessage).props()
          return {
            pass: isMatch(expected, messageProps),
            message: () =>
              `Expected to include message "${expected.id}", but got "${
                messageProps.id
              }"`,
          }
        } catch (e) {
          return {
            pass: false,
            message: () =>
              `Counld not find FormattedMessage as child of ${actual}`,
          }
        }
      },
    }
  },
  toIncludeInnerText() {
    return {
      compare(actual, expected) {
        const text = textRecursively(actual)

        return {
          pass: text.includes(expected),
          message: () =>
            `Expected text to include "${expected}", but got "${text}"`,
        }
      },
    }
  },
  toHaveInnerText() {
    return {
      compare(actual, expected) {
        const text = textRecursively(actual)

        return {
          pass: text === expected,
          message: () => `Expected text to be "${expected}", but got "${text}"`,
        }
      },
    }
  },
}

beforeEach(() => {
  jest.addMatchers({ ...matchers })
})
