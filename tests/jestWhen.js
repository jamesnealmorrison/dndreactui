import findLast from 'lodash/fp/findLast'
import isEqual from 'lodash/fp/isEqual'

const jestWhen = mockFn => {
  const mockImplementations = []
  mockFn.mockImplementation((...args) => {
    const match = findLast(
      mock => isEqual(mock.args, args),
      mockImplementations,
    )

    return match ? match.returns : undefined
  })

  return (...expectedArgs) => {
    const mock = { args: expectedArgs, returns: undefined }
    mockImplementations.push(mock)

    return {
      thenReturn(val) {
        mock.returns = val
      },
      thenThrow(val) {
        Object.defineProperty(mock, 'returns', {
          get() {
            throw val
          },
        })
      },
      thenResolve(val) {
        mock.returns = Promise.resolve(val)
      },
      thenReject(val) {
        mock.returns = Promise.reject(val)
      },
    }
  }
}

export default jestWhen
