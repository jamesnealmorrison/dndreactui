import { Field } from 'redux-form/immutable'
import set from 'lodash/fp/set'

import ConnectedTextfield, {
  FieldComponent,
} from 'App/shared/containers/ConnectedTextfield'
import Textfield from 'App/shared/components/Textfield'

const render = shallowComponent(ConnectedTextfield)
const renderField = shallowComponent(FieldComponent)

describe('<ConnectedTextfield>', () => {
  let component
  let fieldComponent
  let props
  let fieldProps

  beforeEach(() => {
    props = {
      name: 'name',
      foo: 'foo',
      bar: 'bar',
    }
    fieldProps = {
      input: {
        name: 'name',
        required: true,
      },
      label: 'label',
      type: 'text',
      meta: {
        active: false,
        error: 'error',
        touched: false,
      },
    }
    component = render(props)
    fieldComponent = renderField(fieldProps)
  })

  it('renders a redux form Field', () => {
    expect(component.find(Field)).toBePresent()
  })

  it('spreads props to Field', () => {
    expect(component.find(Field).props()).toMatchObject(props)
  })

  it('sets the redux form field component to <FieldComponent>', () => {
    expect(component.find(Field)).toHaveProp('component', FieldComponent)
  })

  describe('field component', () => {
    it('renders a Textfield', () => {
      expect(fieldComponent.find(Textfield)).toBePresent()
    })

    it('spreads input props to Textfield', () => {
      expect(fieldComponent.find(Textfield).props()).toMatchObject(
        fieldProps.input,
      )
    })

    it('sets label', () => {
      expect(fieldComponent.find(Textfield)).toHaveProp(
        'label',
        fieldProps.label,
      )
    })

    it('sets type', () => {
      expect(fieldComponent.find(Textfield)).toHaveProp('type', fieldProps.type)
    })

    it('sets focused state', () => {
      expect(fieldComponent.find(Textfield)).toHaveProp(
        'focused',
        fieldProps.meta.active,
      )
    })

    it('shows errors only when touched', () => {
      expect(fieldComponent.find(Textfield)).toHaveProp('error', undefined)
      fieldComponent.setProps(set('meta.touched', true, fieldProps))
      expect(fieldComponent.find(Textfield)).toHaveProp(
        'error',
        fieldProps.meta.error,
      )
    })
  })
})
