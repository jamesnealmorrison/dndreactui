import React from 'react'
import ReactSelect from 'react-select'

import { mount } from 'enzyme'
import AppProviders from 'AppProviders'
import EquipmentCodeSelect from 'App/shared/containers/DndClassSelect'
import apiClient from 'apiClient'

const dndClasses = [
  { id: 1, name: 'Dwarf' },
]

describe('DndClassSelect', () => {
  it('loads dndClasses and displays them in a Select component', async () => {
    const promise = Promise.resolve(dndClasses)
    apiClient.get = jest.fn(() => promise)

    const wrapper = mount(
      <AppProviders>
        <DndClassSelect />
      </AppProviders>,
    )

    await promise
    wrapper.update()

    // expect(apiClient.get).toHaveBeenCalledWith('/domain/dndClass')
    expect(wrapper.find(ReactSelect).prop('options')[0]).toEqual(
      expect.objectContaining({
        id: 1,
        name: 'Dwarf',
      }),
    )
  })
})
