import { defineMessages } from 'react-intl'
import React from 'react'

import { FilterRecord } from 'store/filters/filters.records'
import { shallow } from 'enzyme'
import FilterRow from 'App/shared/containers/Filters/FilterRow'

const messages = defineMessages({
  foo: {
    id: 'foo',
    defaultMessage: 'foo',
  },
  is: {
    id: 'is',
    defaultMessage: 'is',
  },
  isNot: {
    id: 'isNot',
    defaultMessage: 'is not',
  },
})

describe('FilterRow', () => {
  let wrapper
  let props
  const fields = {
    foo: {
      field: 'foo',
      label: messages.foo,
      component: () => null,
      operators: [
        { label: messages.is, value: 'is' },
        { label: messages.isNot, value: 'is not' },
      ],
    },
  }

  beforeEach(() => {
    props = {
      onClickDelete: jest.fn(),
      onEditField: jest.fn(),
      onEditOperator: jest.fn(),
      onEditValue: jest.fn(),
      filter: FilterRecord(),
      isLast: false,
      fields,
    }

    wrapper = shallow(<FilterRow {...props} />)
  })

  it('can delete the filter', () => {
    wrapper.find({ 'data-test-id': 'delete-filter' }).simulate('click')
    expect(props.onClickDelete).toHaveBeenCalled()
  })

  it('can edit the field', () => {
    wrapper
      .find({ 'data-test-id': 'filter-field' })
      .simulate('change', { field: 'foo' })
    expect(props.onEditField).toHaveBeenCalledWith('foo')
  })

  it('autofocuses newly created filter fields', () => {
    expect(
      wrapper
        .setProps({
          isLast: false,
          filter: FilterRecord(),
        })
        .find({ 'data-test-id': 'filter-field' }),
    ).toHaveProp('autoFocus', false)
    expect(
      wrapper
        .setProps({
          isLast: true,
          filter: FilterRecord({ field: 'foo' }),
        })
        .find({ 'data-test-id': 'filter-field' }),
    ).toHaveProp('autoFocus', false)
    expect(
      wrapper
        .setProps({
          isLast: true,
          filter: FilterRecord(),
        })
        .find({ 'data-test-id': 'filter-field' }),
    ).toHaveProp('autoFocus', true)
  })

  describe('when a field is not defined', () => {
    it('disables operator select', () => {
      expect(wrapper.find({ 'data-test-id': 'filter-operator' })).toHaveProp(
        'disabled',
        true,
      )
    })
    it('disables value select', () => {
      expect(wrapper.find({ 'data-test-id': 'filter-value' })).toHaveProp(
        'disabled',
        true,
      )
    })
  })

  describe('when a field is defined', () => {
    beforeEach(() => {
      wrapper.setProps({ filter: FilterRecord({ field: 'foo' }) })
    })

    it('enables operator select', () => {
      expect(wrapper.find({ 'data-test-id': 'filter-operator' })).toHaveProp(
        'disabled',
        false,
      )
    })
    it('enables value select', () => {
      expect(wrapper.find({ 'data-test-id': 'filter-value' })).toHaveProp(
        'disabled',
        false,
      )
    })

    it('can edit the operator', () => {
      wrapper
        .find({ 'data-test-id': 'filter-operator' })
        .simulate('change', { value: 'is' })
      expect(props.onEditOperator).toHaveBeenCalledWith('is')
    })

    it('can edit the value', () => {
      wrapper.find({ 'data-test-id': 'filter-value' }).simulate('change', 1)
      expect(props.onEditValue).toHaveBeenCalledWith(1)
    })
  })
})
