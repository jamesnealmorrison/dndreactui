import { List } from 'immutable'
import React from 'react'

import { FilterRecord } from 'store/filters/filters.records'
import { FilterWhere } from 'App/shared/containers/Filters/Filters.style'
import { FiltersBase } from 'App/shared/containers/Filters'
import { shallow } from 'enzyme'
import FilterRow from 'App/shared/containers/Filters/FilterRow'

describe('Filters', () => {
  let props
  let wrapper

  beforeEach(() => {
    props = {
      addFilter: jest.fn(),
      clearFilters: jest.fn(),
      removeFilter: jest.fn(),
      editFilter: jest.fn(),
      applyFilters: jest.fn(),
      revertFilters: jest.fn(),
      hideFilters: jest.fn(),
      onChange: jest.fn(),
      pendingFilters: List([
        FilterRecord({ field: 'equipmentCodeId', operator: 'is', value: 1 }),
        FilterRecord({ field: 'modelId', operator: 'is', value: 1 }),
      ]),
    }

    wrapper = shallow(<FiltersBase {...props} />)
  })

  it('shows a list of filters', () => {
    expect(wrapper.find(FilterRow)).toHaveLength(2)
  })

  it('can add a filter', () => {
    wrapper.find({ 'data-test-id': 'add-filter' }).simulate('click')
    expect(props.addFilter).toHaveBeenCalled()
  })

  it('can add remove filters', () => {
    wrapper
      .find(FilterRow)
      .at(0)
      .prop('onClickDelete')()
    expect(props.removeFilter).toHaveBeenCalledWith(0)
  })

  it('can add edit filter field and reset operator and value', () => {
    wrapper
      .find(FilterRow)
      .at(0)
      .prop('onEditField')('makeId')
    expect(props.editFilter).toHaveBeenCalled()
    expect(props.editFilter.mock.calls[0][0]).toEqual(0)
    expect(props.editFilter.mock.calls[0][1]).toEqualImmutable(
      FilterRecord({ field: 'makeId' }),
    )
  })

  it('can add edit filter operator', () => {
    wrapper
      .find(FilterRow)
      .at(0)
      .prop('onEditOperator')('is not')
    expect(props.editFilter).toHaveBeenCalled()
    expect(props.editFilter.mock.calls[0][0]).toEqual(0)
    expect(props.editFilter.mock.calls[0][1]).toEqualImmutable(
      FilterRecord({ field: 'equipmentCodeId', operator: 'is not', value: 1 }),
    )
  })

  it('can add edit filter value', () => {
    wrapper
      .find(FilterRow)
      .at(0)
      .prop('onEditValue')(2)
    expect(props.editFilter).toHaveBeenCalled()
    expect(props.editFilter.mock.calls[0][0]).toEqual(0)
    expect(props.editFilter.mock.calls[0][1]).toEqualImmutable(
      FilterRecord({ field: 'equipmentCodeId', operator: 'is', value: 2 }),
    )
  })

  it('can clear all filters', () => {
    wrapper.find({ 'data-test-id': 'clear-filters' }).simulate('click')
    expect(props.clearFilters).toHaveBeenCalled()
  })

  it('can apply filters', () => {
    wrapper.find({ 'data-test-id': 'apply-filters' }).simulate('click')
    expect(props.applyFilters).toHaveBeenCalled()
    expect(props.hideFilters).toHaveBeenCalled()
  })

  it('can do something when filters are applied', () => {
    wrapper.find({ 'data-test-id': 'apply-filters' }).simulate('click')
    expect(props.onChange).toHaveBeenCalledWith()
  })

  it('can revert filters', () => {
    wrapper.find({ 'data-test-id': 'revert-filters' }).simulate('click')
    expect(props.revertFilters).toHaveBeenCalled()
    expect(props.hideFilters).toHaveBeenCalled()
  })

  it('marks the last filter', () => {
    expect(wrapper.find({ filter: props.pendingFilters.first() })).toHaveProp(
      'isLast',
      false,
    )
    expect(wrapper.find({ filter: props.pendingFilters.last() })).toHaveProp(
      'isLast',
      true,
    )
  })

  it('hides the where message when there are no filters', () => {
    expect(wrapper.find(FilterWhere)).toBePresent()
    expect(
      wrapper.setProps({ pendingFilters: List() }).find(FilterWhere),
    ).not.toBePresent()
  })
})
