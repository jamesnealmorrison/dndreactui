import React from 'react'
import ReactSelect from 'react-select'

import { mount } from 'enzyme'
import AppProviders from 'AppProviders'
import RaceSelect from 'App/shared/containers/RaceSelect'
import apiClient from 'apiClient'

const races = [{ id: 1, name: 'Fighter' }]

describe('RaceSelect', () => {
  it('loads races and displays them in a Select component', async () => {
    const promise = Promise.resolve(races)
    apiClient.get = jest.fn(() => promise)

    const wrapper = mount(
      <AppProviders>
        <RaceSelect />
      </AppProviders>,
    )

    await promise
    wrapper.update()

    // expect(apiClient.get).toHaveBeenCalledWith('/domain/race')
    expect(wrapper.find(ReactSelect).prop('options')[0]).toEqual(
      expect.objectContaining(races[0]),
    )
  })
})
