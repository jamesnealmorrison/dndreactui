import React from 'react'

import { SubmitBase } from 'App/shared/containers/Submit'
import { SubmitButton } from 'App/shared/containers/Submit/Submit.style'
import { shallow } from 'enzyme'
import Spinner from 'App/shared/components/Spinner'

describe('Submit', () => {
  let props
  let wrapper

  beforeEach(() => {
    props = {
      submitting: false,
      invalid: false,
      pristine: false,
      text: 'Submit',
      pendingText: 'Submitting',
      submit: jest.fn(),
      form: 'THE_FORM',
    }
    wrapper = shallow(<SubmitBase {...props} />)
  })

  it('should be able to submit a form by name', () => {
    wrapper.find(SubmitButton).simulate('click')
    expect(props.submit).toHaveBeenCalledWith('THE_FORM')
  })

  it('should show a spinner when submitting', () => {
    expect(wrapper.find(SubmitButton)).toHaveProp('submitting', false)
    expect(wrapper.find(Spinner)).not.toBePresent()
    wrapper.setProps({ submitting: true })
    expect(wrapper.find(SubmitButton)).toHaveProp('submitting', true)
    expect(wrapper.find(Spinner)).toBePresent()
  })

  it('should change the text displayed when submitting', () => {
    expect(wrapper.find(SubmitButton)).toIncludeInnerText('Submit')
    wrapper.setProps({ submitting: true })
    expect(wrapper.find(SubmitButton)).toIncludeInnerText('Submitting')
  })

  it('should be disabled when submitting', () => {
    expect(wrapper.find(SubmitButton)).toHaveProp('disabled', false)
    wrapper.setProps({ submitting: true })
    expect(wrapper.find(SubmitButton)).toHaveProp('disabled', true)
  })

  it('should be disabled when pristine', () => {
    expect(wrapper.find(SubmitButton)).toHaveProp('disabled', false)
    wrapper.setProps({ pristine: true })
    expect(wrapper.find(SubmitButton)).toHaveProp('disabled', true)
  })

  it('should be disabled when invalid', () => {
    expect(wrapper.find(SubmitButton)).toHaveProp('disabled', false)
    wrapper.setProps({ invalid: true })
    expect(wrapper.find(SubmitButton)).toHaveProp('disabled', true)
  })

  it('should be disabled when configured that way', () => {
    expect(wrapper.find(SubmitButton)).toHaveProp('disabled', false)
    wrapper.setProps({ shouldBeDisabled: () => true })
    expect(wrapper.find(SubmitButton)).toHaveProp('disabled', true)
  })
})
