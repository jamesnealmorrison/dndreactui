import React from 'react'
import ReactSelect from 'react-select'

import { mount } from 'enzyme'
import AppProviders from 'AppProviders'
import SubRaceSelect from 'App/shared/containers/SubRaceSelect'
import apiClient from 'apiClient'

const subRaces = [
  {
    id: 1,
    name: 'Hill Dwarf',
  },
]

describe('SubRaceSelect', () => {
  it('loads subRaces and displays them in a Select component', async () => {
    const promise = Promise.resolve(subRaces)
    apiClient.get = jest.fn(() => promise)

    const wrapper = mount(
      <AppProviders>
        <SubRaceSelect />
      </AppProviders>,
    )

    await promise
    wrapper.update()

    // expect(apiClient.get).toHaveBeenCalledWith('/domain/subRaces')
    expect(wrapper.find(ReactSelect).prop('options')[0]).toEqual(
      expect.objectContaining({ id: 1, name: 'Hill Dwarf' }),
    )
  })
})
