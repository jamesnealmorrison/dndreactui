import Loading from 'App/shared/components/Loading'

const render = shallowComponent(Loading)

describe('<Loading />', () => {
  it('should render a spinner', () => {
    expect(render().find('Spinner')).toBePresent()
  })
})
