import React from 'react'

import { shallow } from 'enzyme'
import SVGIcon from 'App/shared/components/SVGIcon'
import getMaterialIconContents from 'App/shared/components/SVGIcon/getMaterialIconContents'
import renderer from 'react-test-renderer'

import jestWhen from '../../../../../jestWhen'

jest.mock('App/shared/components/SVGIcon/getMaterialIconContents', () =>
  jest.fn(() => Promise.resolve('')),
)

describe('SVGIcon', () => {
  it('renders correctly', () => {
    jestWhen(getMaterialIconContents)('foo.bar').thenReturn('<path />')
    const tree = renderer.create(<SVGIcon name="foo.bar" size={16} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('sets the SVG Contents on mount', () => {
    jestWhen(getMaterialIconContents)('foo.bar').thenReturn('<path />')
    const wrapper = shallow(<SVGIcon name="foo.bar" />)
    wrapper.update()
    expect(wrapper.find('svg')).toHaveProp('dangerouslySetInnerHTML', {
      __html: '<path />',
    })
  })

  it('updates the SVG content when the name prop changes', () => {
    const wrapper = shallow(<SVGIcon name="foo.bar" />)
    getMaterialIconContents.mockClear()
    jestWhen(getMaterialIconContents)('foo.baz').thenReturn('<path />')
    wrapper.setProps({ name: 'foo.baz' })
    expect(getMaterialIconContents).toHaveBeenCalledWith('foo.baz')
  })
})
