import React from 'react'

import ActionList from 'App/shared/components/ActionList'
import AppProviders from 'AppProviders'
import renderer from 'react-test-renderer'

describe('ActionList', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <AppProviders>
          <ActionList>
            <ActionList.Item
              title="title"
              description="description"
              actionText="Click Me"
              onActionClick={() => {}}
            />
            <ActionList.Item
              title="title2"
              description="description2"
              actionText="Click Me2"
              onActionClick={() => {}}
            />
          </ActionList>
        </AppProviders>,
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
