import React from 'react'
import renderer from 'react-test-renderer'

import { shallow } from 'enzyme'
import { FloatingInputBase } from 'App/shared/components/FloatingInput'
import Spinner from 'App/shared/components/Spinner'

describe('FloatingInput', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<FloatingInputBase />).toJSON()
    expect(tree).toMatchSnapshot()
  })
  it('shows a spinner when it is submitting', () => {
    const wrapper = shallow(<FloatingInputBase isSubmitting />)
    expect(wrapper.find(Spinner)).toBePresent()
  })
  it('does not show a spinner when not submitting', () => {
    const wrapper = shallow(<FloatingInputBase />)
    expect(wrapper.find(Spinner)).not.toBePresent()
  })
})
