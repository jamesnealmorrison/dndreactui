import React from 'react'

import { TableIcon } from 'App/shared/components/Table/Table.style'
import { shallow } from 'enzyme'
import HeaderCell from 'App/shared/components/Table/HeaderCell'

describe('HeaderCell', () => {
  it('shows an icon when it is sorted', () => {
    const wrapper = shallow(<HeaderCell sorted />)
    expect(wrapper.find(TableIcon)).toBePresent()
  })

  it('does not show an icon when it is not sorted', () => {
    const wrapper = shallow(<HeaderCell sorted={false} />)
    expect(wrapper.find(TableIcon)).not.toBePresent()
  })

  it('flips the icon arround when the sorting direction is desc', () => {
    const wrapper = shallow(<HeaderCell sorted sortDirection="desc" />)
    expect(wrapper.find(TableIcon)).toHaveProp('flip', true)
  })

  it('does not flip the icon arround when the sorting direction is asc', () => {
    const wrapper = shallow(<HeaderCell sorted sortDirection="asc" />)
    expect(wrapper.find(TableIcon)).toHaveProp('flip', false)
  })
})
