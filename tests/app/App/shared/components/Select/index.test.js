import { FormattedMessage } from 'react-intl'
import { fromJS } from 'immutable'
import React from 'react'

import {
  LabelWrap,
  SideLabel,
  SideLabelWrap,
  TopLabel,
} from 'App/shared/components/Label/Label.style'
import { SelectBase } from 'App/shared/components/Select'
import { shallow } from 'enzyme'
import SVGIcon from 'App/shared/components/SVGIcon'
import SelectMessages from 'App/shared/components/Select/messages'
import SelectOption from 'App/shared/components/Select/SelectOption'

describe('Select', () => {
  let props
  let wrapper
  let getSelect

  beforeEach(() => {
    props = {
      options: fromJS([{ label: 'foo', value: 1 }, { label: 'bar', value: 2 }]),
      label: 'label',
      isAndroid: false,
      error: null,
      labelKey: 'label',
      valueKey: 'value',
      safeFormatMessage: x => x,
    }
    wrapper = shallow(<SelectBase {...props} />)
    getSelect = () => wrapper.find({ labelKey: 'label', valueKey: 'value' })
  })

  describe('defaults', () => {
    it('has a default no results message', () => {
      expect(SelectBase.defaultProps.noResultsText).toEqual(
        <FormattedMessage {...SelectMessages.noResultsText} />,
      )
    })
    it('renders nothing by default for descriptionRenderer', () => {
      expect(SelectBase.defaultProps.descriptionRenderer()).toEqual(null)
    })
    it('filters nothing by default', () => {
      expect(SelectBase.defaultProps.filter()).toEqual(true)
    })
    it('has an empty array for default options', () => {
      expect(SelectBase.defaultProps.options).toEqual([])
    })
    it('has an empty placeholder', () => {
      expect(SelectBase.defaultProps.placeholder).toEqual('')
    })
  })

  it('can set filter', () => {
    getSelect().prop('onInputChange')('foo')
    wrapper.update()
    expect(getSelect()).toHaveProp('filterText', 'foo')
  })

  it('trims filter to given max length', () => {
    wrapper.setProps({ maxLength: 3 })
    getSelect().prop('onInputChange')('foooooooo')
    wrapper.update()
    expect(getSelect()).toHaveProp('filterText', 'foo')
  })

  it('passes options as native js to underlying select component', () => {
    expect(getSelect().prop('options')).toEqual(props.options.toJS())
  })

  it('can filter options with internal filter', () => {
    getSelect().prop('onInputChange')('fo')
    wrapper.update()
    expect(getSelect().prop('options')).toEqual([{ label: 'foo', value: 1 }])
  })

  it('can filter options with external filter', () => {
    wrapper.setProps({ filter: o => o.value === 2 })
    wrapper.update()
    expect(getSelect().prop('options')).toEqual([{ label: 'bar', value: 2 }])
  })

  it('can filter options with internal and external filter', () => {
    getSelect().prop('onInputChange')('fo')
    wrapper.setProps({ filter: o => o.value === 2 })
    wrapper.update()
    expect(getSelect().prop('options')).toEqual([])
  })

  it('uses the label key to render the selected value', () => {
    const value = wrapper
      .setProps({ value: props.options.get(0).toJS() })
      .find({ options: props.options.toJS() })
      .prop('valueRenderer')()

    expect(value).toEqual('foo')
  })

  it('uses the label key to render the selected value when it is a simple value', () => {
    const value = wrapper
      .setProps({ value: 1 })
      .find({ options: props.options.toJS() })
      .prop('valueRenderer')()

    expect(value).toEqual('foo')
  })

  describe('rendering options', () => {
    const getOption = option => {
      const Option = () =>
        getSelect().prop('optionRenderer')(props.options.get(option).toJS())
      return shallow(<Option />).find(SelectOption)
    }

    it('displays differently on mobile and desktop', () => {
      const option = getOption(0)

      expect(option).toHaveProp('isMobile', false)
    })

    it('shows a title', () => {
      const option = getOption(0)

      expect(option).toHaveProp('title', 'foo')
    })

    it('shows a description', () => {
      wrapper.setProps({ descriptionRenderer: option => option.label })
      const option = getOption(0)

      expect(option).toHaveProp('content', 'foo')
    })

    it('passes through whether or not a value is set', () => {
      expect(getOption(0)).toHaveProp('hasValue', false)

      wrapper.setProps({ value: props.options.get(0).toJS() })

      expect(getOption(0)).toHaveProp('hasValue', true)
    })

    it('shows whether or not the option is selected', () => {
      wrapper.setProps({ value: props.options.get(0).toJS() })

      expect(getOption(0)).toHaveProp('selected', true)
      expect(getOption(1)).toHaveProp('selected', false)
    })
  })

  describe('arrow renderer', () => {
    it('renders a down arrow when the menu is closed', () => {
      const arrow = getSelect().prop('arrowRenderer')({ isOpen: false })

      expect(arrow).toEqual(
        <SVGIcon name="navigation.arrow_drop_down" size={24} />,
      )
    })
    it('renders an up arrow when the menu is open', () => {
      const arrow = getSelect().prop('arrowRenderer')({ isOpen: true })

      expect(arrow).toEqual(
        <SVGIcon name="navigation.arrow_drop_up" size={24} />,
      )
    })
  })

  describe('label position', () => {
    it('can render a label on the side', () => {
      wrapper.setProps({ labelOnSide: true, label: 'label' })
      expect(wrapper.find(SideLabel)).toBePresent()
      expect(wrapper.find(SideLabelWrap)).toBePresent()
    })
    it('can render a label above', () => {
      wrapper.setProps({ labelOnSide: false, label: 'label' })
      expect(wrapper.find(TopLabel)).toBePresent()
      expect(wrapper.find(LabelWrap)).toBePresent()
    })
    it('does not render a label when none is passed in ', () => {
      wrapper.setProps({ label: undefined })
      expect(wrapper.find(TopLabel)).not.toBePresent()
      expect(wrapper.find(SideLabel)).not.toBePresent()
    })
    it('shows errors with the label', () => {
      wrapper.setProps({ label: 'label', error: 'bad' })
      expect(wrapper.find(TopLabel)).toHaveProp('hasError', true)
    })
  })
})
