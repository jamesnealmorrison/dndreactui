import React from 'react'

import { StyledDesktopSelect } from 'App/shared/components/Select/DesktopSelect/DesktopSelect.style'
import { shallow } from 'enzyme'
import DesktopSelect from 'App/shared/components/Select/DesktopSelect'

describe('Desktop select', () => {
  it('select a value with tab, only when the user has started typing', () => {
    const wrapper = shallow(<DesktopSelect filterText="" />)
    expect(wrapper.find(StyledDesktopSelect)).toHaveProp(
      'tabSelectsValue',
      false,
    )

    wrapper.setProps({ filterText: 'foo' })
    expect(wrapper.find(StyledDesktopSelect)).toHaveProp(
      'tabSelectsValue',
      true,
    )
  })
})
