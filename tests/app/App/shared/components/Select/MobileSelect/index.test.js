import React from 'react'

import { shallow } from 'enzyme'
import MobileSearchBar from 'App/shared/components/MobileSearchBar'
import MobileSelect from 'App/shared/components/Select/MobileSelect'
import Modal from 'App/shared/components/Modal'

import jestWhen from '../../../../../../jestWhen'

describe('MobileSelect', () => {
  let wrapper
  let props

  beforeEach(() => {
    props = {
      valueRenderer: jest.fn(),
      arrowRenderer: jest.fn(),
      options: [{ label: 'foo', value: 0 }, { label: 'bar', value: 1 }],
      label: 'the label',
      optionRenderer: o => o.label,
      onInputChange: jest.fn(),
      filterText: '',
      valueKey: 'value',
      noResultsText: 'No results',
      onChange: jest.fn(),
      value: { label: 'foo', value: 0 },
    }
    jestWhen(props.valueRenderer)(props.value).thenReturn('the value')
    wrapper = shallow(<MobileSelect {...props} />)
  })

  it('displays the selected value', () => {
    expect(wrapper.find({ value: 'the value' })).toBePresent()
  })

  it('shows errors', () => {
    expect(wrapper.find({ value: 'the value' })).toHaveProp('hasError', false)
    wrapper.setProps({ error: 'bad' })
    expect(wrapper.find({ value: 'the value' })).toHaveProp('hasError', true)
  })

  it('always shows a down arrow next to the value', () => {
    jestWhen(props.arrowRenderer)({ isOpen: false }).thenReturn('the arrow')
    wrapper.update()
    wrapper.find({ children: 'the arrow' })
  })

  it('allows the user to filter the options by typing and select the top value', () => {
    wrapper.setProps({ filterText: 'foo' })
    wrapper.find(MobileSearchBar).simulate('submit')
    expect(props.onChange).toHaveBeenCalledWith(props.options[0])
  })

  it('does not submit when there is no filter text', () => {
    wrapper.setProps({ filterText: '' })
    wrapper.find(MobileSearchBar).simulate('submit')
    expect(props.onChange).not.toHaveBeenCalled()
  })

  it('does not submit when there are no options', () => {
    wrapper.setProps({ options: [], filterText: 'foo' })
    wrapper.find(MobileSearchBar).simulate('submit')
    expect(props.onChange).not.toHaveBeenCalled()
  })

  it('shows which option submitting will select', () => {
    expect(wrapper.find({ children: 'foo' })).toHaveProp('willChoose', false)
    wrapper.setProps({ filterText: 'foo' })
    expect(wrapper.find({ children: 'foo' })).toHaveProp('willChoose', true)
    expect(wrapper.find({ children: 'bar' })).toHaveProp('willChoose', false)
  })

  it('can select options by tapping them', () => {
    wrapper.find({ children: 'bar' }).simulate('click')
    expect(props.onChange).toHaveBeenCalledWith(props.options[1])
  })

  it('clears filter text when it submits', () => {
    wrapper.setProps({ filterText: 'foo' })
    wrapper.find(MobileSearchBar).simulate('submit')
    expect(props.onInputChange).toHaveBeenCalledWith('')
  })

  it('closes the modal when it submits', () => {
    wrapper.setState({ isOpen: true })
    wrapper.setProps({ filterText: 'foo' })
    wrapper.find(MobileSearchBar).simulate('submit')
    expect(wrapper.find(Modal)).toHaveProp('isOpen', false)
  })

  it('opens a modal when the value display is clicked', () => {
    wrapper.find({ value: 'the value' }).simulate('click')
    expect(wrapper.find(Modal)).toHaveProp('isOpen', true)
  })

  it('shows the label in the header so the user knows which select they opened', () => {
    expect(wrapper.find(Modal).find({ children: 'the label' })).toBePresent()
  })

  it('shows the options in the modal', () => {
    expect(wrapper.find(Modal).find({ children: 'foo' })).toBePresent()
    expect(wrapper.find(Modal).find({ children: 'bar' })).toBePresent()
  })

  it('shows an empty message when there are no options', () => {
    expect(wrapper.find({ children: props.noResultsText })).not.toBePresent()
    wrapper.setProps({ options: [] })
    expect(wrapper.find({ children: props.noResultsText })).toBePresent()
  })
})
