import React from 'react'

import { OptionStyle } from 'App/shared/components/Select/Select.style'
import { shallow } from 'enzyme'
import SVGIcon from 'App/shared/components/SVGIcon'
import SelectOption from 'App/shared/components/Select/SelectOption'

describe('SelectOption', () => {
  let wrapper
  let props

  beforeEach(() => {
    props = {
      selected: false,
      hasValue: false,
      isMobile: false,
    }

    wrapper = shallow(<SelectOption {...props} />)
  })

  describe('when some option has been picked', () => {
    beforeEach(() => {
      wrapper.setProps({ hasValue: true })
    })
    describe('on mobile', () => {
      beforeEach(() => {
        wrapper.setProps({ isMobile: true })
      })

      it('shows a checkmark on the left when selected', () => {
        wrapper.setProps({ selected: true })
        expect(wrapper.find(OptionStyle)).toHaveProp(
          'icon',
          <SVGIcon primaryAlt name="navigation.check" />,
        )
      })

      it('shows an empty space on the left when not selected', () => {
        wrapper.setProps({ selected: false })
        expect(wrapper.find(OptionStyle)).toHaveProp('icon', null)
      })

      it('renders select options more loosely', () => {
        expect(wrapper.find(OptionStyle)).toHaveProp('tight', false)
      })
    })

    describe('on desktop', () => {
      beforeEach(() => {
        wrapper.setProps({ isMobile: false })
      })

      it('shows a checkmark on the right when selected', () => {
        wrapper.setProps({ selected: true })
        expect(wrapper.find(OptionStyle)).toHaveProp(
          'iconRight',
          <SVGIcon primaryAlt name="navigation.check" />,
        )
      })

      it('shows an empty space on the right when not selected', () => {
        wrapper.setProps({ selected: false })
        expect(wrapper.find(OptionStyle)).toHaveProp('iconRight', null)
      })

      it('renders select options more compactly', () => {
        expect(wrapper.find(OptionStyle)).toHaveProp('tight', true)
      })
    })
  })

  describe('when no option has been picked', () => {
    beforeEach(() => {
      wrapper.setProps({ hasValue: false })
    })

    it('does not render any icons', () => {
      expect(wrapper.find(OptionStyle)).not.toHaveProp('iconRight')
      expect(wrapper.find(OptionStyle)).not.toHaveProp('icon')
    })
  })
})
