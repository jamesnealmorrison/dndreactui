import { Switch } from 'App/shared/components/Switch'

const render = mountComponent(Switch)

describe('<Switch />', () => {
  let switcher

  beforeEach(() => {
    switcher = render({
      name: 'name',
      value: 'value',
      label: 'label',
      disabled: false,
      defaultChecked: false,
    })
  })

  it('renders a SwitchLabel with the label text', () => {
    expect(switcher.find('SwitchLabel')).toBePresent()
    expect(switcher.find('SwitchLabel')).toHaveText('label')
  })

  it('renders a real input[type="checkbox"]', () => {
    expect(switcher.find('input[type="checkbox"]')).toBePresent()
  })

  it('handles [prop] name, value, and defaultChecked', () => {
    switcher.setProps({ defaultChecked: true })
    expect(switcher.find('input[type="checkbox"]')).toHaveProp('name', 'name')
    expect(switcher.find('input[type="checkbox"]')).toHaveProp('value', 'value')
    expect(switcher.find('input[type="checkbox"]')).toHaveProp(
      'defaultChecked',
      true,
    )
  })

  it('handles [prop] disabled', () => {
    switcher = render({ disabled: true, label: 'label' })
    expect(switcher.find('SwitchButton')).toHaveProp('disabled', true)
    expect(switcher.find('SwitchLabel')).toHaveProp('disabled', true)
    expect(switcher.find('Track')).toHaveProp('disabled', true)
    expect(switcher.find('Thumb')).toHaveProp('disabled', true)
  })

  it('does not render a <SwitchLabel> when there is no label', () => {
    switcher.setProps({ label: undefined })
    expect(switcher.find('SwitchLabel')).not.toBePresent()
  })
})
