import React from 'react'
import ReactDOM from 'react-dom'

import PageScrollOffset from 'App/shared/components/PageScrollOffset'

describe('PageScrollOffset', () => {
  it('it adds a div with height to Page wrap for padding', () => {
    const pageWrap = document.createElement('div')
    pageWrap.setAttribute('id', 'page-wrap')
    document.body.appendChild(pageWrap)
    expect(PageScrollOffset({ height: '1px' })).toEqual(
      ReactDOM.createPortal(<div style={{ height: '1px' }} />, pageWrap),
    )
    pageWrap.remove()
  })

  it('returns null if no page wrap found', () => {
    expect(PageScrollOffset({ height: '1px' })).toEqual(null)
  })
})
