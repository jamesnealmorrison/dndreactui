import Menu from 'App/shared/components/Menu'

const render = shallowComponent(Menu.Divider)

describe('<Menu.Divider>', () => {
  let divider

  beforeEach(() => {
    divider = render()
  })

  describe('refs', () => {
    it('sets a ref to the menuItem', () => {
      divider.find('MenuDivider').prop('innerRef')('menu divider ref')
      expect(divider.instance().menuItem).toEqual('menu divider ref')
    })
  })
})
