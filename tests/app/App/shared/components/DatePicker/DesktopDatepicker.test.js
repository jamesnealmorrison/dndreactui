import React from 'react'
import ReactDatePicker from 'react-datepicker'
import moment from 'moment'

import { shallow } from 'enzyme'
import BorderedTextfield from 'App/shared/components/BorderedTextfield'
import DesktopDatepicker from 'App/shared/components/Datepicker/DesktopDatepicker'

const getTextfield = wrapper =>
  shallow(wrapper.find(ReactDatePicker).prop('customInput')).find(
    BorderedTextfield,
  )

describe('DesktopDatepicker', () => {
  const date1 = '2018-01-01T12:00:00.000Z'
  const date2 = '2018-01-02T12:00:00.000Z'

  it('sets internal moment value from passed in date string', () => {
    const wrapper = shallow(<DesktopDatepicker value={date1} />)
    expect(wrapper.find(ReactDatePicker)).toHaveProp('selected', moment(date1))
  })

  it('sets empty date strings to null', () => {
    const wrapper = shallow(<DesktopDatepicker value="" />)
    expect(wrapper.find(ReactDatePicker)).toHaveProp('selected', null)
  })

  it('sets inputText from passed in date string', () => {
    const wrapper = shallow(<DesktopDatepicker value={date1} />)

    expect(getTextfield(wrapper)).toHaveProp('value', '01/01/2018')
  })

  it('sets inputText as empty when no value is given', () => {
    const wrapper = shallow(<DesktopDatepicker value="" />)

    expect(getTextfield(wrapper)).toHaveProp('value', '')
  })

  it('updates internal values on prop changes', () => {
    const wrapper = shallow(<DesktopDatepicker value={date1} />)

    wrapper.setProps({ value: date2 })

    expect(wrapper.find(ReactDatePicker)).toHaveProp('selected', moment(date2))

    expect(getTextfield(wrapper)).toHaveProp('value', '01/02/2018')
  })

  it('shows errors for invalid dates', () => {
    const wrapper = shallow(<DesktopDatepicker value={date1} />)

    getTextfield(wrapper).simulate('change', {
      target: { value: '10/' },
    })

    wrapper.update()

    expect(getTextfield(wrapper)).toHaveProp('hasError', true)
  })

  it('does not show errors when initially entering a date', () => {
    const wrapper = shallow(<DesktopDatepicker value="" />)

    getTextfield(wrapper).simulate('change', {
      target: { value: '10/' },
    })

    wrapper.update()

    expect(getTextfield(wrapper)).toHaveProp('hasError', false)
  })

  it('does show error when updating a date after the user has touched it', () => {
    const wrapper = shallow(<DesktopDatepicker value="" />)

    getTextfield(wrapper).simulate('blur')
    getTextfield(wrapper).simulate('change', {
      target: { value: '10/' },
    })

    wrapper.update()

    expect(getTextfield(wrapper)).toHaveProp('hasError', true)
  })

  it('triggers a change event when user types a valid date', () => {
    const onChange = jest.fn()
    const wrapper = shallow(<DesktopDatepicker value="" onChange={onChange} />)

    getTextfield(wrapper).simulate('change', {
      target: { value: '10/15/2018' },
    })

    expect(onChange).toHaveBeenCalledWith(moment('2018-10-15').toISOString())
  })

  it('clears errors when user types a valid date', () => {
    const wrapper = shallow(<DesktopDatepicker value={date1} />)

    getTextfield(wrapper).simulate('change', {
      target: { value: 'bad date' },
    })

    wrapper.update()

    getTextfield(wrapper).simulate('change', {
      target: { value: '10/15/2018' },
    })

    wrapper.update()

    expect(getTextfield(wrapper)).toHaveProp('hasError', false)
  })

  it('blocks keyboard navigation is there is a value in the textfield', () => {
    const wrapper = shallow(<DesktopDatepicker value={date1} />)

    expect(wrapper.find(ReactDatePicker)).toHaveProp(
      'disabledKeyboardNavigation',
      true,
    )

    wrapper.setProps({ value: '' })

    expect(wrapper.find(ReactDatePicker)).toHaveProp(
      'disabledKeyboardNavigation',
      false,
    )
  })

  it('triggers a change event when the user picks a date from the calendar', () => {
    const onChange = jest.fn()
    const wrapper = shallow(
      <DesktopDatepicker value={date1} onChange={onChange} />,
    )

    wrapper.find(ReactDatePicker).simulate('change', moment('2018-10-15'))

    expect(onChange).toHaveBeenCalledWith(moment('2018-10-15').toISOString())
  })
})
