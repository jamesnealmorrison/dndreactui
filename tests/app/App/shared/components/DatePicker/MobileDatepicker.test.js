import React from 'react'
import moment from 'moment'

import { shallow } from 'enzyme'
import BorderedTextfield from 'App/shared/components/BorderedTextfield'
import MobileDatepicker from 'App/shared/components/Datepicker/MobileDatepicker'

describe('DatePicker', () => {
  it('creates onChange events with ISO8601 formatted dates', () => {
    const onChange = jest.fn()
    const wrapper = shallow(<MobileDatepicker onChange={onChange} />)
    wrapper
      .find(BorderedTextfield)
      .simulate('change', { target: { value: '2018-01-01' } })
    expect(onChange).toHaveBeenCalledWith(moment('2018-01-01').toISOString())
  })

  it('stores internal date in short format', () => {
    const wrapper = shallow(
      <MobileDatepicker value={moment('2018-01-01').toISOString()} />,
    )
    expect(wrapper.find(BorderedTextfield)).toHaveProp('value', '2018-01-01')
  })
})
