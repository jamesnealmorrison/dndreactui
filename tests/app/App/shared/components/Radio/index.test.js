import { Radio } from 'App/shared/components/Radio'

const render = mountComponent(Radio)

describe('<Radio />', () => {
  let radio

  beforeEach(() => {
    radio = render({
      name: 'name',
      value: 'value',
      label: 'label',
      disabled: false,
      defaultChecked: false,
    })
  })

  it('renders the radioStyle on an input', () => {
    expect(radio.find('input')).toHaveProp('type', 'radio')
    expect(radio.find('input')).toHaveProp('className')
  })

  it('renders a RadioLabel with the label text', () => {
    expect(radio.find('RadioLabel')).toBePresent()
    expect(radio.find('RadioLabel')).toHaveText('label')
  })

  it('renders a real input[type="radio"]', () => {
    expect(radio.find('input[type="radio"]')).toBePresent()
  })

  it('handles [prop] name, value, and defaultChecked', () => {
    radio.setProps({ defaultChecked: true })
    expect(radio.find('input[type="radio"]')).toHaveProp('name', 'name')
    expect(radio.find('input[type="radio"]')).toHaveProp('value', 'value')
    expect(radio.find('input[type="radio"]')).toHaveProp('defaultChecked', true)
  })

  it('handles [prop] disabled', () => {
    radio = render({ disabled: true, label: 'foo' })
    expect(radio.find('RadioButton')).toHaveProp('disabled', true)
    expect(radio.find('RadioLabel')).toHaveProp('disabled', true)
    expect(radio.find('OuterCircle')).toHaveProp('disabled', true)
    expect(radio.find('InnerCircle')).toHaveProp('disabled', true)
  })

  it('does not render a <RadioLabel> when there is no label', () => {
    radio.setProps({ label: undefined })
    expect(radio.find('RadioLabel')).not.toBePresent()
  })
})
