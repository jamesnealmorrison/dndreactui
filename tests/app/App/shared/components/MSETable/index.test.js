import React from 'react'

import { MSETableBase } from 'App/shared/components/MSETable'
import { shallow } from 'enzyme'
import EmptyMessage from 'App/routes/EquipmentCodesPage/components/EmptyMessage'
import FloatingSpinner from 'App/routes/EquipmentCodesPage/components/FloatingSpinner'

describe('MSETable', () => {
  let wrapper
  let props

  beforeEach(() => {
    props = {
      isLoading: false,
      isFiltered: false,
      isEmpty: false,
      emptyMessage: 'empty',
      filteredEmptyMessage: 'filtered',
      children: 'children',
    }
    wrapper = shallow(<MSETableBase {...props} />)
  })

  it('should display a loading spinner when it is loading', () => {
    wrapper.setProps({ isLoading: true })
    expect(wrapper.find(FloatingSpinner)).toHaveProp('isLoading', true)
  })
  it('should not display a loading spinner when it is not loading models', () => {
    wrapper.setProps({ isLoading: false })
    expect(wrapper.find(FloatingSpinner)).toHaveProp('isLoading', false)
  })
  it('should display an empty message when not loading and there are no models', () => {
    wrapper.setProps({ isLoading: false, isEmpty: true })
    expect(wrapper.find(EmptyMessage)).toHaveProp('isLoading', false)
    expect(wrapper.find(EmptyMessage)).toHaveProp('isEmpty', true)
    expect(wrapper.find(EmptyMessage)).toHaveInnerText('empty')
  })
  it('should display an empty message when not loading and there are no models', () => {
    wrapper.setProps({ isLoading: false, isEmpty: true, isFiltered: true })
    expect(wrapper.find(EmptyMessage)).toHaveProp('isLoading', false)
    expect(wrapper.find(EmptyMessage)).toHaveProp('isEmpty', true)
    expect(wrapper.find(EmptyMessage)).toHaveInnerText('filtered')
  })
})
