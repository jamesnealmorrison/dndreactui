import Spinner from 'App/shared/components/Spinner'

const render = shallowComponent(Spinner)

describe('<Spinner />', () => {
  let spinner

  beforeEach(() => {
    jest.useFakeTimers()
    spinner = render()
  })

  it('renders each layer', () => {
    expect(spinner.find('LayerOne')).toBePresent()
    expect(spinner.find('LayerTwo')).toBePresent()
    expect(spinner.find('LayerThree')).toBePresent()
    expect(spinner.find('LayerFour')).toBePresent()
  })

  it('only displays after a short delay', () => {
    expect(spinner.find('LayerOne')).toHaveProp('active', false)
    jest.runTimersToTime(300)
    spinner.update()
    expect(spinner.find('LayerOne')).toHaveProp('active', true)
  })

  it('only displays after a short delay', () => {
    expect(spinner.find('LayerOne')).toHaveProp('active', false)
    jest.runTimersToTime(300)
    spinner.update()
    expect(spinner.find('LayerOne')).toHaveProp('active', true)
  })

  it('can be configured to have a different delay', () => {
    spinner = render({ delay: 100 })
    jest.runTimersToTime(99)
    spinner.update()
    expect(spinner.find('LayerOne')).toHaveProp('active', false)
    jest.runTimersToTime(100)
    spinner.update()
    expect(spinner.find('LayerOne')).toHaveProp('active', true)
  })

  it('does not try to update if the component is unmounted before the delay is up', () => {
    window.console.error = jest.fn()

    jest.runTimersToTime(299)
    spinner.unmount()
    jest.runAllTimers()

    /*
    React will complain but not actually throw if you try
    to call setState on on an unmounted component, we are
    just listening for those complaints here.
    */

    expect(window.console.error).not.toHaveBeenCalled()
  })
})
