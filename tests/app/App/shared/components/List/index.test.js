import React from 'react'

import {
  ListItemAction,
  ListItemText,
} from 'App/shared/components/List/List.style'
import { shallow } from 'enzyme'
import List from 'App/shared/components/List'
import Ripple from 'App/shared/components/Ripple'
import renderer from 'react-test-renderer'

describe('List', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <List forCard>
          <List.SectionTitle>Section 1</List.SectionTitle>
          <List.Item
            title="Title 1"
            content="Content 1"
            actionText="Action"
            onActionClick={() => {}}
          />
          <List.Item title="Title 2" content="Content 2" />
          <List.Divider />
          <List.SectionTitle>Section 2</List.SectionTitle>
          <List.Item title="Title 3" content="Content 3" />
        </List>,
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('can optionaly show actions', () => {
    let wrapper = shallow(<List.Item />)
    expect(wrapper.find(ListItemAction)).not.toBePresent()

    const onActionClick = jest.fn()
    wrapper = shallow(
      <List.Item actionText="foo" onActionClick={onActionClick} />,
    )
    expect(wrapper.find(ListItemAction)).toBePresent()
    wrapper.find(ListItemAction).simulate('click')
    expect(onActionClick).toHaveBeenCalled()
  })

  it('can optionally show content below title', () => {
    let wrapper = shallow(<List.Item />)
    expect(wrapper.find(ListItemText)).not.toBePresent()
    wrapper = shallow(<List.Item content="somecontent" />)
    expect(wrapper.find(ListItemText)).toBePresent()
  })

  it('shows a ripple when the ListItem is clickable', () => {
    let wrapper = shallow(<List.Item />)
    expect(wrapper.find(Ripple)).not.toBePresent()
    wrapper = shallow(<List.Item onClick={() => {}} />)
    expect(wrapper.find(Ripple)).toBePresent()
  })
})
