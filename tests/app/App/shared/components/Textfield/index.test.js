import { Textfield } from 'App/shared/components/Textfield'

const render = shallowComponent(Textfield)

describe('Textfield', () => {
  it('renders a Textarea when [prop] multiline is true', () => {
    expect(render({ multiLine: true }).find('Textarea')).toBePresent()
    expect(render({ multiLine: true }).find('Input')).not.toBePresent()
  })

  it('renders an Input when [prop] multiline is false', () => {
    expect(render({ multiLine: false }).find('Input')).toBePresent()
    expect(render({ multiLine: false }).find('Textarea')).not.toBePresent()
  })

  it('sets a sane max length when [prop] multiline is false', () => {
    expect(render({ multiLine: false }).find('Input')).toHaveProp(
      'maxLength',
      255,
    )
  })

  it('renders an error when an error is given', () => {
    expect(render().find('ErrorMessage')).not.toBePresent()
    expect(render({ error: 'snap!' }).find('ErrorMessage')).toBePresent()
    expect(render({ error: 'snap!' }).find('ErrorMessage')).toHaveInnerText(
      'snap!',
    )
  })

  it('renders helper text when it is given', () => {
    expect(render().find('HelperText')).not.toBePresent()
    expect(render({ helperText: 'help' }).find('HelperText')).toBePresent()
    expect(render({ helperText: 'help' }).find('HelperText')).toHaveInnerText(
      'help',
    )
  })

  it('renders a label when it is given', () => {
    expect(render().find('Label')).not.toBePresent()
    expect(render({ label: 'label' }).find('Label')).toBePresent()
    expect(render({ label: 'label' }).find('Label')).toHaveInnerText('label')
  })

  it('clearly marks errors', () => {
    expect(render({ error: 'error', label: 'label' }).find('Label')).toHaveProp(
      'error',
      true,
    )
    expect(render({ error: 'error' }).find('Border')).toHaveProp('error', true)
    expect(render({ error: 'error' }).find('Input')).toHaveProp('error', true)
  })

  it('lifts the label away when there is a value', () => {
    expect(render({ value: 'value', label: 'label' }).find('Label')).toHaveProp(
      'hasValue',
      true,
    )
    expect(render({ value: null, label: 'label' }).find('Label')).toHaveProp(
      'hasValue',
      false,
    )
  })

  it('clearly denotes when it is required', () => {
    const input = render({ value: null, label: 'label', required: true })
    expect(input.find('RequiredMarker')).toBePresent()
  })
})
