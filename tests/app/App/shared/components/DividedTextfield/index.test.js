import React from 'react'

import {
  BoxInput,
  BoxInputStyle,
  BoxInputWrapper,
  DividedTextfieldLabel,
} from 'App/shared/components/DividedTextfield/DividedTextfield.style'
import { DividedTextfieldBase } from 'App/shared/components/DividedTextfield'
import { ErrorMessage } from 'App/shared/components/Textfield/Textfield.style'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

describe('DividedTextfield', () => {
  let wrapper
  let props

  beforeEach(() => {
    props = {
      maxLength: 3,
      bgColor: '#FFF',
      label: 'label',
    }
    wrapper = shallow(<DividedTextfieldBase {...props} />)
  })

  it('renders correctly', () => {
    const tree = renderer
      .create(
        <DividedTextfieldBase
          maxLength={3}
          bgColor="#FFF"
          label="label"
          error="bad"
        />,
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('does not show a label when none is passed in', () => {
    expect(wrapper.find(DividedTextfieldLabel)).toBePresent()
    wrapper.setProps({ label: undefined })
    expect(wrapper.find(DividedTextfieldLabel)).not.toBePresent()
  })

  it('does not show an error when none is passed in', () => {
    expect(wrapper.find(ErrorMessage)).not.toBePresent()
    expect(wrapper.find(BoxInputStyle)).toHaveProp('hasError', false)
    expect(wrapper.find(DividedTextfieldLabel)).toHaveProp('hasError', false)
    expect(wrapper.find(BoxInputWrapper)).toHaveProp('hasError', false)
    expect(wrapper.find(BoxInput)).toHaveProp('hasError', false)

    wrapper.setProps({ error: 'bad' })

    expect(wrapper.find(ErrorMessage)).toBePresent()
    expect(wrapper.find(BoxInputStyle)).toHaveProp('hasError', true)
    expect(wrapper.find(DividedTextfieldLabel)).toHaveProp('hasError', true)
    expect(wrapper.find(BoxInputWrapper)).toHaveProp('hasError', true)
    expect(wrapper.find(BoxInput)).toHaveProp('hasError', true)
  })
})
