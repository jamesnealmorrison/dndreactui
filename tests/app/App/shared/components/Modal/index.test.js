import ReactModal from 'react-modal'

import { MobileModalClose } from 'App/shared/components/Modal/Modal.style'
import Modal from 'App/shared/components/Modal'

const render = shallowComponent(Modal)

describe('<Modal>', () => {
  let modal

  beforeEach(() => {
    modal = render()
  })

  it('passes through props', () => {
    modal.setProps({ foo: 'foo' })
    expect(modal.dive().find(ReactModal)).toHaveProp('foo', 'foo')
  })

  it('sets up modal base styles', () => {
    expect(modal.dive().find(ReactModal)).toHaveProp('style')
  })

  it('shows an mobile close button when it is not an alert', () => {
    expect(modal.dive().find(MobileModalClose)).toBePresent()
    modal.setProps({ alert: true })
    expect(modal.dive().find(MobileModalClose)).not.toBePresent()
  })

  it('passes styled-component classname', () => {
    expect(modal.dive().find(ReactModal)).toHaveProp('className')
  })
})
