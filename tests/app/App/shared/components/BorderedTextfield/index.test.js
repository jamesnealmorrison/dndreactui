import React from 'react'

import { BorderedTextfieldBase } from 'App/shared/components/BorderedTextfield'
import { ErrorMessage } from 'App/shared/components/Textfield/Textfield.style'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'

describe('BorderedTextfield', () => {
  let wrapper
  let props

  beforeEach(() => {
    props = {
      maxLength: 3,
      bgColor: '#FFF',
      label: 'label',
    }
    wrapper = shallow(<BorderedTextfieldBase {...props} />)
  })

  it('renders correctly', () => {
    const tree = renderer.create(<BorderedTextfieldBase error="bad" />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('shows an error when one is passed in', () => {
    wrapper.setProps({ error: 'bad' })
    expect(wrapper.find(ErrorMessage)).toBePresent()
  })
})
