import React from 'react'

import Text from 'App/shared/components/Text'
import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const tree = renderer.create(<Text />).toJSON()
  expect(tree).toMatchSnapshot()
})
