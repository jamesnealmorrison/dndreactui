import React from 'react'

import { IconButtonText } from 'App/shared/components/Button/Button.style'
import { shallow } from 'enzyme'
import Button from 'App/shared/components/Button'

const render = Btn => shallow(<Btn />)

function buttonTests(button) {
  it('displays a ripple effect by default', () => {
    expect(button.find('Ripple')).toBePresent()
  })
  it('does not show ripple effect when button is disabled', () => {
    expect(button.setProps({ disabled: true }).find('Ripple')).not.toBePresent()
  })
  it('does not show ripple effect when it is explicitly disabled', () => {
    expect(button.setProps({ ripple: false }).find('Ripple')).not.toBePresent()
  })
}

describe('Button Types', () => {
  describe('<FlatButton>', () => {
    buttonTests(render(Button))
  })
  describe('<RaisedButton>', () => {
    buttonTests(render(Button.Raised))
  })
  describe('<Fab>', () => {
    buttonTests(render(Button.Fab))
  })
  describe('<IconButton>', () => {
    it('does not display a ripple effect', () => {
      expect(render(Button.Icon).find('Ripple')).not.toBePresent()
    })
    it('can display a label', () => {
      expect(
        shallow(<Button.Icon iconLabel="foo" />).find(IconButtonText),
      ).toBePresent()
      expect(shallow(<Button.Icon />).find(IconButtonText)).not.toBePresent()
    })
  })
})
