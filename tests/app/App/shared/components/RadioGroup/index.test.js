import { RadioGroup } from 'App/shared/components/RadioGroup'

const render = shallowComponent(RadioGroup)

describe('<RadioGroup>', () => {
  let component
  let props

  beforeEach(() => {
    props = {
      onChange: jest.fn(),
      value: 'bar',
      options: [{ label: 'foo', value: 'foo' }, { label: 'bar', value: 'bar' }],
    }

    component = render(props)
  })

  it('displays a label if it is given', () => {
    expect(component.find('RadioGroupLabel')).not.toBePresent()
    expect(
      component.setProps({ label: 'label' }).find('RadioGroupLabel'),
    ).toBePresent()
  })

  it('displays each option as a Radio', () => {
    expect(component.find('Radio')).toHaveLength(2)
  })

  it('checks the active radio', () => {
    const checkedRadio = component
      .find('Radio')
      .filterWhere(r => r.prop('checked'))
    expect(checkedRadio).toHaveLength(1)
    expect(checkedRadio).toHaveProp('value', props.value)
  })

  it('updates the value', () => {
    component
      .find('Radio')
      .at(0)
      .simulate('change')

    expect(props.onChange).toHaveBeenCalledWith('foo')
  })
})
