import React from 'react'

import Offset from 'App/shared/components/Offset'
import renderer from 'react-test-renderer'

it('renders correctly', () => {
  const tree = renderer.create(<Offset />).toJSON()
  expect(tree).toMatchSnapshot()
})
