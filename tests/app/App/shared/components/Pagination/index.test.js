import React from 'react'

import { PaginationRecord } from 'store/inventoryItems/inventoryItems.records'
import { shallow } from 'enzyme'
import Button from 'App/shared/components/Button'
import Pagination from 'App/shared/components/Pagination'
import renderer from 'react-test-renderer'

describe('Pagination', () => {
  let props
  let wrapper

  beforeEach(() => {
    props = {
      pagination: PaginationRecord({
        first: false,
        last: false,
        currentIndex: 0,
        numberOfElements: 10,
        totalElements: 20,
      }),
      onClickPrevious: jest.fn(),
      onClickNext: jest.fn(),
    }
    wrapper = shallow(<Pagination {...props} />)
  })

  it('renders correctly', () => {
    const tree = renderer.create(<Pagination {...props} />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('does not show navigation when there is only one page', () => {
    wrapper.setProps({
      pagination: props.pagination.merge({ first: true, last: true }),
    })
    expect(wrapper.find(Button.Icon)).not.toBePresent()
  })

  it('disables the previous button when on the first page', () => {
    wrapper.setProps({
      pagination: props.pagination.merge({ first: true }),
    })
    expect(wrapper.find({ 'data-test-id': 'pagination-previous' })).toHaveProp(
      'disabled',
      true,
    )
  })

  it('does not show a next button when on the last page', () => {
    wrapper.setProps({
      pagination: props.pagination.merge({ last: true }),
    })
    expect(wrapper.find({ 'data-test-id': 'pagination-next' })).toHaveProp(
      'disabled',
      true,
    )
  })

  it('can navigate to previous page', () => {
    wrapper.find({ 'data-test-id': 'pagination-previous' }).simulate('click')
    expect(props.onClickPrevious).toHaveBeenCalled()
  })

  it('can navigate to next page', () => {
    wrapper.find({ 'data-test-id': 'pagination-next' }).simulate('click')
    expect(props.onClickNext).toHaveBeenCalled()
  })
})
