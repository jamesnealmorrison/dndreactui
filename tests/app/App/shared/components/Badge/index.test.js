import React from 'react'

import { BadgeText, BadgeWrap } from 'App/shared/components/Badge/Badge.style'
import { shallow } from 'enzyme'
import Badge from 'App/shared/components/Badge'
import renderer from 'react-test-renderer'

describe('<Badge />', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(
      <Badge text="foo">
        <div>Hello</div>
      </Badge>,
    )
  })

  it('renders correctly', () => {
    const tree = renderer.create(<Badge text="1">Hello</Badge>).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('renders a BadgeWrap', () => {
    expect(wrapper.find(BadgeWrap)).toBePresent()
  })

  it('renders a BadgeText with passed in text', () => {
    expect(wrapper.find(BadgeText)).toBePresent()
    expect(wrapper.find(BadgeText)).toHaveInnerText('foo')
  })

  it('renders its children', () => {
    expect(wrapper.find(BadgeWrap).find('div')).toHaveText('Hello')
  })
})
