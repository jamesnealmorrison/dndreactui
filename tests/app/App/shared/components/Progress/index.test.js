import Progress from 'App/shared/components/Progress'

const render = shallowComponent(Progress)

describe('<Progress>', () => {
  it('has a configurable width', () => {
    const progress = render({ width: '200px' })
    expect(progress.find('ProgressBase')).toHaveProp('width', '200px')
  })

  it('can be indeterminate', () => {
    const progress = render({ indeterminate: true })
    expect(progress.find('ProgressBar')).toHaveProp('indeterminate', true)
    expect(progress.find('BufferBar')).toHaveProp('indeterminate', true)
    expect(progress.find('AuxBar')).toHaveProp('indeterminate', true)
  })

  it('can be set to a specific percent filled', () => {
    const progress = render({ fill: 0.3 })
    expect(progress.find('ProgressBar')).toHaveProp('fill', 0.3)
    expect(progress.find('BufferBar')).toHaveProp('fill', 0.3)
    expect(progress.find('AuxBar')).toHaveProp('fill', 0.3)
  })
})
