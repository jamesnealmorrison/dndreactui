import React from 'react'

import {
  SearchInput,
  SearchWrap,
} from 'App/shared/components/MobileSearchBar/MobileSearchBar.style'
import { shallow } from 'enzyme'
import MobileSearchBar from 'App/shared/components/MobileSearchBar'
import renderer from 'react-test-renderer'

describe('MobileSearchBar', () => {
  it('renders correctly', () => {
    const tree = renderer.create(<MobileSearchBar />).toJSON()
    expect(tree).toMatchSnapshot()
  })

  it('passes up value on change', () => {
    const onChange = jest.fn()
    const wrapper = shallow(<MobileSearchBar onChange={onChange} />)
    wrapper.find(SearchInput).simulate('change', { target: { value: 'foo' } })
    expect(onChange).toHaveBeenCalledWith('foo')
  })

  it('passes up onSubmit but does not propgate it', () => {
    const onSubmit = jest.fn()
    const stopPropagation = jest.fn()
    const preventDefault = jest.fn()
    const wrapper = shallow(<MobileSearchBar onSubmit={onSubmit} />)
    wrapper
      .find(SearchWrap)
      .simulate('submit', { stopPropagation, preventDefault })
    expect(onSubmit).toHaveBeenCalled()
    expect(stopPropagation).toHaveBeenCalled()
    expect(preventDefault).toHaveBeenCalled()
  })
})
