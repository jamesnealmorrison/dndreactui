import { Checkbox } from 'App/shared/components/Checkbox'

const render = mountComponent(Checkbox)

describe('<Checkbox />', () => {
  let checkbox

  beforeEach(() => {
    checkbox = render({
      name: 'name',
      value: 'value',
      label: 'label',
      disabled: false,
      defaultChecked: false,
    })
  })

  it('renders an input with checkBoxStyle', () => {
    expect(checkbox.find('input')).toHaveProp('className')
  })

  it('renders a CheckboxLabel with the label text', () => {
    expect(checkbox.find('CheckboxLabel')).toBePresent()
    expect(checkbox.find('CheckboxLabel')).toHaveText('label')
  })

  it('renders a real input[type="checkbox"]', () => {
    expect(checkbox.find('input[type="checkbox"]')).toBePresent()
  })

  it('handles [prop] name, value, and defaultChecked', () => {
    checkbox.setProps({ defaultChecked: true })
    expect(checkbox.find('input[type="checkbox"]')).toHaveProp('name', 'name')
    expect(checkbox.find('input[type="checkbox"]')).toHaveProp('value', 'value')
    expect(checkbox.find('input[type="checkbox"]')).toHaveProp(
      'defaultChecked',
      true,
    )
  })

  it('handles [prop] disabled', () => {
    checkbox = render({ disabled: true, label: 'label' })
    expect(checkbox.find('CheckboxButton')).toHaveProp('disabled', true)
    expect(checkbox.find('CheckboxLabel')).toHaveProp('disabled', true)
    expect(checkbox.find('BoxOutline')).toHaveProp('disabled', true)
    expect(checkbox.find('TickOutline')).toHaveProp('disabled', true)
  })

  it('does not render a <CheckboxLabel> when there is no label', () => {
    checkbox.setProps({ label: undefined })
    expect(checkbox.find('CheckboxLabel')).not.toBePresent()
  })
})
