import React from 'react'

import { shallow } from 'enzyme'
import withState from 'App/shared/higherOrderComponents/withState'

const Dummy = () => null

describe('withState', () => {
  it('can have a default value set by value', () => {
    const Wrapped = withState('foo', 'setFoo', 'wow')(Dummy)
    const wrapper = shallow(<Wrapped />)
    expect(wrapper.find(Dummy)).toHaveProp('foo', 'wow')
  })
  it('can have a default value set by a mapping function called with props', () => {
    const Wrapped = withState('foo', 'setFoo', props => props.someProp)(Dummy)
    const wrapper = shallow(<Wrapped someProp="even more wow" />)
    expect(wrapper.find(Dummy)).toHaveProp('foo', 'even more wow')
  })
  it('can update the state prop with the update function', () => {
    const Wrapped = withState('foo', 'setFoo')(Dummy)
    const wrapper = shallow(<Wrapped />)
    wrapper.find(Dummy).prop('setFoo')('the best wow')
    wrapper.update()
    expect(wrapper.find(Dummy)).toHaveProp('foo', 'the best wow')
  })
  it('spreads its own props through', () => {
    const Wrapped = withState('foo', 'setFoo')(Dummy)
    const wrapper = shallow(<Wrapped bar="bar" />)
    expect(wrapper.find(Dummy)).toHaveProp('bar', 'bar')
  })
})
