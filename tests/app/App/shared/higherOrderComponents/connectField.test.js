import { Field } from 'redux-form/immutable'
import React from 'react'
import set from 'lodash/fp/set'

import { Textfield } from 'App/shared/components/Textfield'
import { shallow } from 'enzyme'
import connectField from 'App/shared/higherOrderComponents/connectField'

const ConnectedField = connectField(Textfield)

describe('connectField', () => {
  let component
  let fieldComponent
  let props
  let fieldProps

  beforeEach(() => {
    props = {
      name: 'name',
      foo: 'foo',
      bar: 'bar',
    }
    fieldProps = {
      input: {
        name: 'name',
        required: true,
        onBlur: jest.fn(),
        value: 'foo',
      },
      label: 'label',
      type: 'text',
      meta: {
        active: false,
        error: 'error',
        touched: false,
      },
    }
    component = shallow(<ConnectedField {...props} />)
    const FieldComponent = component.find(Field).prop('component')
    fieldComponent = shallow(<FieldComponent {...fieldProps} />)
  })

  it('renders a redux form Field', () => {
    expect(component.find(Field)).toBePresent()
  })

  it('spreads props to Field', () => {
    expect(component.find(Field).props()).toMatchObject(props)
  })

  it('sets the redux form field component to <FieldComponent>', () => {
    expect(component.find(Field)).toHaveProp('component')
  })

  describe('field component', () => {
    it('renders a Textfield', () => {
      expect(fieldComponent.find(Textfield)).toBePresent()
    })

    it('spreads input props to Textfield', () => {
      expect(fieldComponent.find(Textfield).props()).toMatchObject({
        name: 'name',
        required: true,
        value: 'foo',
      })
    })

    it('sets label', () => {
      expect(fieldComponent.find(Textfield)).toHaveProp(
        'label',
        fieldProps.label,
      )
    })

    it('calls onBlur with value', () => {
      fieldComponent.find(Textfield).simulate('blur')
      expect(fieldProps.input.onBlur).toHaveBeenCalledWith(
        fieldProps.input.value,
      )
    })

    it('sets type', () => {
      expect(fieldComponent.find(Textfield)).toHaveProp('type', fieldProps.type)
    })

    it('sets focused state', () => {
      expect(fieldComponent.find(Textfield)).toHaveProp(
        'focused',
        fieldProps.meta.active,
      )
    })

    it('shows errors only when touched', () => {
      expect(fieldComponent.find(Textfield)).toHaveProp('error', undefined)
      fieldComponent.setProps(set('meta.touched', true, fieldProps))
      expect(fieldComponent.find(Textfield)).toHaveProp(
        'error',
        fieldProps.meta.error,
      )
    })
  })
})
