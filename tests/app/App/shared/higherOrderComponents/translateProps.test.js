import { Provider } from 'react-redux'
import { defineMessages } from 'react-intl'
import { translationMessages } from 'intl/i18n'
import React from 'react'

import { mount } from 'enzyme'
import LanguageProvider from 'App/containers/LanguageProvider'
import configureStore from 'store/configureStore'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

const messages = defineMessages({
  foo: {
    id: 'foo',
    defaultMessage: 'foo {value}',
    values: {
      value: 'value',
    },
  },
  bar: {
    id: 'bar',
    defaultMessage: 'bar',
  },
})

const Dummy = () => null
const store = configureStore()
const WrappedDummy = translateProps(['foo', 'bar', 'baz'])(Dummy)

const render = () =>
  mount(
    <Provider store={store}>
      <LanguageProvider messages={translationMessages}>
        <WrappedDummy foo={messages.foo} bar={messages.bar} baz="baz" />
      </LanguageProvider>
    </Provider>,
  )

describe('translateProps', () => {
  it('takes a list of props and provides translations if those props that are passed as messages', () => {
    const component = render()
    expect(component.find('Dummy')).toHaveProp('foo', 'foo value')
    expect(component.find('Dummy')).toHaveProp('bar', 'bar')
    expect(component.find('Dummy')).toHaveProp('baz', 'baz')
  })
})
