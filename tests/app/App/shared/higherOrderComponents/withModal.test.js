import React from 'react'
import compose from 'lodash/fp/flowRight'

import withModal from 'App/shared/higherOrderComponents/withModal'

const DummyComponent = () => null
const ModalComponent = () => null
const renderModal = props => <ModalComponent {...props} />

describe('withModal', () => {
  it('passes modal state controls as props to wrapped component', () => {
    const wrapped = compose(
      withModal('modal1', renderModal),
      withModal('modal2', renderModal),
    )(DummyComponent)
    const render = mountComponent(wrapped, { foo: 'foo' })
    expect(
      render()
        .find(DummyComponent)
        .prop('modals').modal1.isOpen,
    ).toEqual(false)
    expect(
      render()
        .find(DummyComponent)
        .prop('modals').modal2.isOpen,
    ).toEqual(false)
  })

  it('passes ownProps when rendering the modal', () => {
    const wrapped = withModal('modal1', renderModal)(DummyComponent)
    const render = mountComponent(wrapped)
    expect(render({ foo: 'foo' }).find(ModalComponent)).toHaveProp('foo', 'foo')
  })

  it('handles opening a modal', () => {
    const wrapped = withModal('modal1', renderModal)(DummyComponent)
    const render = shallowComponent(wrapped)
    const baseComponent = render()
    const dummy = () => baseComponent.find(DummyComponent)
    dummy()
      .prop('modals')
      .modal1.open()
    baseComponent.update()
    expect(dummy().prop('modals').modal1.isOpen).toEqual(true)
    expect(baseComponent.find(ModalComponent)).toHaveProp('isOpen', true)
  })

  it('handles closing a modal', () => {
    const wrapped = withModal('modal1', renderModal)(DummyComponent)
    const render = shallowComponent(wrapped)
    const baseComponent = render()
    const dummy = () => baseComponent.find(DummyComponent)
    dummy()
      .prop('modals')
      .modal1.open()
    baseComponent.update()
    expect(dummy().prop('modals').modal1.isOpen).toEqual(true)
    expect(baseComponent.find(ModalComponent)).toHaveProp('isOpen', true)
    dummy()
      .prop('modals')
      .modal1.close()
    baseComponent.update()
    expect(dummy().prop('modals').modal1.isOpen).toEqual(false)
    expect(baseComponent.find(ModalComponent)).toHaveProp('isOpen', false)
  })
})
