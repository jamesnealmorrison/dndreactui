import React from 'react'

import { shallow } from 'enzyme'
import onPropChange from 'App/shared/higherOrderComponents/onPropChange'

describe('onPropChange', () => {
  it('calls passed in function when specified prop changes', () => {
    const Dummy = () => null
    const testFn = jest.fn()
    const Wrapped = onPropChange('foo', testFn)(Dummy)
    const props = { foo: 'foo', bar: 'bar' }

    const wrapper = shallow(<Wrapped {...props} />)

    wrapper.setProps({ foo: 'foo2' })

    expect(testFn).toHaveBeenCalledWith(
      { foo: 'foo2', bar: 'bar' },
      { foo: 'foo', bar: 'bar' },
    )
  })
})
