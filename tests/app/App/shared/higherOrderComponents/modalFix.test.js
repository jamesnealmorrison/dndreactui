import modalFix from 'App/shared/higherOrderComponents/modalFix'

describe('modalFix', () => {
  let wrapper
  let props
  const Dummy = () => null

  beforeEach(() => {
    props = {
      isOpen: true,
      foo: 'foo',
      bar: 'bar',
    }

    wrapper = shallowComponent(modalFix(Dummy))(props)
  })

  it('renders the wrapped component when isOpen is true', () => {
    expect(wrapper.find(Dummy)).toBePresent()
    expect(wrapper.find(Dummy)).toHaveProp('isOpen', true)
  })

  it('renders nothing when isOpen is false', () => {
    wrapper.setProps({ isOpen: false })
    expect(wrapper.find(Dummy)).not.toBePresent()
    expect(wrapper).toHaveText('')
  })

  it('passes through props', () => {
    expect(wrapper.find(Dummy)).toHaveProp('foo', 'foo')
    expect(wrapper.find(Dummy)).toHaveProp('bar', 'bar')
  })
})
