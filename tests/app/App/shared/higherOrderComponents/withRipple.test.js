import React from 'react'

import { shallow } from 'enzyme'
import Ripple from 'App/shared/components/Ripple'
import withRipple from 'App/shared/higherOrderComponents/withRipple'

const Dummy = () => <div>foo</div>

describe('withModal', () => {
  let component
  beforeEach(() => {
    const Wrapped = withRipple({ dark: true })(Dummy)
    component = shallow(<Wrapped foo="foo" />)
  })

  it('adds a ripple to the wrapped component', () => {
    expect(component.find(Ripple)).toBePresent()
  })

  it('passes through props', () => {
    expect(component.find(Dummy)).toHaveProp('foo', 'foo')
  })

  it('passes props to ripple', () => {
    expect(component.find(Ripple)).toHaveProp('dark', true)
  })
})
