import React from 'react'

import { shallow } from 'enzyme'
import onMount from 'App/shared/higherOrderComponents/onMount'

const Dummy = () => null
describe('onMount', () => {
  let props
  let Wrapped
  beforeEach(() => {
    props = {
      testFn: jest.fn(),
    }
    Wrapped = onMount(p => p.testFn())(Dummy)
  })

  it('calls given function with its own props on mount', () => {
    expect(props.testFn).not.toHaveBeenCalled()
    shallow(<Wrapped {...props} />)
    expect(props.testFn).toHaveBeenCalled()
  })

  it('passes all props through to wrapped component', () => {
    expect(
      shallow(<Wrapped {...props} />)
        .find(Dummy)
        .props(),
    ).toEqual(props)
  })
})
