import React from 'react'

import { mount } from 'enzyme'
import stickyable from 'App/shared/higherOrderComponents/stickyable'

const Dummy = () => null
const Sticky = {}

const Wrapped = stickyable(Dummy)

describe('stickable', () => {
  let component
  beforeEach(() => {
    Sticky.add = jest.fn()
  })

  describe('when sticky', () => {
    beforeEach(() => {
      component = mount(<Wrapped sticky foo="foo" />)
    })

    it('adds sticky styles to a wrapping div when sticky', () => {
      expect(component.find('div')).toHaveProp('style', {
        position: 'sticky',
        top: '-1px',
        zIndex: '1',
      })
    })

    it('has a configurable top offset', () => {
      expect(
        component
          .setProps({ top: 100 })
          .find('div')
          .prop('style'),
      ).toMatchObject({
        top: '100px',
      })
    })

    it('passes through props', () => {
      expect(component.find(Dummy)).toHaveProp('foo', 'foo')
    })
  })

  describe('when not sticky', () => {
    beforeEach(() => {
      component.setProps({ sticky: false })
    })

    it('does add sticky styles', () => {
      expect(component.find('div')).toHaveProp('style', {})
    })
  })
})
