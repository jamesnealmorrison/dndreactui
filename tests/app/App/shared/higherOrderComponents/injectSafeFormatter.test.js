import { defineMessages } from 'react-intl'
import React from 'react'

import { mount } from 'enzyme'
import AppProviders from 'AppProviders'
import injectSafeFormatter from 'App/shared/higherOrderComponents/injectSafeFormatter'

const messages = defineMessages({
  foo: {
    id: 'foo',
    defaultMessage: 'foo {value}',
    values: {
      value: 'value',
    },
  },
})

describe('injectSafeFormatter', () => {
  it('formats messages', () => {
    const Wrapped = injectSafeFormatter(props => (
      <div>{props.safeFormatMessage(messages.foo)}</div>
    ))
    const component = mount(
      <AppProviders>
        <Wrapped />
      </AppProviders>,
    )

    expect(component.find('div')).toHaveText('foo value')
  })
  it('does passes back simple strings', () => {
    const Wrapped = injectSafeFormatter(props => (
      <div>{props.safeFormatMessage('hello')}</div>
    ))
    const component = mount(
      <AppProviders>
        <Wrapped />
      </AppProviders>,
    )

    expect(component.find('div')).toHaveText('hello')
  })
})
