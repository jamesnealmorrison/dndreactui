import React from 'react'

import { mount } from 'enzyme'
import handleInput from 'App/shared/higherOrderComponents/handleInput'

describe('handleInput', () => {
  const Wrapped = handleInput(props => (
    <input
      type="text"
      value={props.value}
      onChange={props.onChange}
      defaultValue={props.defaultValue}
      onFocus={props.onFocus}
    />
  ))

  it('creates a stateful input', () => {
    const wrapper = mount(<Wrapped defaultValue="foo" />)
    expect(wrapper.find('input')).toHaveProp('value', 'foo')
    wrapper.find('input').simulate('change', { target: { value: 'bar' } })
    expect(wrapper.find('input')).toHaveProp('value', 'bar')
  })

  it('converts defaultValue to value', () => {
    const wrapper = mount(<Wrapped defaultValue="foo" />)
    expect(wrapper.find('input')).toHaveProp('defaultValue', undefined)
  })

  it('converts falls back to an empty string for value', () => {
    const wrapper = mount(<Wrapped />)
    expect(wrapper.find('input')).toHaveProp('value', '')
  })

  it('passes up change events', () => {
    const onChange = jest.fn()
    const wrapper = mount(<Wrapped defaultValue="foo" onChange={onChange} />)
    const event = { target: { value: 'bar' } }
    wrapper.find('input').prop('onChange')(event)
    expect(onChange).toHaveBeenCalledWith(event)
  })

  it('passes up stringy change events', () => {
    const onChange = jest.fn()
    const wrapper = mount(<Wrapped defaultValue="foo" onChange={onChange} />)
    const event = 'bar'
    wrapper.find('input').prop('onChange')(event)
    expect(onChange).toHaveBeenCalledWith(event)
  })

  it('passes up focus events', () => {
    const onFocus = jest.fn()
    const wrapper = mount(<Wrapped defaultValue="foo" onFocus={onFocus} />)
    wrapper.find('input').simulate('focus')
    expect(onFocus).toHaveBeenCalled()
  })

  it('scrolls element into view on android when it is above the screen', () => {
    jest.useFakeTimers()
    const wrapper = mount(<Wrapped isAndroid />)
    window.innerHeight = 500
    const target = {
      getBoundingClientRect: () => ({ top: -1, bottom: 10 }),
      scrollIntoView: jest.fn(),
    }
    wrapper.find('input').simulate('focus', { target })
    jest.runTimersToTime(250)

    expect(target.scrollIntoView).toHaveBeenCalled()
  })

  it('scrolls element into view on android when it is below the screen', () => {
    jest.useFakeTimers()
    const wrapper = mount(<Wrapped isAndroid />)
    window.innerHeight = 500
    const target = {
      getBoundingClientRect: () => ({ top: 550, bottom: 600 }),
      scrollIntoView: jest.fn(),
    }
    wrapper.find('input').simulate('focus', { target })
    jest.runTimersToTime(250)

    expect(target.scrollIntoView).toHaveBeenCalled()
  })

  it('does not scroll when element is already in view', () => {
    jest.useFakeTimers()
    const wrapper = mount(<Wrapped isAndroid />)
    window.innerHeight = 650
    const target = {
      getBoundingClientRect: () => ({ top: 550, bottom: 600 }),
      scrollIntoView: jest.fn(),
    }
    wrapper.find('input').simulate('focus', { target })
    jest.runTimersToTime(250)

    expect(target.scrollIntoView).not.toHaveBeenCalled()
  })

  it('works as a controlled component', () => {
    const wrapper = mount(<Wrapped value="foo" />)
    expect(wrapper.find('input')).toHaveProp('value', 'foo')
    wrapper.setProps({ value: 'bar' })
    expect(wrapper.find('input')).toHaveProp('value', 'bar')
  })

  it('does not try to interally change value of controlled component', () => {
    const wrapper = mount(<Wrapped value="foo" />)
    wrapper.find('input').simulate('change', { target: { value: 'bar' } })
    expect(wrapper.find('input')).toHaveProp('value', 'foo')
  })
})
