import React from 'react'

import AppProviders from 'AppProviders'
import PlayerCharactersPageHeader from 'App/routes/PlayerCharactersPage/PlayerCharactersPageHeader'
import renderer from 'react-test-renderer'

describe('PlayerCharactersPageHeader', () => {
  it('should render correctly', () => {
    const tree = renderer
      .create(
        <AppProviders>
          <PlayerCharactersPageHeader />
        </AppProviders>,
      )
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
