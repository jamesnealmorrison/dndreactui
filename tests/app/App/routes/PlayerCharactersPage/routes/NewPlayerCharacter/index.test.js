import { SubmissionError } from 'redux-form/immutable'
import { fromJS } from 'immutable'
import React from 'react'

import {
  NewEquipmentCodePageBase,
  onSubmit,
} from 'App/routes/PlayerCharactersPage/routes/NewPlayerCharacter'
import { required } from 'helpers/validators'
import { shallow } from 'enzyme'
import { showAlertError } from 'store/errors/errors.actions'

import jestWhen from '../../../../../../jestWhen'

describe('NewPlayerCharacter', () => {
  let props
  let wrapper

  beforeEach(() => {
    props = {
      handleSubmit: jest.fn(),
      resetField: jest.fn(),
      selectedEquipmentCodeId: 1,
    }
    wrapper = shallow(<NewEquipmentCodePageBase {...props} />)
  })

  it('is wired up to reduxForm', () => {
    wrapper.find('form').simulate('submit', 'mockEvent')
    expect(props.handleSubmit).toHaveBeenCalledWith('mockEvent')
  })

  it('requires a code field', () => {
    expect(wrapper.find({ name: 'code' }).prop('validate')).toContain(required)
  })

  it('requires a description field', () => {
    expect(wrapper.find({ name: 'description' }).prop('validate')).toContain(
      required,
    )
  })

  describe('onSubmit', () => {
    let createEquipmentCode
    let dispatch
    let values
    const action = Math.random()

    beforeEach(() => {
      createEquipmentCode = jest.fn()
      dispatch = jest.fn()
      values = fromJS({
        code: 'NEC',
        description: 'New Equipment Code',
      })

      jestWhen(createEquipmentCode)({
        code: 'NEC',
        description: 'New Equipment Code',
      }).thenReturn(action)
    })

    it('posts new model to API', () => {
      onSubmit(createEquipmentCode)(values, dispatch)

      expect(dispatch).toHaveBeenCalledWith(action)
    })

    it('handles generic failures', async () => {
      const error = new Error('boom!')
      error.status = 0
      dispatch = jest.fn().mockReturnValueOnce(Promise.reject(error))

      const e = await onSubmit(createEquipmentCode)(values, dispatch).catch(
        e => e,
      )
      expect(dispatch).toHaveBeenCalledWith(showAlertError(e))
    })

    it('handles validation failures', async () => {
      const error = new Error('boom!')
      error.status = 400
      dispatch = jest.fn(() => Promise.reject(error))

      const e = await onSubmit(createEquipmentCode)(values, dispatch).catch(
        e => e,
      )

      expect(e instanceof SubmissionError).toEqual(true)
      expect(e.errors).toEqual({ _error: 'boom!' })
    })
  })
})
