import React from 'react'

import { App, runOnMount } from 'App'
import { PageWrap } from 'App/components/App.style'
import { shallow } from 'enzyme'
import Progress from 'App/shared/components/Progress'

jest.mock('helpers/screenForm', () => ({
  getScreenForm: () => 'DESKTOP',
}))
jest.mock('helpers/onScreenFormChange', () => cb => cb())

describe('<App />', () => {
  let component
  let props

  beforeEach(() => {
    props = {
      setNavOpen: jest.fn(),
      isNavOpen: true,
      isLoading: false,
    }

    component = shallow(<App {...props} />)
  })

  describe('onMount', () => {
    beforeEach(() => {
      props = {
        setNavOpen: jest.fn(),
        dispatchGetEquipmentCodes: jest.fn(() => Promise.resolve()),
        dispatchGetItemStatuses: jest.fn(() => Promise.resolve()),
        dispatchScreenFormChange: jest.fn(),
        setIsLoading: jest.fn(),
      }
    })
    it('should get equipment codes when the app mounts', () => {
      runOnMount(props)
      expect(props.dispatchGetEquipmentCodes).toHaveBeenCalled()
    })
    it('should get itemStatuses when the app mounts', () => {
      runOnMount(props)
      expect(props.dispatchGetItemStatuses).toHaveBeenCalled()
    })
    it('hides the loading bar after all data is fetched', async () => {
      const p = runOnMount(props)
      expect(props.setIsLoading).not.toHaveBeenCalled()
      await p
      expect(props.setIsLoading).toHaveBeenCalledWith(false)
    })
  })

  it('should show a loading bar when app data is loading', () => {
    component.setProps({ isLoading: true })
    expect(component.find(Progress)).toBePresent()
    expect(component.find(PageWrap)).not.toBePresent()

    component.setProps({ isLoading: false })
    expect(component.find(PageWrap)).toBePresent()
    expect(component.find(Progress)).not.toBePresent()
  })
})
