import React from 'react'

import { PageHeader } from 'App/containers/PageHeader'
import {
  PageHeaderNavToggle,
  PageHeaderTitle,
} from 'App/containers/PageHeader/PageHeader.style'
import { shallow } from 'enzyme'

describe('PageHeader', () => {
  let component
  let props

  beforeEach(() => {
    props = {
      title: 'title',
      isNavOpen: false,
      dispatchSetNavOpen: jest.fn(),
    }

    component = shallow(<PageHeader {...props} />)
  })

  it('can open the menu', () => {
    component
      .find(PageHeaderNavToggle)
      .children()
      .simulate('click')
    expect(props.dispatchSetNavOpen).toHaveBeenCalledWith(true)
  })

  it('hides the menu toggler when the menu is open', () => {
    expect(component.find(PageHeaderNavToggle)).toHaveProp('isNavOpen', false)
    component.setProps({ isNavOpen: true })
    expect(component.find(PageHeaderNavToggle)).toHaveProp('isNavOpen', true)
  })

  it('shows the page title', () => {
    expect(component.find(PageHeaderTitle)).toHaveInnerText('title')
  })
})
