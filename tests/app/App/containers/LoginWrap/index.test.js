import React from 'react'

import {
  LoginFormStyle,
  Login,
  LoginForm,
  WrappedLoginForm,
} from 'App/containers/LoginWrap'
import { required } from 'helpers/validators'
import { shallow } from 'enzyme'

describe('LoginWrap', () => {
  it('shows children when logged in', () => {
    const wrapper = shallow(<Login isLoggedIn>foo</Login>)
    expect(wrapper).toIncludeText('foo')
  })

  it('shows LoginForm when not logged in', () => {
    const wrapper = shallow(<Login isLoggedIn={false}>foo</Login>)
    expect(wrapper).not.toIncludeText('foo')
    expect(wrapper.find(WrappedLoginForm)).toBePresent()
  })
})

describe('LoginForm', () => {
  let props
  let wrapper
  beforeEach(() => {
    props = {
      handleSubmit: jest.fn(),
    }
    wrapper = shallow(<LoginForm {...props} />)
  })

  it('has a required username field', () => {
    expect(wrapper.find({ name: 'username' }).prop('validate')).toContain(
      required,
    )
  })
  it('has a required password field', () => {
    expect(wrapper.find({ name: 'password' }).prop('validate')).toContain(
      required,
    )
  })
  it('is wired to redux form', () => {
    wrapper.find(LoginFormStyle).simulate('submit', 'mockEvent')
    expect(props.handleSubmit).toHaveBeenCalledWith('mockEvent')
  })
})
