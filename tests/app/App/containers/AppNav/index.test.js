import React from 'react'

import {
  AppNavBackdrop,
  AppNavMain,
  AppNavToggle,
  LogoutButton,
} from 'App/containers/AppNav/AppNav.style'
import { AppNavBase } from 'App/containers/AppNav'
import { shallow } from 'enzyme'

describe('<App />', () => {
  let wrapper
  let props

  beforeEach(() => {
    props = {
      closeNav: jest.fn(),
      openNav: jest.fn(),
      logout: jest.fn(),
      isOpen: true,
      children: 'foo',
    }

    wrapper = shallow(<AppNavBase {...props} />)
  })

  it('should openable and closeable', () => {
    wrapper.setProps({ isOpen: true })
    expect(wrapper.find(AppNavMain)).toHaveProp('isNavOpen', true)
    expect(wrapper.find(AppNavBackdrop)).toHaveProp('isVisible', true)

    wrapper.setProps({ isOpen: false })
    expect(wrapper.find(AppNavMain)).toHaveProp('isNavOpen', false)
    expect(wrapper.find(AppNavBackdrop)).toHaveProp('isVisible', false)
  })

  it('should be toggleable', () => {
    wrapper.setProps({ isOpen: true })
    wrapper.find(AppNavToggle).simulate('click')
    expect(props.closeNav).toHaveBeenCalled()

    wrapper.setProps({ isOpen: false })
    wrapper.find(AppNavToggle).simulate('click')
    expect(props.openNav).toHaveBeenCalled()
  })

  it('should request close when backdrop is clicked', () => {
    wrapper.find(AppNavBackdrop).simulate('click')
    expect(props.closeNav).toHaveBeenCalled()
  })

  it('should handle logoout', () => {
    wrapper.find(LogoutButton).simulate('click')
    expect(props.logout).toHaveBeenCalled()
  })

  it('should render its children', () => {
    expect(wrapper.find(AppNavMain)).toIncludeInnerText('foo')
  })
})
