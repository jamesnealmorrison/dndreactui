import { Provider } from 'react-redux'
import { appLocales, translationMessages } from 'intl/i18n'
import { browserHistory } from 'react-router-dom'
import React from 'react'
import compose from 'lodash/fp/flowRight'
import findKey from 'lodash/fp/findKey'

import { mount } from 'enzyme'
import { selectLocale } from 'store/app/app.selectors'
import LanguageProvider from 'App/containers/LanguageProvider'
import LocaleToggle, { longLanguageNames } from 'App/containers/LocaleToggle'
import configureStore from 'store/configureStore'

const getComponent = store => (
  <Provider store={store}>
    <LanguageProvider messages={translationMessages}>
      <LocaleToggle />
    </LanguageProvider>
  </Provider>
)

const render = compose(mount, getComponent)

describe('<LocaleToggle />', () => {
  let store
  let toggle

  beforeAll(() => {
    store = configureStore({}, browserHistory)
    toggle = render(store)
    jest.useFakeTimers()
  })

  it('should present each language option', () => {
    appLocales.forEach(locale => {
      expect(
        toggle
          .find('MenuItem')
          .filterWhere(m => m.text() === longLanguageNames[locale]),
      )
    })
  })

  it('can change the language', () => {
    const nonEnglishMenuOption = toggle
      .find('MenuItem')
      .filterWhere(m => m.text() !== 'English')
      .at(0)

    const lang = findKey(
      l => l === nonEnglishMenuOption.text(),
      longLanguageNames,
    )

    nonEnglishMenuOption.simulate('click')
    jest.runAllTimers()
    toggle.update()
    expect(
      mount(toggle.find('Menu').prop('control'))
        .find('ToggleButton')
        .text(),
    ).toEqual(lang)
    expect(selectLocale(store.getState())).toEqual(lang)
  })
})
