import { Provider } from 'react-redux'
import { defineMessages } from 'react-intl'
import { translationMessages } from 'intl/i18n'
import React from 'react'

import { createMessage } from 'store/messages/messages.actions'
import { mount } from 'enzyme'
import LanguageProvider from 'App/containers/LanguageProvider'
import MessageProvider from 'App/containers/MessageProvider'
import configureStore from 'store/configureStore'

const testMessages = defineMessages({
  foo: { defaultMessage: 'foo', id: 'foo' },
})

describe('<MessageProvider />', () => {
  let messages
  let store

  const setMessage = (...args) => {
    store.dispatch(createMessage(...args))
    messages.update()
  }

  beforeEach(() => {
    store = configureStore()
    const Wrapped = () => (
      <Provider store={store}>
        <LanguageProvider messages={translationMessages}>
          <MessageProvider />
        </LanguageProvider>
      </Provider>
    )
    jest.useFakeTimers()
    messages = mount(<Wrapped />)
  })

  describe('when it receives a timed message', () => {
    it('displays the message in the next frame', () => {
      setMessage(testMessages.foo, 3000)
      expect(messages.find('Toast')).toHaveProp('isActive', false)
      jest.runTimersToTime(0)
      messages.update()
      expect(messages.find('Toast')).toHaveProp('isActive', true)
    })

    it('is not dismissable', () => {
      setMessage(testMessages.foo, 3000)
      jest.runTimersToTime(0)
      messages.update()
      expect(messages.find('SnackbarButton')).not.toBePresent()
    })

    it('hides the message after its timeout', () => {
      setMessage(testMessages.foo, 3000)
      jest.runTimersToTime(2999)
      messages.update()
      expect(messages.find('Toast')).toHaveProp('isActive', true)
      jest.runTimersToTime(1)
      messages.update()
      expect(messages.find('Toast')).toHaveProp('isActive', false)
    })

    it('deletes the message only after it animates out', () => {
      setMessage(testMessages.foo, 3000)
      jest.runTimersToTime(3299)
      messages.update()
      expect(messages.find('Snackbar')).toIncludeText('foo')
      jest.runTimersToTime(1)
      messages.update()
      expect(messages.find('Snackbar')).not.toIncludeText('foo')
    })
  })

  describe('when it receives a timed message', () => {
    beforeEach(() => {
      setMessage(testMessages.foo, 0)
      jest.runTimersToTime(0)
      messages.update()
    })

    it('is dismissable', () => {
      expect(messages.find('SnackbarButton')).toBePresent()
    })

    it('hides the message when dismissed', () => {
      messages.find('SnackbarButton').simulate('click')
      expect(messages.find('Toast')).toHaveProp('isActive', false)
    })

    it('deletes the message only after it animates out', () => {
      messages.find('SnackbarButton').simulate('click')
      jest.runTimersToTime(299)
      messages.update()
      expect(messages.find('Snackbar')).toIncludeText('foo')
      jest.runTimersToTime(1)
      messages.update()
      expect(messages.find('Snackbar')).not.toIncludeText('foo')
    })
  })

  it('works with simple string messages', () => {
    setMessage('foo', 3000)
    jest.runTimersToTime(0)
    messages.update()
    expect(messages.find('Snackbar')).toIncludeText('foo')
  })
})
