import Portal from 'react-portal'

import Toast from 'App/containers/MessageProvider/Toast'

const render = shallowComponent(Toast)

describe('<Toast>', () => {
  let toast

  beforeEach(() => {
    toast = render({ isActive: false })
    jest.useFakeTimers()
  })

  describe('when the toast is inactive', () => {
    it('has a closed portal', () => {
      expect(toast.find(Portal)).toHaveProp('isOpened', false)
    })
  })

  describe('when the toast becomes active', () => {
    beforeEach(() => {
      toast.setProps({ isActive: true })
    })

    it('opens a portal to display the toast', () => {
      expect(toast.find(Portal)).toHaveProp('isOpened', true)
    })

    it('starts animating in the next frame', () => {
      expect(toast.find('ToastAnimation')).toHaveProp('isActive', false)
      jest.runAllTimers()
      toast.update()
      expect(toast.find('ToastAnimation')).toHaveProp('isActive', true)
    })

    it('does not error if the component unmounts', () => {
      window.console.error = jest.fn()

      toast.unmount()
      jest.runAllTimers()

      expect(window.console.error).not.toHaveBeenCalled()
    })
  })

  describe('when the toast becomes inactive', () => {
    beforeEach(() => {
      toast.setProps({ isActive: true })
      jest.runAllTimers()
      toast.update()
      toast.setProps({ isActive: false })
    })

    it('starts a close animation', () => {
      expect(toast.find('ToastAnimation')).toHaveProp('isActive', false)
    })

    it('it closes the portal after the close animation', () => {
      expect(toast.find(Portal)).toHaveProp('isOpened', true)
      jest.runAllTimers()
      toast.update()
      expect(toast.find(Portal)).toHaveProp('isOpened', false)
    })

    it('does not close the portal if it becomes active again before the animation ends', () => {
      jest.runTimersToTime(50)
      toast.setProps({ isActive: true })
      jest.runAllTimers()
      toast.update()
      expect(toast.find(Portal)).toHaveProp('isOpened', true)
      expect(toast.find('ToastAnimation')).toHaveProp('isActive', true)
    })
  })
})
