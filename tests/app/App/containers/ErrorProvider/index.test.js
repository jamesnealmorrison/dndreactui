import React from 'react'

import { ErrorMessageRecord } from 'store/errors/errors.records'
import { ErrorProvider } from 'App/containers/ErrorProvider'
import { shallow } from 'enzyme'
import ErrorModal from 'App/containers/ErrorProvider/ErrorModal'
import ErrorPage from 'App/containers/ErrorProvider/ErrorPage'

describe('ErrorProvider', () => {
  let component
  let props
  const alertError = new ErrorMessageRecord({
    message: 'message',
    title: 'title',
  })
  const pageError = alertError.set('pageError', true)
  const ChildComponent = () => null
  beforeEach(() => {
    props = {
      hasError: true,
      error: alertError,
      dispatchClearError: jest.fn(),
      children: <ChildComponent />,
    }
    component = shallow(<ErrorProvider {...props} />)
  })

  describe('when there is a page error', () => {
    beforeEach(() => {
      component.setProps({ hasError: true, error: pageError })
    })

    it('shows an error page', () => {
      expect(component.find(ErrorPage)).toBePresent()
      expect(component.find(ErrorPage)).toHaveProp('message', pageError.message)
      expect(component.find(ErrorPage)).toHaveProp('title', pageError.title)
    })

    it('does not show the error as an alert', () => {
      expect(component.find(ErrorModal)).not.toBePresent()
    })

    it('does not render its children', () => {
      expect(component.find(ChildComponent)).not.toBePresent()
    })
  })

  describe('when there is an alert error', () => {
    beforeEach(() => {
      component.setProps({ hasError: true, error: alertError })
    })

    it('shows an error alert', () => {
      expect(component.find(ErrorModal)).toBePresent()
      expect(component.find(ErrorModal)).toHaveProp(
        'message',
        alertError.message,
      )
      expect(component.find(ErrorModal)).toHaveProp('title', alertError.title)
      expect(component.find(ErrorModal)).toHaveProp('isOpen', true)
    })

    it('is dismissable', () => {
      component.find(ErrorModal).prop('clearError')()
      expect(props.dispatchClearError).toHaveBeenCalled()
    })

    it('does not show the error as a page', () => {
      expect(component.find(ErrorPage)).not.toBePresent()
    })

    it('renders its children', () => {
      expect(component.find(ChildComponent)).toBePresent()
    })
  })

  describe('when there is no error', () => {
    beforeEach(() => {
      component.setProps({ hasError: false, error: undefined })
    })

    it('does not show the alert', () => {
      expect(component.find(ErrorModal)).toHaveProp('isOpen', false)
    })

    it('does not show the error page', () => {
      expect(component.find(ErrorPage)).not.toBePresent()
    })

    it('renders its children', () => {
      expect(component.find(ChildComponent)).toBePresent()
    })
  })
})
