import React from 'react'

import {
  ErrorMessage,
  ErrorTitle,
} from 'App/containers/ErrorProvider/ErrorPage/ErrorPage.style'
import { shallow } from 'enzyme'
import { ErrorPageBase } from 'App/containers/ErrorProvider/ErrorPage'

describe('ErrorPage', () => {
  let component
  let props

  beforeEach(() => {
    props = {
      message: 'message',
      title: 'title',
    }

    component = shallow(<ErrorPageBase {...props} />)
  })

  it('is shows errors', () => {
    expect(component.find(ErrorTitle)).toHaveInnerText('title')
    expect(component.find(ErrorMessage)).toHaveInnerText('message')
  })
})
