import React from 'react'

import { ErrorMessageRecord } from 'store/errors/errors.records'
import { ErrorModalBase } from 'App/containers/ErrorProvider/ErrorModal'
import { shallow } from 'enzyme'
import Button from 'App/shared/components/Button'
import ErrorText from 'App/shared/components/ErrorText'
import Modal from 'App/shared/components/Modal'

describe('ErrorModal', () => {
  let component
  let props

  beforeEach(() => {
    props = {
      isOpen: true,
      message: 'message',
      title: 'title',
      clearError: jest.fn(),
    }

    component = shallow(<ErrorModalBase {...props} />)
  })

  it('is openable', () => {
    component.setProps({ isOpen: false })
    expect(component.find(Modal)).toHaveProp('isOpen', false)

    component.setProps({ isOpen: true })
    expect(component.find(Modal)).toHaveProp('isOpen', true)
  })

  it('is shows errors when there is an error', () => {
    component.setProps({
      isOpen: true,
      error: new ErrorMessageRecord({ message: 'message', title: 'title' }),
    })

    expect(component.find(Modal)).toHaveProp('isOpen', true)
    expect(component.find(Modal.Title)).toIncludeInnerText('title')
    expect(component.find(Modal.Content).find(ErrorText)).toHaveInnerText(
      'message',
    )
  })

  it('is dismissable', () => {
    component.find(Button).simulate('click')
    expect(props.clearError).toHaveBeenCalled()
  })
})
