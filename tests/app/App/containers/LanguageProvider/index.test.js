import { FormattedMessage, defineMessages } from 'react-intl'
import { Provider } from 'react-redux'
import { browserHistory } from 'react-router-dom'
import { translationMessages } from 'intl/i18n'
import React from 'react'

import { mount, shallow } from 'enzyme'
import ConnectedLanguageProvider, {
  LanguageProvider,
} from 'App/containers/LanguageProvider'
import configureStore from 'store/configureStore'

const messages = defineMessages({
  someMessage: {
    id: 'some.id',
    defaultMessage: 'This is some default message',
    en: 'This is some en message',
  },
})

describe('<LanguageProvider />', () => {
  it('should render its children', () => {
    const children = <h1>Test</h1>
    const renderedComponent = shallow(
      <LanguageProvider messages={messages} locale="en">
        {children}
      </LanguageProvider>,
    )
    expect(renderedComponent.contains(children)).toBe(true)
  })
})

describe('<ConnectedLanguageProvider />', () => {
  let store

  beforeAll(() => {
    store = configureStore({}, browserHistory)
  })

  it('should render the default language messages', () => {
    const renderedComponent = mount(
      <Provider store={store}>
        <ConnectedLanguageProvider messages={translationMessages}>
          <FormattedMessage {...messages.someMessage} />
        </ConnectedLanguageProvider>
      </Provider>,
    )
    expect(
      renderedComponent.contains(
        <FormattedMessage {...messages.someMessage} />,
      ),
    ).toBe(true)
  })
})
