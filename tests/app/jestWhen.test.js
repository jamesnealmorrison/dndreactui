import jestWhen from '../jestWhen'

describe('jestWhen', () => {
  it('works', async () => {
    const mockFunction = jest.fn()
    const whenMockFunction = jestWhen(mockFunction)

    whenMockFunction(1, 2).thenReturn(3)
    whenMockFunction('foo').thenReturn('bar')
    whenMockFunction('foo').thenResolve('baz')
    whenMockFunction('bad').thenThrow(new Error('bad'))
    expect(mockFunction(1, 2)).toEqual(3)
    expect(mockFunction).toHaveBeenCalledWith(1, 2)
    expect(await mockFunction('foo')).toEqual('baz')
    expect(() => mockFunction('bad')).toThrow(new Error('bad'))
    expect(mockFunction('something else')).toEqual(undefined)
  })
})
