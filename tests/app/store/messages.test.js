import { List } from 'immutable'

import { MessageRecord } from 'store/messages/messages.records'
import { createMessage, deleteMessage } from 'store/messages/messages.actions'
import {
  selectCurrentMessage,
  selectMessagesList,
} from 'store/messages/messages.selectors'
import configureStore from 'store/configureStore'

let store
let state

const messages = {
  foo: { defaultMessage: 'foo', id: 'foo' },
  bar: { defaultMessage: 'bar', id: 'bar' },
  baz: { defaultMessage: 'baz', id: 'baz' },
  fooStr: 'foo',
}

describe('messages duck', () => {
  beforeEach(() => {
    store = configureStore()
  })

  describe('adding a message', () => {
    it('adds a timed message to state', () => {
      store.dispatch(createMessage(messages.foo))
      state = store.getState()
      const result = selectCurrentMessage(state)

      expect(result).toEqualImmutable(
        MessageRecord({ text: messages.foo, timeout: 3000 }),
      )
    })

    it('can have a custom timeout', () => {
      store.dispatch(createMessage(messages.foo, 200))
      state = store.getState()
      const result = selectCurrentMessage(state)

      expect(result).toEqualImmutable(
        MessageRecord({ text: messages.foo, timeout: 200 }),
      )
    })

    it('is added to a queue', () => {
      store.dispatch(createMessage(messages.foo))
      store.dispatch(createMessage(messages.bar))
      state = store.getState()
      const result = selectMessagesList(state)

      expect(result).toEqualImmutable(
        List([
          MessageRecord({ text: messages.foo, timeout: 3000 }),
          MessageRecord({ text: messages.bar, timeout: 3000 }),
        ]),
      )
    })

    it('works with simple string messages', () => {
      store.dispatch(createMessage(messages.fooStr))
      state = store.getState()
      const result = selectCurrentMessage(state)

      expect(result).toEqualImmutable(
        MessageRecord({ text: messages.fooStr, timeout: 3000 }),
      )
    })
  })

  describe('deleting a message', () => {
    it('deletes the given message from state', () => {
      store.dispatch(createMessage(messages.foo))
      store.dispatch(createMessage(messages.bar))
      store.dispatch(createMessage(messages.baz))
      state = store.getState()
      const barMessage = selectMessagesList(state).get(1)
      store.dispatch(deleteMessage(barMessage))
      state = store.getState()
      const result = selectMessagesList(state)

      expect(result).toEqualImmutable(
        List([
          MessageRecord({ text: messages.foo, timeout: 3000 }),
          MessageRecord({ text: messages.baz, timeout: 3000 }),
        ]),
      )
    })
  })
})
