import { LOCATION_CHANGE } from 'react-router-redux'
import { List, OrderedMap } from 'immutable'

import { ModelRecord } from 'store/models/models.records'
import {
  computeModelList,
  selectModelById,
  selectModelFilter,
  selectModels,
} from 'store/models/models.selectors'
import {
  createModel,
  deactivateModel,
  setModelListFilter,
} from 'store/models/models.actions'
import { selectCurrentMessage } from 'store/messages/messages.selectors'
import { selectHasError } from 'store/errors/errors.selectors'
import apiClient from 'apiClient'
import configureStore from 'store/configureStore'
import modelsMessages from 'store/models/models.messages'

import mockState from '../../../mockData/mockState'

describe('models duck', () => {
  let store

  beforeEach(() => {
    store = configureStore(mockState.setIn(['models', 'records'], List()))
    apiClient.get = jest.fn(() =>
      Promise.resolve([
        { id: 1, makeId: 1, name: 'Nuvi' },
        { id: 2, makeId: 3, name: 'EZPass' },
      ]),
    )
  })

  it('strips out deactivated models', () => {
    store = configureStore(
      mockState.setIn(
        ['models', 'records'],
        List([
          ModelRecord({
            id: 1,
            name: 'Nuvi',
            equipmentCodeId: 1,
            equipmentCode: 'GPS',
            makeId: 1,
            make: 'Garmin',
            deactivated: false,
          }),
          ModelRecord({
            id: 2,
            name: 'Muvi',
            equipmentCodeId: 1,
            equipmentCode: 'GPS',
            makeId: 1,
            make: 'Garmin',
            deactivated: true,
          }),
        ]),
      ),
    )
    expect(selectModels(store.getState())).toEqualImmutable(
      List([
        ModelRecord({
          id: 1,
          name: 'Nuvi',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
          makeId: 1,
          make: 'Garmin',
          deactivated: false,
        }),
      ]),
    )
  })

  it('can select a make by id', () => {
    store = configureStore(
      mockState.setIn(
        ['models', 'records'],
        OrderedMap([
          [
            1,
            ModelRecord({
              id: 1,
              name: 'Nuvi',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 1,
              make: 'Garmin',
            }),
          ],
          [
            2,
            ModelRecord({
              id: 2,
              name: 'EZPass',
              equipmentCodeId: 2,
              equipmentCode: 'TPD',
              makeId: 2,
              make: 'EZPass',
            }),
          ],
        ]),
      ),
    )

    expect(selectModelById(1)(store.getState())).toEqualImmutable(
      ModelRecord({
        id: 1,
        name: 'Nuvi',
        equipmentCodeId: 1,
        equipmentCode: 'GPS',
        makeId: 1,
        make: 'Garmin',
      }),
    )
  })

  it('can set a filter', () => {
    store.dispatch(setModelListFilter('GPS'))
    expect(selectModelFilter(store.getState())).toEqual('GPS')
  })

  it('should return a sorted model list', () => {
    store = configureStore(
      mockState.setIn(
        ['models', 'records'],
        OrderedMap([
          [
            1,
            ModelRecord({
              id: 1,
              name: 'Nuvi',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 1,
              make: 'Garmin',
            }),
          ],
          [
            2,
            ModelRecord({
              id: 2,
              name: 'Whereami',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 2,
              make: 'Motorolla',
            }),
          ],
          [
            3,
            ModelRecord({
              id: 3,
              name: 'EZPass',
              equipmentCodeId: 2,
              equipmentCode: 'TPD',
              makeId: 3,
              make: 'EZPass',
            }),
          ],
        ]),
      ),
    )
    const expected = List([
      ModelRecord({
        id: 3,
        name: 'EZPass',
        equipmentCodeId: 2,
        equipmentCode: 'TPD',
        makeId: 3,
        make: 'EZPass',
      }),
      ModelRecord({
        id: 1,
        name: 'Nuvi',
        equipmentCodeId: 1,
        equipmentCode: 'GPS',
        makeId: 1,
        make: 'Garmin',
      }),
      ModelRecord({
        id: 2,
        name: 'Whereami',
        equipmentCodeId: 1,
        equipmentCode: 'GPS',
        makeId: 2,
        make: 'Motorolla',
      }),
    ])
    expect(computeModelList(store.getState())).toEqualImmutable(expected)
  })

  it('can fuzzy filter model list', () => {
    store = configureStore(
      mockState.setIn(
        ['models', 'records'],
        OrderedMap([
          [
            1,
            ModelRecord({
              id: 1,
              name: 'Nuvi',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 1,
              make: 'Garmin',
            }),
          ],
          [
            2,
            ModelRecord({
              id: 2,
              name: 'Whereami',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 2,
              make: 'Motorolla',
            }),
          ],
          [
            3,
            ModelRecord({
              id: 3,
              name: 'EZPass',
              equipmentCodeId: 2,
              equipmentCode: 'TPD',
              makeId: 3,
              make: 'EZPass',
            }),
          ],
        ]),
      ),
    )
    store.dispatch(setModelListFilter('GPS'))
    expect(computeModelList(store.getState())).toEqual(
      List([
        ModelRecord({
          id: 1,
          name: 'Nuvi',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
          makeId: 1,
          make: 'Garmin',
        }),
        ModelRecord({
          id: 2,
          name: 'Whereami',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
          makeId: 2,
          make: 'Motorolla',
        }),
      ]),
    )
    store.dispatch(setModelListFilter('GPS Moto'))
    expect(computeModelList(store.getState())).toEqual(
      List([
        ModelRecord({
          id: 2,
          name: 'Whereami',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
          makeId: 2,
          make: 'Motorolla',
        }),
      ]),
    )
  })

  it('should trim whitespace when filtering', () => {
    store = configureStore(
      mockState.setIn(
        ['models', 'records'],
        OrderedMap([
          [
            1,
            ModelRecord({
              id: 1,
              name: 'Nuvi',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 1,
              make: 'Garmin',
            }),
          ],
          [
            2,
            ModelRecord({
              id: 2,
              name: 'Whereami',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 2,
              make: 'Motorolla',
            }),
          ],
          [
            3,
            ModelRecord({
              id: 3,
              name: 'EZPass',
              equipmentCodeId: 2,
              equipmentCode: 'TPD',
              makeId: 3,
              make: 'EZPass',
            }),
          ],
        ]),
      ),
    )
    store.dispatch(setModelListFilter('  GPS  '))
    expect(computeModelList(store.getState())).toEqual(
      List([
        ModelRecord({
          id: 1,
          name: 'Nuvi',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
          makeId: 1,
          make: 'Garmin',
        }),
        ModelRecord({
          id: 2,
          name: 'Whereami',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
          makeId: 2,
          make: 'Motorolla',
        }),
      ]),
    )
  })

  it('does not filter when filter is empty', () => {
    store = configureStore(
      mockState.setIn(
        ['models', 'records'],
        OrderedMap([
          [
            1,
            ModelRecord({
              id: 1,
              name: 'Nuvi',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 1,
              make: 'Garmin',
            }),
          ],
        ]),
      ),
    )
    store.dispatch(setModelListFilter(''))
    expect(computeModelList(store.getState()).size).toEqual(1)
  })

  it('clears filter on route change', () => {
    store.dispatch(setModelListFilter('foo'))
    store.dispatch({ type: LOCATION_CHANGE })
    expect(selectModelFilter(store.getState())).toEqual('')
  })

  describe('creating a model', () => {
    it('posts model information to the api', () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      store.dispatch(createModel({ name: 'New Model', makeId: 2 }))

      expect(apiClient.post).toHaveBeenCalledWith('/domain/model', {
        name: 'New Model',
        makeId: 2,
      })
    })

    it('shows a success message', async () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      await store.dispatch(createModel({ name: 'New Model', makeId: 2 }))

      expect(selectCurrentMessage(store.getState()).text).toEqual({
        ...modelsMessages.addedNewModel,
        values: { modelName: 'New Model' },
      })
    })

    it('reloads models', async () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      apiClient.get = jest.fn(() => Promise.resolve([]))
      await store.dispatch(createModel({ name: 'New Model', makeId: 2 }))

      expect(apiClient.get).toHaveBeenCalledWith('/domain/equipmentCode')
    })

    it('does not catch when reloading models fails', async () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      apiClient.get = jest.fn(() => Promise.reject(new Error()))

      try {
        await store.dispatch(createModel({ name: 'New Model', makeId: 2 }))
      } catch (e) {
        throw new Error('it should not throw')
      }
    })
  })

  describe('deactivating a model', () => {
    beforeEach(() => {
      apiClient.delete = jest.fn(() => Promise.resolve())
      apiClient.get = jest.fn(() => Promise.resolve([]))
    })

    it('sends a delete for the given model id', () => {
      store.dispatch(deactivateModel(ModelRecord({ id: 1, name: 'Nuvi' })))
      expect(apiClient.delete).toHaveBeenCalledWith('/domain/model/1')
    })

    it('shows a message that the model was deactivated', async () => {
      await store.dispatch(
        deactivateModel(ModelRecord({ id: 1, name: 'Nuvi' })),
      )

      expect(selectCurrentMessage(store.getState()).text).toEqual({
        ...modelsMessages.deactivatedModel,
        values: { modelName: 'Nuvi' },
      })
    })

    it('reloads models', async () => {
      await store.dispatch(
        deactivateModel(ModelRecord({ id: 1, name: 'Nuvi' })),
      )

      expect(apiClient.get).toHaveBeenCalledWith('/domain/equipmentCode')
    })

    it('shows an alert error when it errors and rethrows the error', async () => {
      const error = new Error()
      apiClient.delete = jest.fn(() => Promise.reject(error))
      const err = await store
        .dispatch(deactivateModel(ModelRecord({ id: 1, name: 'Nuvi' })))
        .catch(e => e)

      expect(selectHasError(store.getState())).toEqual(true)
      expect(err).toEqual(error)
    })
  })
})
