import { List, OrderedMap, fromJS } from 'immutable'

import { EquipmentCodeRecord } from 'store/equipmentCodes/equipmentCodes.records'
import { FilterRecord } from 'store/filters/filters.records'
import {
  InventoryItemRecord,
  PaginationRecord,
  SortingRecord,
} from 'store/inventoryItems/inventoryItems.records'
import { ItemStatusRecord } from 'store/itemStatuses/itemStatuses.records'
import { MakeRecord } from 'store/makes/makes.records'
import { ModelRecord } from 'store/models/models.records'
import {
  computeInstallCounts,
  selectCurrentQuery,
  selectInventoryItemById,
  selectInventoryItems,
  selectIsLoadingItems,
  selectItemPagination,
  selectItemSorting,
} from 'store/inventoryItems/inventoryItems.selectors'
import {
  createInventoryItem,
  getFilteredInventoryItems,
  getInventoryItem,
  getInventoryItems,
  getInventoryItemsForInstallLocation,
  setCurrentItemQuery,
  setItemSorting,
  updateItemStatusCode,
} from 'store/inventoryItems/inventoryItems.actions'
import { selectCurrentMessage } from 'store/messages/messages.selectors'
import apiClient from 'apiClient'
import configureStore from 'store/configureStore'
import inventoryItemsMessages from 'store/inventoryItems/inventoryItems.messages'

import jestWhen from '../../jestWhen'
import mockState from '../../../mockData/mockState'

describe('inventoryItems duck', () => {
  let store

  beforeEach(() => {
    store = configureStore()
  })

  describe('getting an inventory item', () => {
    const inventoryItem = {
      id: 1,
      installLocation: 'ABC122',
      controllingLocation: 'ABC123',
      assetTag: '123456789123',
      equipmentCodeId: 1,
      makeId: 1,
      modelId: 1,
      itemStatusId: 1,
    }

    beforeEach(() => {
      store = configureStore(
        mockState.setIn(['inventoryItems', 'records'], OrderedMap()),
      )

      apiClient.get = jest.fn(() => Promise.resolve(inventoryItem))
    })

    it('calls the api to get the item', () => {
      store.dispatch(getInventoryItem(1))
      expect(apiClient.get).toHaveBeenCalledWith('/inventoryItem/1')
    })

    it('sets isLoadingItems to true while getting the item', async () => {
      expect(selectIsLoadingItems(store.getState())).toEqual(false)
      const p = store.dispatch(getInventoryItem(1))
      expect(selectIsLoadingItems(store.getState())).toEqual(true)
      await p
      expect(selectIsLoadingItems(store.getState())).toEqual(false)
    })

    it('can get an item from the api and store it in state', async () => {
      await store.dispatch(getInventoryItem(1))

      expect(selectInventoryItemById(1)(store.getState())).toEqualImmutable(
        InventoryItemRecord({
          id: 1,
          installLocation: 'ABC122',
          controllingLocation: 'ABC123',
          assetTag: '123456789123',
          equipmentCode: EquipmentCodeRecord({
            id: 1,
            code: 'GPS',
            description: 'GPS NAVIGATION DEVICE',
          }),
          make: MakeRecord({
            id: 1,
            name: 'Garmin',
            equipmentCodeId: 1,
            equipmentCode: 'GPS',
          }),
          model: ModelRecord({
            id: 1,
            name: 'Nuvi 2015',
            equipmentCodeId: 1,
            equipmentCode: 'GPS',
            makeId: 1,
            make: 'Garmin',
          }),
          status: ItemStatusRecord({
            id: 1,
            code: 'AV',
            description: 'Available',
          }),
        }),
      )
    })

    it('stops to reload domain data if a lookup fails when populating', async () => {
      store = configureStore() // no domain data
      apiClient.get = jest.fn()
      const whenGet = jestWhen(apiClient.get)

      whenGet('/inventoryItem/1').thenResolve(inventoryItem)
      whenGet('/domain/equipmentCode').thenResolve([
        {
          id: 1,
          code: 'GPS',
          description: 'GPS NAVIGATION DEVICE',
          makes: [
            {
              id: 1,
              name: 'Garmin',
              equipmentCodeId: 1,
              models: [
                {
                  id: 1,
                  name: 'Nuvi 2015',
                  makeId: 1,
                },
              ],
            },
          ],
        },
      ])
      whenGet('/domain/itemStatus').thenResolve([
        {
          id: 1,
          code: 'AV',
          description: 'Available',
        },
      ])

      await store.dispatch(getInventoryItem(1))

      expect(apiClient.get).toHaveBeenCalledWith('/inventoryItem/1')
      expect(apiClient.get).toHaveBeenCalledWith('/domain/equipmentCode')
      expect(apiClient.get).toHaveBeenCalledWith('/domain/itemStatus')
      expect(selectInventoryItemById(1)(store.getState())).toEqualImmutable(
        InventoryItemRecord({
          id: 1,
          installLocation: 'ABC122',
          controllingLocation: 'ABC123',
          assetTag: '123456789123',
          equipmentCode: EquipmentCodeRecord({
            id: 1,
            code: 'GPS',
            description: 'GPS NAVIGATION DEVICE',
          }),
          make: MakeRecord({
            id: 1,
            name: 'Garmin',
            equipmentCodeId: 1,
            equipmentCode: 'GPS',
          }),
          model: ModelRecord({
            id: 1,
            name: 'Nuvi 2015',
            equipmentCodeId: 1,
            equipmentCode: 'GPS',
            makeId: 1,
            make: 'Garmin',
          }),
          status: ItemStatusRecord({
            id: 1,
            code: 'AV',
            description: 'Available',
          }),
        }),
      )
    })
  })

  describe('adding an inventory item', () => {
    const inventoryItem = {
      assetTag: '01',
      equipmentCode: {
        code: 'GPS',
        id: 1,
      },
      make: {
        name: 'Garmin',
        id: 2,
      },
      model: {
        name: 'Nuvi',
        id: 3,
      },
    }

    beforeEach(async () => {
      store = configureStore(
        mockState
          .setIn(['app', 'userLocation'], 'foo123')
          .setIn(['inventoryItems', 'records'], OrderedMap()),
      )
      apiClient.post = jest.fn(() =>
        Promise.resolve({
          id: 1,
          assetTag: '01',
          equipmentCodeId: 1,
          makeId: 2,
          modelId: 3,
        }),
      )
      await store.dispatch(createInventoryItem(inventoryItem))
    })

    it('posts the item to the server', () => {
      expect(apiClient.post).toHaveBeenCalledWith('/inventoryItem', {
        equipmentCodeId: inventoryItem.equipmentCode.id,
        assetTag: inventoryItem.assetTag,
        installLocation: 'foo123',
        controllingLocation: 'foo123',
        makeId: 2,
        modelId: 3,
      })
    })

    it('shows a message', () => {
      expect(selectCurrentMessage(store.getState()).text).toEqual({
        ...inventoryItemsMessages.installedInventoryItem,
        values: { equipmentCode: 'GPS' },
      })
    })

    it('adds the item to state', () => {
      expect(selectInventoryItemById(1)(store.getState())).toBeDefined()
    })
  })

  describe('getting multiple inventory items', () => {
    beforeEach(() => {
      store = configureStore(
        mockState.setIn(['inventoryItems', 'records'], OrderedMap()),
      )
      apiClient.get = jest.fn(() =>
        Promise.resolve({
          content: [
            {
              id: 1,
              installLocation: 'ABC122',
              controllingLocation: 'ABC123',
              assetTag: '123456789123',
              equipmentCodeId: 1,
              makeId: 1,
              modelId: 1,
              itemStatusId: 1,
            },
            {
              id: 2,
              assetTag: '22222222222',
              installLocation: 'ABC122',
              controllingLocation: 'ABC123',
              equipmentCodeId: 1,
              makeId: 1,
              modelId: 1,
              itemStatusId: 1,
            },
          ],
          totalElements: 25,
          totalPages: 5,
          last: false,
          first: true,
          size: 5,
          number: 1,
          sort: null,
          numberOfElements: 5,
        }),
      )
    })

    it('can get a list of inventoryItems for the current user location', () => {
      store.dispatch(getInventoryItemsForInstallLocation())

      expect(apiClient.get).toHaveBeenCalledWith('/inventoryItem', {
        params: {
          installLocation: 'ABC123',
        },
      })
    })

    it('can get a list of inventory items with a query from the server', async () => {
      await store.dispatch(getInventoryItems({ fooParam: 'foo' }))

      expect(apiClient.get).toHaveBeenCalledWith('/inventoryItem', {
        params: {
          fooParam: 'foo',
        },
      })
      expect(selectInventoryItems(store.getState())).toEqualImmutable(
        OrderedMap([
          [
            1,
            InventoryItemRecord({
              id: 1,
              installLocation: 'ABC122',
              controllingLocation: 'ABC123',
              assetTag: '123456789123',
              equipmentCode: EquipmentCodeRecord({
                id: 1,
                code: 'GPS',
                description: 'GPS NAVIGATION DEVICE',
              }),
              make: MakeRecord({
                id: 1,
                name: 'Garmin',
                equipmentCodeId: 1,
                equipmentCode: 'GPS',
              }),
              model: ModelRecord({
                id: 1,
                name: 'Nuvi 2015',
                equipmentCodeId: 1,
                equipmentCode: 'GPS',
                makeId: 1,
                make: 'Garmin',
              }),
              status: ItemStatusRecord({
                id: 1,
                code: 'AV',
                description: 'Available',
              }),
            }),
          ],
          [
            2,
            InventoryItemRecord({
              id: 2,
              installLocation: 'ABC122',
              controllingLocation: 'ABC123',
              assetTag: '22222222222',
              equipmentCode: EquipmentCodeRecord({
                id: 1,
                code: 'GPS',
                description: 'GPS NAVIGATION DEVICE',
              }),
              make: MakeRecord({
                id: 1,
                name: 'Garmin',
                equipmentCodeId: 1,
                equipmentCode: 'GPS',
              }),
              model: ModelRecord({
                id: 1,
                name: 'Nuvi 2015',
                equipmentCodeId: 1,
                equipmentCode: 'GPS',
                makeId: 1,
                make: 'Garmin',
              }),
              status: ItemStatusRecord({
                id: 1,
                code: 'AV',
                description: 'Available',
              }),
            }),
          ],
        ]),
      )
    })

    it('sets isLoadingItems to true while getting a list of items', async () => {
      expect(selectIsLoadingItems(store.getState())).toEqual(false)
      const p = store.dispatch(getInventoryItems())
      expect(selectIsLoadingItems(store.getState())).toEqual(true)
      await p
      expect(selectIsLoadingItems(store.getState())).toEqual(false)
    })

    it('sets the current query when getting a list of items', async () => {
      await store.dispatch(getInventoryItems({ fooParam: 'foo' }))

      expect(apiClient.get).toHaveBeenCalledWith('/inventoryItem', {
        params: {
          fooParam: 'foo',
        },
      })
      expect(selectCurrentQuery(store.getState())).toEqualImmutable(
        List([1, 2]),
      )
    })

    it('sets the pagination state when getting a list of items', async () => {
      await store.dispatch(getInventoryItems({ fooParam: 'foo' }))

      expect(selectItemPagination(store.getState())).toEqualImmutable(
        PaginationRecord({
          totalElements: 25,
          totalPages: 5,
          last: false,
          first: true,
          number: 1,
          sort: null,
          currentIndex: 5,
          numberOfElements: 5,
        }),
      )
    })

    it('can get a list of codes/makes/models and quantities of each', () => {
      store = configureStore(
        mockState.setIn(
          ['inventoryItems', 'records'],
          OrderedMap([
            [
              1,
              InventoryItemRecord({
                id: 1,
                equipmentCode: EquipmentCodeRecord({
                  code: 'GPS',
                }),
                make: MakeRecord({
                  name: 'Garmin',
                }),
                model: ModelRecord({
                  id: 1,
                  name: 'Nuvi 2015',
                }),
              }),
            ],
            [
              2,
              InventoryItemRecord({
                id: 2,
                equipmentCode: EquipmentCodeRecord({
                  code: 'GPS',
                }),
                make: MakeRecord({
                  name: 'Garmin',
                }),
                model: ModelRecord({
                  id: 1,
                  name: 'Nuvi 2015',
                }),
              }),
            ],
            [
              3,
              InventoryItemRecord({
                id: 3,
                equipmentCode: EquipmentCodeRecord({
                  code: 'GPS',
                }),
                make: MakeRecord({
                  name: 'Motorolla',
                }),
                model: ModelRecord({
                  id: 3,
                  name: 'Whereami',
                }),
              }),
            ],
            [
              4,
              InventoryItemRecord({
                id: 4,
                equipmentCode: EquipmentCodeRecord({
                  code: 'TPD',
                }),
                make: MakeRecord({
                  name: 'EZPass',
                }),
                model: ModelRecord({
                  id: 4,
                  name: 'EZPass',
                }),
              }),
            ],
            [
              5,
              InventoryItemRecord({
                id: 5,
                equipmentCode: EquipmentCodeRecord({
                  code: 'TPD',
                }),
                make: MakeRecord({
                  name: 'EZPass',
                }),
                model: ModelRecord({
                  id: 4,
                  name: 'EZPass',
                }),
              }),
            ],
          ]),
        ),
      )

      store.dispatch(setCurrentItemQuery([1, 2, 3, 4]))

      expect(computeInstallCounts(store.getState())).toEqualImmutable(
        fromJS([
          { key: 1, count: 2, code: 'GPS', make: 'Garmin', model: 'Nuvi 2015' },
          {
            key: 3,
            count: 1,
            code: 'GPS',
            make: 'Motorolla',
            model: 'Whereami',
          },
          { key: 4, count: 1, code: 'TPD', make: 'EZPass', model: 'EZPass' },
        ]),
      )
    })
  })

  describe('updating item status', () => {
    beforeEach(() => {
      store = configureStore(
        mockState.setIn(['itemHistory', 'pagination', 'number'], 5),
      )
      apiClient.get = jest.fn(() => ({
        content: [],
      }))
      apiClient.put = jest.fn().mockReturnValue(
        Promise.resolve({
          id: 1,
          assetTag: '123456789123',
          equipmentCodeId: 1,
          installLocation: 'ABC122',
          controllingLocation: 'ABC123',
          createTimestamp: '2018-01-01T00:00:00Z',
          makeId: 1,
          modelId: 1,
          itemStatusId: 2,
        }),
      )
    })

    it('calls the api to update the status', () => {
      store.dispatch(updateItemStatusCode(1, { id: 2 }))
      expect(apiClient.put).toBeCalledWith('/inventoryItem/1/itemStatusId', 2)
    })

    it('saves the updated item to state', async () => {
      await store.dispatch(updateItemStatusCode(1, { id: 2 }))
      expect(
        selectInventoryItemById(1)(store.getState()).status,
      ).toEqualImmutable(
        ItemStatusRecord({
          id: 2,
          code: 'OR',
          description: 'On Rent',
        }),
      )
    })

    it('informs the user of status code updates', async () => {
      await store.dispatch(updateItemStatusCode(1, { id: 2, code: 'OR' }))
      expect(selectCurrentMessage(store.getState()).text).toEqual({
        ...inventoryItemsMessages.updatedStatusCode,
        values: { statusCode: 'OR' },
      })
    })

    it('reloads itemHistory for the current page', async () => {
      await store.dispatch(updateItemStatusCode(1, { id: 2 }))

      expect(apiClient.get).toHaveBeenCalledWith(
        '/inventoryItem/1/itemHistory',
        { params: { page: 5 } },
      )
    })
  })

  describe('getting filtered inventory items', () => {
    beforeEach(() => {
      store = configureStore(
        mockState
          .setIn(['app', 'userLocation'], 'ASY123')
          .setIn(['inventoryItems', 'records'], OrderedMap())
          .setIn(
            ['filters', 'records'],
            List([
              FilterRecord({
                field: 'equipmentCodeId',
                operator: 'is',
                value: 1,
              }),
            ]),
          ),
      )
      apiClient.post = jest.fn(() =>
        Promise.resolve({
          content: [
            {
              id: 1,
              installLocation: 'ABC122',
              controllingLocation: 'ABC123',
              assetTag: '123456789123',
              equipmentCodeId: 1,
              makeId: 1,
              modelId: 1,
              itemStatusId: 1,
            },
          ],
          totalElements: 25,
          totalPages: 5,
          last: false,
          first: true,
          size: 5,
          number: 1,
          sort: null,
          numberOfElements: 5,
        }),
      )
    })

    it('calls the api with merged filters and globally set controllingLocation', async () => {
      await store.dispatch(getFilteredInventoryItems())
      expect(apiClient.post.mock.calls[0][0]).toEqual('/inventoryItemSearch')
      expect(apiClient.post.mock.calls[0][1]).toEqual(
        expect.objectContaining({
          filters: [
            { field: 'equipmentCodeId', operator: 'is', values: [1] },
            {
              field: 'controllingLocation',
              operator: 'is',
              values: ['ASY123'],
            },
          ],
        }),
      )
    })

    it('can handle a passed in query', async () => {
      await store.dispatch(getFilteredInventoryItems({ page: 1 }))
      expect(apiClient.post.mock.calls[0][2].params).toEqual(
        expect.objectContaining({ page: 1 }),
      )
    })

    it('sends the configured sorting', async () => {
      store = configureStore(
        mockState.setIn(
          ['inventoryItems', 'sorting'],
          SortingRecord({ column: 'foo', direction: 'asc' }),
        ),
      )
      await store.dispatch(getFilteredInventoryItems({ page: 1 }))
      expect(apiClient.post.mock.calls[0][2].params).toEqual(
        expect.objectContaining({ sort: 'foo,asc' }),
      )
    })

    it('adds the items to state', async () => {
      await store.dispatch(getFilteredInventoryItems())
      expect(selectInventoryItems(store.getState()).get(1).id).toEqual(1)
    })

    it('sets isLoadingItems to true while getting items', async () => {
      expect(selectIsLoadingItems(store.getState())).toEqual(false)
      const p = store.dispatch(getFilteredInventoryItems())
      expect(selectIsLoadingItems(store.getState())).toEqual(true)
      await p
      expect(selectIsLoadingItems(store.getState())).toEqual(false)
    })

    it('sets the current query', async () => {
      await store.dispatch(getFilteredInventoryItems())

      expect(selectCurrentQuery(store.getState())).toEqualImmutable(List([1]))
    })

    it('sets the pagination state', async () => {
      await store.dispatch(getFilteredInventoryItems())

      expect(selectItemPagination(store.getState())).toEqualImmutable(
        PaginationRecord({
          totalElements: 25,
          totalPages: 5,
          last: false,
          first: true,
          number: 1,
          sort: null,
          currentIndex: 5,
          numberOfElements: 5,
        }),
      )
    })
  })

  it('can set sorting', () => {
    store.dispatch(setItemSorting({ column: 'foo', direction: 'asc' }))
    expect(selectItemSorting(store.getState())).toEqual(
      SortingRecord({ column: 'foo', direction: 'asc' }),
    )
  })
})
