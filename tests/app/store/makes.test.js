import { LOCATION_CHANGE } from 'react-router-redux'
import { List, OrderedMap } from 'immutable'

import { MakeRecord } from 'store/makes/makes.records'
import {
  computeMakeList,
  selectMakeById,
  selectMakeFilter,
} from 'store/makes/makes.selectors'
import { createMake, setMakeListFilter } from 'store/makes/makes.actions'
import { selectCurrentMessage } from 'store/messages/messages.selectors'
import apiClient from 'apiClient'
import configureStore from 'store/configureStore'
import makesMessages from 'store/makes/makes.messages'

import mockState from '../../../mockData/mockState'

describe('makes duck', () => {
  let store

  beforeEach(() => {
    store = configureStore(mockState.setIn(['makes', 'records'], List()))
    apiClient.get = jest.fn(() =>
      Promise.resolve([
        { id: 1, equipmentCodeId: 1, name: 'Garmin' },
        { id: 2, equipmentCodeId: 2, name: 'EZPass' },
      ]),
    )
  })

  it('can select a make by id', () => {
    store = configureStore(
      mockState.setIn(
        ['makes', 'records'],
        OrderedMap([
          [
            1,
            MakeRecord({
              id: 1,
              name: 'Nuvi',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 1,
              make: 'Garmin',
            }),
          ],
          [
            2,
            MakeRecord({
              id: 2,
              name: 'EZPass',
              equipmentCodeId: 2,
              equipmentCode: 'TPD',
              makeId: 2,
              make: 'EZPass',
            }),
          ],
        ]),
      ),
    )

    expect(selectMakeById(1)(store.getState())).toEqualImmutable(
      MakeRecord({
        id: 1,
        name: 'Nuvi',
        equipmentCodeId: 1,
        equipmentCode: 'GPS',
        makeId: 1,
        make: 'Garmin',
      }),
    )
  })

  it('can set a filter', () => {
    store.dispatch(setMakeListFilter('GPS'))
    expect(selectMakeFilter(store.getState())).toEqual('GPS')
  })

  it('should return a sorted make list', () => {
    store = configureStore(
      mockState.setIn(
        ['makes', 'records'],
        OrderedMap([
          [
            1,
            MakeRecord({
              id: 1,
              name: 'Garmin',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
            }),
          ],
          [
            2,
            MakeRecord({
              id: 2,
              name: 'Motorolla',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
            }),
          ],
          [
            3,
            MakeRecord({
              id: 3,
              name: 'EZPass',
              equipmentCodeId: 2,
              equipmentCode: 'TPD',
            }),
          ],
        ]),
      ),
    )
    const expected = List([
      MakeRecord({
        id: 3,
        name: 'EZPass',
        equipmentCodeId: 2,
        equipmentCode: 'TPD',
      }),
      MakeRecord({
        id: 1,
        name: 'Garmin',
        equipmentCodeId: 1,
        equipmentCode: 'GPS',
      }),
      MakeRecord({
        id: 2,
        name: 'Motorolla',
        equipmentCodeId: 1,
        equipmentCode: 'GPS',
      }),
    ])
    expect(computeMakeList(store.getState())).toEqualImmutable(expected)
  })

  it('can fuzzy filter make list', () => {
    store = configureStore(
      mockState.setIn(
        ['makes', 'records'],
        OrderedMap([
          [
            1,
            MakeRecord({
              id: 1,
              name: 'Garmin',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
            }),
          ],
          [
            2,
            MakeRecord({
              id: 2,
              name: 'Motorolla',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
            }),
          ],
          [
            3,
            MakeRecord({
              id: 3,
              name: 'EZPass',
              equipmentCodeId: 2,
              equipmentCode: 'TPD',
            }),
          ],
        ]),
      ),
    )
    store.dispatch(setMakeListFilter('GPS'))
    expect(computeMakeList(store.getState())).toEqual(
      List([
        MakeRecord({
          id: 1,
          name: 'Garmin',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
        }),
        MakeRecord({
          id: 2,
          name: 'Motorolla',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
        }),
      ]),
    )
    store.dispatch(setMakeListFilter('GPS Moto'))
    expect(computeMakeList(store.getState())).toEqual(
      List([
        MakeRecord({
          id: 2,
          name: 'Motorolla',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
        }),
      ]),
    )
  })

  it('should trim whitespace when filtering', () => {
    store = configureStore(
      mockState.setIn(
        ['makes', 'records'],
        OrderedMap([
          [
            1,
            MakeRecord({
              id: 1,
              name: 'Nuvi',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 1,
              make: 'Garmin',
            }),
          ],
          [
            2,
            MakeRecord({
              id: 2,
              name: 'Whereami',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 2,
              make: 'Motorolla',
            }),
          ],
          [
            3,
            MakeRecord({
              id: 3,
              name: 'EZPass',
              equipmentCodeId: 2,
              equipmentCode: 'TPD',
              makeId: 3,
              make: 'EZPass',
            }),
          ],
        ]),
      ),
    )
    store.dispatch(setMakeListFilter('  GPS  '))
    expect(computeMakeList(store.getState())).toEqual(
      List([
        MakeRecord({
          id: 1,
          name: 'Nuvi',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
          makeId: 1,
          make: 'Garmin',
        }),
        MakeRecord({
          id: 2,
          name: 'Whereami',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
          makeId: 2,
          make: 'Motorolla',
        }),
      ]),
    )
  })

  it('does not filter when filter is empty', () => {
    store = configureStore(
      mockState.setIn(
        ['makes', 'records'],
        OrderedMap([
          [
            1,
            MakeRecord({
              id: 1,
              name: 'Nuvi',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 1,
              make: 'Garmin',
            }),
          ],
        ]),
      ),
    )
    store.dispatch(setMakeListFilter(''))
    expect(computeMakeList(store.getState()).size).toEqual(1)
  })

  it('clears filter on route change', () => {
    store.dispatch(setMakeListFilter('foo'))
    store.dispatch({ type: LOCATION_CHANGE })
    expect(selectMakeFilter(store.getState())).toEqual('')
  })

  describe('creating a make', () => {
    it('posts make information to the api', () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      store.dispatch(createMake({ name: 'New Make', equipmentCodeId: 2 }))

      expect(apiClient.post).toHaveBeenCalledWith('/domain/make', {
        name: 'New Make',
        equipmentCodeId: 2,
      })
    })

    it('shows a success message', async () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      await store.dispatch(createMake({ name: 'New Make', makeId: 2 }))

      expect(selectCurrentMessage(store.getState()).text).toEqual({
        ...makesMessages.addedNewMake,
        values: { makeName: 'New Make' },
      })
    })

    it('reloads makes', async () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      apiClient.get = jest.fn(() => Promise.resolve([]))
      await store.dispatch(createMake({ name: 'New Make', makeId: 2 }))

      expect(apiClient.get).toHaveBeenCalledWith('/domain/equipmentCode')
    })

    it('does not catch when reloading makes fails', async () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      apiClient.get = jest.fn(() => Promise.reject(new Error()))

      try {
        await store.dispatch(createMake({ name: 'New Make', makeId: 2 }))
      } catch (e) {
        throw new Error('it should not throw')
      }
    })
  })
})
