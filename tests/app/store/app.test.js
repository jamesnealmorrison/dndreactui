import { LOCATION_CHANGE } from 'react-router-redux'

import { DEFAULT_LOCALE } from 'store/app/app.records'
import { PHONE, TABLET, WIDESCREEN } from 'helpers/screenForm'
import {
  changeLocale,
  closeNav,
  login,
  logout,
  openNav,
  screenFormChange,
  setIsEditingUserLocation,
  setUserLocation,
} from 'store/app/app.actions'
import {
  selectIsEditingUserLocation,
  selectIsLoggedIn,
  selectIsNavOpen,
  selectLocale,
  selectScreenForm,
  selectUserLocation,
} from 'store/app/app.selectors'
import apiClient from 'apiClient'
import configureStore from 'store/configureStore'

import StorageShim from '../../StorageShim'
import jestWhen from '../../jestWhen'
import mockState from '../../../mockData/mockState'

describe('App Reducer', () => {
  let store
  beforeEach(() => {
    window.localStorage = new StorageShim()
    store = configureStore(mockState.setIn(['app', 'userLocation'], 'ASY234'))
  })

  it('allows the global nav to be opened and closed', () => {
    store.dispatch(openNav(true))
    expect(selectIsNavOpen(store.getState())).toBe(true)

    store.dispatch(closeNav(false))
    expect(selectIsNavOpen(store.getState())).toBe(false)
  })
  it('tracks screenform changes', () => {
    store.dispatch(screenFormChange(PHONE))
    expect(selectScreenForm(store.getState())).toEqual(PHONE)
  })

  it('closes nav on route change when on mobile', () => {
    store.dispatch(openNav())
    store.dispatch(screenFormChange(PHONE))
    store.dispatch({ type: LOCATION_CHANGE })
    expect(selectIsNavOpen(store.getState())).toBe(false)
  })

  it('does not close nav on route change when on widescreen', () => {
    store.dispatch(openNav())
    store.dispatch(screenFormChange(WIDESCREEN))
    store.dispatch({ type: LOCATION_CHANGE })
    expect(selectIsNavOpen(store.getState())).toBe(true)
  })

  it('opens nav when screen switches to widescreen', () => {
    store.dispatch(closeNav())
    store.dispatch(screenFormChange(WIDESCREEN))
    expect(selectIsNavOpen(store.getState())).toBe(true)
  })

  it('closes nav when screen switches away from widescreen', () => {
    store.dispatch(openNav())
    store.dispatch(screenFormChange(TABLET))
    expect(selectIsNavOpen(store.getState())).toBe(false)
  })

  it('sets the default locale', () => {
    expect(selectLocale(store.getState())).toEqual(DEFAULT_LOCALE)
  })

  it('lets you change the locale', () => {
    store.dispatch(changeLocale('de'))
    expect(selectLocale(store.getState())).toEqual('de')
  })

  it('lets you change user location with valid location', async () => {
    apiClient.get = jest.fn()
    const promise = Promise.resolve()
    jestWhen(apiClient.get)('/domain/location/ASY124').thenReturn(promise)
    store.dispatch(setUserLocation('ASY124'))
    expect(apiClient.get).toHaveBeenCalledWith('/domain/location/ASY124')
    expect(selectUserLocation(store.getState())).not.toEqual('ASY124')
    await promise
    expect(selectUserLocation(store.getState())).toEqual('ASY124')
  })

  it('does not let you change user location with invalid location', async () => {
    apiClient.get = jest.fn()
    const thrownError = new Error()
    jestWhen(apiClient.get)('/domain/location/ASY123').thenReject(thrownError)
    const e = await store.dispatch(setUserLocation('ASY123')).catch(e => e)
    expect(e).toBe(thrownError)
    expect(apiClient.get).toHaveBeenCalledWith('/domain/location/ASY123')
    expect(selectUserLocation(store.getState())).toEqual('ASY234')
  })

  it('lets a user login', async () => {
    expect(selectIsLoggedIn(store.getState())).toEqual(false)
    await store.dispatch(login({ eid: 'eid', password: 'password' }))
    expect(selectIsLoggedIn(store.getState())).toEqual(true)
  })

  it('stores apiToken in localStorage', async () => {
    expect(window.localStorage.getItem('apiToken')).toEqual(null)
    await store.dispatch(login({ eid: 'eid', password: 'password' }))
    expect(window.localStorage.getItem('apiToken')).toEqual('token')
  })

  it('lets a user logout', () => {
    store = configureStore(mockState.setIn(['app', 'apiToken'], 'token'))
    expect(selectIsLoggedIn(store.getState())).toEqual(true)
    store.dispatch(logout())
    expect(selectIsLoggedIn(store.getState())).toEqual(false)
  })

  it('clears localStorage apiToken on logout', () => {
    window.localStorage.setItem('apiToken', 'token')
    store.dispatch(logout())
    expect(window.localStorage.getItem('apiToken')).toEqual(null)
  })

  it('can set editing state for user location', () => {
    store.dispatch(setIsEditingUserLocation(true))
    expect(selectIsEditingUserLocation(store.getState())).toEqual(true)
    store.dispatch(setIsEditingUserLocation(false))
    expect(selectIsEditingUserLocation(store.getState())).toEqual(false)
  })
})
