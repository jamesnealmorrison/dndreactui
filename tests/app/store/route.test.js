import { LOCATION_CHANGE } from 'react-router-redux'

import { selectLocation } from 'store/route/route.selectors'
import configureStore from 'store/configureStore'

let store
let state

describe('route duck', () => {
  beforeEach(() => {
    store = configureStore()
  })

  it('tracks route changes', async () => {
    await store.dispatch({
      type: LOCATION_CHANGE,
      payload: 'new location',
    })
    state = store.getState()
    const result = selectLocation(state)

    expect(result).toEqual('new location')
  })
})
