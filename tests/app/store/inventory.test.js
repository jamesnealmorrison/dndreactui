import { List } from 'immutable'

import { InventoryRecord } from 'store/inventory/inventory.records'
import { PUSH_INVENTORY } from 'store/inventory/inventory.types'
import { removeInventory } from 'store/inventory/inventory.actions'
import configureStore from 'store/configureStore'
import inventory from 'store/inventory/inventory.reducer'

describe('inventory', () => {
  it('should be able to do an animated clear of an item from the list of inventoried items', async () => {
    jest.useFakeTimers()
    const store = configureStore({
      inventory: List([
        InventoryRecord({ itemId: 2 }),
        InventoryRecord({ itemId: 1 }),
        InventoryRecord({ itemId: 2 }),
      ]),
    })

    store.dispatch(removeInventory(2))

    expect(store.getState().inventory).toEqualImmutable(
      List([
        InventoryRecord({ itemId: 2 }),
        InventoryRecord({ itemId: 1 }),
        InventoryRecord({ itemId: 2, willLeave: true }),
      ]),
    )

    jest.runTimersToTime(300)

    expect(store.getState().inventory).toEqualImmutable(
      List([InventoryRecord({ itemId: 2 }), InventoryRecord({ itemId: 1 })]),
    )
  })

  it('should add the item to the list of inventoried inventory items', () => {
    const action = {
      type: PUSH_INVENTORY,
      payload: {
        itemId: 1,
      },
    }
    expect(inventory(undefined, action)).toEqualImmutable(
      List([InventoryRecord({ itemId: 1 })]),
    )
  })
})
