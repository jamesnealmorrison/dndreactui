import { LOCATION_CHANGE } from 'react-router-redux'
import { List, OrderedMap } from 'immutable'
import moment from 'moment'

import { FilterRecord, filterOperators } from 'store/filters/filters.records'
import {
  addFilter,
  applyFilters,
  clearFilters,
  editFilter,
  removeFilter,
  revertFilters,
  showFilters,
  hideFilters,
} from 'store/filters/filters.actions'
import {
  computeMergedFilters,
  selectFilters,
  selectFiltersCount,
  selectIsShowingFilters,
  selectPendingFilters,
} from 'store/filters/filters.selectors'
import configureStore from 'store/configureStore'

import mockState from '../../../mockData/mockState'

describe('filters duck', () => {
  let store

  beforeEach(() => {
    store = configureStore()
  })

  it('can show and hide filters', () => {
    store.dispatch(showFilters())
    expect(selectIsShowingFilters(store.getState())).toEqualImmutable(true)
    store.dispatch(hideFilters())
    expect(selectIsShowingFilters(store.getState())).toEqualImmutable(false)
  })

  it('can add blank filters', () => {
    store.dispatch(addFilter())
    expect(selectPendingFilters(store.getState())).toEqualImmutable(
      List([FilterRecord()]),
    )
  })

  it('can update filters', () => {
    store = configureStore(
      mockState.setIn(
        ['filters', 'pending'],
        List([
          FilterRecord({ field: 'assetTag', operator: 'is', value: '420' }),
          FilterRecord({
            field: 'assetTag',
            operator: 'is',
            value: 'boring',
          }),
        ]),
      ),
    )
    store.dispatch(
      editFilter(
        1,
        FilterRecord({
          field: 'assetTag',
          operator: 'is not',
          value: 'boring',
        }),
      ),
    )
    expect(selectPendingFilters(store.getState())).toEqualImmutable(
      List([
        FilterRecord({ field: 'assetTag', operator: 'is', value: '420' }),
        FilterRecord({
          field: 'assetTag',
          operator: 'is not',
          value: 'boring',
        }),
      ]),
    )
  })
  it('can remove filters by index', () => {
    store = configureStore(
      mockState.setIn(
        ['filters', 'pending'],
        List([
          FilterRecord({ field: 'assetTag', operator: 'is', value: '420' }),
          FilterRecord({
            field: 'assetTag',
            operator: 'is not',
            value: 'boring',
          }),
        ]),
      ),
    )
    store.dispatch(removeFilter(0))
    expect(selectPendingFilters(store.getState())).toEqualImmutable(
      List([
        FilterRecord({
          field: 'assetTag',
          operator: 'is not',
          value: 'boring',
        }),
      ]),
    )
  })
  it('can clear filters', () => {
    store = configureStore(
      mockState.setIn(
        ['filters', 'pending'],
        List([
          FilterRecord({ field: 'assetTag', operator: 'is', value: '420' }),
          FilterRecord({
            field: 'assetTag',
            operator: 'is not',
            value: 'boring',
          }),
        ]),
      ),
    )
    store.dispatch(clearFilters())
    expect(selectPendingFilters(store.getState())).toEqualImmutable(List([]))
  })

  it('can apply filters', () => {
    store = configureStore(
      mockState.setIn(
        ['filters', 'pending'],
        List([
          FilterRecord({ field: 'assetTag', operator: 'is', value: '420' }),
          FilterRecord({
            field: 'assetTag',
            operator: 'is not',
            value: 'boring',
          }),
        ]),
      ),
    )
    store.dispatch(applyFilters())
    expect(selectFilters(store.getState())).toEqualImmutable(
      List([
        FilterRecord({ field: 'assetTag', operator: 'is', value: '420' }),
        FilterRecord({
          field: 'assetTag',
          operator: 'is not',
          value: 'boring',
        }),
      ]),
    )
  })

  it('removes incomplete filters when they are applied', () => {
    store = configureStore(
      mockState.setIn(
        ['filters', 'pending'],
        List([
          FilterRecord({ field: 'assetTag', operator: 'is', value: 'good' }),
          FilterRecord({ field: 'assetTag', operator: 'is', value: '     ' }),
          FilterRecord({
            field: 'assetTag',
            operator: null,
            value: 'boring',
          }),
        ]),
      ),
    )
    store.dispatch(applyFilters())
    expect(selectFilters(store.getState())).toEqualImmutable(
      List([
        FilterRecord({ field: 'assetTag', operator: 'is', value: 'good' }),
      ]),
    )
  })

  it('can abort changes to inventory item filters', () => {
    store = configureStore(
      mockState
        .setIn(
          ['filters', 'pending'],
          List([
            FilterRecord({ field: 'assetTag', operator: 'is', value: '420' }),
          ]),
        )
        .setIn(
          ['filters', 'records'],
          List([
            FilterRecord({ field: 'assetTag', operator: 'is', value: '420' }),
            FilterRecord({
              field: 'assetTag',
              operator: 'is not',
              value: 'boring',
            }),
          ]),
        ),
    )
    store.dispatch(revertFilters())
    expect(selectFilters(store.getState())).toEqualImmutable(
      List([
        FilterRecord({ field: 'assetTag', operator: 'is', value: '420' }),
        FilterRecord({
          field: 'assetTag',
          operator: 'is not',
          value: 'boring',
        }),
      ]),
    )
    expect(selectPendingFilters(store.getState())).toEqualImmutable(
      List([
        FilterRecord({ field: 'assetTag', operator: 'is', value: '420' }),
        FilterRecord({
          field: 'assetTag',
          operator: 'is not',
          value: 'boring',
        }),
      ]),
    )
  })

  it('can select active filter count', () => {
    store = configureStore(
      mockState.setIn(
        ['filters', 'records'],
        List([FilterRecord(), FilterRecord()]),
      ),
    )
    expect(selectFiltersCount(store.getState())).toEqual(2)
  })

  it('hides filters on route change', () => {
    store = configureStore(
      mockState.setIn(['filters', 'isShowingFilters'], true),
    )

    store.dispatch({ type: LOCATION_CHANGE })

    expect(selectIsShowingFilters(store.getState())).toEqual(false)
  })

  it('can compute merged filters', () => {
    store = configureStore(
      mockState
        .setIn(['app', 'userLocation'], 'ASY123')
        .setIn(['inventoryItems', 'records'], OrderedMap())
        .setIn(
          ['filters', 'records'],
          List([
            FilterRecord({
              field: 'equipmentCodeId',
              operator: 'is',
              value: 1,
            }),
            FilterRecord({
              field: 'equipmentCodeId',
              operator: 'is not',
              value: 2,
            }),
            FilterRecord({
              field: 'equipmentCodeId',
              operator: 'is not',
              value: 3,
            }),
          ]),
        ),
    )

    expect(computeMergedFilters(store.getState())).toEqual([
      { field: 'equipmentCodeId', operator: 'is', values: [1] },
      { field: 'equipmentCodeId', operator: 'is not', values: [2, 3] },
    ])
  })

  it('breaks up `on` for dates', () => {
    store = configureStore(
      mockState
        .setIn(['app', 'userLocation'], 'ASY123')
        .setIn(['inventoryItems', 'records'], OrderedMap())
        .setIn(
          ['filters', 'records'],
          List([
            FilterRecord({
              field: 'createTimestamp',
              operator: filterOperators.on.value,
              value: '2018-01-01',
            }),
          ]),
        ),
    )

    expect(computeMergedFilters(store.getState())).toEqual([
      {
        field: 'createTimestamp',
        operator: filterOperators.between.value,
        values: [
          `${moment('2018-01-01')
            .startOf('day')
            .toISOString()}/${moment('2018-01-01')
            .endOf('day')
            .toISOString()}`,
        ],
      },
    ])
  })

  it('sets after dates to end of day', () => {
    store = configureStore(
      mockState
        .setIn(['app', 'userLocation'], 'ASY123')
        .setIn(['inventoryItems', 'records'], OrderedMap())
        .setIn(
          ['filters', 'records'],
          List([
            FilterRecord({
              field: 'createTimestamp',
              operator: filterOperators.after.value,
              value: '2018-01-01',
            }),
          ]),
        ),
    )

    expect(computeMergedFilters(store.getState())).toEqual([
      {
        field: 'createTimestamp',
        operator: filterOperators.after.value,
        values: [
          `${moment('2018-01-01')
            .endOf('day')
            .toISOString()}`,
        ],
      },
    ])
  })

  it('sets before dates to start of day', () => {
    store = configureStore(
      mockState
        .setIn(['app', 'userLocation'], 'ASY123')
        .setIn(['inventoryItems', 'records'], OrderedMap())
        .setIn(
          ['filters', 'records'],
          List([
            FilterRecord({
              field: 'createTimestamp',
              operator: filterOperators.before.value,
              value: '2018-01-01',
            }),
          ]),
        ),
    )

    expect(computeMergedFilters(store.getState())).toEqual([
      {
        field: 'createTimestamp',
        operator: filterOperators.before.value,
        values: [
          `${moment('2018-01-01')
            .startOf('day')
            .toISOString()}`,
        ],
      },
    ])
  })
})
