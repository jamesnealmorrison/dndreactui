import { OrderedMap } from 'immutable'

import { ItemStatusRecord } from 'store/itemStatuses/itemStatuses.records'
import {
  computeItemsStatuses,
  selectItemStatusById,
  selectItemStatuses,
} from 'store/itemStatuses/itemStatuses.selectors'
import { getItemStatuses } from 'store/itemStatuses/itemStatuses.actions'
import apiClient from 'apiClient'
import configureStore from 'store/configureStore'

import mockState from '../../../mockData/mockState'

describe('item statuses duck', () => {
  let store

  beforeEach(() => {
    store = configureStore(
      mockState.setIn(['itemStatuses', 'records'], OrderedMap()),
    )
    apiClient.get = jest.fn(() =>
      Promise.resolve([
        {
          id: 1,
          code: 'AV',
          description: 'Available',
        },
        {
          id: 2,
          code: 'OR',
          description: 'On Rent',
        },
        {
          id: 3,
          code: 'RC',
          description: 'Recalled',
        },
      ]),
    )
  })

  it('can get a list of item statuses from the server', () => {
    store.dispatch(getItemStatuses())
    expect(apiClient.get).toHaveBeenCalledWith('/domain/itemStatus')
  })

  it('can save itemStatuses in state', async () => {
    store = configureStore()

    await store.dispatch(getItemStatuses())
    expect(selectItemStatuses(store.getState())).toEqualImmutable(
      OrderedMap([
        [
          1,
          ItemStatusRecord({
            id: 1,
            code: 'AV',
            description: 'Available',
          }),
        ],
        [
          2,
          ItemStatusRecord({
            id: 2,
            code: 'OR',
            description: 'On Rent',
          }),
        ],
        [
          3,
          ItemStatusRecord({
            id: 3,
            code: 'RC',
            description: 'Recalled',
          }),
        ],
      ]),
    )
  })

  it('sorts itemStatuses alphabetically by code', async () => {
    store = configureStore(
      mockState.setIn(
        ['itemStatuses', 'records'],
        OrderedMap([
          [
            1,
            ItemStatusRecord({
              id: 1,
              code: 'OR',
              description: 'On Rent',
            }),
          ],
          [
            2,
            ItemStatusRecord({
              id: 2,
              code: 'RC',
              description: 'Recalled',
            }),
          ],
          [
            3,
            ItemStatusRecord({
              id: 3,
              code: 'AV',
              description: 'Available',
            }),
          ],
        ]),
      ),
    )
    const result = computeItemsStatuses(store.getState())
    expect(result.get(0).code).toEqual('AV')
    expect(result.get(1).code).toEqual('OR')
    expect(result.get(2).code).toEqual('RC')
  })

  it('allows you to select an item status by id', () => {
    store = configureStore(
      mockState.setIn(
        ['itemStatuses', 'records'],
        OrderedMap([
          [
            1,
            ItemStatusRecord({
              id: 1,
              code: 'OR',
              description: 'On Rent',
            }),
          ],
          [
            2,
            ItemStatusRecord({
              id: 2,
              code: 'RC',
              description: 'Recalled',
            }),
          ],
          [
            3,
            ItemStatusRecord({
              id: 3,
              code: 'AV',
              description: 'Available',
            }),
          ],
        ]),
      ),
    )
    expect(selectItemStatusById(1)(store.getState())).toEqual(
      ItemStatusRecord({
        id: 1,
        code: 'OR',
        description: 'On Rent',
      }),
    )
  })
})
