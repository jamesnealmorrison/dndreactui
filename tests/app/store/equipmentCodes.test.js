import { LOCATION_CHANGE } from 'react-router-redux'
import { List, OrderedMap } from 'immutable'

import { EquipmentCodeRecord } from 'store/equipmentCodes/equipmentCodes.records'
import { MakeRecord } from 'store/makes/makes.records'
import { ModelRecord } from 'store/models/models.records'
// import {
//   computeEquipmentCodeList,
//   selectEquipmentCodeFilter,
//   selectEquipmentCodes,
//   selectIsLoadingEquipmentCodes,
// } from 'store/equipmentCodes/equipmentCodes.selectors'
// import {
//   createEquipmentCode,
//   deactivateEquipmentCode,
//   getEquipmentCodes,
//   setEquipmentCodeListFilter,
// } from 'store/equipmentCodes/equipmentCodes.actions'
import { selectCurrentMessage } from 'store/messages/messages.selectors'
import { selectHasError } from 'store/errors/errors.selectors'
import { selectMakes } from 'store/makes/makes.selectors'
import { selectModels } from 'store/models/models.selectors'
import apiClient from 'apiClient'
import configureStore from 'store/configureStore'
import equipmentCodesMessages from 'store/equipmentCodes/equipmentCodes.messages'

import mockState from '../../../mockData/mockState'

const mockEquipmentCodes = [
  {
    id: 1,
    code: 'GPS',
    description: 'GPS NAVIGATION DEVICE',
    makes: [
      {
        id: 1,
        name: 'Garmin',
        equipmentCodeId: 1,
        models: [
          { id: 1, name: 'Nuvi 2015', makeId: 1 },
          { id: 2, name: 'Nuvi 2016', makeId: 1 },
        ],
      },
      {
        id: 2,
        name: 'Motorolla',
        equipmentCodeId: 1,
        models: [{ id: 3, name: 'Whereami', makeId: 2 }],
      },
    ],
    deactivated: false,
  },
  {
    id: 2,
    code: 'TPD',
    description: 'TOLL PASS DEVICE',
    makes: [
      {
        id: 3,
        name: 'EZPass',
        equipmentCodeId: 2,
        models: [{ id: 4, name: 'EZPass', makeId: 3 }],
      },
    ],
    deactivated: false,
  },
  {
    id: 3,
    code: 'SAD',
    description: 'Deactivated Code',
    makes: [],
    deactivated: true,
  },
]

describe('equipmentCodes duck', () => {
  let store

  beforeEach(() => {
    store = configureStore(
      mockState.setIn(['equipmentCodes', 'records'], List()),
    )
    apiClient.get = jest.fn(() => Promise.resolve(mockEquipmentCodes))
  })

  it('gets a list of equipmentCodes from the API', async () => {
    await store.dispatch(getEquipmentCodes())

    expect(apiClient.get).toHaveBeenCalledWith('/domain/equipmentCode')
  })

  it('sets a loading flag', async () => {
    const p = store.dispatch(getEquipmentCodes())
    expect(selectIsLoadingEquipmentCodes(store.getState())).toEqual(true)
    await p
    expect(selectIsLoadingEquipmentCodes(store.getState())).toEqual(false)
  })

  it('populates equipment codes', async () => {
    await store.dispatch(getEquipmentCodes())
    expect(selectEquipmentCodes(store.getState())).toEqualImmutable(
      List([
        EquipmentCodeRecord({
          id: 1,
          code: 'GPS',
          description: 'GPS NAVIGATION DEVICE',
        }),
        EquipmentCodeRecord({
          id: 2,
          code: 'TPD',
          description: 'TOLL PASS DEVICE',
        }),
      ]),
    )
  })

  it('populates makes', async () => {
    await store.dispatch(getEquipmentCodes())

    expect(selectMakes(store.getState())).toEqualImmutable(
      List([
        MakeRecord({
          id: 3,
          name: 'EZPass',
          equipmentCodeId: 2,
          equipmentCode: 'TPD',
        }),
        MakeRecord({
          id: 1,
          name: 'Garmin',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
        }),
        MakeRecord({
          id: 2,
          name: 'Motorolla',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
        }),
      ]),
    )
  })

  it('populates models', async () => {
    await store.dispatch(getEquipmentCodes())

    expect(selectModels(store.getState())).toEqualImmutable(
      List([
        ModelRecord({
          id: 4,
          name: 'EZPass',
          makeId: 3,
          make: 'EZPass',
          equipmentCodeId: 2,
          equipmentCode: 'TPD',
        }),
        ModelRecord({
          id: 1,
          name: 'Nuvi 2015',
          makeId: 1,
          make: 'Garmin',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
        }),
        ModelRecord({
          id: 2,
          name: 'Nuvi 2016',
          makeId: 1,
          make: 'Garmin',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
        }),
        ModelRecord({
          id: 3,
          name: 'Whereami',
          makeId: 2,
          make: 'Motorolla',
          equipmentCodeId: 1,
          equipmentCode: 'GPS',
        }),
      ]),
    )
  })

  it('strips out deactivated equipmentCodes', () => {
    store = configureStore(
      mockState.setIn(
        ['equipmentCodes', 'records'],
        OrderedMap([
          [
            1,
            EquipmentCodeRecord({
              id: 1,
              code: 'GPS',
              description: 'GPS NAVIGATION DEVICE',
            }),
          ],
          [
            2,
            EquipmentCodeRecord({
              id: 2,
              code: 'TPD',
              description: 'TOLL PASS DEVICE',
            }),
          ],
          [
            3,
            EquipmentCodeRecord({
              id: 3,
              code: 'TPW',
              description: 'TOLL PASS WAIVER',
              deactivated: true,
            }),
          ],
        ]),
      ),
    )
    expect(selectEquipmentCodes(store.getState())).toEqualImmutable(
      List([
        EquipmentCodeRecord({
          id: 1,
          code: 'GPS',
          description: 'GPS NAVIGATION DEVICE',
        }),
        EquipmentCodeRecord({
          id: 2,
          code: 'TPD',
          description: 'TOLL PASS DEVICE',
        }),
      ]),
    )
  })

  it('can set a filter', () => {
    store.dispatch(setEquipmentCodeListFilter('GPS'))
    expect(selectEquipmentCodeFilter(store.getState())).toEqual('GPS')
  })

  it('should return a sorted equipmentCode list', () => {
    store = configureStore(
      mockState.setIn(
        ['equipmentCodes', 'records'],
        OrderedMap([
          [
            1,
            EquipmentCodeRecord({
              id: 1,
              code: 'TPD',
              description: 'TOLL PASS DEVICE',
            }),
          ],
          [
            2,
            EquipmentCodeRecord({
              id: 2,
              code: 'TPW',
              description: 'TOLL PASS WAIVER',
            }),
          ],
          [
            3,
            EquipmentCodeRecord({
              id: 3,
              code: 'GPS',
              description: 'GPS NAVIGATION DEVICE',
            }),
          ],
        ]),
      ),
    )
    const expected = List([
      EquipmentCodeRecord({
        id: 3,
        code: 'GPS',
        description: 'GPS NAVIGATION DEVICE',
      }),
      EquipmentCodeRecord({
        id: 1,
        code: 'TPD',
        description: 'TOLL PASS DEVICE',
      }),
      EquipmentCodeRecord({
        id: 2,
        code: 'TPW',
        description: 'TOLL PASS WAIVER',
      }),
    ])
    expect(computeEquipmentCodeList(store.getState())).toEqualImmutable(
      expected,
    )
  })

  it('can fuzzy filter equipmentCode list', () => {
    store = configureStore(
      mockState.setIn(
        ['equipmentCodes', 'records'],
        OrderedMap([
          [
            1,
            EquipmentCodeRecord({
              id: 1,
              code: 'TPD',
              description: 'TOLL PASS DEVICE',
            }),
          ],
          [
            2,
            EquipmentCodeRecord({
              id: 2,
              code: 'TPW',
              description: 'TOLL PASS WAIVER',
            }),
          ],
          [
            3,
            EquipmentCodeRecord({
              id: 3,
              code: 'GPS',
              description: 'GPS NAVIGATION DEVICE',
            }),
          ],
        ]),
      ),
    )
    store.dispatch(setEquipmentCodeListFilter('TOLL'))
    expect(computeEquipmentCodeList(store.getState())).toEqual(
      List([
        EquipmentCodeRecord({
          id: 1,
          code: 'TPD',
          description: 'TOLL PASS DEVICE',
        }),
        EquipmentCodeRecord({
          id: 2,
          code: 'TPW',
          description: 'TOLL PASS WAIVER',
        }),
      ]),
    )
  })

  it('should trim whitespace when filtering', () => {
    store = configureStore(
      mockState.setIn(
        ['equipmentCodes', 'records'],
        OrderedMap([
          [
            1,
            EquipmentCodeRecord({
              id: 1,
              code: 'TPD',
              description: 'TOLL PASS DEVICE',
            }),
          ],
          [
            2,
            EquipmentCodeRecord({
              id: 2,
              code: 'TPW',
              description: 'TOLL PASS WAIVER',
            }),
          ],
          [
            3,
            EquipmentCodeRecord({
              id: 3,
              code: 'GPS',
              description: 'GPS NAVIGATION DEVICE',
            }),
          ],
        ]),
      ),
    )
    store.dispatch(setEquipmentCodeListFilter('  GPS  '))
    expect(computeEquipmentCodeList(store.getState())).toEqual(
      List([
        EquipmentCodeRecord({
          id: 3,
          code: 'GPS',
          description: 'GPS NAVIGATION DEVICE',
        }),
      ]),
    )
  })

  it('does not filter when filter is empty', () => {
    store = configureStore(
      mockState.setIn(
        ['equipmentCodes', 'records'],
        OrderedMap([
          [
            1,
            EquipmentCodeRecord({
              id: 1,
              name: 'Nuvi',
              equipmentCodeId: 1,
              equipmentCode: 'GPS',
              makeId: 1,
              make: 'Garmin',
            }),
          ],
        ]),
      ),
    )
    store.dispatch(setEquipmentCodeListFilter(''))
    expect(computeEquipmentCodeList(store.getState()).size).toEqual(1)
  })

  it('clears filter on route change', () => {
    store.dispatch(setEquipmentCodeListFilter('foo'))
    store.dispatch({ type: LOCATION_CHANGE })
    expect(selectEquipmentCodeFilter(store.getState())).toEqual('')
  })

  describe('creating a equipmentCode', () => {
    it('posts equipmentCode information to the api', () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      store.dispatch(
        createEquipmentCode({ code: 'NEC', description: 'New Equipment Code' }),
      )

      expect(apiClient.post).toHaveBeenCalledWith('/domain/equipmentCode', {
        code: 'NEC',
        description: 'New Equipment Code',
      })
    })

    it('shows a success message', async () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      await store.dispatch(
        createEquipmentCode({ code: 'NEC', description: 'New Equipment Code' }),
      )

      expect(selectCurrentMessage(store.getState()).text).toEqual({
        ...equipmentCodesMessages.addedNewEquipmentCode,
        values: { equipmentCode: 'NEC' },
      })
    })

    it('reloads equipmentCodes', async () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      apiClient.get = jest.fn(() => Promise.resolve([]))
      await store.dispatch(
        createEquipmentCode({ code: 'NEC', description: 'New Equipment Code' }),
      )

      expect(apiClient.get).toHaveBeenCalledWith('/domain/equipmentCode')
    })

    it('does not catch when reloading equipmentCodes fails', async () => {
      apiClient.post = jest.fn(() => Promise.resolve())
      apiClient.get = jest.fn(() => Promise.reject(new Error()))

      try {
        await store.dispatch(
          createEquipmentCode({
            code: 'NEC',
            description: 'New Equipment Code',
          }),
        )
      } catch (e) {
        throw new Error('it should not throw')
      }
    })
  })

  describe('deactivating a equipmentCode', () => {
    beforeEach(() => {
      apiClient.delete = jest.fn(() => Promise.resolve())
      apiClient.get = jest.fn(() => Promise.resolve([]))
    })

    it('sends a delete for the given equipmentCode id', () => {
      store.dispatch(
        deactivateEquipmentCode(EquipmentCodeRecord({ id: 3, code: 'GPS' })),
      )
      expect(apiClient.delete).toHaveBeenCalledWith('/domain/equipmentCode/3')
    })

    it('shows a message that the equipmentCode was deactivated', async () => {
      await store.dispatch(
        deactivateEquipmentCode(EquipmentCodeRecord({ id: 3, code: 'GPS' })),
      )

      expect(selectCurrentMessage(store.getState()).text).toEqual({
        ...equipmentCodesMessages.deactivatedEquipmentCode,
        values: { equipmentCode: 'GPS' },
      })
    })

    it('reloads equipmentCodes', async () => {
      await store.dispatch(
        deactivateEquipmentCode(EquipmentCodeRecord({ id: 1, code: 'GPS' })),
      )

      expect(apiClient.get).toHaveBeenCalledWith('/domain/equipmentCode')
    })

    it('shows an alert error when it errors and rethrows the error', async () => {
      const error = new Error()
      apiClient.delete = jest.fn(() => Promise.reject(error))
      const err = await store
        .dispatch(
          deactivateEquipmentCode(EquipmentCodeRecord({ id: 1, code: 'GPS' })),
        )
        .catch(e => e)

      expect(selectHasError(store.getState())).toEqual(true)
      expect(err).toEqual(error)
    })
  })
})
