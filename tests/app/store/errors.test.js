import { LOCATION_CHANGE } from 'react-router-redux'

import { ErrorMessageRecord } from 'store/errors/errors.records'
import {
  clearError,
  showAlertError,
  showPageError,
} from 'store/errors/errors.actions'
import {
  selectCurrentError,
  selectHasError,
} from 'store/errors/errors.selectors'
import configureStore from 'store/configureStore'

let store
let state

describe('error duck', () => {
  const error1 = { title: 'title1', message: 'message1' }
  const error2 = { title: 'title2', message: 'message2' }
  const error3 = { title: 'title3', message: 'message3' }

  beforeEach(() => {
    store = configureStore()
  })

  it('can show page errors', () => {
    store.dispatch(showPageError(error1))
    state = store.getState()

    expect(selectCurrentError(state)).toEqualImmutable(
      new ErrorMessageRecord({ ...error1, pageError: true }),
    )
  })

  it('can hold errors', () => {
    store.dispatch(showAlertError(error1))
    store.dispatch(showAlertError(error2))
    store.dispatch(showAlertError(error3))
    state = store.getState()

    expect(selectCurrentError(state)).toEqualImmutable(
      new ErrorMessageRecord(error1),
    )
  })

  it('can remove errors', () => {
    store.dispatch(showAlertError(error1))
    store.dispatch(showAlertError(error2))
    store.dispatch(showAlertError(error3))
    store.dispatch(clearError())
    state = store.getState()

    expect(selectCurrentError(state)).toEqualImmutable(
      new ErrorMessageRecord(error2),
    )
  })

  it('can check if there are errors', () => {
    state = store.getState()
    expect(selectHasError(state)).toBe(false)
    store.dispatch(showAlertError(error1))
    state = store.getState()
    expect(selectHasError(state)).toBe(true)
  })

  it('clears page errors on a navigation change', () => {
    store.dispatch(showPageError(error1))
    store.dispatch({ type: LOCATION_CHANGE })
    state = store.getState()
    expect(selectHasError(state)).toBe(false)
  })

  it('does not clear alert errors on a navigation change', () => {
    store.dispatch(showAlertError(error1))
    store.dispatch({ type: LOCATION_CHANGE })
    state = store.getState()
    expect(selectHasError(state)).toBe(true)
  })
})
