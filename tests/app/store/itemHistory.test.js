import { List } from 'immutable'

import { ItemHistoryRecord } from 'store/itemHistory/itemHistory.records'
import { ItemStatusRecord } from 'store/itemStatuses/itemStatuses.records'
import { PaginationRecord } from 'store/inventoryItems/inventoryItems.records'
import { getItemHistory } from 'store/itemHistory/itemHistory.actions'
import {
  selectHistoryPagination,
  selectIsLoadingHistory,
  selectItemHistory,
} from 'store/itemHistory/itemHistory.selectors'
import apiClient from 'apiClient'
import configureStore from 'store/configureStore'

import jestWhen from '../../jestWhen'
import mockState from '../../../mockData/mockState'

const itemHistory = {
  content: [
    {
      id: 1,
      inventoryItemId: 1,
      createTimestamp: '2018-01-01T06:00:00Z',
      createdBy: 'E123A4',
      location: 'ABC123',
      itemStatusId: 1,
    },
    {
      id: 2,
      inventoryItemId: 1,
      createTimestamp: '2018-01-02T06:00:00Z',
      createdBy: 'E123A4',
      location: 'ABC123',
      itemStatusId: 2,
    },
  ],
  totalElements: 10,
  totalPages: 5,
  last: false,
  first: true,
  size: 10,
  number: 0,
  sort: null,
  numberOfElements: 10,
}

describe('itemHistory duck', () => {
  let store

  beforeEach(() => {
    store = configureStore()
  })

  describe('getting history', () => {
    beforeEach(() => {
      store = configureStore(
        mockState.setIn(['itemHistory', 'records']),
        List(),
      )

      apiClient.get = jest.fn(() => Promise.resolve(itemHistory))
    })

    it('calls the api for history', () => {
      store.dispatch(getItemHistory(1, { page: 0 }))
      expect(apiClient.get).toHaveBeenCalledWith(
        '/inventoryItem/1/itemHistory',
        {
          params: { page: 0 },
        },
      )
    })

    it('sets a loading flag', async () => {
      expect(selectIsLoadingHistory(store.getState())).toEqual(false)
      const p = store.dispatch(getItemHistory(1, { page: 0 }))
      expect(selectIsLoadingHistory(store.getState())).toEqual(true)
      await p
      expect(selectIsLoadingHistory(store.getState())).toEqual(false)
    })

    it('clears the loading flag if fails', async () => {
      apiClient.get = () => Promise.reject(new Error())
      expect(selectIsLoadingHistory(store.getState())).toEqual(false)
      await store.dispatch(getItemHistory(1, { page: 0 })).catch(() => {})
      expect(selectIsLoadingHistory(store.getState())).toEqual(false)
    })

    it('saves item hitsory in state', async () => {
      await store.dispatch(getItemHistory(1, { page: 0 }))

      expect(selectItemHistory(store.getState())).toEqualImmutable(
        List([
          ItemHistoryRecord({
            id: 1,
            inventoryItemId: 1,
            createTimestamp: '2018-01-01T06:00:00Z',
            createdBy: 'E123A4',
            location: 'ABC123',
            itemStatus: ItemStatusRecord({
              id: 1,
              code: 'AV',
              description: 'Available',
            }),
          }),
          ItemHistoryRecord({
            id: 2,
            inventoryItemId: 1,
            createTimestamp: '2018-01-02T06:00:00Z',
            createdBy: 'E123A4',
            location: 'ABC123',
            itemStatus: ItemStatusRecord({
              id: 2,
              code: 'OR',
              description: 'On Rent',
            }),
          }),
        ]),
      )
    })

    it('stops to reload itemStatus data if a lookup fails when populating', async () => {
      store = configureStore() // no itemStatus data
      apiClient.get = jest.fn()
      const whenGet = jestWhen(apiClient.get)
      whenGet('/inventoryItem/1/itemHistory', {
        params: { page: 0 },
      }).thenResolve(itemHistory)
      whenGet('/domain/itemStatus').thenResolve([
        { id: 1, code: 'AV', description: 'Available' },
        { id: 2, code: 'OR', description: 'On Rent' },
      ])

      await store.dispatch(getItemHistory(1, { page: 0 }))

      expect(apiClient.get).toHaveBeenCalledWith('/domain/itemStatus')

      expect(selectItemHistory(store.getState())).toEqualImmutable(
        List([
          ItemHistoryRecord({
            id: 1,
            inventoryItemId: 1,
            createTimestamp: '2018-01-01T06:00:00Z',
            createdBy: 'E123A4',
            location: 'ABC123',
            itemStatus: ItemStatusRecord({
              id: 1,
              code: 'AV',
              description: 'Available',
            }),
          }),
          ItemHistoryRecord({
            id: 2,
            inventoryItemId: 1,
            createTimestamp: '2018-01-02T06:00:00Z',
            createdBy: 'E123A4',
            location: 'ABC123',
            itemStatus: ItemStatusRecord({
              id: 2,
              code: 'OR',
              description: 'On Rent',
            }),
          }),
        ]),
      )
    })

    it('saves pagination information about item history in state', async () => {
      await store.dispatch(getItemHistory(1, { page: 0 }))

      expect(selectHistoryPagination(store.getState())).toEqualImmutable(
        PaginationRecord({
          totalElements: 10,
          totalPages: 5,
          last: false,
          first: true,
          number: 0,
          sort: null,
          numberOfElements: 10,
          currentIndex: 0,
        }),
      )
    })
  })
})
