import always from 'lodash/fp/constant'
import isFunction from 'lodash/fp/isFunction'

import unless from 'helpers/unless'

describe('unless', () => {
  it('is useful for safely wrapping up maybe functions', () => {
    const foo = 'foo'
    const funFoo = () => 'funFoo'

    // invariant: foo can not be safely invoked
    expect(() => foo()).toThrow()

    const makeSafe = unless(isFunction, always)
    const safeFoo = makeSafe(foo)
    const safeFunFoo = makeSafe(funFoo)

    // now it can be
    expect(safeFoo).not.toThrow()
    expect(safeFunFoo).not.toThrow()
    expect(safeFoo()).toEqual('foo')
    expect(safeFunFoo()).toEqual('funFoo')
  })
})
