import { getScreenForm } from 'helpers/screenForm'
import onScreenFormChange from 'helpers/onScreenFormChange'

jest.mock('helpers/screenForm', () => ({
  getScreenForm: jest.fn(),
}))

describe('onScreenFormChange', () => {
  let cb
  let listener

  beforeEach(() => {
    cb = jest.fn()
  })

  afterEach(() => {
    listener.remove()
  })

  it('callsback when it changes form', () => {
    getScreenForm.mockReturnValueOnce('foo').mockReturnValueOnce('bar')
    listener = onScreenFormChange(cb)
    window.dispatchEvent(new Event('resize'))

    expect(cb).toHaveBeenCalled()
  })

  it('does not callsback when it does not change form', () => {
    getScreenForm.mockReturnValueOnce('foo').mockReturnValueOnce('foo')
    listener = onScreenFormChange(cb)
    window.dispatchEvent(new Event('resize'))

    expect(cb).not.toHaveBeenCalled()
  })
})
