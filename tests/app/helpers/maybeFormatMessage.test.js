import { FormattedMessage } from 'react-intl'
import React from 'react'

import maybeFormatMessage from 'helpers/maybeFormatMessage'

describe('value renderer', () => {
  it('works with messages', () => {
    expect(
      maybeFormatMessage({
        id: 'message',
        defaultMessage: 'message',
      }),
    ).toEqual(<FormattedMessage id="message" defaultMessage="message" />)
  })
  it('works with strings', () => {
    expect(maybeFormatMessage('string')).toEqual('string')
  })
})
