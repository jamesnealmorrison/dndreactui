import {
  DESKTOP,
  PHONE,
  TABLET,
  WIDESCREEN,
  getScreenForm,
  isDesktop,
  isPhone,
  isTablet,
  isWidescreen,
} from 'helpers/screenForm'
import theme from 'theme'

describe('screenForm', () => {
  describe('isPhone', () => {
    it('returns true when the screen is a phone', () => {
      expect(isPhone({ innerWidth: theme.tabletBreakpoint - 1 })).toBe(true)
    })

    it('returns false when the screen is bigger than a phone', () => {
      expect(isPhone({ innerWidth: theme.tabletBreakpoint })).toBe(false)
    })
  })

  describe('isTablet', () => {
    it('returns true when the screen is a tablet', () => {
      expect(isTablet({ innerWidth: theme.desktopBreakpoint - 1 })).toBe(true)
    })

    it('returns false when the screen is bigger than a tablet', () => {
      expect(isTablet({ innerWidth: theme.desktopBreakpoint })).toBe(false)
    })

    it('returns false when the screen is smaller than a tablet', () => {
      expect(isTablet({ innerWidth: theme.tabletBreakpoint - 1 })).toBe(false)
    })
  })

  describe('isDesktop', () => {
    it('returns true when the screen is a desktop', () => {
      expect(isDesktop({ innerWidth: theme.desktopBreakpoint })).toBe(true)
    })

    it('returns false when the screen is smaller than a desktop', () => {
      expect(isDesktop({ innerWidth: theme.desktopBreakpoint - 1 })).toBe(false)
    })
  })

  describe('isWidescreen', () => {
    it('returns true when the screen is widescreen', () => {
      expect(isWidescreen({ innerWidth: theme.widescreenBreakpoint })).toBe(
        true,
      )
    })

    it('returns false when the screen is smaller than a widescreen', () => {
      expect(isWidescreen({ innerWidth: theme.widescreenBreakpoint - 1 })).toBe(
        false,
      )
    })
  })

  describe('getScreenForm', () => {
    it('returns PHONE when the screen is a phone', () => {
      expect(getScreenForm({ innerWidth: theme.tabletBreakpoint - 1 })).toEqual(
        PHONE,
      )
    })
    it('returns TABLET when the screen is a tablet', () => {
      expect(getScreenForm({ innerWidth: theme.tabletBreakpoint })).toEqual(
        TABLET,
      )
    })
    it('returns DESKTOP when the screen is a desktop', () => {
      expect(getScreenForm({ innerWidth: theme.desktopBreakpoint })).toEqual(
        DESKTOP,
      )
    })
    it('returns WIDESCREEN when the screen is a desktop', () => {
      expect(getScreenForm({ innerWidth: theme.widescreenBreakpoint })).toEqual(
        WIDESCREEN,
      )
    })
  })
})
