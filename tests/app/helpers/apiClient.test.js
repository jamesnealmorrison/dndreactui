import MockAdapter from 'axios-mock-adapter'

import apiClient from 'apiClient'
import httpErrorCodes from 'helpers/httpErrorCodes'

describe('api client', () => {
  let mock

  beforeEach(() => {
    mock = new MockAdapter(apiClient)
  })

  it('should format success resonses', async () => {
    mock.onGet('/foo').reply(() => [200, { foo: 'foo' }])
    const res = await apiClient.get('/foo')
    expect(res).toEqual({ foo: 'foo' })
  })

  it('should format server errors', async () => {
    mock
      .onGet('/foo')
      .reply(() => [500, { errors: ['it broke', 'it broke again'] }])
    expect.assertions(3)

    try {
      await apiClient.get('/foo')
    } catch (e) {
      expect(e.message).toEqual('it broke\nit broke again')
      expect(e.status).toEqual(500)
      expect(e.title).toEqual(httpErrorCodes[500])
    }
  })

  it('should format unhandled server errors', async () => {
    mock.onGet('/foo').reply(() => [500, { wtf: 'uh oh' }])
    expect.assertions(3)

    try {
      await apiClient.get('/foo')
    } catch (e) {
      expect(e.message).toEqual('Server Error')
      expect(e.status).toEqual(500)
      expect(e.title).toEqual(httpErrorCodes[500])
    }
  })

  it('should format improperly formatted server errors', async () => {
    mock.onGet('/foo').reply(() => [500, { errors: 'uh oh' }])
    expect.assertions(3)

    try {
      await apiClient.get('/foo')
    } catch (e) {
      expect(e.message).toEqual('Server Error')
      expect(e.status).toEqual(500)
      expect(e.title).toEqual(httpErrorCodes[500])
    }
  })

  it('should format timeout errors', async () => {
    mock.onGet('/foo').timeout()
    expect.assertions(3)

    try {
      await apiClient.get('/foo')
    } catch (e) {
      expect(e.title).toEqual('Connection Issues')
      expect(e.status).toEqual(0)
      expect(typeof e.message).toEqual('string')
    }
  })

  it('should format low level network errors', async () => {
    mock.onGet('/foo').networkError()
    expect.assertions(3)

    try {
      await apiClient.get('/foo')
    } catch (e) {
      expect(e.title).toEqual('Error')
      expect(e.status).toEqual(0)
      expect(typeof e.message).toEqual('string')
    }
  })
})
