import resetField from 'helpers/resetField'
import { change, untouch } from 'redux-form/immutable'

describe('resetField', () => {
  it('resets field value', () => {
    const dispatch = jest.fn()
    resetField('form')('field')(dispatch)
    expect(dispatch).toHaveBeenCalledWith(change('form', 'field', null))
  })

  it('untouches the field', () => {
    const dispatch = jest.fn()
    resetField('form')('field')(dispatch)
    expect(dispatch).toHaveBeenCalledWith(untouch('form', 'field'))
  })
})
