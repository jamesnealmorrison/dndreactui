import errors from 'intl/appMessages/errors'

import { minLength, required } from 'helpers/validators'

describe('validators', () => {
  describe('required', () => {
    it('fails for nill values', () => {
      expect(required()).toEqual(errors.required)
      expect(required(null)).toEqual(errors.required)
      expect(required(undefined)).toEqual(errors.required)
    })

    it('fails for an empty/whitespace string', () => {
      expect(required('')).toEqual(errors.required)
      expect(required('  ')).toEqual(errors.required)
    })

    it('passes on non empty strings', () => {
      expect(required('foo')).toEqual(undefined)
    })
  })

  describe('minLength', () => {
    it('fails on string + whitespace shorter than min length', () => {
      expect(minLength(7, 'failure message')('short      ')).toEqual(
        'failure message',
      )
    })

    it('has a default message', () => {
      expect(minLength(7)('short')).toEqual('Must be at least 7 characters')
    })

    it('fails on string shorter than min length', () => {
      expect(minLength(7, 'failure message')('short')).toEqual(
        'failure message',
      )
    })

    it('passes if string is equal to min Length', () => {
      expect(minLength(4, 'failure message')('good')).toBeUndefined()
    })

    it('passes if string is greater than min Length', () => {
      expect(minLength(3, 'failure message')('good')).toBeUndefined()
    })
  })
})
