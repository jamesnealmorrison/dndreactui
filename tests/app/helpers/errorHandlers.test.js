import noop from 'lodash/fp/noop'

import { ErrorMessageRecord } from 'store/errors/errors.records'
import {
  selectCurrentError,
  selectHasError,
} from 'store/errors/errors.selectors'
import { withAlertError, withPageError } from 'helpers/errorHandlers'
import configureStore from 'store/configureStore'

describe('errorHandlers', () => {
  let store
  const dummyFailThunk = () => Promise.reject(new Error('boom'))
  const dummyPassThunk = () => Promise.resolve()

  beforeEach(() => {
    store = configureStore()
  })

  describe('withPageError', () => {
    it('should dispatch page errors for thunks that reject', async () => {
      await store.dispatch(withPageError(dummyFailThunk)).catch(noop)
      expect(selectCurrentError(store.getState())).toEqualImmutable(
        new ErrorMessageRecord({ message: 'boom', pageError: true }),
      )
    })

    it('should ignore thunks that do not reject', async () => {
      await store.dispatch(withPageError(dummyPassThunk))
      expect(selectHasError(store.getState())).toEqual(false)
    })
  })

  describe('withAlertError', () => {
    it('should dispatch page errors for thunks that reject', async () => {
      await store.dispatch(withAlertError(dummyFailThunk)).catch(noop)
      expect(selectCurrentError(store.getState())).toEqualImmutable(
        new ErrorMessageRecord({ message: 'boom' }),
      )
    })

    it('should ignore thunks that do not reject', async () => {
      await store.dispatch(withAlertError(dummyPassThunk))
      expect(selectHasError(store.getState())).toEqual(false)
    })
  })
})
