import get from 'lodash/fp/get'

import ifProp from 'helpers/ifProp'

describe('ifProp', () => {
  describe('when the predicate is a string', () => {
    describe('when it is a truthy prop', () => {
      describe('when the pass case is a function', () => {
        it('it is invoked with the props and returned', () => {
          expect(ifProp('foo', get('bar'))({ foo: true, bar: 'bar' })).toEqual(
            'bar',
          )
        })
      })
      describe('when the pass case is some other value', () => {
        it('is returned', () => {
          expect(ifProp('foo', 'bar')({ foo: true })).toEqual('bar')
        })
      })
    })
    describe('when it is a falsey prop', () => {
      describe('when the fail case is a function', () => {
        it('it is invoked with the props and returned', () => {
          expect(
            ifProp('foo', null, get('bar'))({ foo: false, bar: 'bar' }),
          ).toEqual('bar')
        })
      })
      describe('when the fail case is some other value', () => {
        it('is returned', () => {
          expect(ifProp('foo', null, 'bar')({ foo: false })).toEqual('bar')
        })
      })
    })
  })
  describe('when the predicate is a function', () => {
    describe('when it returns a truthy value', () => {
      describe('when the pass case is a function', () => {
        it('it is invoked with the props and returned', () => {
          expect(
            ifProp(get('foo'), get('bar'))({ foo: true, bar: 'bar' }),
          ).toEqual('bar')
        })
      })
      describe('when the pass case is some other value', () => {
        it('is returned', () => {
          expect(ifProp(get('foo'), 'bar')({ foo: true })).toEqual('bar')
        })
      })
    })
    describe('when it is a returns a falsey value', () => {
      describe('when the fail case is a function', () => {
        it('it is invoked with the props and returned', () => {
          expect(
            ifProp(get('foo'), null, get('bar'))({ foo: false, bar: 'bar' }),
          ).toEqual('bar')
        })
      })
      describe('when the fail case is some other value', () => {
        it('is returned', () => {
          expect(ifProp(get('foo'), null, 'bar')({ foo: false })).toEqual('bar')
        })
      })
    })
  })
})
