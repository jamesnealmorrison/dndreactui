import swallowAsyncErrors from 'helpers/swallowAsyncErrors'

describe('swallowAsyncErrors', () => {
  it('it should swallow errors', async () => {
    const iThrow = new Promise((resolve, reject) => {
      reject(new Error('bad'))
    })

    const test = async () => {
      await swallowAsyncErrors(iThrow)
      return 'good'
    }

    expect.assertions(1)
    const result = await test()

    expect(result).toEqual('good')
  })
})
