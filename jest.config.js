module.exports = {
  collectCoverageFrom: [
    'app/**/*.{js,jsx}',
    '!app/**/style.{js}',
    '!app/**/*.style.{js}',
    '!app/**/*.test.{js,jsx}',
    '!app/*/RbGenerated*/*.{js,jsx}',
    '!app/app.js',
    '!app/global-styles.js',
    '!app/*/*/Loadable.{js,jsx}',
  ],
  coverageThreshold: {
    global: {
      statements: 98,
      branches: 91,
      functions: 98,
      lines: 98,
    },
  },
  moduleDirectories: ['node_modules', 'app'],
  moduleNameMapper: {
    '.*\\.(css|less|styl|scss|sass)$': '<rootDir>/internals/mocks/cssModule.js',
    '.*\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/internals/mocks/image.js',
    'react-portal': '<rootDir>/internals/mocks/mockPortal.js',
    'react-modal': '<rootDir>/internals/mocks/mockModal.js',
    'App/shared/components/SVGIcon/getMaterialIconContents':
      '<rootDir>/internals/mocks/getMaterialIconContents.js',
  },
  setupTestFrameworkScriptFile: '<rootDir>/tests/test-bundler.js',
  testRegex: 'tests/.*\\.test\\.js$',
}
