import { connect } from 'react-redux'
import { injectIntl, intlShape } from 'react-intl'
import PropTypes from 'prop-types'
import React from 'react'
import compose from 'lodash/fp/flowRight'
import get from 'lodash/fp/get'
import isObject from 'lodash/fp/isObject'

import { MessageRecord } from 'store/messages/messages.records'
import { deleteMessage } from 'store/messages/messages.actions'
import { selectCurrentMessage } from 'store/messages/messages.selectors'
import Snackbar from 'App/containers/MessageProvider/Snackbar'
import Toast from 'App/containers/MessageProvider/Toast'

export class MessageProvider extends React.Component {
  state = {
    showMessage: false,
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.message && nextProps.message !== this.props.message) {
      setTimeout(() => this.setState({ showMessage: true }), 0)

      if (nextProps.message.timeout) {
        setTimeout(this.deleteMessage, nextProps.message.timeout)
      }
    }
  }

  deleteMessage = () => {
    this.setState({ showMessage: false })
    setTimeout(() => {
      this.props.deleteMessage(this.props.message)
    }, 300)
  }

  render() {
    const messageText = get('message.text', this.props)
    let intlText

    if (isObject(messageText) && messageText.values) {
      intlText = this.props.intl.formatMessage(messageText, messageText.values)
    } else if (isObject(messageText)) {
      intlText = this.props.intl.formatMessage(messageText)
    } else {
      intlText = messageText
    }

    return (
      <Toast isActive={this.state.showMessage} position="right">
        <Snackbar>
          {intlText}
          {get('message.timeout', this.props) === 0 ? (
            <Snackbar.Button onClick={this.deleteMessage}>
              Dismiss
            </Snackbar.Button>
          ) : null}
        </Snackbar>
      </Toast>
    )
  }
}

MessageProvider.propTypes = {
  message: PropTypes.instanceOf(MessageRecord),
  deleteMessage: PropTypes.func.isRequired,
  intl: intlShape.isRequired,
}

export default compose(
  injectIntl,
  connect(state => ({ message: selectCurrentMessage(state) }), {
    deleteMessage,
  }),
)(MessageProvider)
