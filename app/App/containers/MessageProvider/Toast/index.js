import Portal from 'react-portal'
import PropTypes from 'prop-types'
import React, { Component } from 'react'

import { ToastAnimation } from 'App/containers/MessageProvider/Toast/Toast.style'

export default class Toast extends Component {
  state = {
    isOpened: this.props.isActive,
    isActive: this.props.isActive,
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isActive && nextProps.isActive) {
      this.handleEnter()
    }
    if (this.props.isActive && !nextProps.isActive) {
      this.handleLeave()
    }
  }

  componentWillUnmount() {
    clearTimeout(this.animation)
  }

  handleEnter() {
    clearTimeout(this.animation)
    this.setState({ isOpened: true })
    this.animation = setTimeout(() => {
      this.setState({ isActive: true })
    })
  }

  handleLeave() {
    clearTimeout(this.animation)
    this.setState({ isActive: false })
    this.animation = setTimeout(() => {
      this.setState({ isOpened: false })
    }, 300)
  }

  render() {
    return (
      <Portal isOpened={this.state.isOpened}>
        <ToastAnimation {...this.props} isActive={this.state.isActive} />
      </Portal>
    )
  }
}

Toast.defaultProps = {
  position: 'left',
}

Toast.propTypes = {
  isActive: PropTypes.bool,
  position: PropTypes.string,
  children: PropTypes.node,
}
