import always from 'lodash/fp/constant'
import cond from 'lodash/fp/cond'
import propEq from 'lodash/fp/matchesProperty'
import styled, { css } from 'styled-components'

import ifProp from 'helpers/ifProp'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'

const position = propEq('position')

export const ToastAnimation = setDisplayName('ToastAnimation')(
  styled.div`
    position: fixed;
    bottom: 0;
    z-index: 3;
    display: block;
    will-change: transform;
    transform: translate(0, 100%);
    transition: transform 0.25s ${theme.animationCurveFastOutLinearIn};
    @media(max-width: ${theme.tabletBreakpoint - 1}px) {
      width: 100%;
      left: 0;
    }
    @media(min-width: ${theme.tabletBreakpoint}px) {
      ${cond([
        [position('left'), always(css`
          bottom: 0;
          left: 0;
          transform: translate(0, 100%);
        `)],
        [position('right'), always(css`
          bottom: 0;
          right: 0;
          transform: translate(0, 100%);
        `)],
        [position('center'), always(css`
          left: 50%;
          bottom: 0;
          transform: translate(-50%, 100%);
        `)],
      ])}
    }
    ${ifProp('isActive', css`
      transform: translate(0, 0);
      transition: transform 0.25s ${theme.animationCurveLinearOutSlowIn};
      @media(min-width: ${theme.tabletBreakpoint}px) {
        ${ifProp(position('center'), css`
          left: 50%;
          transform: translate(-50%, 0);
        `, css`
          transform: translate(0, 0);
        `)}
      }
    `)}
  `)
