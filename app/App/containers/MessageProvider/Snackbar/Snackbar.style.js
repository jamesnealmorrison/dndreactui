import styled from 'styled-components'

import { typoButton } from 'helpers/styleMixins/type.style'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'


export const SnackbarBase = setDisplayName('Snackbar')(
  styled.div`
    background-color: ${theme.colorDark};
    display: flex;
    justify-content: space-between;
    padding: 14px 12px 14px 24px;
    align-items: center;
    color: white;
    @media(max-width: ${theme.tabletBreakpoint - 1}px) {
      width: 100%;
      min-height: 48px;
    }
    @media(min-width: ${theme.tabletBreakpoint}px) {
      min-width: 288px;
      max-width: 568px;
      border-radius: 2px;
      margin: 0 16px 16px 16px;
    }
  `)


export const SnackbarButton = setDisplayName('SnackbarButton')(
  styled.button`
    background: transparent;
    border: none;
    color: ${theme.colorAccent};
    float: right;
    text-transform: uppercase;
    margin: -14px -12px  -14px 0;
    padding: 14px 24px 14px 12px;
    ${typoButton()}
    overflow: hidden;
    outline: none;
    opacity: 0;
    pointer-events: none;
    cursor: pointer;
    text-decoration: none;
    text-align: center;
    align-self: center;
    flex-shrink: 0;
    font-weight: 600;
    line-height: inherit;

    &::-moz-focus-inner {
      border: 0;
    }
    &:not([aria-hidden]) {
      opacity: 1;
      pointer-events: auto;
    }
  `)
