import {
  SnackbarBase,
  SnackbarButton,
} from 'App/containers/MessageProvider/Snackbar/Snackbar.style'

export default SnackbarBase

SnackbarBase.Button = SnackbarButton
