import { appLocales } from 'intl/i18n'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

import { Wrapper } from 'App/containers/LocaleToggle/LocaleToggle.styled'
import { changeLocale } from 'store/app/app.actions'
import { selectLocale } from 'store/app/app.selectors'
import Button from 'App/shared/components/Button'
import Menu from 'App/shared/components/Menu'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'

const ToggleButton = setDisplayName('ToggleButton')(styled(Button.Icon)`
  font-size: 14px;
  color: rgba(255, 255, 255, 0.54);
  &:hover,
  &:active,
  &:focus {
    color: white;
  }
`)

export const longLanguageNames = {
  en: 'English',
  de: 'Deutsch',
  fr: 'Française',
  es: 'Español',
  pt: 'Português',
}

export function LocaleToggle({ locale, onLocaleToggle }) {
  return (
    <Wrapper>
      <Menu bottomRight control={<ToggleButton>{locale}</ToggleButton>}>
        {appLocales.map(loc => (
          <Menu.Item key={loc} onClick={onLocaleToggle(loc)}>
            {longLanguageNames[loc]}
          </Menu.Item>
        ))}
      </Menu>
    </Wrapper>
  )
}

LocaleToggle.propTypes = {
  onLocaleToggle: PropTypes.func,
  locale: PropTypes.string,
}

export const mapDispatchToProps = dispatch => ({
  onLocaleToggle: locale => () =>
    setTimeout(() => dispatch(changeLocale(locale)), 100),
})

export default connect(
  state => ({
    locale: selectLocale(state),
  }),
  mapDispatchToProps,
)(LocaleToggle)
