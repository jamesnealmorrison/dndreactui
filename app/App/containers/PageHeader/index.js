import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import React from 'react'
import compose from 'lodash/fp/flowRight'

import { selectIsNavOpen } from 'store/app/app.selectors'
import { setNavOpen } from 'store/app/app.actions'
import Button from 'App/shared/components/Button'
import Fill from 'App/shared/components/Fill'
import LocaleToggle from 'App/containers/LocaleToggle'
import SVGIcon from 'App/shared/components/SVGIcon'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

import {
  PageHeaderBar,
  PageHeaderNavToggle,
  PageHeaderTitle,
  PageHeaderRow,
  PageHeaderTabs,
  PageHeaderTab,
} from './PageHeader.style'

export function PageHeader({ title, isNavOpen, dispatchSetNavOpen, children }) {
  return (
    <PageHeaderBar isNavOpen={isNavOpen}>
      <PageHeaderNavToggle isNavOpen={isNavOpen}>
        <Button.Icon inverse onClick={() => dispatchSetNavOpen(true)}>
          <SVGIcon name="navigation.menu" />
        </Button.Icon>
      </PageHeaderNavToggle>
      <PageHeaderRow>
        <PageHeaderTitle isNavOpen={isNavOpen}>{title}</PageHeaderTitle>
        <Fill />
        <LocaleToggle />
      </PageHeaderRow>
      {children}
    </PageHeaderBar>
  )
}

PageHeader.propTypes = {
  isNavOpen: PropTypes.bool,
  dispatchSetNavOpen: PropTypes.func,
  title: PropTypes.string,
  children: PropTypes.node,
}

const WrappedPageHeader = compose(
  withRouter,
  translateProps(['title']),
  connect(
    state => ({
      isNavOpen: selectIsNavOpen(state),
    }),
    { dispatchSetNavOpen: setNavOpen },
  ),
)(PageHeader)

WrappedPageHeader.Tabs = PageHeaderTabs
WrappedPageHeader.Tab = PageHeaderTab

export default WrappedPageHeader
