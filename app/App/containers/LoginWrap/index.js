import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form/immutable'
import PropTypes from 'prop-types'
import React from 'react'
import styled, { css } from 'styled-components'

import { login } from 'store/app/app.actions'
import { phoneOnly } from 'helpers/styleMixins/media'
import { required } from 'helpers/validators'
import { selectIsLoggedIn } from 'store/app/app.selectors'
import Card from 'App/shared/components/Card'
import ConnectField from 'App/shared/higherOrderComponents/connectField'
import LoginWrapMessages from 'App/containers/LoginWrap/messages'
import Offset from 'App/shared/components/Offset'
import Submit from 'App/shared/containers/Submit'
import Textfield from 'App/shared/components/Textfield'
import theme from 'theme'

export const LoginFormStyle = styled.form`
  height: 100%;
  width: 100%;
  position: relative;
  background: ${theme.colorPrimary};
  padding-bottom: 4rem;
  padding-top: 20vh;
  ${phoneOnly(css`
    padding-top: 0;
    padding-bottom: 0;
  `)};
  display: flex;
  flex-direction: column;
`

export const LoginButton = styled(Submit)`
  text-align: right;
`

const LoginButtonWrap = styled.div`
  text-align: right;
`

export const LoginMobile = styled(Submit)`
  margin-top: 1.5rem;
  border-radius: 0;
  flex-shrink: 0;
  &:disabled:disabled {
    background-color: #ccc;
    pointer-events: none;
  }
  display: none;
  ${phoneOnly(css`
    display: block;
    margin: 0;
    border-radius: 0;
  `)};
`

const LoginFields = styled(Card)`
  margin: 0 auto;
  ${phoneOnly(css`
    border-radius: 0;
    width: auto;
    flex: 1;
    margin: 0;
  `)};
`

const Input = ConnectField(Textfield)

export const LoginForm = ({ handleSubmit }) => (
  <LoginFormStyle onSubmit={handleSubmit}>
    <LoginFields width="22rem">
      <Offset vertical={1} />
      <Card.Title>
        <FormattedMessage {...LoginWrapMessages.title} />
      </Card.Title>
      <Offset vertical={2} />
      <Input
        label={LoginWrapMessages.username}
        name="username"
        autoComplete="off"
        fullWidth
        autoFocus
        validate={[required]}
      />
      <Input
        label={LoginWrapMessages.password}
        name="password"
        type="password"
        fullWidth
        validate={[required]}
      />
      <LoginButtonWrap>
        <LoginButton
          form="forms/LOGIN"
          text={LoginWrapMessages.login}
          pendingText={LoginWrapMessages.loginPending}
          autoComplete="off"
        />
      </LoginButtonWrap>
      <Offset vertical={0.5} />
    </LoginFields>
  </LoginFormStyle>
)

LoginForm.propTypes = {
  handleSubmit: PropTypes.func,
}

export const WrappedLoginForm = reduxForm({
  form: 'forms/LOGIN',
  onSubmit: async (values, dispatch) => {
    await dispatch(login())
  },
})(LoginForm)

export const Login = ({ isLoggedIn, children }) =>
  isLoggedIn ? children : <WrappedLoginForm />

Login.propTypes = {
  isLoggedIn: PropTypes.bool,
  children: PropTypes.node,
}

export default connect(state => ({ isLoggedIn: selectIsLoggedIn(state) }))(
  Login,
)
