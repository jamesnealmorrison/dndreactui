import { defineMessages } from 'react-intl'

export default defineMessages({
  title: {
    id: 'app.containers.LoginWrap.title',
    defaultMessage: 'MSE Login',
  },
  password: {
    id: 'app.containers.LoginWrap.password',
    defaultMessage: 'Password',
  },
  username: {
    id: 'app.containers.LoginWrap.username',
    defaultMessage: 'Username',
  },
  login: {
    id: 'app.containers.LoginWrap.login',
    defaultMessage: 'Login',
  },
  loginPending: {
    id: 'app.containers.LoginWrap.loginPending',
    defaultMessage: 'Logging in…',
  },
})
