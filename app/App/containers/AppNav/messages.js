import { defineMessages } from 'react-intl'

export default defineMessages({
  dnd: {
    id: 'app.containers.AppNav.dnd',
    defaultMessage: 'D&D',
  },
  logout: {
    id: 'app.containers.AppNav.logout',
    defaultMessage: 'Logout',
  },
})
