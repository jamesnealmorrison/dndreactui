import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import React from 'react'

import {
  AppLogo,
  AppNavBackdrop,
  AppNavLink,
  AppNavMain,
  AppNavOffset,
  AppNavSection,
  AppNavToggle,
  LogoutButton,
} from 'App/containers/AppNav/AppNav.style'
import { closeNav, logout, openNav } from 'store/app/app.actions'
import { selectIsNavOpen } from 'store/app/app.selectors'
import AppNavMessages from 'App/containers/AppNav/messages'
import SVGIcon from 'App/shared/components/SVGIcon'

export const AppNavBase = ({ isOpen, openNav, closeNav, children, logout }) => (
  <React.Fragment>
    <AppNavMain isNavOpen={isOpen}>
      <AppNavToggle onClick={isOpen ? closeNav : openNav}>
        <SVGIcon name="navigation.arrow_back" />
      </AppNavToggle>
      <AppLogo>
        <FormattedMessage {...AppNavMessages.dnd} />
      </AppLogo>
      {children}
      <LogoutButton onClick={logout}>
        <FormattedMessage {...AppNavMessages.logout} />
      </LogoutButton>
    </AppNavMain>
    <AppNavBackdrop isVisible={isOpen} onClick={closeNav} />
  </React.Fragment>
)

AppNavBase.propTypes = {
  isOpen: PropTypes.bool,
  openNav: PropTypes.func,
  closeNav: PropTypes.func,
  children: PropTypes.node,
  logout: PropTypes.func,
}

const AppNav = connect(
  state => ({
    isOpen: selectIsNavOpen(state),
  }),
  { openNav, closeNav, logout },
)(AppNavBase)

AppNav.Section = AppNavSection
AppNav.Offset = connect(state => ({
  isOpen: selectIsNavOpen(state),
}))(AppNavOffset)
AppNav.Link = AppNavLink

export default AppNav
