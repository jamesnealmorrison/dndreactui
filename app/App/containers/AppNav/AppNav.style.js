import { NavLink } from 'react-router-dom'
import styled, { css, keyframes } from 'styled-components'

import { desktopOnly, widescreenOnly } from 'helpers/styleMixins/media'
import {
  materialAnimationDefault,
} from 'helpers/styleMixins/animations.style'
import {
  shadow16dp,
  shadow2dp,
} from 'helpers/styleMixins/shadows.style'
import Button from 'App/shared/components/Button'
import ifProp from 'helpers/ifProp'
import theme from 'theme'
import withRipple from 'App/shared/higherOrderComponents/withRipple'

const navWidth = '16rem'

export const AppNavMain = styled.aside`
  position: fixed;
  top: 0;
  bottom: 0;
  left: -${navWidth};
  width: ${navWidth};
  background: white;
  z-index: 3;
  box-shadow: none;
  ${materialAnimationDefault('0.3s')}
  transition-property: width, left, box-shadow;
  will-change: width, left, box-shadow;
  ${ifProp('isNavOpen', css`
    left: 0;
    ${shadow16dp()};
    ${widescreenOnly(css`
      left: 0;
      ${shadow2dp()}
    `)}
  `)}
`

const fadeIn = keyframes`
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
  }
`

export const AppNavBackdrop = styled.div`
  display: none;
  ${ifProp('isVisible', css`
    @media (max-width: ${theme.widescreenBreakpoint - 1}px) {
      cursor: pointer;
      display: block;
      background-color: rgba(0, 0, 0, 0.5);
      position: fixed;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      z-index: 2;
      animation: ${fadeIn} 0.2s ${theme.animationCurveDefault};
    }
  `)}
`

export const AppNavSection = styled.div`
  padding: 0 18px;
  border-bottom: 1px solid ${theme.colorFaint};
  border-top: 1px solid ${theme.colorFaint};
  &:last-of-type {
    margin-bottom: 3rem;
  }
`

export const AppNavOffset = styled.div`
  position: relative;
  padding-left: 0;
  transition-property: padding-left;
  ${materialAnimationDefault('0.3s')}
  z-index: 1;
  ${ifProp('isOpen', css`
    ${widescreenOnly(css`padding-left: ${navWidth};`)}
  `)}
`

export const AppNavToggle = styled(Button.Icon)`
  display: none;
  ${desktopOnly(css`
    display: block;
    position: absolute;
    right: 16px;
    top: 0.75rem;
    margin: 0.25rem;
    z-index: 2;
  `)}
`

export const AppLogo = styled.h1`
  display: flex;
  align-items: center;
  padding: 0 18px;
  width: 16rem;
  height: 64px;
  font-size: 28px;
  z-index: 1;
  color: ${theme.colorPrimary};
  pointer-events: none;
  user-select: none;
  margin-bottom: 3rem;
`

const activeClassName = 'nav-item-active'

export const AppNavLink = withRipple({ dark: true })(styled(NavLink).attrs({
  activeClassName,
})`
  position: relative;
  height: 3rem;
  display: flex;
  align-items: center;
  padding-left: 1.125rem;
  font-size: 0.875rem;
  color: ${theme.colorDark};
  text-decoration: none;
  &.${activeClassName} {
    background-color: ${theme.colorOffWhite};
    font-weight: bold;
    letter-spacing: 0.48px;
    word-spacing: 1px;
  }
`)

export const LogoutButton = styled(Button)`
  position: absolute;
  bottom: 12px;
  left: 12px;
  font-size: 0.75rem;
  padding: 0 0.5rem;
  height: 28px;
  color: ${theme.colorMedium};
`
