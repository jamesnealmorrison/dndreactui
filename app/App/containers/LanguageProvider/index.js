import { IntlProvider } from 'react-intl'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import React from 'react'

import { selectLocale } from 'store/app/app.selectors'

export function LanguageProvider({ locale, messages, children }) {
  return (
    <IntlProvider locale={locale} key={locale} messages={messages[locale]}>
      {React.Children.only(children)}
    </IntlProvider>
  )
}

LanguageProvider.propTypes = {
  locale: PropTypes.string,
  messages: PropTypes.object,
  children: PropTypes.element.isRequired,
}

export default connect(state => ({
  locale: selectLocale(state),
}))(LanguageProvider)
