import PropTypes from 'prop-types'
import React from 'react'

import {
  ErrorContainer,
  ErrorMessage,
  ErrorTitle,
} from 'App/containers/ErrorProvider/ErrorPage/ErrorPage.style'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

export const ErrorPageBase = ({ title, message }) => (
  <React.Fragment>
    <ErrorContainer>
      <ErrorTitle>{title}</ErrorTitle>
      <ErrorMessage>{message}</ErrorMessage>
    </ErrorContainer>
  </React.Fragment>
)

ErrorPageBase.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
}

export default translateProps(['title', 'message'])(ErrorPageBase)
