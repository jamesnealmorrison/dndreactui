import styled from 'styled-components'

import Container from 'App/shared/components/Container'
import theme from 'theme'

export const ErrorContainer = styled(Container)`
  text-align: center;
  margin-top: 15vh;
`

export const ErrorTitle = styled.h1`
  font-size: 3rem;
  line-height: 3rem;
  margin-bottom: 0.5rem;
  color: ${theme.colorDark};
`
export const ErrorMessage = styled.p`
  margin: 0;
  font-size: 1.25rem;
  color: ${theme.colorDanger};
`
