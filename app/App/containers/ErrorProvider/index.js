import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import React from 'react'

import { ErrorMessageRecord } from 'store/errors/errors.records'
import { clearError } from 'store/errors/errors.actions'
import {
  selectCurrentError,
  selectHasError,
} from 'store/errors/errors.selectors'
import ErrorModal from 'App/containers/ErrorProvider/ErrorModal'
import ErrorPage from 'App/containers/ErrorProvider/ErrorPage'

export const ErrorProvider = ({
  hasError,
  error,
  dispatchClearError,
  children,
}) => {
  if (hasError && error.pageError) {
    return <ErrorPage title={error.title} message={error.message} />
  }

  return (
    <React.Fragment>
      {children}
      <ErrorModal
        isOpen={hasError}
        title={error && error.title}
        message={error && error.message}
        clearError={dispatchClearError}
      />
    </React.Fragment>
  )
}

ErrorProvider.propTypes = {
  error: PropTypes.instanceOf(ErrorMessageRecord),
  hasError: PropTypes.bool,
  dispatchClearError: PropTypes.func,
  children: PropTypes.node,
}

export default connect(
  state => ({
    error: selectCurrentError(state),
    hasError: selectHasError(state),
  }),
  {
    dispatchClearError: clearError,
  },
)(ErrorProvider)
