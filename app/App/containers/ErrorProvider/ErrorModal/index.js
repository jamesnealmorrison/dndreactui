import PropTypes from 'prop-types'
import React from 'react'

import Button from 'App/shared/components/Button'
import ErrorText from 'App/shared/components/ErrorText'
import Modal from 'App/shared/components/Modal'
import SVGIcon from 'App/shared/components/SVGIcon'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

export const ErrorModalBase = ({ isOpen, title, message, clearError }) => (
  <Modal
    isOpen={isOpen}
    shouldCloseOnOverlayClick={false}
    shouldCloseOnEsc={false}
    size={10}
    alert
  >
    <Modal.Title>
      <ErrorText>
        <SVGIcon name="alert.warning" size={32} />
      </ErrorText>
      &nbsp;&nbsp;
      {title}
    </Modal.Title>
    <Modal.Content>
      <ErrorText>{message}</ErrorText>
    </Modal.Content>
    <Modal.Actions>
      <Button onClick={clearError}>Dismiss</Button>
    </Modal.Actions>
  </Modal>
)

ErrorModalBase.propTypes = {
  isOpen: PropTypes.bool,
  title: PropTypes.string,
  message: PropTypes.string,
  clearError: PropTypes.func,
}

export default translateProps(['title', 'message'])(ErrorModalBase)
