import { defineMessages } from 'react-intl'

export default defineMessages({
  home: {
    id: 'app.containers.app.home',
    defaultMessage: 'Home',
  },
  users: {
    id: 'app.containers.app.users',
    defaultMessage: 'Users',
  },
  kitchenSink: {
    id: 'app.containers.app.kitchenSink',
    defaultMessage: 'Kitchen Sink',
  },
})
