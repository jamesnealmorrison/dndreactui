import { Route, Switch } from 'react-router-dom'
import React from 'react'

import NewPlayerCharacter from 'App/routes/PlayerCharactersPage/routes/NewPlayerCharacter'
// import Inventory from 'App/routes/PlayerCharactersPage/routes/Inventory'
// import ItemInfo from 'App/routes/PlayerCharactersPage/routes/Lookup/routes/ItemInfo'
// import LocationOverview from 'App/routes/PlayerCharactersPage/routes/LocationOverview'
// import Lookup from 'App/routes/PlayerCharactersPage/routes/Lookup'
// import Filters from 'App/shared/containers/Filters'
// import ViewAllMSE from 'App/routes/PlayerCharactersPage/routes/ViewAllMSE'

const PlayerCharactersPage = () => (
  <Switch>
    {/* <Route exact path="/playerCharacters" component={LocationOverview} /> */ }
    <Route path="/playerCharacters/newPlayerCharacter" component={NewPlayerCharacter} />
    {/*<Route exact path="/playerCharacters/lookup" component={Lookup} />
    <Route path="/playerCharacters/lookup/:id" component={ItemInfo} />
    <Route path="/playerCharacters/inventory" component={Inventory} />
    <Route path="/playerCharacters/viewAllMSE/filters" component={Filters} />
    <Route path="/playerCharacters/viewAllMSE" component={ViewAllMSE} /> */}
  </Switch>
)

export default PlayerCharactersPage
