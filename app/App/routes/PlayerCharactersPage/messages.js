import { defineMessages } from 'react-intl'

export default defineMessages({
  title: {
    id: 'app.containers.PlayerCharactersPage.title',
    defaultMessage: 'Dungeons and Dragons',
  },
})
