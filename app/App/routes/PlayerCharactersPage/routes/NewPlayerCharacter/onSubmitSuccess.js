export default function onSubmitSuccess(values, dispatch, props) {
  document.querySelector('[name="assetTag"]').focus()
  props.resetField('assetTag')
  props.getItems()
}
