import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux'
import { list } from 'react-immutable-proptypes'
import { reduxForm, formValueSelector } from 'redux-form/immutable'
import PropTypes from 'prop-types'
import React from 'react'
import compose from 'lodash/fp/flowRight'
import get from 'lodash/fp/get'
import isNil from 'lodash/fp/isNil'
import moment from 'moment'
import propEq from 'lodash/fp/matchesProperty'

import { required } from 'helpers/validators'
import { selectUserLocation } from 'store/app/app.selectors'
import { selectRaceById } from 'store/races/races.selectors'
import Button from 'App/shared/components/Button'
import Container from 'App/shared/components/Container'
import DividedTextfield from 'App/shared/components/DividedTextfield'
import RaceSelect from 'App/shared/containers/RaceSelect'
import RaceChoiceSelect from 'App/shared/containers/RaceChoiceSelect'
import DndClassSelect from 'App/shared/containers/DndClassSelect'
import ErrorText from 'App/shared/components/ErrorText'
import Offset from 'App/shared/components/Offset'
import connectField from 'App/shared/higherOrderComponents/connectField'
import messages from 'App/routes/PlayerCharactersPage/routes/NewPlayerCharacter/messages'
import onMount from 'App/shared/higherOrderComponents/onMount'
import onPropChange from 'App/shared/higherOrderComponents/onPropChange'
import resetField from 'helpers/resetField'

import {
  FieldInputWrap,
  PlayerCharacterCard,
  PlayerCharacterCardBody,
  PlayerCharacterCardHeader,
  PlayerCharacterCardHeaderDivider,
  InstallWrap,
} from './NewPlayerCharacter.style'
import onSubmit from './onSubmit'
import onSubmitSuccess from './onSubmitSuccess'

// const ConnectedDividedTextfield = connectField(DividedTextfield)
const ConnectedRaceSelect = connectField(RaceSelect)
const ConnectedRaceChoiceSelect = connectField(RaceChoiceSelect)
const ConnectedDndClassSelect = connectField(DndClassSelect)

export class NewPlayerCharacterPage extends React.PureComponent {
  hasRaceChoices = () => {
    const selectedRace = this.props.selectedRace;
    if (selectedRace == null || selectedRace.raceChoices == null) {
      return false;
    } else if (selectedRace.raceChoices.length > 0) {
      return true;
    }
    return false;
  }

  render() {
    const {
      handleSubmit,
      error,
      submitting,
      invalid,
      pristine,
      selectedRaceId,
      selectedRace,
      selectedDndClassId
    } = this.props

    return (
      <Container>
        <Offset vertical={3} />
        <form onSubmit={handleSubmit}>
            <PlayerCharacterCard>
              <PlayerCharacterCardHeader>
                <FieldInputWrap>
                  <ConnectedRaceSelect
                    labelOnSide
                    name="race"
                    validate={[required]}
                    />
                </FieldInputWrap>
                {selectedRace && selectedRace.raceChoices && selectedRace.raceChoices.map(choice => (
                  <FieldInputWrap>
                    <ConnectedRaceChoiceSelect
                      labelOnSide
                      name={choice.nameOfChoice}
                      validate={[required]}
                      options={choice.validChoices}
                      key={choice.nameOfChoice}
                    />
                  </FieldInputWrap>
                ))}
                <FieldInputWrap>
                  <ConnectedDndClassSelect
                    labelOnSide
                    name="dndClass"
                    validate={[required]}
                  />
                </FieldInputWrap>
                {/*<ConnectedMakeSelect
                  name="make"
                  labelOnSide
                  fillSpace
                  disabled={isNil(selectedEquipmentCodeId)}
                  filter={propEq('equipmentCodeId', selectedEquipmentCodeId)}
                  descriptionRenderer={undefined}
                  validate={[required]}
                />
                <PlayerCharacterCardHeaderDivider />
                <ConnectedModelSelect
                  name="model"
                  fillSpace
                  labelOnSide
                  disabled={isNil(selectedMakeId)}
                  filter={propEq('makeId', selectedMakeId)}
                  descriptionRenderer={undefined}
                  validate={[required]}
                />*/}
              </PlayerCharacterCardHeader>
              {/*
              <PlayerCharacterCardBody>
                <FieldInputWrap>
                  <ConnectedDividedTextfield
                    name="assetTag"
                    maxLength={12}
                    autoComplete="off"
                    label={messages.assetTag}
                    validate={[required]}
                  />
                </FieldInputWrap>
                {error && (
                  <React.Fragment>
                    <p>
                      <ErrorText>{error}</ErrorText>
                    </p>
                    <Offset vertical={1.5} />
                  </React.Fragment>
                )}
                <Button.Raised
                  accent
                  type="submit"
                  disabled={pristine || submitting || invalid}
                  data-test-id="submitInstall"
                >
                  <FormattedMessage
                    {...(submitting ? messages.installing : messages.install)}
                  />
                </Button.Raised>
              </PlayerCharacterCardBody>
              */}
            </PlayerCharacterCard>
        </form>
      </Container>
    )
  }
}

NewPlayerCharacterPage.propTypes = {
  handleSubmit: PropTypes.func,
  resetField: PropTypes.func,
  dispatchGetItems: PropTypes.func,
  submitting: PropTypes.bool,
  error: PropTypes.string,
  invalid: PropTypes.bool,
  pristine: PropTypes.bool,
  selectedRaceId: PropTypes.number,
  selectedRaceChoices: PropTypes.array,
  selectedDndClassId: PropTypes.number,
  installedItems: list,
}

export const NEW_PLAYER_CHARACTER = 'forms/NEW_PLAYER_CHARACTER'

export const mapStateToProps = state => {
  const raceId = get(
    'id',
    formValueSelector(NEW_PLAYER_CHARACTER)(state, 'race'),
  );
  const race = selectRaceById(raceId)(state);
  const raceChoices = (race && race.raceChoices) ? race.raceChoices.map(choice => {
    return {
      nameOfChoice: choice.nameOfChoice,
      value: get(
        'id',
        formValueSelector(NEW_PLAYER_CHARACTER)(state, choice.nameOfChoice),
      )
    }
  }) : undefined;

  return {
    selectedRaceId: raceId,
    selectedDndClassId: get(
      'id',
      formValueSelector(NEW_PLAYER_CHARACTER)(state, 'dndClass'),
    ),
    selectedRace: race,
    selectedRaceChoices: raceChoices
  }
}

export const mapDispatchToProps = {
  resetField: resetField(NEW_PLAYER_CHARACTER),
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({
    form: NEW_PLAYER_CHARACTER,
    onSubmit,
    onSubmitSuccess,
  }),
  onPropChange('selectedRace', p => {
    p.resetField('Sub Race');
    p.resetField('Tool Proficiency');
    p.resetField('Cantrip');
  }),
)(NewPlayerCharacterPage)
