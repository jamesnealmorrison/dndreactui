import { defineMessages } from 'react-intl'

export default defineMessages({
  title: {
    id: 'app.containers.PlayerCharactersPage.NewPlayerCharacter.title',
    defaultMessage: 'New Player Character',
  },
  install: {
    id: 'app.containers.PlayerCharactersPage.NewPlayerCharacter.newPlayerCharacter',
    defaultMessage: 'New Player Character',
  },
  installing: {
    id: 'app.containers.PlayerCharactersPage.NewPlayerCharacter.creating',
    defaultMessage: 'Creating…',
  },
  code: {
    id: 'app.containers.PlayerCharactersPage.NewPlayerCharacter.code',
    defaultMessage: 'Code',
  },
})
