import styled, { css } from 'styled-components'

import { phoneOnly, widescreenOnly } from 'helpers/styleMixins/media'
import { shadow2dp } from 'helpers/styleMixins/shadows.style'
import theme from 'theme'

export const PlayerCharacterCard = styled.div`
  ${shadow2dp()}
  border-radius: 3px;
  background: white;
  width: 300px;
  height: 1000px;
  max-width: 100%;
  ${phoneOnly(css`
    width: calc(100% + 2rem);
    max-width: none;
    margin: 0 -1rem;
    border-radius: 0;
    padding-bottom: 2rem;
  `)};
`

export const PlayerCharacterCardHeader = styled.div`
  border-bottom: 1px solid ${theme.colorFaint};
  height: 10rem;
  display: flex;
  flex-direction: column;
  align-items: flowRight;
  padding: 0 0.5rem 0 1.5rem;
  ${phoneOnly(css`
    display: block;
    height: auto;
    padding: 1.5rem 1rem 1.5rem 1rem;
  `)};
`

export const PlayerCharacterCardBody = styled.div`
  padding: 2rem 1.5rem;
  ${phoneOnly(css`
    padding: 1.5rem 1rem;
  `)};
`

export const PlayerCharacterCardHeaderDivider = styled.div`
  width: 1rem;
  flex-shrink: 0;
  ${phoneOnly(css`
    width: 0;
    height: 0.75rem;
  `)};
`

export const FieldInputWrap = styled.div`
  margin-top: 0.5rem;
  margin-bottom: 0.5rem;
`
