import { FormattedMessage } from 'react-intl'
import React from 'react'

import NonMobileContent from 'App/shared/components/NonMobileContent'
import PageHeader from 'App/containers/PageHeader'
import installMessages from 'App/routes/PlayerCharactersPage/routes/NewPlayerCharacter/messages'
// import inventoryMessages from 'App/routes/PlayerCharactersPage/routes/Inventory/messages'
// import locationOverviewMessages from 'App/routes/PlayerCharactersPage/routes/LocationOverview/messages'
// import lookupMessages from 'App/routes/PlayerCharactersPage/routes/Lookup/messages'
import messages from 'App/routes/PlayerCharactersPage/messages'
// import viewAllMSEMessages from 'App/routes/PlayerCharactersPage/routes/ViewAllMSE/messages'

const PlayerCharactersPageHeader = () => (
  <PageHeader title={messages.title}>
    <PageHeader.Tabs>
      {/*<PageHeader.Tab exact to="/playerCharacters">
        <FormattedMessage {...locationOverviewMessages.title} />
      </PageHeader.Tab>*/}
      <PageHeader.Tab to="/playerCharacters/newPlayerCharacter">
        <FormattedMessage {...installMessages.title} />
      </PageHeader.Tab>
      {/*<PageHeader.Tab to="/inventory/lookup">
        <FormattedMessage {...lookupMessages.title} />
      </PageHeader.Tab>
      <PageHeader.Tab to="/inventory/inventory">
        <FormattedMessage {...inventoryMessages.title} />
      </PageHeader.Tab>
      <NonMobileContent>
        <PageHeader.Tab to="/inventory/viewAllMSE">
          <FormattedMessage {...viewAllMSEMessages.title} />
        </PageHeader.Tab>
      </NonMobileContent> */}
    </PageHeader.Tabs>
  </PageHeader>
)

export default PlayerCharactersPageHeader
