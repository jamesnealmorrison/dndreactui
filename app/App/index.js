import { Redirect, Route, Switch, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import React from 'react'
import compose from 'lodash/fp/flowRight'

import {
  AppLoading,
  AppLoadingLogo,
  AppLoadingWrap,
  PageWrap,
} from 'App/components/App.style'
import { getRaces } from 'store/races/races.actions'
import { screenFormChange } from 'store/app/app.actions'
import { withAlertError } from 'helpers/errorHandlers'
import AppNav from 'App/containers/AppNav'
// import RacePage from 'App/routes/RacePage/routes/RacePage'
// import RacePageHeader from 'App/routes/EquipmentCodesPage/routes/EquipmentCodePage/EquipmentCodePageHeader'
// import EquipmentCodesPage from 'App/routes/EquipmentCodesPage'
// import EquipmentCodesPageHeader from 'App/routes/EquipmentCodesPage/EquipmentCodesPageHeader'
import ErrorProvider from 'App/containers/ErrorProvider'
import PlayerCharactersPage from 'App/routes/PlayerCharactersPage'
import PlayerCharactersPageHeader from 'App/routes/PlayerCharactersPage/PlayerCharactersPageHeader'
import MessageProvider from 'App/containers/MessageProvider'
// import NewEquipmentCodePage from 'App/routes/EquipmentCodesPage/routes/NewEquipmentCodePage'
import PageScroller from 'App/shared/components/PageScroller'
import Progress from 'App/shared/components/Progress'
import onMount from 'App/shared/higherOrderComponents/onMount'
import onScreenFormChange from 'helpers/onScreenFormChange'
import withState from 'App/shared/higherOrderComponents/withState'

export function App({ isLoading }) {
  return isLoading ? (
    <AppLoading>
      <AppLoadingWrap>
        <AppLoadingLogo>Dungeons & Dragons</AppLoadingLogo>
        <Progress indeterminate width="70vw" maxWidth="30rem" />
      </AppLoadingWrap>
    </AppLoading>
  ) : (
    <React.Fragment>
      <AppNav>
        <AppNav.Link to="/playerCharacters">Player Characters</AppNav.Link>
        {/* <AppNav.Link to="/equipmentCodes">Equipment Codes</AppNav.Link>
        <AppNav.Link to="/makes">Makes</AppNav.Link>
        <AppNav.Link to="/models">Models</AppNav.Link> */}
      </AppNav>
      {/* prettier-ignore */}
      <AppNav.Offset>
        <PageWrap id="page-wrap">
          <Switch>
            <Route path="/playerCharacters" component={PlayerCharactersPageHeader} />

            {/*<Route exact path="/equipmentCodes" component={EquipmentCodesPageHeader} />
            <Route exact path="/equipmentCodes/new" component={EquipmentCodesPageHeader} />
            <Route path="/equipmentCodes/:id" component={EquipmentCodePageHeader} />

            <Route exact path="/models" component={ModelsPageHeader} />
            <Route path="/models/new" component={ModelsPageHeader} />
            <Route path="/models/:id" component={ModelPageHeader} />

            <Route exact path="/makes" component={MakesPageHeader} />
            <Route path="/makes/new" component={MakesPageHeader} />
            <Route path="/makes/:id" component={MakePageHeader} />
            */}
          </Switch>
          <PageScroller id="page-scroller">
            <ErrorProvider>
              <Switch>
                <Redirect exact from="/" to="playerCharacters" />

                <Route path="/playerCharacters" component={PlayerCharactersPage} />
{/*
                <Route exact path="/equipmentCodes" component={EquipmentCodesPage} />
                <Route exact path="/equipmentCodes/new" component={NewEquipmentCodePage} />
                <Route path="/equipmentCodes/:id" component={EquipmentCodePage} />

                <Route exact path="/models" component={ModelsPage} />
                <Route path="/models/new" component={NewModelPage} />
                <Route path="/models/:id" component={ModelPage} />

                <Route exact path="/makes" component={MakesPage} />
                <Route path="/makes/new" component={NewMakePage} />
                <Route path="/makes/:id" component={MakePage} />
*/}
              </Switch>
            </ErrorProvider>
          </PageScroller>
        </PageWrap>
      </AppNav.Offset>
      <MessageProvider />
    </React.Fragment>
  )
}

App.propTypes = {
  isLoading: PropTypes.bool,
}

export const runOnMount = async ({
  dispatchGetRaces,
  dispatchScreenFormChange,
  setIsLoading,
}) => {
  onScreenFormChange(dispatchScreenFormChange)

  await Promise.all([dispatchGetRaces()])

  setIsLoading(false)
}

export default compose(
  withRouter,
  connect(null, {
    dispatchScreenFormChange: screenFormChange,
    dispatchGetRaces: compose(withAlertError, getRaces),
  }),
  withState('isLoading', 'setIsLoading', true),
  onMount(runOnMount),
)(App)
