export default function setDisplayName(name) {
  return Component => {
    Component.displayName = name // eslint-disable-line no-param-reassign
    return Component
  }
}
