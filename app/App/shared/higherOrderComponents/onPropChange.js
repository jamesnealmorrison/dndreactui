import React from 'react'

const onPropChange = (prop, fn) => Component => {
  class WithOnPropChange extends React.PureComponent {
    componentWillReceiveProps(nextProps) {
      if (nextProps[prop] !== this.props[prop]) {
        fn(nextProps, this.props)
      }
    }

    render() {
      return <Component {...this.props} />
    }
  }

  return WithOnPropChange
}

export default onPropChange
