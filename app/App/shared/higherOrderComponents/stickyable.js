import React from 'react'

import Sticker from 'App/shared/components/Sticker'

/* eslint-disable react/prop-types */
const stickyable = Component => props => (
  <Sticker>
    <div
      style={
        props.sticky
          ? {
              position: 'sticky',
              top: `${props.top || -1}px`,
              zIndex: '1',
            }
          : {}
      }
    >
      <Component {...props} />
    </div>
  </Sticker>
)

export default stickyable
