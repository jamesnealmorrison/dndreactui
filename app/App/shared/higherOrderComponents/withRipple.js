import React from 'react'

import Ripple from 'App/shared/components/Ripple'

/* eslint-disable react/prop-types */
const withRipple = (rippleProps = {}) => Component => props => (
  <Component {...props}>
    {props.children}
    <Ripple {...rippleProps} />
  </Component>
)

export default withRipple
