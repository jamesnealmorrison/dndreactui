import React from 'react'

/* this is really only reliable since we know what kind of devices we will be
deploying to. Generally we should avoid useragent sniffing */
const isDeviceAndroid =
  /Android/i.test(navigator.userAgent) &&
  !/Windows Phone|IEMobile|WPDesktop/i.test(navigator.userAgent)

const isAndroid = Component => props => (
  <Component isAndroid={isDeviceAndroid} {...props} />
)

export default isAndroid
