import { injectIntl } from 'react-intl'
import React from 'react'

import { isMessage } from 'App/shared/higherOrderComponents/translateProps'

/* eslint-disable react/prop-types */
const injectSafeFormatter = Component =>
  injectIntl(
    class extends React.PureComponent {
      safeFormatMessage = message =>
        isMessage(message)
          ? this.props.intl.formatMessage(message, message.values)
          : message

      render() {
        return (
          <Component
            {...this.props}
            safeFormatMessage={this.safeFormatMessage}
          />
        )
      }
    },
  )

export default injectSafeFormatter
