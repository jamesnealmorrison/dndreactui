import React from 'react'

/* Redux form isn't resetting its state because the modal never unmounts,
   therefore previously entered data is lingering in the modal. To fix this we
   force the modal to unmount. */

// eslint-disable-next-line react/prop-types
export default Component => ({ isOpen, ...props }) =>
  isOpen ? <Component {...props} isOpen /> : null
