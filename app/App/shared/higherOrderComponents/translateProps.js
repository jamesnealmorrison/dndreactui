import { injectIntl } from 'react-intl'
import React from 'react'
import allPass from 'lodash/fp/overEvery'
import has from 'lodash/fp/has'
import isObject from 'lodash/fp/isObject'
import reduce from 'lodash/fp/reduce'

export const isMessage = allPass([isObject, has('defaultMessage'), has('id')])

export default function translateProps(propsArray) {
  return Component =>
    injectIntl(({ intl, ...props }) => {
      const formatted = reduce(
        (aggr, key) => {
          /* eslint-disable no-param-reassign */
          const prop = props[key]
          aggr[key] = isMessage(prop)
            ? intl.formatMessage(prop, prop.values)
            : prop
          return aggr
          /* eslint-enable no-param-reassign */
        },
        {},
        propsArray,
      )

      return <Component {...props} {...formatted} />
    })
}
