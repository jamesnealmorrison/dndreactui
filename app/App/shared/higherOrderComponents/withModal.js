import PropTypes from 'prop-types'
import React from 'react'

export default function withModals(name, renderModal) {
  return Component =>
    class ModalControl extends React.Component {
      static propTypes = {
        modals: PropTypes.object,
      }

      state = {
        isOpen: false,
      }

      openModal = () => {
        this.setState({ isOpen: true })
      }

      closeModal = () => {
        this.setState({ isOpen: false })
      }

      render() {
        const modalComponent = React.cloneElement(renderModal(this.props), {
          isOpen: this.state.isOpen,
          onRequestClose: this.closeModal,
        })

        return (
          <React.Fragment>
            <Component
              {...this.props}
              modals={{
                ...(this.props.modals || {}),
                [name]: {
                  isOpen: this.state.isOpen,
                  open: this.openModal,
                  close: this.closeModal,
                },
              }}
            />
            {modalComponent}
          </React.Fragment>
        )
      }
    }
}

export const modalShape = PropTypes.shape({
  isOpen: PropTypes.bool.isRequired,
  open: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
})
