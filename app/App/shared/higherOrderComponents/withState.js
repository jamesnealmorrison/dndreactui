import React from 'react'
import isFunction from 'lodash/fp/isFunction'

const withState = (valueName, updateFn, defaultValue) => Component =>
  class WithStateComponent extends React.Component {
    constructor(props) {
      super(props)
      this.state = {
        stateValue: isFunction(defaultValue)
          ? defaultValue(props)
          : defaultValue,
      }
    }

    updateState = value => {
      this.setState({ stateValue: value })
    }

    render() {
      return React.createElement(Component, {
        ...this.props,
        [valueName]: this.state.stateValue,
        [updateFn]: this.updateState,
      })
    }
  }

export default withState
