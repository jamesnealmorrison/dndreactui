import PropTypes from 'prop-types'
import React from 'react'
import compose from 'lodash/fp/flowRight'
import isUndefined from 'lodash/fp/isUndefined'
import noop from 'lodash/fp/noop'
import omit from 'lodash/fp/omit'

import isAndroid from 'App/shared/higherOrderComponents/isAndroid'

const handleInput = Component =>
  class InputHandler extends React.Component {
    static propTypes = {
      value: PropTypes.any,
      defaultValue: PropTypes.any,
      onChange: PropTypes.func,
      onFocus: PropTypes.func,
      isAndroid: PropTypes.bool,
    }

    static defaultProps = {
      onBlur: noop,
      onChange: noop,
      onFocus: noop,
    }

    state = {
      value: this.props.value || this.props.defaultValue || '',
    }

    componentWillReceiveProps(nextProps) {
      if (nextProps.value !== this.props.value) {
        this.setState({ value: nextProps.value })
      }
    }

    handleChange = e => {
      if (isUndefined(this.props.value)) {
        this.setState({ value: e && e.target ? e.target.value : e })
      }

      this.props.onChange(e)
    }

    avoidKeyboard(element) {
      setTimeout(() => {
        const { top, bottom } = element.getBoundingClientRect()
        if (bottom > window.innerHeight || top < 0) {
          element.scrollIntoView({
            behavior: 'instant',
            block: 'center',
            inline: 'center',
          })
        }
      }, 250)
    }

    handleFocus = e => {
      if (this.props.isAndroid) {
        this.avoidKeyboard(e.target)
      }

      this.props.onFocus(e)
    }

    render() {
      return (
        <Component
          {...omit(['defaultValue'], this.props)}
          value={this.state.value}
          onChange={this.handleChange}
          onFocus={this.handleFocus}
        />
      )
    }
  }

export default compose(
  isAndroid,
  handleInput,
)
