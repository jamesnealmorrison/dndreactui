import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import React from 'react'

import { selectLocation } from 'store/route/route.selectors'

const onQueryChange = fn => Component => {
  class OnQueryChange extends React.Component {
    componentWillReceiveProps(nextProps) {
      if (nextProps.location.search !== this.props.location.search) {
        fn(nextProps)
      }
    }

    render() {
      return <Component {...this.props} />
    }
  }

  OnQueryChange.propTypes = {
    location: PropTypes.shape({
      search: PropTypes.string,
    }),
  }

  return connect(state => ({ location: selectLocation(state) }))(OnQueryChange)
}

export default onQueryChange
