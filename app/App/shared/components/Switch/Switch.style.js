import { transparentize } from 'polished'
import styled, { css } from 'styled-components'

import { materialAnimationDefault } from 'helpers/styleMixins/animations.style'
import { shadow2dp, shadow3dp } from 'helpers/styleMixins/shadows.style'
import ifProp from 'helpers/ifProp'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'

const switchLabelFontSize = 1
const switchLabelHeight = 1.5
const switchTrackHeight = 0.875
const switchTrackLength = 2
const switchThumbSize = 1.25
const switchTrackTop = (switchLabelHeight - switchTrackHeight) / 2
const switchThumbTop = (switchLabelHeight - switchThumbSize) / 2

export const SwitchWrap = setDisplayName('SwitchWrap')(
  styled.div`
    display: inline-block;
    padding: 1.75rem 3rem 1.75rem 0;
  `)

export const SwitchStyle = setDisplayName('SwitchStyle')(
  styled.label`
    position: relative;
    z-index: 1;
    vertical-align: middle;
    display: inline-block;
    box-sizing: border-box;
    width: 100%;
    height: ${switchLabelHeight}rem;
    margin: 0;
    padding: 0;
    overflow: visible;
    padding-left: 1rem;
    -webkit-touch-callout: none;
    user-select: none;
  `)

export const SwitchButton = setDisplayName('SwitchButton')(
  styled.input`
    line-height: ${switchLabelHeight}rem;
    position: absolute;
    width: 0;
    height: 0;
    margin: 0;
    padding: 0;
    opacity: 0;
    -ms-appearance: none;
    -moz-appearance: none;
    -webkit-appearance: none;
    appearance: none;
    border: none;
    &:checked ~ div:first-of-type {
      background: ${transparentize(0.5, theme.colorPrimary)};
    }
  `)


export const Track = setDisplayName('Track')(
  styled.div`
    background: ${theme.colorLight};
    position: absolute;
    left: 0;
    top: ${switchTrackTop}rem;
    height: ${switchTrackHeight}rem;
    width: ${switchTrackLength}rem;
    border-radius: ${switchTrackHeight}rem;
    cursor: pointer;
    ${({ disabled }) =>
      disabled &&
      css`
        border: 2px solid ${theme.colorFaint};
        cursor: auto;
      `};
  `)

export const Thumb = setDisplayName('Thumb')(
  styled.div`
    position: absolute;
    left: 0;
    top: ${switchThumbTop}rem;
    ${materialAnimationDefault('0.28s')}
    transition-property: left;
    &:after {
      content: '';
      display: block;
      background: rgb(250,250,250);
      ${shadow2dp()}
      border-radius: 50%;
      cursor: pointer;
      position: absolute;
      top: 0;
      left: 0;
      height: ${switchThumbSize}rem;
      width: ${switchThumbSize}rem;
    }
    &:before {
      border-radius: 50%;
      content: '';
      display: block;
      opacity: 0;
      background: ${theme.colorMedium};
      ${materialAnimationDefault('0.28s')}
      transition-property: transform, opacity;
      transform: scale(0.1);
      height: 2.75rem;
      width: 2.75rem;
      position: absolute;
      top: -0.75rem;
      left: -0.75rem;
    }
    ${SwitchButton}:disabled ~ & {
      &:after {
        background: rgb(189,189,189);
      }
    }
    ${SwitchButton}:focus:not(:disabled) ~ &,
    ${SwitchButton}:active:not(:disabled) ~ & {
      &:before {
        transform: scale(1);
        opacity: 0.24;
      }
    }
    ${SwitchButton}:checked ~ & {
      left: ${switchTrackLength - switchThumbSize}rem;
      &:before {
        background: ${theme.colorPrimary};
      }
      &:after {
        background: ${theme.colorPrimary};
        ${shadow3dp()};
      }
    }
  `)

export const SwitchLabel = setDisplayName('SwitchLabel')(
  styled.span`
    position: relative;
    cursor: pointer;
    font-size: ${switchLabelFontSize}rem;
    line-height: ${switchLabelHeight}rem;
    margin: 0;
    left: 24px;
    ${ifProp('disabled', css`
      color: rgb(189,189,189);
      cursor: auto;
    `)}
  `)
