import PropTypes from 'prop-types'
import React from 'react'

import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

import {
  SwitchWrap,
  SwitchStyle,
  SwitchButton,
  SwitchLabel,
  Track,
  Thumb,
} from './Switch.style'

export function Switch({ label, disabled, format, ...props }) {
  return (
    <SwitchWrap {...props}>
      <SwitchStyle>
        <SwitchButton type="checkbox" disabled={disabled} {...props} />
        {label && <SwitchLabel disabled={disabled}>{label}</SwitchLabel>}
        <Track disabled={disabled} />
        <Thumb disabled={disabled} />
      </SwitchStyle>
    </SwitchWrap>
  )
}

Switch.propTypes = {
  label: PropTypes.string,
  disabled: PropTypes.bool,
  format: PropTypes.func,
}

export default setDisplayName('Switch')(translateProps(['label'])(Switch))
