import styled, { css } from 'styled-components'

import { desktopOnly, tabletUp } from 'helpers/styleMixins/media'
import ifProp from 'helpers/ifProp'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'

export const Container = setDisplayName('Container')(
  styled.div`
    padding: 0 16px;
    margin: 0 auto;
    width: 100%;
    ${tabletUp(css`
      padding: 0 24px;
    `)}
    ${desktopOnly(css`
      padding: 0 48px;
    `)}
    ${ifProp('voffset', css`
      margin-top: ${p => p.voffset * 16}px;
    `)}
  `)
