import styled, { css } from 'styled-components'

import { SVGIconWrap } from 'App/shared/components/SVGIcon/SVGIcon.style'
import { phoneOnly } from 'helpers/styleMixins/media'
import Button from 'App/shared/components/Button'
import ifProp from 'helpers/ifProp'
import theme from 'theme'

export const List = styled.div`
  ${ifProp(
    'forCard',
    css`
      margin: 0.5rem -2rem -1.5rem -2rem;
      width: calc(100% + 4rem);
      ${phoneOnly(css`
        margin: 0.5rem -1.5rem -1.5rem -1.5rem;
        width: calc(100% + 3rem);
      `)};
    `,
  )};
  padding: 1rem 0;
`

export const ListDivider = styled.hr`
  border: 0;
  margin: 0;
  border-bottom: 1px solid ${theme.colorFaint};
  margin-bottom: 1rem;
`

export const ListSectionTitle = styled.div`
  color: ${theme.primary};
  font-size: 0.875rem;
  font-weight: 500;
  margin-bottom: 1.5rem;
  padding: 0 2rem;
  color: ${theme.colorMedium};
  ${phoneOnly(css`
    padding: 0 1.5rem;
  `)};
`
export const ListItemIcon = styled.div`
  width: 1.5rem;
  height: 1.5rem;
  margin: 0 2rem 0 0;
  ${ifProp('iconRight', css`
    margin: 0 0 0 1rem;
  `)}
  ${SVGIconWrap} {
    height: 1.5rem;
    width: 1.5rem;
  }
  ${ifProp('tight', css`
    margin-right: 0.75rem;
    margin-left: -0.25rem;
    ${ifProp('iconRight', css`
      margin-left: 0.75rem;
      margin-right: -0.25rem;
    `)}
    height: 1rem;
    width: 1rem;
    ${SVGIconWrap} {
      height: 1rem;
      width: 1rem;
    }
  `)}
`

export const ListItemTitle = styled.div`
  font-size: 1em;
  color: ${theme.colorDark};
`

export const ListItemText = styled.div`
  font-size: 0.875em;
  color: ${theme.colorMedium};
`


export const ListItem = styled.div`
  padding: 1rem 2rem;
  display: flex;
  align-items: center;
  font-size: 1rem;
  ${ifProp('selected', css`
    ${ListItemTitle} {
      font-weight: bold;
      }
    ${ListItemText} {
      font-weight: 500;
    }
  `)}
  ${phoneOnly(css`
    padding: 1rem 1.5rem;
  `)};
  ${ifProp('onClick', css`
    cursor: pointer;
  `)}
  ${ifProp('tight', css`
    padding: 0.75rem 1rem;
    font-size: 0.875rem;
  `)}
  -webkit-tap-highlight-color:  rgba(255, 255, 255, 0);
`


export const ListItemContent = styled.div`
  flex: 1;
`

export const ListItemAction = styled(Button)`
  margin-right: -1rem;
`

ListItemAction.defaultProps = {
  primary: true,
}
