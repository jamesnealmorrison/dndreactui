import PropTypes from 'prop-types'
import React from 'react'

import {
  List,
  ListDivider,
  ListItem,
  ListItemAction,
  ListItemContent,
  ListItemText,
  ListItemTitle,
  ListSectionTitle,
  ListItemIcon,
} from 'App/shared/components/List/List.style'
import Ripple from 'App/shared/components/Ripple'

List.Divider = ListDivider
List.SectionTitle = ListSectionTitle
List.Item = ({
  content,
  title,
  actionText,
  icon,
  iconRight,
  onActionClick,
  tight,
  ...props
}) => (
  <ListItem tight={tight} {...props}>
    {icon !== undefined && <ListItemIcon tight={tight}>{icon}</ListItemIcon>}
    <ListItemContent>
      <ListItemTitle>{title}</ListItemTitle>
      {content && <ListItemText>{content}</ListItemText>}
    </ListItemContent>
    {actionText && (
      <ListItemAction onClick={onActionClick}>{actionText}</ListItemAction>
    )}
    <ListItemIcon iconRight tight={tight}>
      {iconRight}
    </ListItemIcon>
    {props.onClick && <Ripple dark />}
  </ListItem>
)

List.Item.propTypes = {
  content: PropTypes.string,
  title: PropTypes.string,
  actionText: PropTypes.string,
  onActionClick: PropTypes.func,
  onClick: PropTypes.func,
  icon: PropTypes.node,
  iconRight: PropTypes.node,
  tight: PropTypes.bool,
}

List.Item.Text = ListItemText
List.Item.Action = ListItemAction

export default List
