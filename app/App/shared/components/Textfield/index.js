import PropTypes from 'prop-types'
import React from 'react'
import compose from 'lodash/fp/flowRight'

import handleInput from 'App/shared/higherOrderComponents/handleInput'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

import {
  ErrorMessage,
  HelperText,
  Input,
  Label,
  Border,
  Textarea,
  TextfieldWrapper,
  RequiredMarker,
} from './Textfield.style'

export function Textfield({
  multiLine,
  label,
  value,
  error,
  helperText,
  alignRight,
  fullWidth,
  onChange,
  required,
  width,
  placeholder,
  className,
  ...props
}) {
  const InputComponent = multiLine ? Textarea : Input
  return (
    <TextfieldWrapper
      setWidth={width}
      alignRight={alignRight}
      fullWidth={fullWidth}
    >
      <InputComponent
        value={value}
        error={Boolean(error)}
        onChange={onChange}
        placeholder={placeholder}
        maxLength={multiLine ? undefined : 255}
        {...props}
      />
      {label && (
        <Label hasValue={Boolean(value)} error={Boolean(error)}>
          {label}
          {required && <RequiredMarker>*</RequiredMarker>}
        </Label>
      )}
      <Border error={Boolean(error)} />
      {error && <ErrorMessage>{error}</ErrorMessage>}
      {helperText && <HelperText>{helperText}</HelperText>}
    </TextfieldWrapper>
  )
}

Textfield.propTypes = {
  error: PropTypes.string,
  helperText: PropTypes.string,
  alignRight: PropTypes.bool,
  fullWidth: PropTypes.bool,
  autoFocus: PropTypes.bool,
  value: PropTypes.string,
  label: PropTypes.string,
  multiLine: PropTypes.bool,
  onChange: PropTypes.func,
  required: PropTypes.bool,
  width: PropTypes.string,
  placeholder: PropTypes.string,
  format: PropTypes.func,
  className: PropTypes.string,
}

Textfield.defaultProps = {
  type: 'text',
}

const WrappedTextfield = compose(
  setDisplayName('Textfield'),
  translateProps(['placeholder', 'label', 'helperText', 'error']),
  handleInput,
)(Textfield)

WrappedTextfield.ErrorMessage = ErrorMessage
WrappedTextfield.HelperText = HelperText
WrappedTextfield.Input = Input
WrappedTextfield.Label = Label
WrappedTextfield.Border = Border
WrappedTextfield.Textarea = Textarea
WrappedTextfield.TextfieldWrapper = TextfieldWrapper
WrappedTextfield.RequiredMarker = RequiredMarker

export default WrappedTextfield
