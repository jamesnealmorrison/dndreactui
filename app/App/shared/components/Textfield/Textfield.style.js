import get from 'lodash/fp/get'
import styled, { css } from 'styled-components'

import { materialAnimationDefault } from 'helpers/styleMixins/animations.style'
import ifProp from 'helpers/ifProp'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'

const inputTextPadding = 0.25
const inputTextVerticalSpacing = 1.75
const inputTextFloatingLabelFontsize = 0.75

export const TextfieldWrapper = setDisplayName('TextfieldWrapper')(
  styled.div`
    position: relative;
    font-size: 1rem;
    display: block;
    box-sizing: border-box;
    width: ${ifProp('setWidth', get('setWidth'), '18.75rem')};
    max-width: 100%;
    margin: 0;
    padding: ${inputTextVerticalSpacing}rem 0;
    ${ifProp('alignRight', css`
      text-align: right;
    `)}
    ${ifProp('fullWidth', css`
      width: 100%;
    `)}
    ${ifProp('inline', css`
      display: inline-block;
    `)}
  `)

export const Input = setDisplayName('Input')(
  styled.input`
    border: none;
    border-bottom: 1px solid ${theme.colorFaint};
    display: block;
    font-size: 1rem;
    font-family: ${theme.preferredFont};
    margin: 0;
    padding: ${inputTextPadding}rem 0;
    width: 100%;
    background: none;
    text-align: left;
    color: inherit;
    outline: none;
    -webkit-font-smoothing: antialiased;
    letter-spacing: 0.04em;
    &[type="number"] {
      -moz-appearance: textfield;
    }
    &[type="number"]::-webkit-inner-spin-button,
    &[type="number"]::-webkit-outer-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }
    &:focus {
      outline: none;
    }
    &:disabled {
      background-color: transparent;
      border-bottom: 1px dotted ${theme.colorFaint};
      color: ${theme.colorLight};
    }
    ${ifProp('error', css`
      border-color: ${theme.colorDanger};
      box-shadow: none;
    `)}
    caret-color: ${theme.colorPrimary};
    &::placeholder {
      opacity: 0;
      color: ${theme.textLight};
      ${materialAnimationDefault('0.2s')}
      transition-property: opacity;
    }
    &:focus::placeholder {
      opacity: 0.36;
      color: ${theme.textLight};
    }

    ${ifProp('error', css`
      border-color: ${theme.colorDanger};
      box-shadow: none;
    `)}
  `)

export const Textarea = setDisplayName('Textarea')(
  Input.withComponent('textarea').extend`
    display: block;
  `)

// Styling for the label / floating label.
export const Border = setDisplayName('Border')(
  styled.div`
    background-color: ${theme.colorPrimary};
    bottom: ${inputTextVerticalSpacing}rem;
    height: 2px;
    left: 45%;
    position: absolute;
    ${materialAnimationDefault()}
    visibility: hidden;
    width: 10px;
    ${Input}:focus ~ & {
      left: 0;
      visibility: visible;
      width: 100%;
    }
    ${ifProp('error', css`
      background-color: ${theme.colorDanger};
    `)}
  `)

export const focusedLabel = css`
  color: ${theme.colorMedium};
  font-size : ${inputTextFloatingLabelFontsize}rem;
  top: ${inputTextVerticalSpacing - (inputTextFloatingLabelFontsize + inputTextPadding)}rem;
  visibility: visible;
`

export const Label = setDisplayName('Label')(
  styled.label`
    bottom: 0;
    color: ${theme.colorMedium};
    font-weight: normal;
    font-size: 1rem;
    left: 0;
    right: 0;
    pointer-events: none;
    position: absolute;
    display: block;
    top: ${inputTextPadding + inputTextVerticalSpacing}rem;
    width: 100%;
    overflow: hidden;
    white-space: nowrap;
    text-align: left;
    ${materialAnimationDefault()}
    ${ifProp('hasValue', focusedLabel)}
    ${Input}:focus ~ &, ${Textarea}:focus ~ & {
      ${focusedLabel}
      color: ${theme.colorPrimary};
    }
    ${Input}:disabled ~ &, ${Textarea}:disabled ~ & {
      color: ${theme.colorLight};
      border-color: rgba(35, 31, 32, 0.2);
    }
    ${ifProp('error', css`
      color: ${theme.colorDanger};
      top: ${inputTextVerticalSpacing - (inputTextFloatingLabelFontsize + inputTextPadding)}rem;
      font-size: ${inputTextFloatingLabelFontsize}rem;
      ${Input}:focus ~ &, ${Textarea}:focus ~ & {
        color: ${theme.colorDanger};
      }
    `)}
  `)

export const RequiredMarker = setDisplayName('RequiredMarker')(
  styled.span`
    ${Input}:focus ~ ${Label} &, ${Textarea}:focus ~ ${Label} & {
      color: ${theme.colorDanger};
    }
  `)

export const HelperText = setDisplayName('HelperText')(
  styled.div`
    color: ${theme.colorLight};
    position: absolute;
    font-size: ${inputTextFloatingLabelFontsize}rem;
    margin-top: 0.1875rem;
    display: block;
  `)

export const ErrorMessage = setDisplayName('ErrorMessage')(
  HelperText.extend`
    color: ${theme.colorDanger};
  `)
