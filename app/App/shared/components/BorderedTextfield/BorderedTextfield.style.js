import get from 'lodash/fp/get'
import styled, { css } from 'styled-components'

import ifProp from 'helpers/ifProp'
import theme from 'theme'

export const BorderedTextfieldWrap = styled.div`
  position:relative;
  ${ifProp('width', css`
    width: ${get('width')};
  `)}
  ${ifProp('fillSpace', css`
    flex-grow: 1;
  `)}
`

export const BorderdTextfieldStyle = styled.input`
  height: 36px;
  border: 2px solid ${theme.colorFaint};
  border-radius: 4px;
  padding: 0 0.5rem;
  font-size: 1rem;
  font-family: ${theme.preferredFont};
  color: ${theme.colorDark};
  letter-spacing: 0.04em;
  ${ifProp('monospace', css`
    font-family: ${theme.monoFont};
  `)}
  outline: none;
  text-overflow: ellipsis;
  &:focus {
    border-color: ${theme.colorPrimaryAlt};
  }
  min-width: 0;
  width: 100%;
  ${ifProp('hasError', css`
    border-color: ${theme.colorDanger};
    &:focus {
      border-color: ${theme.colorDanger};
    }
  `)}
  &:disabled {
    background-color: ${theme.colorOffWhite};
    border-color: #e5e5e5;
  }
`
