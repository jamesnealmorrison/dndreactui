import PropTypes from 'prop-types'
import React from 'react'
import compose from 'lodash/fp/flowRight'

import {
  BorderdTextfieldStyle,
  BorderedTextfieldWrap,
} from 'App/shared/components/BorderedTextfield/BorderedTextfield.style'
import { ErrorMessage } from 'App/shared/components/Textfield/Textfield.style'
import handleInput from 'App/shared/higherOrderComponents/handleInput'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

export const BorderedTextfieldBase = ({
  width,
  fillSpace,
  error,
  ...props
}) => (
  <BorderedTextfieldWrap width={width} fillSpace={fillSpace}>
    <BorderdTextfieldStyle {...props} />
    {error && <ErrorMessage>{error}</ErrorMessage>}
  </BorderedTextfieldWrap>
)

BorderedTextfieldBase.propTypes = {
  width: PropTypes.string,
  fillSpace: PropTypes.bool,
  error: PropTypes.string,
}

export default compose(
  translateProps(['error']),
  handleInput,
)(BorderedTextfieldBase)
