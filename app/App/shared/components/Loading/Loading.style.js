import styled from 'styled-components'

import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'

export const LoadingWrap = setDisplayName('LoadingWrap')(styled.div`
  margin: ${({ size }) => size}rem auto;
  text-align: center;
`)

LoadingWrap.defaultProps = {
  size: 3.75,
}
