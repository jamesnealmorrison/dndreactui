import React from 'react'

import { LoadingWrap } from 'App/shared/components/Loading/Loading.style'
import Spinner from 'App/shared/components/Spinner'

export default function Loading(props) {
  return (
    <LoadingWrap {...props}>
      <Spinner />
    </LoadingWrap>
  )
}
