import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import React from 'react'

import Button from 'App/shared/components/Button'
import SVGIcon from 'App/shared/components/SVGIcon'

const BackButton = ({ history }) => (
  <Button onClick={() => history.goBack()} primary>
    <SVGIcon name="navigation.arrow_back" forButtonLeft />Back
  </Button>
)

BackButton.propTypes = {
  history: PropTypes.object.isRequired,
}

export default withRouter(BackButton)
