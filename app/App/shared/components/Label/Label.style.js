import styled, { css } from 'styled-components'

import ifProp from 'helpers/ifProp'
import theme from 'theme'

export const TopLabel = styled.label`
  display: block;
  font-size: 14px;
  color: ${theme.colorMedium};
  font-weight: 500;
  margin-bottom: 0.5rem;
  ${ifProp('hasError', css`
    color: ${theme.colorDanger};
  `)}
`

export const SideLabel = styled(TopLabel)`
  margin-bottom: 0;
  margin-right: 0.5rem;
`

export const LabelWrap = styled.div`
  display: inline-flex;
  flex-direction: column;
  width: ${p => p.width || '200px'};
  ${ifProp('fillSpace', css`
    flex: 1;
  `)}
  ${ifProp('fullWidth', css`
    width: 100%;
  `)}
`

export const SideLabelWrap = styled(LabelWrap)`
  align-items: center;
  flex-direction: row;
`
