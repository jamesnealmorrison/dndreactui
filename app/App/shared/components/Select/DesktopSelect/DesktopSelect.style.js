import { darken } from 'polished'
import ReactSelect from 'react-select'
import styled, { css } from 'styled-components'

import { shadow8dp } from 'helpers/styleMixins/shadows.style'
import SVGIcon from 'App/shared/components/SVGIcon'
import ifProp from 'helpers/ifProp'
import setAlpha from 'helpers/setAlpha'
import theme from 'theme'

const lightRed = setAlpha(theme.colorDanger, 0.5)
const borderAround = (color, size = '1px') => css`
  box-shadow: 0 0 0 ${size} ${color} inset, 0 0 0 ${size} ${color};
`
export const OptionRow = styled.div`
  padding: ${ifProp('hasDescription', '0.75rem 0.5rem', '0.5rem')};
  padding-right: 3rem;
  color: ${theme.colorDark};
  position: relative;
  ${ifProp(
    'isActive',
    css`
      color: ${darken(0.2, theme.colorPrimaryAlt)};
    `,
  )};
`
export const OptionActiveIcon = styled(SVGIcon)`
  position: absolute;
  right: 0.5rem;
  top: 50%;
  margin-top: -0.75em;
`
export const OptionRowTitle = styled.div`
  line-height: 1;
  white-space: nowrap;
`
export const OptionRowDescription = styled.div`
  font-family: ${theme.preferredFont};
  font-size: 13px;
  opacity: 0.7;
  white-space: nowrap;
`

export const StyledDesktopSelect = styled(ReactSelect)`
 width: ${p => p.inputWidth || '200px'};
 flex-grow: 1;
 .Select-control {
   border: 0;
   padding: 0;
   display: flex;
   align-items: center;
   position: static;
   width: auto;
   height: auto;
   border-radius: 3px;
   ${ifProp('monospace', css`
     font-family: ${theme.monoFont};
   `)}
   ${borderAround(theme.colorFaint)}
   cursor: text;
   &:hover {
     ${borderAround(theme.colorFaint)}
     background-color: white;
   }
   ${ifProp('error', css`
     ${borderAround(lightRed)}
     &:hover {
       ${borderAround(lightRed)}
     }
   `)}

   > span:first-child {
     flex: 1;
     overflow: hidden;
     text-overflow: ellipsis;
   }
 }
 .Select-control:after {
     content: "";
     display: block;
     height: 28px;
     width: 25px;
     position: absolute;
     right: 0;
     cursor: pointer;
 }
 .Select-arrow-zone {
   pointer-events: none;
 }
 &.Select.is-open {
   .Select-control:after {
     content: none;
   }
   .Select-arrow-zone {
     pointer-events: auto;
   }
 }
 &.Select.is-disabled > .Select-control {
   background-color: ${theme.colorOffWhite};
   ${borderAround('#e5e5e5')}
   cursor: default;
   &:hover {
     background-color: ${theme.colorOffWhite};
     ${borderAround('#e5e5e5')}
   }
   .Select-input { opacity: 0; }
 }
 &.Select.is-open > .Select-control,
 &.Select.is-focused:not(.is-open) > .Select-control {
   ${borderAround(theme.colorPrimaryAlt)}
   border-radius: 2px;
 }
 &.Select.is-focused > .Select-control {
   ${borderAround(theme.colorPrimaryAlt)}
   border-radius: 2px;
 }
 &.Select--single > .Select-control .Select-input {
   padding-left: 0.5rem;
   display: flex;
   align-items: center;
   > input {
     letter-spacing: 0.04em;
     padding: 0 0 2px 0;
     line-height: 1;
     height: calc(100% - 2px);
   }
 }
 .Select-placeholder,
 &.Select--single > .Select-control .Select-value {
   padding-left: 0.5rem;
   line-height: auto;
   display: flex;
   align-items: center;
   right:24px;
   overflow: hidden;
   padding-right: 0;
   > span {
     width: 100%;
     text-overflow: ellipsis;
     overflow: hidden;
   }
 }
 .Select-input > input:focus {
   background: transparent;
 }
 .Select-menu-outer {
   margin-top: 2px;
   border-radius: 2px;
   width: auto;
   min-width: 100%;
   border: 0;
   z-index: 100;
   max-height: 250px;
   ${shadow8dp()};
 }
 .Select-menu {
   max-height: 250px;
 }
 .Select-option {
   padding: 0;
 }
 .Select-option.is-selected {
   background-color: transparent;
   color: ${darken(0.1, theme.colorPrimaryAlt)};
   &.is-focused {
     color: ${darken(0.15, theme.colorPrimaryAlt)};
   }
 }
 .Select-option.is-focused {
   background-color: ${theme.colorOffWhite};
 }
`
