import 'react-select/dist/react-select.css'

import PropTypes from 'prop-types'
import React from 'react'

import { StyledDesktopSelect } from 'App/shared/components/Select/DesktopSelect/DesktopSelect.style'

export default class DesktopSelect extends React.Component {
  static propTypes = {
    filterText: PropTypes.string,
  }

  render() {
    return (
      <StyledDesktopSelect
        {...this.props}
        clearable={false}
        autosize={false}
        deleteRemoves
        openOnFocus
        tabSelectsValue={Boolean(this.props.filterText)}
      />
    )
  }
}

DesktopSelect.defaultProps = {}
