import PropTypes from 'prop-types'
import React from 'react'

import { OptionStyle } from 'App/shared/components/Select/Select.style'
import SVGIcon from 'App/shared/components/SVGIcon'

const SelectOption = ({ hasValue, isMobile, ...props }) => {
  const iconProps = {}

  if (hasValue && isMobile) {
    iconProps.icon = props.selected ? (
      <SVGIcon primaryAlt name="navigation.check" />
    ) : null
  } else if (hasValue) {
    iconProps.iconRight = props.selected ? (
      <SVGIcon primaryAlt name="navigation.check" />
    ) : null
  }

  return (
    <OptionStyle
      selected={props.selected}
      tight={!isMobile}
      {...iconProps}
      {...props}
    />
  )
}

SelectOption.propTypes = {
  selected: PropTypes.bool,
  hasValue: PropTypes.bool,
  isMobile: PropTypes.bool,
}

export default SelectOption
