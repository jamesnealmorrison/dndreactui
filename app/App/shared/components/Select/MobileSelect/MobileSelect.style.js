import styled, { css } from 'styled-components'

import {
  BorderdTextfieldStyle,
  BorderedTextfieldWrap,
} from 'App/shared/components/BorderedTextfield/BorderedTextfield.style'
import Text from 'App/shared/components/Text'
import ifProp from 'helpers/ifProp'
import theme from 'theme'
import withRipple from 'App/shared/higherOrderComponents/withRipple'

export const MobileOptionWrap = withRipple({ dark: true })(styled.div`
  ${ifProp('willChoose', css`
    background: ${theme.colorOffWhite};
  `)}
  position: relative;
  cursor: pointer;
`)

export const MobileSelectValueStyle = styled(BorderdTextfieldStyle)`
  padding-right: 1.75rem;
  overflow: hidden;
  text-overflow: ellipsis;
`

export const MobileSelectValueWrap = styled(BorderedTextfieldWrap)`
  position: relative;
`

export const MobileSelectValueIcon = styled.div`
  position: absolute;
  right: 0.25rem;
  top: 50%;
  margin-top: -0.75rem;
`

export const MobileSelectEmptyMessage = styled(Text)`
  display: block;
  text-align: center;
  margin-top: 1rem;
  color: ${theme.colorMedium};
  font-style: italic;
  font-size: 1.25rem;
  font-weight: 300;
`
