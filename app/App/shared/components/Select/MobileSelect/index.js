import PropTypes from 'prop-types'
import React from 'react'

import {
  MobileOptionWrap,
  MobileSelectEmptyMessage,
  MobileSelectValueIcon,
  MobileSelectValueStyle,
  MobileSelectValueWrap,
} from 'App/shared/components/Select/MobileSelect/MobileSelect.style'
import Fill from 'App/shared/components/Fill'
import List from 'App/shared/components/List'
import MobileSearchBar from 'App/shared/components/MobileSearchBar'
import Modal from 'App/shared/components/Modal'
import theme from 'theme'

export default class MobileSelect extends React.PureComponent {
  static propTypes = {
    inputWidth: PropTypes.string,
    fillSpace: PropTypes.bool,
    value: PropTypes.any,
    valueKey: PropTypes.string,
    options: PropTypes.array,
    onChange: PropTypes.func,
    optionRenderer: PropTypes.func,
    disabled: PropTypes.bool,
    noResultsText: PropTypes.node,
    onInputChange: PropTypes.func,
    valueRenderer: PropTypes.func,
    arrowRenderer: PropTypes.func,
    filterText: PropTypes.string,
    label: PropTypes.string,
    mobileLabel: PropTypes.string,
    placeholder: PropTypes.string,
    error: PropTypes.string,
  }

  state = {
    isOpen: false,
  }

  handleClose = () => {
    this.setState({ isOpen: false })
  }

  handleOpen = () => {
    this.setState({ isOpen: true })
  }

  handleSelect = option => {
    this.props.onChange(option)
    this.props.onInputChange('')
    this.handleClose()
  }

  handleSubmit = () => {
    if (this.props.filterText && this.props.options.length > 0) {
      this.handleSelect(this.props.options[0])
    }
  }

  render() {
    return (
      <React.Fragment>
        <MobileSelectValueWrap
          width={this.props.inputWidth}
          fillSpace={this.props.fillSpace}
        >
          <MobileSelectValueStyle
            value={this.props.valueRenderer(this.props.value)}
            placeholder={this.props.placeholder}
            disabled={this.props.disabled}
            onClick={this.handleOpen}
            hasError={Boolean(this.props.error)}
            readOnly
          />
          <MobileSelectValueIcon>
            {this.props.arrowRenderer({ isOpen: false })}
          </MobileSelectValueIcon>
        </MobileSelectValueWrap>
        <Modal
          isOpen={this.state.isOpen}
          onRequestClose={this.handleClose}
          overflow="hidden"
        >
          <Modal.Title>
            {this.props.mobileLabel || this.props.label}
          </Modal.Title>
          <Fill style={{ overflow: 'auto', background: theme.colorOffWhite }}>
            <MobileSearchBar
              value={this.props.filterText}
              onChange={this.props.onInputChange}
              onSubmit={this.handleSubmit}
              sticky
            />
            <List>
              {this.props.options.length === 0 && (
                <MobileSelectEmptyMessage>
                  {this.props.noResultsText}
                </MobileSelectEmptyMessage>
              )}
              {this.props.options.map((option, i) => (
                <MobileOptionWrap
                  key={option[this.props.valueKey]}
                  willChoose={Boolean(this.props.filterText.trim()) && i === 0}
                  onClick={() => this.handleSelect(option)}
                >
                  {this.props.optionRenderer(option)}
                </MobileOptionWrap>
              ))}
            </List>
          </Fill>
        </Modal>
      </React.Fragment>
    )
  }
}
