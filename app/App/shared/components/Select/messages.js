import { defineMessages } from 'react-intl'

export default defineMessages({
  noResultsText: {
    id: 'shared.Components.Select',
    defaultMessage: 'No results found…',
  },
})
