import { FormattedMessage } from 'react-intl'
import { list } from 'react-immutable-proptypes'
import PropTypes from 'prop-types'
import React from 'react'
import allPass from 'lodash/fp/overEvery'
import compose from 'lodash/fp/flowRight'

import {
  LabelWrap,
  SideLabel,
  SideLabelWrap,
  TopLabel,
} from 'App/shared/components/Label/Label.style'
import DesktopSelect from 'App/shared/components/Select/DesktopSelect'
import MobileSelect from 'App/shared/components/Select/MobileSelect'
import SVGIcon from 'App/shared/components/SVGIcon'
import SelectMessages from 'App/shared/components/Select/messages'
import SelectOption from 'App/shared/components/Select/SelectOption'
import handleInput from 'App/shared/higherOrderComponents/handleInput'
import injectSafeFormatter from 'App/shared/higherOrderComponents/injectSafeFormatter'
import isAndroid from 'App/shared/higherOrderComponents/isAndroid'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

export class SelectBase extends React.PureComponent {
  static propTypes = {
    labelOnSide: PropTypes.bool,
    label: PropTypes.node,
    isAndroid: PropTypes.bool,
    fillSpace: PropTypes.bool,
    fullWidth: PropTypes.bool,
    width: PropTypes.string,
    error: PropTypes.string,
    filter: PropTypes.func,
    labelKey: PropTypes.string,
    valueKey: PropTypes.string,
    descriptionRenderer: PropTypes.func,
    safeFormatMessage: PropTypes.func,
    value: PropTypes.any,
    maxLength: PropTypes.number,
    options: PropTypes.oneOfType([list, PropTypes.array]),
    placeholder: PropTypes.string,
    disabled: PropTypes.bool,
    noResultsText: PropTypes.shape({
      id: PropTypes.string,
      defaultMessage: PropTypes.string,
    }),
  }

  static defaultProps = {
    noResultsText: <FormattedMessage {...SelectMessages.noResultsText} />,
    descriptionRenderer: () => null,
    filter: () => true,
    options: [],
    placeholder: '',
  }

  constructor(props) {
    super(props)
    this.state = {
      filter: '',
      value: this.getValueFromProps(props),
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({ value: this.getValueFromProps(nextProps) })
    }
  }

  get options() {
    return this.props.options.toJS
      ? this.props.options.toJS()
      : this.props.options
  }

  getValueFromProps(props) {
    if (
      !props.value ||
      (props.value[props.valueKey] && props.value[props.labelKey])
    ) {
      return props.value
    }

    return this.options.find(o => o[props.valueKey] === props.value)
  }

  valueRenderer = () =>
    this.state.value
      ? this.props.safeFormatMessage(this.state.value[this.props.labelKey])
      : ''

  optionRenderer = option => (
    <SelectOption
      isMobile={this.props.isAndroid}
      title={this.props.safeFormatMessage(option[this.props.labelKey])}
      content={this.props.descriptionRenderer(option)}
      hasValue={!!this.state.value}
      selected={
        !!this.state.value &&
        option[this.props.valueKey] === this.state.value[this.props.valueKey]
      }
    />
  )

  filterOptions = (options, filter) => {
    const filterByText = option => {
      const trimmedFilter = filter.trim('').toLowerCase()
      return trimmedFilter
        ? this.props
            .safeFormatMessage(option[this.props.labelKey])
            .toLowerCase()
            .startsWith(trimmedFilter)
        : true
    }

    return options.filter(allPass([this.props.filter, filterByText]))
  }

  handleInputChange = value => {
    const filter = this.props.maxLength
      ? value.substring(0, this.props.maxLength)
      : value

    this.setState({ filter })

    return filter
  }

  arrowRenderer({ isOpen }) {
    const icon = isOpen
      ? 'navigation.arrow_drop_up'
      : 'navigation.arrow_drop_down'

    return <SVGIcon name={icon} size={24} />
  }

  render() {
    const Wrap = this.props.labelOnSide ? SideLabelWrap : LabelWrap
    const Label = this.props.labelOnSide ? SideLabel : TopLabel
    const SelectComponent = this.props.isAndroid ? MobileSelect : DesktopSelect

    return (
      <Wrap
        fillSpace={this.props.fillSpace}
        fullWidth={this.props.fullWidth}
        width={this.props.width}
      >
        {this.props.label && (
          <Label hasError={Boolean(this.props.error)}>{this.props.label}</Label>
        )}
        <SelectComponent
          {...this.props}
          options={this.filterOptions(this.options, this.state.filter)}
          optionRenderer={this.optionRenderer}
          filterOptions={options => options}
          valueRenderer={this.valueRenderer}
          onInputChange={this.handleInputChange}
          filterText={this.state.filter}
          tabIndex={this.props.disabled ? -1 : undefined}
          arrowRenderer={this.arrowRenderer}
        />
      </Wrap>
    )
  }
}

export default compose(
  handleInput,
  isAndroid,
  injectSafeFormatter,
  translateProps(['label', 'mobileLabel', 'error', 'placeholder']),
)(SelectBase)
