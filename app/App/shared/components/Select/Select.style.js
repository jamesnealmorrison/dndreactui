import styled from 'styled-components'

import { ListItemText } from 'App/shared/components/List/List.style'
import List from 'App/shared/components/List'

export const OptionStyle = styled(List.Item)`
  ${ListItemText} {
    white-space: nowrap;
  }
`
