import PropTypes from 'prop-types'
import React from 'react'

import { PaginationRecord } from 'store/inventoryItems/inventoryItems.records'
import Button from 'App/shared/components/Button'
import Fill from 'App/shared/components/Fill'
import Offset from 'App/shared/components/Offset'
import SVGIcon from 'App/shared/components/SVGIcon'

import { PaginationStyle, Count } from './Pagination.style'

const Pagination = ({ pagination, onClickPrevious, onClickNext }) => (
  <PaginationStyle>
    <Fill />
    <Count>
      {pagination.currentIndex + 1}–{pagination.currentIndex +
        pagination.numberOfElements}{' '}
      of {pagination.totalElements}
    </Count>
    {(!pagination.first || !pagination.last) && (
      <React.Fragment>
        <Offset horizontal={2} />
        <Button.Icon
          data-test-id="pagination-previous"
          onClick={onClickPrevious}
          disabled={pagination.first}
        >
          <SVGIcon name="hardware.keyboard_arrow_left" />
        </Button.Icon>
        <Offset horizontal={1} />
        <Button.Icon
          data-test-id="pagination-next"
          onClick={onClickNext}
          disabled={pagination.last}
        >
          <SVGIcon name="hardware.keyboard_arrow_right" />
        </Button.Icon>
      </React.Fragment>
    )}
  </PaginationStyle>
)

Pagination.propTypes = {
  pagination: PropTypes.instanceOf(PaginationRecord),
  onClickPrevious: PropTypes.func,
  onClickNext: PropTypes.func,
}

export default Pagination
