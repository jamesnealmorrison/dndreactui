import theme from 'theme'
import styled from 'styled-components'

export const PaginationStyle = styled.div`
  display: flex;
  align-items: center;
`

export const Count = styled.span`
  font-size: 12px;
  color: ${theme.colorMedium};
`
