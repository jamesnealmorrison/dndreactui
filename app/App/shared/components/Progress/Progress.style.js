import { transparentize } from 'polished'
import compose from 'lodash/fp/flowRight'
import get from 'lodash/fp/get'
import multiply from 'lodash/fp/multiply'
import styled, { css, keyframes } from 'styled-components'

import ifProp from 'helpers/ifProp'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'

const progressBarHeight = 0.25

const indeterminate1 = keyframes`
  0% {
    left: 0%;
    width: 0%;
  }
  50% {
    left: 25%;
    width: 75%;
  }
  75% {
    left: 100%;
    width: 0%;
  }
`

const indeterminate2 = keyframes`
  0% {
    left: 0%;
    width: 0%;
  }
  50% {
    left: 0%;
    width: 0%;
  }
  75% {
    left: 0%;
    width: 25%;
  }
  100% {
    left: 100%;
    width: 0%;
  }
`

export const ProgressBase = setDisplayName('ProgressBase')(
  styled.div`
    display: block;
    position: relative;
    height: ${progressBarHeight}rem;
    width: ${({ width }) => width || '31.25rem'};
    max-width: ${({ maxWidth }) => maxWidth || '100%'};
  `,
)

const Bar = styled.div`
  display: block;
  position: absolute;
  top: 0;
  bottom: 0;
  width: 0%;
  transition: width 0.2s ${theme.animationCurveDefault};
`

export const ProgressBar = setDisplayName('ProgressBar')(
  Bar.extend`
    background-color: ${theme.colorPrimary};
    z-index: 1;
    left: 0;
    ${ifProp(
      'indeterminate',
      css`
        background-color: ${theme.colorPrimary};
        animation-name: ${indeterminate1};
        animation-duration: 2s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
      `,
    )} ${ifProp(
      'fill',
      css`
        width: ${compose(multiply(100), get('fill'))}%;
      `,
    )};
  `,
)

export const BufferBar = setDisplayName('BufferBar')(
  Bar.extend`
    background-image: linear-gradient(
        to right,
        ${transparentize(0.3, theme.colorPrimaryContrast)},
        ${transparentize(0.3, theme.colorPrimaryContrast)}
      ),
      linear-gradient(to right, ${theme.colorPrimary}, ${theme.colorPrimary});
    z-index: 0;
    left: 0;
    width: 100%;
  `,
)

export const AuxBar = setDisplayName('AuxBar')(
  Bar.extend`
    right: 0;
    ${ifProp(
      'indeterminate',
      css`
        background-image: linear-gradient(
            to right,
            ${transparentize(0.3, theme.colorPrimaryContrast)},
            ${transparentize(0.3, theme.colorPrimaryContrast)}
          ),
          linear-gradient(
            to right,
            ${theme.colorPrimary},
            ${theme.colorPrimary}
          );
        animation-name: ${indeterminate2};
        animation-duration: 2s;
        animation-iteration-count: infinite;
        animation-timing-function: linear;
      `,
    )};
  `,
)
