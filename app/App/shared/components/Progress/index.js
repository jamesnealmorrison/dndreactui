import PropTypes from 'prop-types'
import React from 'react'

import { ProgressBase, ProgressBar, BufferBar, AuxBar } from './Progress.style'

export default function Progress({ indeterminate, fill, width, maxWidth }) {
  return (
    <ProgressBase width={width} maxWidth={maxWidth}>
      <ProgressBar indeterminate={indeterminate} fill={fill} />
      <BufferBar indeterminate={indeterminate} fill={fill} />
      <AuxBar indeterminate={indeterminate} fill={fill} />
    </ProgressBase>
  )
}

Progress.propTypes = {
  indeterminate: PropTypes.bool,
  fill: PropTypes.number,
  width: PropTypes.string,
  maxWidth: PropTypes.string,
}
