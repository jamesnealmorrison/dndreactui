import always from 'lodash/fp/constant'
import cond from 'lodash/fp/cond'
import propEq from 'lodash/fp/matchesProperty'
import styled, { css } from 'styled-components'

import {
  arrowBottom,
  arrowLeft,
  arrowRight,
  arrowTop,
} from 'helpers/styleMixins/arrow.style'
import ifProp from 'helpers/ifProp'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'

const position = propEq('position')

export const TooltipPosition = setDisplayName('TooltipPosition')(
  styled.div`
    position: fixed;
    transform: scale(0.6);
    opacity: 0;
    z-index: 999999;
    transition: opacity 0.2s ${theme.animationCurveLinearOutSlowIn},
                transform 0.2s ${theme.animationCurveLinearOutSlowIn};
    ${ifProp('isVisible', css`
      opacity: 1;
      transform: none;
    `)}
    ${cond([
      [position('above'), ({ x, y }) => css`
        top: ${y - 7}px;
        left: ${x}px;
        transform-origin: bottom center;
      `],
      [position('below'), ({ x, y }) => css`
        top: ${7 + y}px;
        left: ${x}px;
        transform-origin: top center;
      `],
      [position('left'), ({ x, y }) => css`
        top: ${y}px;
        left: ${x - 7}px;
        transform-origin: center right;
      `],
      [position('right'), ({ x, y }) => css`
        top: ${y}px;
        left: ${x + 7}px;
        transform-origin: center left;
      `],
    ])}
  `)

export const TooltipBase = setDisplayName('TooltipBase')(
  styled.div`
    display: block;
    background: ${theme.colorDark};
    color: #FFF;
    font-size: 10px;
    line-height: 14px;
    padding: 8px;
    border-radius: 2px;
    user-select: none;
    pointer-events: none;
    position: absolute;
    white-space: nowrap;
    text-align: center;
    ${ifProp('large', css`
      font-size: 14px;
      padding: 16px;
    `)}
    ${cond([
      [position('above'), always(css`
        transform: translate(-50%, -100%);
        ${arrowBottom('5px', theme.colorDark)}
      `)],
      [position('below'), always(css`
        transform: translate(-50%, -100%);
        ${arrowTop('5px', theme.colorDark)}
      `)],
      [position('left'), always(css`
        transform: translate(-100%, -50%);
        ${arrowRight('5px', theme.colorDark)}
      `)],
      [position('right'), always(css`
        transform: translate(0%, -50%);
        ${arrowLeft('5px', theme.colorDark)}
      `)],
    ])}
  `)
