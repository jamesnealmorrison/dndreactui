import Portal from 'react-portal'
import PropTypes from 'prop-types'
import React, { Component } from 'react'

import translateProps from 'App/shared/higherOrderComponents/translateProps'

import { TooltipBase, TooltipPosition } from './Tooltip.style'

export class Tooltip extends Component {
  state = {
    isVisible: false,
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.setPosition, true)
    clearTimeout(this.open)
  }

  getPosition() {
    const control = this.control.getBoundingClientRect()
    const horizontalCenter = control.left + control.width / 2
    const verticalCenter = control.top + control.height / 2

    switch (this.props.position) {
      case 'below':
        return { x: horizontalCenter, y: control.bottom }
      case 'left':
        return { x: control.left, y: verticalCenter }
      case 'right':
        return { x: control.right, y: verticalCenter }
      default:
        return { x: horizontalCenter, y: control.top }
    }
  }

  setPosition = () => {
    this.setState({ ...this.getPosition() })
  }

  setControl = ctrl => {
    this.control = ctrl && ctrl.children[0]
  }

  handleOpen = () => {
    this.setState({ isOpened: true })
    this.setPosition()
    window.addEventListener('scroll', this.setPosition, true)
    this.open = setTimeout(() => {
      this.setState({ isVisible: true })
    }, this.props.delay)
  }

  handleClose = () => {
    window.removeEventListener('scroll', this.setPosition, true)
    clearTimeout(this.open)
    this.setState({ isOpened: false, isVisible: false })
  }

  render() {
    const { children, ...props } = this.props
    return (
      <span
        ref={this.setControl}
        onMouseEnter={this.handleOpen}
        onMouseLeave={this.handleClose}
      >
        {children}
        <Portal isOpened={this.state.isOpened}>
          <TooltipPosition {...this.state} {...props}>
            <TooltipBase large={props.large} position={props.position}>
              {props.message}
            </TooltipBase>
          </TooltipPosition>
        </Portal>
      </span>
    )
  }
}

Tooltip.defaultProps = {
  position: 'above',
  delay: 0,
}

Tooltip.propTypes = {
  message: PropTypes.string,
  children: PropTypes.node,
  position: PropTypes.string,
  delay: PropTypes.number,
}

export default translateProps(['message'])(Tooltip)
