import {
  HeaderBar,
  HeaderBarIcon,
  HeaderBarRow,
  HeaderBarTitle,
} from 'App/shared/components/HeaderBar/HeaderBar.style'

HeaderBar.Title = HeaderBarTitle
HeaderBar.Icon = HeaderBarIcon
HeaderBar.Row = HeaderBarRow

export default HeaderBar
