import styled from 'styled-components'

import { shadow4dp } from 'helpers/styleMixins/shadows.style'
import theme from 'theme'

export const HeaderBar = styled.header`
  background-color: ${theme.colorPrimary};
  ${shadow4dp()};
  color: white;
  position: relative;
  z-index: 10;
`

export const HeaderBarRow = styled.div`
  display: flex;
  align-items: center;
  position: relative;
`

export const HeaderBarIcon = styled.div`
  margin: 0 24px;
`

export const HeaderBarTitle = styled.h1`
  font-size: 18px;
  margin: 0;
  padding-left: 0;
  font-weight: 500;
  height: 4rem;
  line-height: 4rem;
  position: relative;
  margin: 0 24px;
`
