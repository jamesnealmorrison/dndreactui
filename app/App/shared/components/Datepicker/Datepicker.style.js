import get from 'lodash/fp/get'
import styled, { css } from 'styled-components'

import { shadow8dp } from 'helpers/styleMixins/shadows.style'
import ifProp from 'helpers/ifProp'
import theme from 'theme'

export const StyleWrap = styled.div`
  position: relative;
  ${ifProp('width', css`
    width: ${get('width')};
  `)}
  ${ifProp('fillSpace', css`
    flex: 1;
  `)}
  .react-datepicker__input-container,
  .react-datepicker-wrapper {
    width: 100%;
  }
  .react-datepicker {
    font-family: ${theme.preferredFont};
    ${shadow8dp()};
    overflow: hidden;
    border: none;
  }
  .react-datepicker-popper[data-placement^="bottom"] {
    margin-top: 1px;
  }
  .react-datepicker__header {
    background: transparent;
    border-color: ${theme.colorFaint};
  }
  .react-datepicker__day--selected,
  .react-datepicker__day--in-selecting-range,
  .react-datepicker__day--in-range {
    background-color: ${theme.colorPrimaryAlt};
  }
  .react-datepicker__current-month,
  .react-datepicker-time__header {
    display: none;
  }
  .react-datepicker__navigation {
    top: 16px;
  }
  .react-datepicker__month-dropdown-container--select,
  .react-datepicker__year-dropdown-container--select {
    position: relative;
    &:after {
      content: '';
      display: block;
      position: absolute;
      right: 8px;
      top: 13px;
      height: 0;
      width: 0;
      border-style: solid;
      border-width: 5px 4px 0 4px;
      border-color: transparent;
      border-top-color: ${theme.colorDark};
    }
    select {
      border-radius: 4px;
      padding: 0 24px 0 8px;
      height: 32px;
      border: 2px solid ${theme.colorFaint};
      line-height: 24px;
      font-size: 14px;
      outline: none;
      cursor: pointer;
      &:focus {
        border-color: ${theme.colorPrimaryAlt};
      }
      -moz-appearance: none;
      &:-moz-focusring {
        color: transparent;
        text-shadow: 0 0 0 #000;
      }
      -webkit-appearance: none;
      &::-ms-expand {
        display: none;
      }
    }t
  }
`
