import PropTypes from 'prop-types'
import React from 'react'
import moment from 'moment'

import BorderedTextfield from 'App/shared/components/BorderedTextfield'

const DatePicker = ({ onChange, value, ...props }) => (
  <BorderedTextfield
    type="date"
    onChange={
      onChange ? e => onChange(moment(e.target.value).toISOString()) : undefined
    }
    value={value ? moment(value).format('YYYY-MM-DD') : ''}
    {...props}
  />
)

DatePicker.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.any,
}

export default DatePicker
