import 'react-datepicker/dist/react-datepicker.css'

import DesktopDatepicker from 'App/shared/components/Datepicker/DesktopDatepicker'
import MobileDatepicker from 'App/shared/components/Datepicker/MobileDatepicker'

const isAndroid =
  /Android/i.test(navigator.userAgent) &&
  !/Windows Phone|IEMobile|WPDesktop/i.test(navigator.userAgent)

export default (isAndroid ? MobileDatepicker : DesktopDatepicker)
