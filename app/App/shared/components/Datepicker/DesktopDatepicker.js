import 'react-datepicker/dist/react-datepicker.css'

import PropTypes from 'prop-types'
import React from 'react'
import ReactDatePicker from 'react-datepicker'
import moment from 'moment'
import noop from 'lodash/fp/noop'

import { StyleWrap } from 'App/shared/components/Datepicker/Datepicker.style'
import BorderedTextfield from 'App/shared/components/BorderedTextfield'

const valueFromProps = p => (p.value ? moment(p.value) : null)
const textValueFromProps = p =>
  p.value ? moment(p.value).format('MM/DD/YYYY') : p.value

export default class Datepicker extends React.Component {
  state = {
    invalidDate: false,
    isTouched: !!this.props.value,
    value: valueFromProps(this.props),
    textValue: textValueFromProps(this.props),
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.props.value) {
      this.setState({
        value: valueFromProps(nextProps),
        textValue: textValueFromProps(nextProps),
      })
    }
  }

  handleInputChange = e => {
    this.setState({ textValue: e.target.value })
    const date = moment(e.target.value, 'MM/DD/YYYY', true)

    if (date.isValid()) {
      this.handleChange(date)
    } else {
      this.setState({ invalidDate: true })
    }
  }

  handleChange = date => {
    this.props.onChange(date ? date.toISOString() : '')
    this.setState({ invalidDate: false })
  }

  handleBlur = () => {
    this.setState({ isTouched: true })
  }

  render() {
    const { onChange, value, ...props } = this.props
    return (
      <StyleWrap {...props}>
        <ReactDatePicker
          customInput={
            <div>
              <BorderedTextfield
                hasError={this.state.isTouched && this.state.invalidDate}
                placeholder="MM/DD/YYYY"
                value={this.state.textValue}
                onChange={this.handleInputChange}
                onBlur={this.handleBlur}
                width="100%"
              />
            </div>
          }
          onChange={this.handleChange}
          selected={this.state.value}
          disabledKeyboardNavigation={!!this.state.textValue}
          showMonthDropdown
          showYearDropdown
          dropdownMode="select"
          {...props}
        />
      </StyleWrap>
    )
  }
}

Datepicker.defaultProps = {
  onChange: noop,
}

Datepicker.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.any,
}
