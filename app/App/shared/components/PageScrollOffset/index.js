import React from 'react'
import ReactDOM from 'react-dom'

const PageScrollOffset = ({ height }) =>
  document.getElementById('page-wrap')
    ? ReactDOM.createPortal(
      <div style={{ height }} />,
        document.getElementById('page-wrap'),
      )
    : null

export default PageScrollOffset
