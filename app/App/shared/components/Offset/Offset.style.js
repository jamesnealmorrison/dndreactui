import get from 'lodash/fp/get'
import styled, { css } from 'styled-components'

import { mobileOnly, phoneOnly } from 'helpers/styleMixins/media'
import ifProp from 'helpers/ifProp'

export const Offset = styled.div`
  ${ifProp('vertical', css`margin-top: ${get('vertical')}rem;`)}
  ${ifProp('horizontal', css`margin-left: ${get('horizontal')}rem; display: inline-block;`)}

  ${ifProp('verticalMobile', mobileOnly(css`margin-top: ${get('verticalMobile')}rem;`))}
  ${ifProp('horizontalMobile', mobileOnly(css`margin-left: ${get('horizontalMobile')}rem; display: inline-block;`))}

  ${ifProp('verticalPhone', phoneOnly(css`margin-top: ${get('verticalPhone')}rem;`))}
  ${ifProp('horizontalPhone', phoneOnly(css`margin-left: ${get('horizontalPhone')}rem; display: inline-block;`))}
`
