import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import Stickyfill from './Stickyfill'

export default class Sticker extends Component {
  static propTypes = {
    media: PropTypes.string,
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.func]),
    forceUpdate: PropTypes.bool,
  }
  constructor(props) {
    super(props)
    this.state = {
      isSticky: false,
    }
  }
  componentDidMount() {
    // eslint-disable-next-line react/no-find-dom-node
    this.stick = ReactDOM.findDOMNode(this)
    if (this.props.media) {
      window.addEventListener('resize', this.handleResize)
      this.handleResize()
    } else {
      this.sticky(this.stick)
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.forceUpdate !== this.props.forceUpdate) {
      this.update()
    }
  }
  componentDidUpdate() {
    this.update()
  }
  componentWillUnmount() {
    if (this.props.media) {
      window.removeEventListener('resize', this.handleResize)
    }
    this.unsticky(this.stick)
  }
  mediaMatch = media => window && window.matchMedia(media).matches
  sticky = stick => {
    Stickyfill.add(stick)
    this.setState({
      isSticky: true,
    })
  }
  unsticky = stick => {
    Stickyfill.remove(stick)
    this.setState({
      isSticky: false,
    })
  }
  update = () => {
    Stickyfill.rebuild()
  }
  handleResize = () => {
    if (this.mediaMatch(this.props.media)) {
      if (!this.state.isSticky) {
        this.sticky(this.stick)
      }
    } else if (this.state.isSticky) {
      this.unsticky(this.stick)
    }
  }
  render() {
    const { children, ...otherProps } = this.props
    return typeof children.type === 'function'
      ? React.cloneElement(this.props.children, { ...otherProps })
      : children
  }
}
