import PropTypes from 'prop-types'
import React from 'react'

import {
  BoxInput,
  BoxInputStyle,
  BoxInputWrapper,
  DividedTextfieldLabel,
} from 'App/shared/components/DividedTextfield/DividedTextfield.style'
import { ErrorMessage } from 'App/shared/components/Textfield/Textfield.style'
import handleInput from 'App/shared/higherOrderComponents/handleInput'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

export const DividedTextfieldBase = props => (
  <BoxInputStyle hasError={!!props.error}>
    {props.label && (
      <DividedTextfieldLabel hasError={!!props.error}>
        {props.label}
      </DividedTextfieldLabel>
    )}
    <BoxInputWrapper
      maxLength={props.maxLength}
      hasError={!!props.error}
      bgColor={props.bgColor}
      large={props.large}
    >
      <BoxInput {...props} hasError={!!props.error} large={props.large} />
    </BoxInputWrapper>
    {props.error && <ErrorMessage>{props.error}</ErrorMessage>}
  </BoxInputStyle>
)

DividedTextfieldBase.propTypes = {
  maxLength: PropTypes.number.isRequired,
  bgColor: PropTypes.string,
  label: PropTypes.string,
  error: PropTypes.string,
  large: PropTypes.bool,
}

export default handleInput(
  translateProps(['label', 'error'])(DividedTextfieldBase),
)
