import styled, { css } from 'styled-components'

import { phoneOnly } from 'helpers/styleMixins/media'

export const NonMobileContentStyle = styled.span`
  ${phoneOnly(css`
    display: none;
  `)}
`
