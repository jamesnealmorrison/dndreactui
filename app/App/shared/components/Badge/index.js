import PropTypes from 'prop-types'
import React from 'react'

import { BadgeWrap, BadgeText } from './Badge.style'

const Badge = ({ text, forButton, overlap, children, ...props }) => (
  <BadgeWrap {...props}>
    {children}
    <BadgeText forButton={forButton} overlap={overlap}>
      {text}
    </BadgeText>
  </BadgeWrap>
)

Badge.propTypes = {
  text: PropTypes.node.isRequired,
  children: PropTypes.node.isRequired,
  forButton: PropTypes.bool,
  overlap: PropTypes.bool,
}

Badge.defaultProps = {
  forButton: false,
  overlap: false,
}

export default Badge
