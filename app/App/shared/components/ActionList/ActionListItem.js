import PropTypes from 'prop-types'
import React from 'react'

import {
  ActionListItemStyle,
  ActionListItemContent,
  ActionListItemDescription,
  ActionListItemTitle,
} from 'App/shared/components/ActionList/ActionList.style'
import Button from 'App/shared/components/Button'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

export const ActionListItemBase = ({
  title,
  description,
  actionText,
  onActionClick,
}) => (
  <ActionListItemStyle>
    <ActionListItemContent>
      <ActionListItemTitle>{title}</ActionListItemTitle>
      <ActionListItemDescription>{description}</ActionListItemDescription>
    </ActionListItemContent>
    <Button.Raised onClick={onActionClick}>{actionText}</Button.Raised>
  </ActionListItemStyle>
)

ActionListItemBase.propTypes = {
  title: PropTypes.node,
  description: PropTypes.node,
  actionText: PropTypes.string,
  onActionClick: PropTypes.func,
}

export default translateProps(['title', 'description', 'actionText'])(
  ActionListItemBase,
)
