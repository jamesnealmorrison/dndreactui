import { ActionListStyle as ActionList } from 'App/shared/components/ActionList/ActionList.style'
import ActionListItem from 'App/shared/components/ActionList/ActionListItem'

ActionList.Item = ActionListItem

export default ActionList
