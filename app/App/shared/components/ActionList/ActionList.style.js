import styled from 'styled-components'

import Card from 'App/shared/components/Card'
import theme from 'theme'

export const ActionListStyle = styled(Card)`
  padding: 0;
`

ActionListStyle.defaultProps = {
  noMarginOnMobile: true,
}

export const ActionListItemStyle = styled.div`
  display: flex;
  padding: 2rem 1.5rem;
  align-items: center;
  &:not(:last-child) {
    border-bottom: 1px solid ${theme.colorFaint};
  }
`

export const ActionListItemContent = styled.div`
  flex: 1;
`

export const ActionListItemTitle = styled.div`
  font-size: 1rem;
  margin-bottom: 0.25rem;
  max-width: 300px;
  color: ${theme.colorPrimary};
`

export const ActionListItemDescription = styled.div`
  font-size: 0.875rem;
  max-width: 300px;
  color: ${theme.colorMedium};
`
