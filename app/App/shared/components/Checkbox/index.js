import PropTypes from 'prop-types'
import React from 'react'

import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

import {
  CheckboxWrap,
  CheckboxButton,
  CheckboxLabel,
  BoxOutline,
  TickOutline,
} from './Checkbox.style'

export function Checkbox({ label, format, ...props }) {
  return (
    <CheckboxWrap {...props}>
      <CheckboxButton type="checkbox" {...props} />
      {label && <CheckboxLabel {...props}>{label}</CheckboxLabel>}
      <BoxOutline {...props}>
        <TickOutline {...props} />
      </BoxOutline>
    </CheckboxWrap>
  )
}

Checkbox.propTypes = {
  label: PropTypes.string,
  disabled: PropTypes.bool,
  format: PropTypes.func,
}

export default setDisplayName('Checkbox')(translateProps(['label'])(Checkbox))
