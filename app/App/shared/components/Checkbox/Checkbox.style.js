import styled, { css } from 'styled-components'

import { materialAnimationDefault } from 'helpers/styleMixins/animations.style'
import ifProp from 'helpers/ifProp'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'

import { mask, bg } from './images'

const radioLabelFontSize = 1
const radioLabelHeight = 1.5
const radioButtonSize = 1
const radioPadding = 0.5
const radioTopOffset = (radioLabelHeight - radioButtonSize) / 2

export const CheckboxWrap = setDisplayName('CheckboxWrap')(
  styled.label`
    position: relative;
    font-size: ${radioLabelFontSize}rem;
    line-height: ${radioLabelHeight}rem;
    display: inline-block;
    vertical-align: middle;
    box-sizing: border-box;
    height: ${radioLabelHeight}rem;
    margin: 0;
    padding-left: ${radioButtonSize + radioPadding}rem;
    padding-right: 1.5rem;
  `)

export const CheckboxButton = setDisplayName('CheckboxButton')(
  styled.input`
    line-height: ${radioLabelHeight}rem;
    position: absolute;
    width: 0;
    height: 0;
    margin: 0;
    padding: 0;
    opacity: 0;
    -ms-appearance: none;
    -moz-appearance: none;
    -webkit-appearance: none;
    appearance: none;
    border: none;
    &:checked ~ div > div {
      background-color: ${theme.colorPrimary};
      background-image: url(${bg});
    }
  `)

export const BoxOutline = setDisplayName('BoxOutline')(
  styled.div`
    position: absolute;
    top: ${radioTopOffset}rem;
    width: ${radioButtonSize}rem;
    height: ${radioButtonSize}rem;
    background-color: white;
    left: 0;
    &:after {
      content: "";
      display: block;
      box-sizing: border-box;
      width: 100%;
      height: 100%;
      margin: 0;
      cursor: pointer;
      overflow: hidden;
      border: 2px solid ${theme.colorMedium};
      border-radius: 2px;
      z-index: 2;
    }
    ${CheckboxButton}:checked ~ &:after {
      border: 2px solid ${theme.colorPrimary};
    }
    ${CheckboxButton}:disabled ~ &:after {
      border: 2px solid ${theme.colorLight};
      cursor: auto;
    }
    &:before {
      border-radius: 50%;
      content: '';
      display: block;
      opacity: 0;
      background: ${theme.colorMedium};
      ${materialAnimationDefault('0.28s')}
      transition-property: transform, opacity;
      transform: scale(0.1);
      height: 2.75rem;
      width: 2.75rem;
      position: absolute;
      top: -0.875rem;
      left: -0.875rem;
      z-index: 0;
    }
    ${CheckboxButton}:focus:not(:disabled) ~ &,
    ${CheckboxButton}:active:not(:disabled) ~ & {
      &:before {
        background: ${theme.colorPrimary};
        transform: scale(1);
        opacity: 0.24;
      }
    }

  `)

export const TickOutline = setDisplayName('TickOutline')(
  styled.div`
    position: absolute;
    top: 1.5px;
    left: 2px;
    bottom: 2px;
    right: 2px;
    mask: url(${mask});
    background: transparent;
    transition-property: background;
    ${materialAnimationDefault('0.28s')}
    ${ifProp('disabled', css`
      background-color: ${theme.colorLight};
    `)};
  `)

export const CheckboxLabel = setDisplayName('CheckboxLabel')(
  styled.span`
    cursor: pointer;
    ${ifProp('disabled', css`
      color: ${theme.colorLight};
      cursor: auto;
    `)};
  `)
