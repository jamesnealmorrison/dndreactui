import { transparentize } from 'polished'
import styled, { css } from 'styled-components'

import ifProp from 'helpers/ifProp'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'


export const RippleEffect = setDisplayName('RippleEffect')(
  styled.div`
    background-color: white;
    border-radius: 50%;
    transform: 'none';
    pointer-events: none;
    position: absolute;
    top: 0;
    left: 0;
    overflow: hidden;
    transition-duration: 0.6s, 0.6s, 0.6s, 1.2s;
    transition-timing-function: ${theme.animationCurveLinearOutSlowIn};
    will-change: transform, width, height, opacity;
    ${ifProp('dark', css`
      background-color: rgba(0,0,0,0.3);
    `)}
    ${ifProp('primary', css`
      background-color: ${transparentize(0.5, theme.colorPrimary)}
    `)}
    ${ifProp('accent', css`
      background-color: ${transparentize(0.5, theme.colorAccent)}
    `)}
    -webkit-tap-highlight-color:  rgba(255, 255, 255, 0);
  `)

export const RippleWrap = setDisplayName('RippleWrap')(styled.div`
  display: block;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 4;
  overflow: hidden;
  -webkit-tap-highlight-color:  rgba(255, 255, 255, 0);
  ${ifProp('toggle', css`
    top: -6px;
    left: -10px;
    right: auto;
    bottom: auto;
    height: 36px;
    width: 36px;
  `)}
  ${ifProp('round', css`
    border-radius: 50%;
    -webkit-mask-image: -webkit-radial-gradient(circle, white, black);
  `)};
`)
