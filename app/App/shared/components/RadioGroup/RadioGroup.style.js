import styled from 'styled-components'

import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'

export const RadioGroupWrapper = setDisplayName('RadioGroupWrapper')(
  styled.div`
    padding: 1.75rem 0;
  `
)

export const RadioGroupLabel = setDisplayName('RadioGroupLabel')(
  styled.div`
    margin-bottom: 0.25rem;
  `
)
