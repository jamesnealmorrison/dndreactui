import PropTypes from 'prop-types'
import React from 'react'
import compose from 'lodash/fp/flowRight'

import Radio from 'App/shared/components/Radio'
import handleInput from 'App/shared/higherOrderComponents/handleInput'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

import { RadioGroupLabel, RadioGroupWrapper } from './RadioGroup.style'

export function RadioGroup({ label, options, value, onChange }) {
  return (
    <RadioGroupWrapper>
      {label && <RadioGroupLabel>{label}</RadioGroupLabel>}
      {options.map(option => (
        <Radio
          name={label}
          key={option.value}
          value={option.value}
          checked={option.value === value}
          onChange={() => onChange(option.value)}
          label={option.label}
        />
      ))}
    </RadioGroupWrapper>
  )
}

RadioGroup.propTypes = {
  label: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.object),
  value: PropTypes.string,
  onChange: PropTypes.func,
}

RadioGroup.defaultProps = {
  options: [],
}

export default compose(
  handleInput,
  setDisplayName('RadioGroup'),
  translateProps(['label']),
)(RadioGroup)
