import PropTypes from 'prop-types'
import React from 'react'

import {
  ButtonInner,
  IconButtonBase,
  IconButtonText,
} from 'App/shared/components/Button/Button.style'
import Ripple from 'App/shared/components/Ripple'
import maybeFormatMessage from 'helpers/maybeFormatMessage'

export default function buttonFactory(ButtonComponent) {
  function Button({ children, ...props }) {
    const shouldShowRipple =
      props.ripple && !props.disabled && ButtonComponent !== IconButtonBase
    return (
      <ButtonComponent {...props}>
        <ButtonInner>
          {children}
          {props.iconLabel && (
            <IconButtonText>
              {maybeFormatMessage(props.iconLabel)}
            </IconButtonText>
          )}
          {shouldShowRipple && <Ripple />}
        </ButtonInner>
      </ButtonComponent>
    )
  }

  Button.defaultProps = {
    ripple: true,
    type: 'button',
  }

  Button.propTypes = {
    text: PropTypes.string,
    children: PropTypes.node,
    ripple: PropTypes.bool,
    fab: PropTypes.bool,
    icon: PropTypes.bool,
    href: PropTypes.string,
    to: PropTypes.string,
    disabled: PropTypes.bool,
    iconLabel: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.shape({
        id: PropTypes.string,
        defaultMessage: PropTypes.string,
      }),
    ]),
  }

  Button.displayName = ButtonComponent.setDisplayName

  return Button
}
