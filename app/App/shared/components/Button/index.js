import {
  FabBase,
  FlatButtonBase,
  IconButtonBase,
  RaisedButtonBase,
} from 'App/shared/components/Button/Button.style'
import buttonFactory from 'App/shared/components/Button/buttonFactory'

const Button = buttonFactory(FlatButtonBase)

Button.Raised = buttonFactory(RaisedButtonBase)
Button.Icon = buttonFactory(IconButtonBase)
Button.Fab = buttonFactory(FabBase)

export default Button
