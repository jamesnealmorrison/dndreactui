import PropTypes from 'prop-types'
import React from 'react'

import EmptyMessage from 'App/routes/EquipmentCodesPage/components/EmptyMessage'
import FloatingSpinner from 'App/routes/EquipmentCodesPage/components/FloatingSpinner'
import Table from 'App/shared/components/Table'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

export const MSETableBase = ({
  children,
  isLoading,
  isEmpty,
  isFiltered,
  emptyMessage,
  filteredEmptyMessage,
  ...props
}) => (
  <React.Fragment>
    <FloatingSpinner isLoading={isLoading} />
    <Table fullWidth {...props}>
      {children}
    </Table>
    <EmptyMessage isLoading={isLoading} isEmpty={isEmpty}>
      {isFiltered ? filteredEmptyMessage : emptyMessage}
    </EmptyMessage>
  </React.Fragment>
)

MSETableBase.propTypes = {
  children: PropTypes.node,
  isLoading: PropTypes.bool,
  isEmpty: PropTypes.bool,
  isFiltered: PropTypes.bool,
  emptyMessage: PropTypes.string,
  filteredEmptyMessage: PropTypes.string,
}

export default translateProps(['emptyMessage', 'filteredEmptyMessage'])(
  MSETableBase,
)
