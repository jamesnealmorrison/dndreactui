import PropTypes from 'prop-types'
import React from 'react'
import ReactModal from 'react-modal'

import Button from 'App/shared/components/Button'
import SVGIcon from 'App/shared/components/SVGIcon'

import {
  ModalStyle,
  fadeIn,
  ModalTitle,
  ModalContent,
  ModalActions,
  ModalForm,
  MobileModalClose,
} from './Modal.style'

const style = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(100, 100, 100, 0.3)',
    zIndex: 999,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    animation: `${fadeIn} 0.3s cubic-bezier(0.4, 0, 0.2, 1) forwards`,
  },
}

const ModalWrap = ({ className, children, ...props }) => (
  <ReactModal style={style} className={className} {...props}>
    {!props.alert && (
      <MobileModalClose onClick={props.onRequestClose}>
        <Button.Icon inverse>
          <SVGIcon name="navigation.close" />
        </Button.Icon>
      </MobileModalClose>
    )}
    {children}
  </ReactModal>
)

ModalWrap.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  onRequestClose: PropTypes.func,
  alert: PropTypes.bool,
}

ModalWrap.defaultProps = {
  onRequestClose: () => {},
}

const Modal = ModalStyle.withComponent(ModalWrap)

Modal.displayName = 'Modal'

Modal.Title = ModalTitle
Modal.Content = ModalContent
Modal.Actions = ModalActions
Modal.Form = ModalForm

export default Modal
