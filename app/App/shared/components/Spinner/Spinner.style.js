import styled, { css, keyframes } from 'styled-components'

import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'

const spinnerSize = 28
const spinnerStrokeWidth = 3
// Amount of circle the arc takes up.
const spinnerArcSize = 270
// Time it takes to expand and contract arc.
const spinnerArcTime = 1333
// How much the start location of the arc should rotate each time.
const spinnerArcStartRot = 216
const spinnerDuration = (360 * (spinnerArcTime / spinnerArcStartRot)) + (360 - spinnerArcSize)
const spinnerColor1 = 'rgb(66,165,245)'
const spinnerColor2 = 'rgb(244,67,54)'
const spinnerColor3 = 'rgb(253,216,53)'
const spinnerColor4 = 'rgb(76,175,80)'

const rotate = keyframes`
  to { transform: rotate(360deg) }
`

const arcSize = scalar => css`
  transform: rotate(${scalar * 270}deg);
`

const fillUnfillRotate = keyframes`
  12.5% { ${arcSize(0.5)} }
  25%   { ${arcSize(1.0)} }
  37.5% { ${arcSize(1.5)} }
  50%   { ${arcSize(2.0)} }
  62.5% { ${arcSize(2.5)} }
  75%   { ${arcSize(3.0)} }
  87.5% { ${arcSize(3.5)} }
  to    { ${arcSize(4.0)} }
`
/**
* HACK: Even though the intention is to have the current .mdl-spinner__layer-N
* at `opacity: 1`, we set it to `opacity: 0.99` instead since this forces Chrome
* to do proper subpixel rendering for the elements being animated. This is
* especially visible in Chrome 39 on Ubuntu 14.04. See:
*
* - https://github.com/Polymer/paper-spinner/issues/9
* - https://code.google.com/p/chromium/issues/detail?id=436255
*/
const layer1FadeInOut = keyframes`
  from { opacity: 0.99; }
  25% { opacity: 0.99; }
  26% { opacity: 0; }
  89% { opacity: 0; }
  90% { opacity: 0.99; }
  100% { opacity: 0.99; }
`
const layer2FadeInOut = keyframes`
  from { opacity: 0; }
  15% { opacity: 0; }
  25% { opacity: 0.99; }
  50% { opacity: 0.99; }
  51% { opacity: 0; }
`
const layer3FadeInOut = keyframes`
  from { opacity: 0; }
  40% { opacity: 0; }
  50% { opacity: 0.99; }
  75% { opacity: 0.99; }
  76% { opacity: 0; }
`
const layer4FadeInOut = keyframes`
  from { opacity: 0; }
  65% { opacity: 0; }
  75% { opacity: 0.99; }
  90% { opacity: 0.99; }
  100% { opacity: 0; }
`
const leftSpin = keyframes`
  from { transform: rotate(130deg); }
  50% { transform: rotate(-5deg); }
  to { transform: rotate(130deg); }
`
const rightSpin = keyframes`
  from { transform: rotate(-130deg); }
  50% { transform: rotate(5deg); }
  to { transform: rotate(-130deg); }
`

export const SpinnerBase = setDisplayName('SpinnerBase')(styled.div`
  display: inline-block;
  position: relative;
  width: ${spinnerSize}px;
  height: ${spinnerSize}px;

  ${({ active }) => active && css`
    animation: ${rotate} ${spinnerDuration}ms linear infinite;
  `}
`)

export const Layer = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  opacity: 0;
`

const arcTime = `${spinnerArcTime * 4}ms ${theme.animationCurveFastOutSlowIn} infinite both`

export const LayerOne = setDisplayName('LayerOne')(Layer.extend`
  border-color: ${({ singleColor }) => (singleColor ? theme.colorPrimary : spinnerColor1)};

  ${({ active }) => active && css`
    animation: ${fillUnfillRotate} ${arcTime},
               ${layer1FadeInOut} ${arcTime};
  `}
`)

export const LayerTwo = setDisplayName('LayerTwo')(Layer.extend`
  border-color: ${({ singleColor }) => (singleColor ? theme.colorPrimary : spinnerColor2)};

  ${({ active }) => active && css`
    animation: ${fillUnfillRotate} ${arcTime},
               ${layer2FadeInOut} ${arcTime};
  `}
`)

export const LayerThree = setDisplayName('LayerThree')(Layer.extend`
  border-color: ${({ singleColor }) => (singleColor ? theme.colorPrimary : spinnerColor3)};

  ${({ active }) => active && css`
    animation: ${fillUnfillRotate} ${arcTime},
               ${layer3FadeInOut} ${arcTime};
  `}
`)

export const LayerFour = setDisplayName('LayerFour')(Layer.extend`
  border-color: ${({ singleColor }) => (singleColor ? theme.colorPrimary : spinnerColor4)};

  ${({ active }) => active && css`
    animation: ${fillUnfillRotate} ${arcTime},
               ${layer4FadeInOut} ${arcTime};
  `}
`)

/**
* Patch the gap that appear between the two adjacent
* div.mdl-spinner__circle-clipper while the spinner is rotating
* (appears on Chrome 38, Safari 7.1, and IE 11).
*
* Update: the gap no longer appears on Chrome when .mdl-spinner__layer-N's
* opacity is 0.99, but still does on Safari and IE.
*/
export const GapPatch = setDisplayName('GapPatch')(styled.div`
  position: absolute;
  box-sizing: border-box;
  top: 0;
  left: 45%;
  width: 10%;
  height: 100%;
  overflow: hidden;
  border-color: inherit;
`)

export const CircleClipper = setDisplayName('CircleClipper')(styled.div`
  display: inline-block;
  position: relative;
  width: 50%;
  height: 100%;
  overflow: hidden;
  border-color: inherit;
  ${({ left }) => left && css`float: left;`}
  ${({ right }) => right && css`float: right;`}
`)

export const Circle = setDisplayName('Circle')(styled.div`
  box-sizing: border-box;
  height: 100%;
  border-width: ${spinnerStrokeWidth}px;
  border-style: solid;
  border-color: inherit;
  border-bottom-color: transparent !important;
  border-radius: 50%;
  animation: none;
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;

  ${({ left }) => left && css`
    border-right-color: transparent !important;
    transform: rotate(129deg);
    ${({ active }) => active && css`
      animation: ${leftSpin} ${spinnerArcTime}ms
                 ${theme.animationCurveFastOutSlowIn} infinite both;
    `}
  `}

  ${({ right }) => right && css`
    left: -100%;
    border-left-color: transparent !important;
    transform: rotate(-129deg);
    ${({ active }) => active && css`
      animation: ${rightSpin} ${spinnerArcTime}ms
                 ${theme.animationCurveFastOutSlowIn} infinite both;
    `}
  `}

  ${({ clipper }) => clipper && css`
    width: 200%;
  `}

  ${({ gap }) => gap && css`
    width: 1000%;
    left: -450%;
  `}
`)
