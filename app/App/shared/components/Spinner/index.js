import PropTypes from 'prop-types'
import React from 'react'

import {
  SpinnerBase,
  LayerOne,
  LayerTwo,
  LayerThree,
  LayerFour,
  GapPatch,
  CircleClipper,
  Circle,
} from './Spinner.style'

const layers = [LayerOne, LayerTwo, LayerThree, LayerFour]

export default class Spinner extends React.Component {
  state = {
    active: false,
  }

  componentDidMount() {
    this.timeout = setTimeout(this.show, this.props.delay)
  }

  componentWillUnmount() {
    clearTimeout(this.timeout)
  }
  show = () => this.setState({ active: true })

  render() {
    return (
      <SpinnerBase {...this.props} active={this.state.active}>
        {layers.map(Layer => (
          <Layer
            key={Layer.displayName}
            {...this.props}
            active={this.state.active}
          >
            <CircleClipper left>
              <Circle {...this.props} left clipper active={this.state.active} />
            </CircleClipper>
            <GapPatch>
              <Circle gap />
            </GapPatch>
            <CircleClipper right>
              <Circle
                {...this.props}
                right
                clipper
                active={this.state.active}
              />
            </CircleClipper>
          </Layer>
        ))}
      </SpinnerBase>
    )
  }
}

Spinner.propTypes = {
  singleColor: PropTypes.bool,
  delay: PropTypes.number,
}

Spinner.defaultProps = {
  delay: 300,
}
