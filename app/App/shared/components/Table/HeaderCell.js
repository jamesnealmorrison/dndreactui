import PropTypes from 'prop-types'
import React from 'react'

import {
  HeaderCellInner,
  HeaderCellOuter,
  TableIcon,
  TableIconPlaceholder,
} from 'App/shared/components/Table/Table.style'

const HeaderCell = props => (
  <HeaderCellOuter {...props}>
    <HeaderCellInner numeric={props.numeric} sorted={props.sorted}>
      {props.children}
      {props.sorted ? (
        <TableIcon
          flip={props.sortDirection === 'desc'}
          name="navigation.arrow_upward"
        />
      ) : (
        <TableIconPlaceholder />
      )}
    </HeaderCellInner>
  </HeaderCellOuter>
)

HeaderCell.propTypes = {
  children: PropTypes.node,
  numeric: PropTypes.bool,
  sorted: PropTypes.bool,
  sortDirection: PropTypes.oneOf(['desc', 'asc']),
}

export default HeaderCell
