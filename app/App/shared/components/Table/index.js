import PropTypes from 'prop-types'

import {
  Cell,
  Row,
  Table,
  TableBody,
  TableFooter,
  TableHeader,
  TableIcon,
} from 'App/shared/components/Table/Table.style'
import HeaderCell from 'App/shared/components/Table/HeaderCell'

Table.propTypes = {
  maxHeight: PropTypes.number,
  children: PropTypes.node.isRequired,
  fixedHeader: PropTypes.bool,
  fullWidth: PropTypes.bool,
}

Table.Header = TableHeader
Table.Body = TableBody
Table.Footer = TableFooter
Table.Row = Row
Table.Cell = Cell
Table.HeaderCell = HeaderCell
Table.Icon = TableIcon

export default Table
