/* eslint-disable no-console */
import get from 'lodash/fp/get'
import map from 'lodash/fp/map'

const iconList = {
  navigation: [
    'arrow_back',
    'arrow_forward',
    'arrow_drop_down',
    'arrow_drop_up',
    'menu',
    'arrow_upward',
    'close',
    'check',
  ],
  alert: ['warning'],
  hardware: ['keyboard_arrow_left', 'keyboard_arrow_right'],
  content: ['add', 'filter_list'],
  image: ['edit'],
}

/* eslint-disable no-param-reassign, global-require */
const icons = Object.entries(iconList).reduce((aggr, [namespace, icons]) => {
  aggr[namespace] = {}

  icons.forEach(icon => {
    aggr[namespace][
      icon
    ] = require(`material-design-icons/${namespace}/svg/production/ic_${icon}_24px.svg`)
  })

  return aggr
}, {})

const getMaterialIconContents = icon => {
  const parser = new DOMParser()
  const serializer = new XMLSerializer()
  const svg = get(icon, icons)
  if (svg) {
    const svgContents = parser
      .parseFromString(svg, 'image/svg+xml')
      .querySelector('svg').childNodes

    return map(node => serializer.serializeToString(node), svgContents).join('')
  }

  console.warn(`icon not found: add ${icon} to icon list`)

  return ''
}

export default getMaterialIconContents
