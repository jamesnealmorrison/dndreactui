import PropTypes from 'prop-types'
import React from 'react'

import { SVGIconWrap } from 'App/shared/components/SVGIcon/SVGIcon.style'
import getMaterialIconContents from 'App/shared/components/SVGIcon/getMaterialIconContents'

export default class SVGIcon extends React.PureComponent {
  state = {
    svgContents: '',
  }

  componentWillMount() {
    this.setState({
      svgContents: getMaterialIconContents(this.props.name),
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.name !== this.props.name) {
      this.setState({
        svgContents: getMaterialIconContents(nextProps.name),
      })
    }
  }

  render() {
    /* eslint-disable react/no-danger */
    const { svgContents } = this.state
    return (
      <SVGIconWrap {...this.props}>
        <svg
          width={this.props.size}
          height={this.props.size}
          viewBox="0 0 24 24"
          dangerouslySetInnerHTML={{ __html: svgContents }}
        />
      </SVGIconWrap>
    )
  }
}

SVGIcon.propTypes = {
  name: PropTypes.string,
  width: PropTypes.number,
  size: PropTypes.number,
}

SVGIcon.defaultProps = {
  size: 24,
}
