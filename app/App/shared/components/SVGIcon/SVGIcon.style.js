import styled, { css } from 'styled-components'
import get from 'lodash/fp/get'

import ifProp from 'helpers/ifProp'
import theme from 'theme'

export const SVGIconWrap = styled.span`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: ${get('size')}px;
  width: ${get('size')}px;
  line-height: inherit;
  ${ifProp('accent', css`
    color: ${theme.colorAccent};
  `)}
  ${ifProp('primary', css`
    color: ${theme.colorPrimary};
  `)}
  ${ifProp('primaryAlt', css`
    color: ${theme.colorPrimaryAlt};
  `)}
  ${ifProp('forButtonRight', css`
    margin-left: 0.5rem;
    margin-right: -0.25rem;
  `)}
  ${ifProp('forButtonLeft', css`
    margin-right: 0.5rem;
    margin-left: -0.25rem;
  `)}
`
