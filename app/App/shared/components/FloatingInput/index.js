import PropTypes from 'prop-types'
import React from 'react'

import Spinner from 'App/shared/components/Spinner'
import handleInput from 'App/shared/higherOrderComponents/handleInput'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

import {
  FloatingInputStyle,
  FloatingInputWrap,
  SpinnerWrap,
} from './FloatingInput.style'

export const FloatingInputBase = props => (
  <FloatingInputWrap width={props.width} maxWidth={props.maxWidth}>
    <FloatingInputStyle {...props} />
    {props.isSubmitting && (
      <SpinnerWrap>
        <Spinner />
      </SpinnerWrap>
    )}
  </FloatingInputWrap>
)

FloatingInputBase.propTypes = {
  isSubmitting: PropTypes.bool,
  width: PropTypes.string,
  maxWidth: PropTypes.string,
}

export default handleInput(translateProps(['placeholder'])(FloatingInputBase))
