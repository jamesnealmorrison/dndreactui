import get from 'lodash/fp/get'
import styled, { css } from 'styled-components'

import { shadow2dp } from 'helpers/styleMixins/shadows.style'
import ifProp from 'helpers/ifProp'
import theme from 'theme'

export const FloatingInputWrap = styled.div`
  text-align: center;
  position: relative;
  margin: 0 auto;
  max-width: ${p => p.maxWidth || '30rem'};
  ${ifProp('width', css`
    width: ${get('width')};
  `)}
`

export const FloatingInputStyle = styled.input`
  height: 3rem;
  padding: 0 1rem;
  ${ifProp(
    'isSubmitting',
    css`
      padding-right: 3rem;
    `,
  )}
  width: 100%;
  font-size: 1.25rem;
  font-family: ${theme.preferredFont};
  background: white;
  border-radius: 4px;
  outline: none;
  border: 0;
  color: ${theme.colorDark};
  ${shadow2dp()};
  &::placeholder {
    font-size: 1.125rem;
    line-height: 3rem;
    font-family: ${theme.preferredFont};
    color: ${theme.colorLight};
  }
`

export const SpinnerWrap = styled.div`
  position: absolute;
  right: 8px;
  top: 50%;
  margin-top: -16px;
  transform: scale(0.8);
`
