import styled, { css } from 'styled-components'

import {
  FloatingInputStyle,
} from 'App/shared/components/FloatingInput/FloatingInput.style'
import { materialAnimationDefault } from 'helpers/styleMixins/animations.style'
import SVGIcon from 'App/shared/components/SVGIcon'
import ifProp from 'helpers/ifProp'
import theme from 'theme'

export const SearchWrap = styled.form`
  padding: 0.5rem;
  position: relative;
  background: transparent;
  ${ifProp('sticky', css`
    position: sticky;
    top: 0;
    z-index: 1;
  `)}
`

export const SearchIcon = styled(SVGIcon).attrs({
  name: 'action.search',
})`
  will-change: color;
  transition-property: color;
  ${materialAnimationDefault()}
  position: absolute;
  left: 1.25rem;
  top: 1.25rem;
  z-index: 2;
  color: ${theme.colorMedium};
`

export const SearchInput = styled(FloatingInputStyle)`
  padding-left: 3rem;
  position: relative;
  z-index: 1;
  border-radius: 2px;
  &:focus + ${SearchIcon} {
    color: ${theme.colorPrimaryAlt};
  }
`

export const HiddenSubmit = styled.input.attrs({
  type: 'submit',
})`
  position: absolute;
  z-index: 0;
  height: 1px;
  width: 1px;
  top: 1rem;
  left: 1rem;
`
