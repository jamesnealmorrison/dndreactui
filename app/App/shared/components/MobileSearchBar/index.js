import PropTypes from 'prop-types'
import React from 'react'

import {
  SearchIcon,
  SearchInput,
  SearchWrap,
  HiddenSubmit,
} from 'App/shared/components/MobileSearchBar/MobileSearchBar.style'

export default class MobileSearchBar extends React.PureComponent {
  static propTypes = {
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    sticky: PropTypes.bool,
  }

  static defaultProps = {
    onChange: () => {},
  }

  handleChange = e => {
    this.props.onChange(e.target.value)
  }

  handleSubmit = e => {
    e.stopPropagation()
    e.preventDefault()
    this.props.onSubmit()
  }

  render() {
    return (
      <SearchWrap onSubmit={this.handleSubmit} sticky={this.props.sticky}>
        <SearchInput {...this.props} onChange={this.handleChange} />
        <SearchIcon />
        <HiddenSubmit />
      </SearchWrap>
    )
  }
}
