import styled from 'styled-components'

const Fill = styled.div`
  flex: 1;
`

export default Fill
