import PropTypes from 'prop-types'
import React, { Component } from 'react'

import Ripple from 'App/shared/components/Ripple'
import { MenuItem as MenuItemBase } from './Menu.style'

export default class MenuItem extends Component {
  state = {
    getTransitionDelay: () => 0,
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.isVisible && nextProps.isVisible) {
      const {
        height,
      } = this.menuItem.parentNode.parentNode.getBoundingClientRect()
      const { offsetTop } = this.menuItem
      const { height: itemHeight } = this.menuItem.getBoundingClientRect()
      const getTransitionDelay = nextProps.fadeDown
        ? duration => offsetTop / height * duration
        : duration => -(offsetTop + itemHeight - height) / height * duration

      this.setState({ getTransitionDelay })
    }
  }

  render() {
    const { children, ...props } = this.props
    return (
      <MenuItemBase
        {...props}
        {...this.state}
        innerRef={menuItem => {
          this.menuItem = menuItem
        }}
      >
        {children}
        <Ripple dark />
      </MenuItemBase>
    )
  }
}

MenuItem.propTypes = {
  fadeDown: PropTypes.bool,
  isVisible: PropTypes.bool,
  children: PropTypes.node,
}
