import Portal from 'react-portal'
import PropTypes from 'prop-types'
import React, { Component, Children, cloneElement } from 'react'

import MenuDivider from 'App/shared/components/Menu/MenuDivider'
import MenuItem from 'App/shared/components/Menu/MenuItem'

import {
  MenuContainer,
  MenuControlWrap,
  MenuOutline,
  MenuBase,
} from './Menu.style'

export default class Menu extends Component {
  state = {
    isVisible: false,
  }

  componentDidMount() {
    this.ismounted = true
  }

  componentWillUnmount() {
    this.ismounted = false
    window.removeEventListener('scroll', this.setMenuPosition, true)
    cancelAnimationFrame(this.closeMenu)
    cancelAnimationFrame(this.openMenu)
  }

  setMenuPosition = () => {
    const control = this.control.getBoundingClientRect()
    const position = {}
    if (this.props.bottomRight) {
      position.left = control.right - this.state.width
      position.top = control.bottom
    } else if (this.props.topLeft) {
      position.left = control.left
      position.top = control.top - this.state.height
    } else if (this.props.topRight) {
      position.left = control.right - this.state.width
      position.top = control.top - this.state.height
    } else {
      position.left = control.left
      position.top = control.bottom
    }

    this.setState({ ...position })
  }

  setControlRef = ctrl => {
    this.control = ctrl && ctrl.children[0]
  }

  handleOpen = () => {
    this.setState({ isVisible: false })
    this.openMenu = requestAnimationFrame(() => {
      if (this.preventOpen) {
        return
      }

      const menu = this.menu.getBoundingClientRect()
      this.setState({
        height: menu.height,
        width: menu.width,
      })

      window.addEventListener('scroll', this.setMenuPosition, true)
      this.setState({ isVisible: true })
      this.setMenuPosition()
    })
  }

  handleClose = () => {
    window.removeEventListener('scroll', this.setMenuPosition, true)

    this.closeMenu = requestAnimationFrame(() => {
      if (this.ismounted) {
        this.setState({
          isVisible: false,
        })
      }
    })
  }

  render() {
    const { children, ...props } = this.props

    const childrenWithProps = Children.map(children, child =>
      cloneElement(child, {
        isVisible: this.state.isVisible,
        fadeDown: !props.topRight && !props.topLeft,
        getTransitionDelay: () => 0,
      }),
    )

    const control = cloneElement(props.control, {
      onClick: () => {
        this.preventOpen = this.state.isVisible
      },
    })

    return (
      <MenuControlWrap>
        <Portal
          openByClickOn={<span ref={this.setControlRef}>{control}</span>}
          closeOnOutsideClick
          onOpen={this.handleOpen}
          onClose={this.handleClose}
        >
          <MenuContainer
            {...this.state}
            {...props}
            onClick={() => {
              setTimeout(() => this.handleClose(), 100)
            }}
          >
            <MenuOutline {...this.state} {...props} />
            <MenuBase
              {...this.state}
              {...props}
              innerRef={menu => {
                this.menu = menu
              }}
            >
              {childrenWithProps}
            </MenuBase>
          </MenuContainer>
        </Portal>
      </MenuControlWrap>
    )
  }
}

Menu.propTypes = {
  children: PropTypes.node,
  control: PropTypes.node,
  bottomRight: PropTypes.bool,
  topLeft: PropTypes.bool,
  topRight: PropTypes.bool,
}

Menu.Item = MenuItem
Menu.Divider = MenuDivider
