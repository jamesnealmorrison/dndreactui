import get from 'lodash/fp/get'
import styled, { css } from 'styled-components'

import { shadow2dp } from 'helpers/styleMixins/shadows.style'
import { typoBody1 } from 'helpers/styleMixins/type.style'
import ifProp from 'helpers/ifProp'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'

const menuExpandDuration = 0.3
const menuFadeDuration = 0.2

export const MenuControlWrap = setDisplayName('MenuControlWrap')(
  styled.div`
    position: relative;
  `)

export const MenuContainer = setDisplayName('MenuContainer')(
  styled.div`
    display: block;
    margin: 0;
    padding: 0;
    border: none;
    position: fixed;
    overflow: visible;
    height: 0;
    width: 0;
    visibility: hidden;
    z-index: -1;
    ${ifProp('isVisible', css`
      z-index: 999;
      visibility: visible;
      top: ${get('top')}px;
      left: ${get('left')}px;
      height: ${get('height')}px;
      width: ${get('width')}px;
    `)};
  `)

export const MenuOutline = setDisplayName('MenuOutline')(
  styled.div`
    display: block;
    background: white;
    margin: 0;
    padding: 0;
    border: none;
    border-radius: 2px;
    position: absolute;
    top: 0;
    left: 0;
    overflow: hidden;
    opacity: 0;
    transform: scale(0);
    transform-origin: 0 0;
    ${shadow2dp()}
    will-change: transform;
    transition:
      transform ${menuExpandDuration}s ${theme.animationCurveDefault},
      opacity ${menuFadeDuration}s ${theme.animationCurveDefault};
    z-index: -1;
    ${ifProp('isVisible', css`
      opacity: 1;
      transform: scale(1);
      z-index: 999;
      height: ${get('height')}px;
      width: ${get('width')}px;
    `)}
    ${ifProp('bottomRight', css`
      transform-origin: 100% 0;
    `)}
    ${ifProp('bottomLeft', css`
      transform-origin: 0 100%;
    `)}
    ${ifProp('topRight', css`
      transform-origin: 100% 100%;
    `)}
    ${ifProp('topLeft', css`
      transform-origin: 0 0;
    `)};
  `)

export const MenuBase = setDisplayName('MenuBase')(
  styled.div`
    position: absolute;
    list-style: none;
    top: 0;
    left: 0;
    height: auto;
    width: auto;
    min-width: 124px;
    box-sizing: border-box;
    padding: 8px 0;
    margin: 0;
    opacity: 0;
    clip: rect(0 0 0 0);
    z-index: -1;
    transition:
      opacity ${menuFadeDuration}s ${theme.animationCurveDefault},
      clip ${menuExpandDuration}s ${theme.animationCurveDefault};
    ${ifProp('isVisible', css`
      opacity: 1;
      clip: ${({ height, width }) => `rect(0px ${width}px ${height}px 0px)`};
      height: ${({ height }) => height}px;
      width: ${({ width }) => width}px;
      z-index: 999;
    `)}
    ${ifProp('bottomRight', css`
      left: auto;
      right: 0;
    `)}
    ${ifProp('bottomLeft', css`
      top: auto;
      left: auto;
    `)}
    ${ifProp('topRight', css`
      top: auto;
      left: auto;
      bottom: 0;
      right: 0;
    `)}
    ${ifProp('topLeft', css`
      top: auto;
      bottom: 0;
    `)};
  `)

export const MenuDivider = setDisplayName('MenuDivider')(
  styled.hr`
    border-bottom: 1px solid ${theme.colorFaint};
    margin: 0;
    opacity: ${ifProp('isVisible', 1, 0)}
    height: 0;
    border-top: 0;
    transition: opacity ${menuFadeDuration}s ${theme.animationCurveDefault};
    transition-delay: ${({ getTransitionDelay }) => getTransitionDelay(menuExpandDuration)}s;
  `)

export const MenuItem = setDisplayName('MenuItem')(
  styled.button`
    display: block;
    width: 100%;
    border: none;
    color: ${theme.colorDark};
    background-color: transparent;
    text-align: left;
    margin: 0;
    padding: 0 16px;
    outline-color: ${theme.colorLight};
    position: relative;
    overflow: hidden;
    ${typoBody1()}
    text-decoration: none;
    cursor: pointer;
    height: 48px;
    line-height: 48px;
    white-space: nowrap;
    opacity: ${ifProp('isVisible', 1, 0)}
    transition: opacity ${menuFadeDuration}s ${theme.animationCurveDefault};
    transition-delay: ${({ getTransitionDelay }) => getTransitionDelay(menuExpandDuration)}s;
    user-select: none;
    &::-moz-focus-inner {
      border: 0;
    }
    ${ifProp('disabled', css`
      color: ${theme.colorLight};
      background-color: transparent;
      cursor: auto;
      &:hover, &:focus {
        background-color: transparent;
      }
    `)}
    &:hover {
      background-color: ${theme.colorOffWhite};
    }
    &:focus {
      outline: none;
      background-color: ${theme.colorOffWhite};
    }
    &:active {
      background-color: ${theme.colorFaint};
    }
  `)
