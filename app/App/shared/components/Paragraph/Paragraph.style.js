import styled, { css } from 'styled-components'

import Text from 'App/shared/components/Text'
import ifProp from 'helpers/ifProp'
import theme from 'theme'

export const ParagraphStyle = styled(Text)`
  font-size: 0.875rem;
  line-height: 1.4;
  color: ${theme.colorDark};
  font-family: ${theme.preferredFont};
  ${ifProp('light', css`
    color: ${theme.colorMedium};
  `)};
  margin: 0 0 1rem 0;
`.withComponent('p')
