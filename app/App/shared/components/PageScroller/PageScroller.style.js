import styled from 'styled-components'

export const PageScrollerStyle = styled.div`
  max-width: 100%;
  overflow: auto;
  flex: 1;
`
