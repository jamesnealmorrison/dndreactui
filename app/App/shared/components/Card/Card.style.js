import styled, { css, keyframes } from 'styled-components'

import { HeaderCellInner, HeaderCellOuter } from 'App/shared/components/Table/Table.style'
import { phoneOnly } from 'helpers/styleMixins/media'
import { shadow2dp } from 'helpers/styleMixins/shadows.style'
import Table from 'App/shared/components/Table'
import ifProp from 'helpers/ifProp'
import theme from 'theme'

const fadeIn = keyframes`
  from {
    opacity: 0;
    transform: scale3d(0.8,0.8,0.8);
  } to {
    opacity: 1;
  }
`

export const CardStyle = styled.div`
  ${shadow2dp()};
  padding: 1.5rem 2rem;
  background-color: white;
  border-radius: 2px;
  color: ${theme.colorDark};
  width: ${p => p.width || 'auto'};
  max-width: 100%;
  ${ifProp('center', css`
    margin: 0 auto;
  `)}
  ${phoneOnly(css`
    padding: 1.5rem;
  `)};
  ${ifProp('noMarginOnMobile', css`
    ${phoneOnly(css`
      margin-left: -1rem;
      margin-right: -1rem;
      max-width: calc(100% + 2rem);
      ${ifProp('center', css`
        margin-left: -1rem;
        margin-right: -1rem;
      `)}
    `)};
  `)}
  ${ifProp('fadeIn', css`
    animation-duration: 150ms;
    animation-fill-mode: forwards;
    animation-timing-function: ${theme.animationCurveLinearOutSlowIn};
    animation-name: ${fadeIn};
  `)}
`

export const CardDivider = styled.hr`
  margin: 1.5rem -2rem;
  border: 0;
  border-bottom: 1px solid ${theme.colorFaint};
  ${phoneOnly(css`
    padding: 1.5rem -1.5rem;
  `)};
`

export const CardActions = styled.div`
  position: relative;
  padding: 1rem 0;
  display: flex;
  flex-direction: row-reverse;
  flex-wrap: wrap;
  margin: 0 -2rem -1.5rem -2rem;
  ${phoneOnly(css`
    margin: 0 -1.5rem -1.5rem -1.5rem;
  `)};
  > * {
    &:last-child {
      margin-left: 1rem;
    }
    margin-right: 1rem;
  }
  ${ifProp('withDivider', css`
    border-top: 1px solid #dedede;
  `)}
`

export const CardTitle = styled.h1`
  font-family: ${theme.preferredFont};
  font-size: 1.5rem;
  margin: 0;
  line-height: 1.16;
  font-weight: 600;
  ${phoneOnly(css`
    font-size: 1.25rem;
  `)};
`

export const CardTable = styled(Table)`
  margin: 0.5rem -2rem -1.5rem -2rem;
  width: calc(100% + 4rem);
  max-width:  calc(100% + 4rem);
  ${phoneOnly(css`
    margin: 0.5rem -1.5rem -1.5rem -1.5rem;
    width: calc(100% + 3rem);
  `)};
  ${Table.Cell}:first-child,
  ${HeaderCellOuter}:first-child > ${HeaderCellInner} {
      padding-left: 2rem;
  }
  ${Table.Cell}:last-child,
  ${HeaderCellOuter}:last-child > ${HeaderCellInner} {
      padding-right: 2rem;
  }
  ${HeaderCellOuter} {
    background: white;
  }
  ${Table.Footer} {
    ${Table.Row}:last-child ${Table.Cell} {
      border-bottom: 0;
    }
  }
`
