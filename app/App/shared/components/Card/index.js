import {
  CardStyle as Card,
  CardActions,
  CardDivider,
  CardTable,
  CardTitle,
} from 'App/shared/components/Card/Card.style'

Card.Title = CardTitle
Card.Table = CardTable
Card.Divider = CardDivider
Card.Actions = CardActions

export default Card
