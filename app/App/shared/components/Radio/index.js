import PropTypes from 'prop-types'
import React from 'react'

import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

import {
  InnerCircle,
  OuterCircle,
  RadioButton,
  RadioLabel,
  RadioStyle,
  Focus,
} from './Radio.style'

export function Radio({ label, format, disabled, ...props }) {
  return (
    <RadioStyle>
      <RadioButton type="radio" {...props} disabled={disabled} />
      {label && <RadioLabel disabled={disabled}>{label}</RadioLabel>}
      <OuterCircle disabled={disabled} />
      <InnerCircle disabled={disabled} />
      <Focus />
    </RadioStyle>
  )
}

Radio.propTypes = {
  label: PropTypes.string,
  disabled: PropTypes.bool,
  format: PropTypes.func,
}

export default setDisplayName('Radio')(translateProps(['label'])(Radio))
