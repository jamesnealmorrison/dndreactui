import styled, { css } from 'styled-components'

import { materialAnimationDefault } from 'helpers/styleMixins/animations.style'
import ifProp from 'helpers/ifProp'
import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'

const radioLabelFontSize = 1
const radioLabelHeight = 1.5
const radioButtonSize = 1
const radioPadding = 0.5
const radioInnerMargin = radioButtonSize / 4
const radioTopOffset = (radioLabelHeight - radioButtonSize) / 2

export const RadioStyle = setDisplayName('RadioStyle')(
  styled.label`
    position: relative;
    font-size: ${radioLabelFontSize}rem;
    line-height: ${radioLabelHeight}rem;
    display: inline-block;
    vertical-align: middle;
    box-sizing: border-box;
    height: ${radioLabelHeight}rem;
    margin: 0;
    padding-left: ${radioButtonSize + radioPadding}rem;
    padding-right: 1.5rem;
  `)

export const RadioButton = setDisplayName('RadioButton')(
  styled.input`
    line-height: ${radioLabelHeight}px;
    position: absolute;
    width: 0;
    height: 0;
    margin: 0;
    padding: 0;
    opacity: 0;
    -ms-appearance: none;
    -moz-appearance: none;
    -webkit-appearance: none;
    appearance: none;
    border: none;
    &:checked ~ div:first-of-type {
      border: 2px solid ${theme.colorPrimary};
    }
    &:checked ~ div:nth-of-type(2) {
      transform: scale(1, 1);
    }
  `)

export const OuterCircle = setDisplayName('OuterCircle')(
  styled.div`
    position: absolute;
    top: ${radioTopOffset}rem;
    left: 0;
    display: inline-block;
    box-sizing: border-box;
    width: ${radioButtonSize}rem;
    height: ${radioButtonSize}rem;
    margin: 0;
    cursor: pointer;
    border: 2px solid ${theme.colorMedium};
    border-radius: 50%;
    z-index: 2;

    ${ifProp('disabled', css`
      border: 2px solid ${theme.colorLight};
      cursor: auto;
    `)};
  `)

export const InnerCircle = setDisplayName('InnerCircle')(
  styled.div`
    position: absolute;
    z-index: 1;
    margin: 0;
    top: ${radioTopOffset + radioInnerMargin}rem;
    left: ${radioInnerMargin}rem;
    box-sizing: border-box;
    width: ${radioButtonSize - radioInnerMargin * 2}rem;
    height: ${radioButtonSize - radioInnerMargin * 2}rem;
    cursor: pointer;
    ${materialAnimationDefault('0.28s')}
    transition-property: transform;
    transform: scale(0, 0);
    border-radius: 50%;
    background: ${theme.colorPrimary};
    ${ifProp('disabled', css`
      background: ${theme.colorLight};
      cursor: auto;
    `)}
    ${ifProp('focused', css`
      box-shadow: 0 0 0px 10px rgba(0, 0, 0, 0.1);
    `)};
  `)

export const RadioLabel = setDisplayName('RadioLabel')(
  styled.span`
    cursor: pointer;
    ${ifProp('disabled', css`
      color: ${theme.colorLight};
      cursor: auto;
    `)};
  `)

export const Focus = setDisplayName('Focus')(
  styled.div`
    border-radius: 50%;
    content: '';
    display: block;
    opacity: 0;
    background: ${theme.colorMedium};
    ${materialAnimationDefault('0.28s')}
    transition-property: transform, opacity;
    transform: scale(0.1);
    height: 2.75rem;
    width: 2.75rem;
    position: absolute;
    top: -0.625rem;
    left: -0.875rem;
    z-index: 0;
    ${RadioButton}:focus:not(:disabled) ~ &,
    ${RadioButton}:active:not(:disabled) ~ & {
      background: ${theme.colorPrimary};
      transform: scale(1);
      opacity: 0.24;
    }
  `
)
