import styled from 'styled-components'

import setDisplayName from 'App/shared/higherOrderComponents/setDisplayName'
import theme from 'theme'

export const ErrorText = setDisplayName('ErrorText')(
  styled.span`
    color: ${theme.colorDanger};
  `
)
