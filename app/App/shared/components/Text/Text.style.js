import styled, { css } from 'styled-components'
import theme from 'theme'
import ifProp from 'helpers/ifProp'

export const Text = styled.span`
  font-size: 1rem;
  line-height: 1.4;
  color: ${theme.colorDark};
  font-family: ${theme.preferredFont};
  ${ifProp(
    'light',
    css`
      color: ${theme.colorMedium};
    `,
  )};
`
