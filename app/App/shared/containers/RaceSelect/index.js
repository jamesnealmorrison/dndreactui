import { connect } from 'react-redux'
import React from 'react'
import compose from 'lodash/fp/flowRight'
import get from 'lodash/fp/get'

import { getRaces } from 'store/races/races.actions'
import { selectRaces } from 'store/races/races.selectors'
import { withAlertError } from 'helpers/errorHandlers'
import Select from 'App/shared/components/Select'
import raceSelectMessages from 'App/shared/containers/RaceSelect/messages'
import onMount from 'App/shared/higherOrderComponents/onMount'

const RaceSelect = props => (
  <Select
    monospace
    width="auto"
    inputWidth="150px"
    maxLength={30}
    valueKey="id"
    labelKey="name"
    label={raceSelectMessages.label}
    {...props}
  />
)

export default compose(
  connect(state => {
    return { options: selectRaces(state) };
  }, {
    getRaces: compose(withAlertError, getRaces),
  }),
  onMount(p => p.getRaces()),
)(RaceSelect)
