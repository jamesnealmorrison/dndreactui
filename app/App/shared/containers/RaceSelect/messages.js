import { defineMessages } from 'react-intl'

export default defineMessages({
  label: {
    id: 'shared.containers.RaceSelect.label',
    defaultMessage: 'Race',
  },
})
