import { recordOf } from 'react-immutable-proptypes'
import PropTypes from 'prop-types'
import React from 'react'
import {
  FilterField,
  FilterOperator,
  FilterRowStyle,
  FilterValue,
} from 'App/shared/containers/Filters/Filters.style'
import Button from 'App/shared/components/Button'
import SVGIcon from 'App/shared/components/SVGIcon'
import Select from 'App/shared/components/Select'

const FilterRow = ({
  onClickDelete,
  filter,
  onEditField,
  onEditOperator,
  onEditValue,
  isLast,
  fields,
}) => {
  const FieldComponent = filter.field ? fields[filter.field].component : Select
  return (
    <FilterRowStyle>
      <FilterField>
        <Select
          options={Object.values(fields)}
          value={fields[filter.field]}
          onChange={o => onEditField(o.field)}
          data-test-id="filter-field"
          labelKey="label"
          valueKey="field"
          inputWidth="100%"
          width="100%"
          autoFocus={isLast && !filter.field}
          fillSpace
        />
      </FilterField>
      <FilterOperator>
        <Select
          disabled={!filter.field}
          options={filter.field ? fields[filter.field].operators : []}
          value={filter.operator}
          onChange={o => onEditOperator(o.value)}
          data-test-id="filter-operator"
          labelKey="label"
          valueKey="value"
          inputWidth="100%"
          width="100%"
          fillSpace
        />
      </FilterOperator>
      <FilterValue>
        <FieldComponent
          value={filter.value}
          disabled={!filter.field}
          onChange={onEditValue}
          data-test-id="filter-value"
          inputWidth="100%"
          width="100%"
          fillSpace
        />
      </FilterValue>
      <Button.Icon data-test-id="delete-filter" onClick={onClickDelete}>
        <SVGIcon name="navigation.close" />
      </Button.Icon>
    </FilterRowStyle>
  )
}

FilterRow.propTypes = {
  onClickDelete: PropTypes.func,
  isLast: PropTypes.bool,
  filter: recordOf({
    field: PropTypes.string,
    operator: PropTypes.string,
    value: PropTypes.any,
  }),
  onEditField: PropTypes.func,
  onEditOperator: PropTypes.func,
  onEditValue: PropTypes.func,
  fields: PropTypes.object,
}

export default FilterRow
