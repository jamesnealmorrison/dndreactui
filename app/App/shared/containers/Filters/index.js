import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux'
import { listOf, recordOf } from 'react-immutable-proptypes'
import PropTypes from 'prop-types'
import React from 'react'

import { FilterRecord } from 'store/filters/filters.records'
import {
  FilterTitle,
  FilterTitleRow,
  FilterWhere,
} from 'App/shared/containers/Filters/Filters.style'
import {
  addFilter,
  applyFilters,
  clearFilters,
  editFilter,
  hideFilters,
  removeFilter,
  revertFilters,
} from 'store/filters/filters.actions'
import { selectPendingFilters } from 'store/filters/filters.selectors'
import Button from 'App/shared/components/Button'
import Card from 'App/shared/components/Card'
import Container from 'App/shared/components/Container'
import Fill from 'App/shared/components/Fill'
import FilterRow from 'App/shared/containers/Filters/FilterRow'
import FiltersMessages from 'App/shared/containers/Filters/messages'
import Offset from 'App/shared/components/Offset'
import SVGIcon from 'App/shared/components/SVGIcon'

export const FiltersBase = ({
  addFilter,
  clearFilters,
  removeFilter,
  editFilter,
  revertFilters,
  applyFilters,
  pendingFilters,
  hideFilters,
  fields,
  onChange,
}) => (
  <Container>
    <Offset vertical={4} />
    <Card width="40rem" noMarginOnMobile center fadeIn>
      <FilterTitleRow>
        <FilterTitle>
          <FormattedMessage {...FiltersMessages.title} />
        </FilterTitle>
        <Fill />
        <Button onClick={clearFilters} data-test-id="clear-filters">
          <FormattedMessage {...FiltersMessages.clearFilters} />
        </Button>
      </FilterTitleRow>
      {!pendingFilters.isEmpty() && (
        <FilterWhere>
          <FormattedMessage {...FiltersMessages.where} />
        </FilterWhere>
      )}
      {pendingFilters.map((filter, i) => (
        <FilterRow
          fields={fields}
          isLast={i === pendingFilters.size - 1}
          key={i} // eslint-disable-line react/no-array-index-key
          filter={filter}
          onEditField={field => editFilter(i, FilterRecord({ field }))}
          onEditOperator={operator =>
            editFilter(i, filter.set('operator', operator))
          }
          onEditValue={value => editFilter(i, filter.set('value', value))}
          onClickDelete={() => removeFilter(i)}
        />
      ))}
      <Offset horizontal={-1}>
        <Button data-test-id="add-filter" primary onClick={addFilter}>
          <SVGIcon forButtonLeft name="content.add" />
          <FormattedMessage {...FiltersMessages.addFilter} />
        </Button>
      </Offset>
      <Card.Actions>
        <Button.Raised
          accent
          data-test-id="apply-filters"
          onClick={() => {
            applyFilters()
            onChange()
            hideFilters()
          }}
        >
          <FormattedMessage {...FiltersMessages.apply} />
        </Button.Raised>
        <Fill />
        <Button
          data-test-id="revert-filters"
          onClick={() => {
            revertFilters()
            hideFilters()
          }}
        >
          <FormattedMessage {...FiltersMessages.cancel} />
        </Button>
      </Card.Actions>
    </Card>
    <Offset vertical={4} />
  </Container>
)

FiltersBase.propTypes = {
  addFilter: PropTypes.func,
  clearFilters: PropTypes.func,
  removeFilter: PropTypes.func,
  editFilter: PropTypes.func,
  revertFilters: PropTypes.func,
  applyFilters: PropTypes.func,
  push: PropTypes.func,
  pendingFilters: listOf(
    recordOf({
      field: PropTypes.string,
      operator: PropTypes.string,
      value: PropTypes.any,
    }),
  ),
  hideFilters: PropTypes.func,
  fields: PropTypes.object,
  onChange: PropTypes.func,
}

export default connect(
  state => ({
    pendingFilters: selectPendingFilters(state),
  }),
  {
    clearFilters,
    removeFilter,
    editFilter,
    addFilter,
    revertFilters,
    applyFilters,
    hideFilters,
  },
)(FiltersBase)
