import { defineMessages } from 'react-intl'

export default defineMessages({
  title: {
    id: 'routes.PlayerCharactersPage.routes.ViewAllMSE.containers.Filters.title',
    defaultMessage: 'Filter MSE',
  },
  apply: {
    id: 'routes.PlayerCharactersPage.routes.ViewAllMSE.containers.Filters.apply',
    defaultMessage: 'Apply',
  },
  cancel: {
    id: 'routes.PlayerCharactersPage.routes.ViewAllMSE.containers.Filters.cancel',
    defaultMessage: 'Cancel',
  },
  addFilter: {
    id: 'routes.PlayerCharactersPage.routes.ViewAllMSE.containers.Filters.addFilter',
    defaultMessage: 'Add Filter',
  },
  clearFilters: {
    id:
      'routes.PlayerCharactersPage.routes.ViewAllMSE.containers.Filters.clearFilters',
    defaultMessage: 'Clear Filters',
  },
  where: {
    id: 'routes.PlayerCharactersPage.routes.ViewAllMSE.containers.Filters.where',
    defaultMessage: 'Where',
  },
})
