import { Field } from 'redux-form/immutable'
import PropTypes from 'prop-types'
import React from 'react'

import Textfield from 'App/shared/components/Textfield'

export const FieldComponent = ({ input, label, type, meta, ...props }) => (
  <Textfield
    {...props}
    {...input}
    label={label}
    type={type}
    focused={meta.active}
    error={meta.touched ? meta.error : undefined}
  />
)

FieldComponent.propTypes = {
  input: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  type: PropTypes.string,
  meta: PropTypes.shape({
    active: PropTypes.bool,
    touched: PropTypes.bool,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  }),
}

const ConnectedTextfield = props => (
  <Field {...props} component={FieldComponent} />
)

export default ConnectedTextfield
