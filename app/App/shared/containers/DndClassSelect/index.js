import { connect } from 'react-redux'
import React from 'react'
import compose from 'lodash/fp/flowRight'
import get from 'lodash/fp/get'

import { getDndClasses } from 'store/dndClasses/dndClasses.actions'
import { selectDndClasses } from 'store/dndClasses/dndClasses.selectors'
import { withAlertError } from 'helpers/errorHandlers'
import Select from 'App/shared/components/Select'
import dndClassSelectMessages from 'App/shared/containers/DndClassSelect/messages'
import onMount from 'App/shared/higherOrderComponents/onMount'

const DndClassSelect = props => (
  <Select
    monospace
    width="auto"
    inputWidth="150px"
    maxLength={30}
    valueKey="id"
    labelKey="name"
    label={dndClassSelectMessages.label}
    {...props}
  />
)

export default compose(
  connect(state => {
    return { options: selectDndClasses(state) };
  }, {
    getDndClasses: compose(withAlertError, getDndClasses),
  }),
  onMount(p => p.getDndClasses()),
)(DndClassSelect)
