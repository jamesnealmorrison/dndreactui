import { defineMessages } from 'react-intl'

export default defineMessages({
  label: {
    id: 'shared.containers.DndClassSelect.label',
    defaultMessage: 'Class',
  },
})
