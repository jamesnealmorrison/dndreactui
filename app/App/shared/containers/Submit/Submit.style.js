import styled, { css } from 'styled-components'

import { SpinnerBase } from 'App/shared/components/Spinner/Spinner.style'
import Button from 'App/shared/components/Button'
import ifProp from 'helpers/ifProp'

export const SpinnerWrap = styled.div`
  position: absolute;
  right: 6px;
  top: 50%;
  margin-top: -14px;
  transform: scale(0.7);
  ${SpinnerBase} > div {
    border-color: #888;
  }
`

export const SubmitButton = styled(Button.Raised)`
  ${ifProp('submitting', css`
    padding-right: 40px;
  `)};
`
