import { connect } from 'react-redux'
import {
  isInvalid,
  isPristine,
  isSubmitting,
  submit,
} from 'redux-form/immutable'
import PropTypes from 'prop-types'
import React from 'react'
import compose from 'lodash/fp/flowRight'

import {
  SpinnerWrap,
  SubmitButton,
} from 'App/shared/containers/Submit/Submit.style'
import Spinner from 'App/shared/components/Spinner'
import translateProps from 'App/shared/higherOrderComponents/translateProps'

export const SubmitBase = ({
  submitting,
  invalid,
  pristine,
  text,
  pendingText,
  submit,
  form,
  shouldBeDisabled,
  ...props
}) => (
  <SubmitButton
    type="submit"
    accent
    onClick={() => submit(form)}
    submitting={submitting}
    disabled={shouldBeDisabled({ invalid, pristine, submitting })}
    {...props}
  >
    {submitting ? pendingText : text}
    {submitting && (
      <SpinnerWrap>
        <Spinner singleColor />
      </SpinnerWrap>
    )}
  </SubmitButton>
)

SubmitBase.propTypes = {
  submitting: PropTypes.bool,
  invalid: PropTypes.bool,
  pristine: PropTypes.bool,
  text: PropTypes.node.isRequired,
  pendingText: PropTypes.node.isRequired,
  submit: PropTypes.func,
  form: PropTypes.string.isRequired,
  shouldBeDisabled: PropTypes.func,
}

SubmitBase.defaultProps = {
  shouldBeDisabled: p => p.submitting || p.invalid || p.pristine,
}

export default compose(
  connect(
    (state, ownProps) => ({
      submitting: isSubmitting(ownProps.form)(state),
      invalid: isInvalid(ownProps.form)(state),
      pristine: isPristine(ownProps.form)(state),
    }),
    { submit },
  ),
  translateProps(['pendingText', 'text']),
)(SubmitBase)
