import { connect } from 'react-redux'
import React from 'react'
import compose from 'lodash/fp/flowRight'
import get from 'lodash/fp/get'

import { withAlertError } from 'helpers/errorHandlers'
import Select from 'App/shared/components/Select'
import onMount from 'App/shared/higherOrderComponents/onMount'

const RaceChoiceSelect = props => (
  <Select
    monospace
    width="auto"
    inputWidth="150px"
    maxLength={30}
    valueKey="id"
    labelKey="name"
    label={props.name}
    options={props.options}
    {...props}
  />
)

export default RaceChoiceSelect
