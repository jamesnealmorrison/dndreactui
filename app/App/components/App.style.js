import styled, { css } from 'styled-components'

import { materialAnimationDefault } from 'helpers/styleMixins/animations.style'
import ifProp from 'helpers/ifProp'
import theme from 'theme'

export const AppWrap = styled.div`
  position: relative;
  padding-left: 0;
  transition-property: padding-left;
  ${materialAnimationDefault('0.3s')} z-index: 1;
  ${ifProp(
    'isOffset',
    css`
      @media (min-width: 960px) {
        padding-left: 16rem;
      }
    `,
  )};
`

export const PageWrap = styled.div`
  position: relative;
  height: 100vh;
  display: flex;
  flex-direction: column;
`

export const AppLoading = styled.div`
  display: flex;
  padding-top: 33vh;
  justify-content: center;
  background-color: ${theme.colorOffWhite};
  height: 100vh;
  width: 100vw;
`

export const AppLoadingWrap = styled.div`
  text-align: center;
`

export const AppLoadingLogo = styled.h1`
  font-size: 2.5rem;
  margin-bottom: 1.5rem;
  font-weight: 800;
  color: ${theme.colorPrimary};
`
