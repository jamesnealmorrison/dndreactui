import { getScreenForm } from 'helpers/screenForm'

export default function onScreenFormChange(cb) {
  let lastScreenForm = getScreenForm(window)
  function onResize() {
    const currentScreenForm = getScreenForm(window)

    if (currentScreenForm !== lastScreenForm) {
      cb(currentScreenForm)
    }

    lastScreenForm = currentScreenForm
  }

  window.addEventListener('resize', onResize)

  return {
    remove: () => window.removeEventListener('resize', onResize),
  }
}
