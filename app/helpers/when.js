import compose from 'lodash/fp/flowRight'
import find from 'lodash/fp/find'
import head from 'lodash/fp/head'
import last from 'lodash/fp/last'

const when = compose(last, find(head))

export default when
