import Color from 'color'

const setAlpha = (c, a) =>
  Color(c)
    .alpha(a)
    .rgbaString()

export default setAlpha
