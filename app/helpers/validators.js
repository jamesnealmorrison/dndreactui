import anyPass from 'lodash/fp/overSome'
import compose from 'lodash/fp/flowRight'
import equals from 'lodash/fp/isEqual'
import isNil from 'lodash/fp/isNil'
import trim from 'lodash/fp/trim'

import errors from 'intl/appMessages/errors'

// helpers
const isWhitespace = compose(equals(''), trim)
const validate = (msg, predicate) => value =>
  predicate(value) ? msg : undefined

export const required = validate(
  errors.required,
  anyPass([isNil, isWhitespace]),
)

export const minLength = (
  n,
  message = `Must be at least ${n} characters`,
) => s => (s.trim().length < n ? message : undefined)
