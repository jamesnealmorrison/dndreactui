import { css } from 'styled-components'

import theme from 'theme'

export function materialAnimationFastOutSlowIn(duration = '0.2s') {
  return css`
    transition-duration: ${duration};
    transition-timing-function: ${theme.animationCurveFastOutSlowIn};
  `
}

export function materialAnimationLinearOutSlowIn(duration = '0.2s') {
  return css`
    transition-duration: ${duration};
    transition-timing-function: ${theme.animationCurveLinearOutSlowIn};
  `
}

export function materialAnimationFastOutLinearIn(duration = '0.2s') {
  return css`
    transition-duration: ${duration};
    transition-timing-function: ${theme.animationCurveFastOutLinearIn};
  `
}

export function materialAnimationDefault(duration = '0.2s') {
  return css`
    transition-duration: ${duration};
    transition-timing-function: ${theme.animationCurveDefault};
  `
}
