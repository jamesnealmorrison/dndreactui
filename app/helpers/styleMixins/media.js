import { css } from 'styled-components'

import theme from 'theme'

export const phoneOnly = styles => css`
  @media (max-width: ${theme.tabletBreakpoint - 1}px) {
    ${styles};
  }
`

export const tabletUp = styles => css`
  @media (min-width: ${theme.tabletBreakpoint}px) {
    ${styles};
  }
`

export const mobileOnly = styles => css`
  @media (max-width: ${theme.desktopBreakpoint - 1}px) {
    ${styles};
  }
`

export const desktopOnly = styles => css`
  @media (min-width: ${theme.desktopBreakpoint}px) {
    ${styles};
  }
`

export const widescreenOnly = styles => css`
  @media (min-width: ${theme.widescreenBreakpoint}px) {
    ${styles};
  }
`
