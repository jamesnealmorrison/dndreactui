const asyncMap = asyncFn => async array => {
  const result = []

  /* eslint-disable */
  for (const item of array) {
    result.push(await asyncFn(item))
  }

  return result
}

export default asyncMap
