import compose from 'lodash/fp/flowRight'

import { showAlertError, showPageError } from 'store/errors/errors.actions'

export const withErrorHandler = handler => fn => dispatch =>
  dispatch(fn).catch(compose(dispatch, handler))

export const withPageError = withErrorHandler(showPageError)
export const withAlertError = withErrorHandler(showAlertError)
