import { change, untouch } from 'redux-form/immutable'

const resetField = form => field => dispatch => {
  dispatch(change(form, field, null))
  dispatch(untouch(form, field))
}

export default resetField
