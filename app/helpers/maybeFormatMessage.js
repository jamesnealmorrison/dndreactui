import { FormattedMessage } from 'react-intl'
import React from 'react'

import { isMessage } from 'App/shared/higherOrderComponents/translateProps'

const maybeFormatMessage = maybeMessage =>
  isMessage(maybeMessage) ? (
    <FormattedMessage {...maybeMessage} />
  ) : (
    maybeMessage
  )

export default maybeFormatMessage
