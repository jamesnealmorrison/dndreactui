import Fuse from 'fuse-immutable'
import curry from 'lodash/fp/curry'

const fuzzySearch = curry((keys, filterString, list) => {
  const filter = filterString.trim()

  if (filter === '') {
    return list
  }

  const options = {
    threshold: 0,
    tokenize: true,
    matchAllTokens: true,
    keys,
  }

  return new Fuse(list, options).search(filter).toList()
})

export default fuzzySearch
