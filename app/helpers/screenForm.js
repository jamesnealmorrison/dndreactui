import always from 'lodash/fp/constant'
import cond from 'lodash/fp/cond'

import theme from 'theme'

export const PHONE = 'PHONE'
export const TABLET = 'TABLET'
export const DESKTOP = 'DESKTOP'
export const WIDESCREEN = 'WIDESCREEN'

export const isPhone = ({ innerWidth }) => innerWidth < theme.tabletBreakpoint
export const isTablet = ({ innerWidth }) =>
  innerWidth < theme.desktopBreakpoint && innerWidth >= theme.tabletBreakpoint
export const isDesktop = ({ innerWidth }) =>
  innerWidth < theme.widescreenBreakpoint &&
  innerWidth >= theme.desktopBreakpoint
export const isWidescreen = ({ innerWidth }) =>
  innerWidth >= theme.widescreenBreakpoint

export const getScreenForm = cond([
  [isPhone, always(PHONE)],
  [isTablet, always(TABLET)],
  [isDesktop, always(DESKTOP)],
  [isWidescreen, always(WIDESCREEN)],
])
