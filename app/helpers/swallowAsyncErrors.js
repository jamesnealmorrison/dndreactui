import noop from 'lodash/fp/noop'

const swallowAsyncErrors = promise => promise.catch(noop)

export default swallowAsyncErrors
