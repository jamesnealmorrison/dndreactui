import { curry, cond, negate } from 'lodash/fp'
import T from 'lodash/fp/stubTrue'
import identity from 'lodash/fp/identity'

const unless = curry((f, g) => cond([[negate(f), g], [T, identity]]))

export default unless
