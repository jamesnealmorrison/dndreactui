import { defineMessages } from 'react-intl'

export default defineMessages({
  cancel: {
    id: 'app.labels.cancel',
    defaultMessage: 'cancel',
  },
})
