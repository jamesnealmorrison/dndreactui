import { defineMessages } from 'react-intl'

export default defineMessages({
  required: {
    id: 'app.errors.required',
    defaultMessage: 'Required',
  },
})
