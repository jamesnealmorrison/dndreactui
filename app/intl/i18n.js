/**
 * i18n.js
 *
 * This will setup the i18n language files and locale data for your app.
 *
 */

import { addLocaleData } from 'react-intl'
import deLocaleData from 'react-intl/locale-data/de'
import enLocaleData from 'react-intl/locale-data/en'
import esLocaleData from 'react-intl/locale-data/es'
import frLocaleData from 'react-intl/locale-data/fr'
import ptLocaleData from 'react-intl/locale-data/pt'

import { DEFAULT_LOCALE } from 'store/app/app.records'

import deTranslationMessages from './translations/de.json'
import enTranslationMessages from './translations/en.json'
import esTranslationMessages from './translations/es.json'
import frTranslationMessages from './translations/fr.json'
import ptTranslationMessages from './translations/pt.json'

addLocaleData(deLocaleData)
addLocaleData(enLocaleData)
addLocaleData(esLocaleData)
addLocaleData(frLocaleData)
addLocaleData(ptLocaleData)

export const appLocales = ['en', 'fr', 'es', 'de', 'pt']

export const formatTranslationMessages = (locale, messages) => {
  const defaultFormattedMessages =
    locale !== DEFAULT_LOCALE
      ? formatTranslationMessages(DEFAULT_LOCALE, enTranslationMessages)
      : {}
  return Object.keys(messages).reduce((formattedMessages, key) => {
    const formattedMessage =
      !messages[key] && locale !== DEFAULT_LOCALE
        ? defaultFormattedMessages[key]
        : messages[key]
    return Object.assign(formattedMessages, { [key]: formattedMessage })
  }, {})
}

export const translationMessages = {
  en: formatTranslationMessages('en', enTranslationMessages),
  es: formatTranslationMessages('es', esTranslationMessages),
  fr: formatTranslationMessages('fr', frTranslationMessages),
  de: formatTranslationMessages('de', deTranslationMessages),
  pt: formatTranslationMessages('pt', ptTranslationMessages),
}
