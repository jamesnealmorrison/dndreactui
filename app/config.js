export default {
  apiUrl:
    process.env.NODE_ENV === 'development'
      ? `http://${window.location.hostname}:8090/mse-svc`
      : '/mse-svc',
}
