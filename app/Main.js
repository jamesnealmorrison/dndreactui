import 'babel-polyfill'
import 'sanitize.css/sanitize.css'

import '!file-loader?name=[name].[ext]!boot/images/favicon.png'
import '!file-loader?name=[name].[ext]!boot/manifest.json'
import 'boot/global-styles'

import { AppContainer } from 'react-hot-loader'
import { translationMessages } from 'intl/i18n'
import Modal from 'react-modal'
import React from 'react'
import ReactDOM from 'react-dom'
import createHistory from 'history/createBrowserHistory'

import App from 'App'
import AppProviders from 'AppProviders'
import configureStore from 'store/configureStore'

/* eslint-enable import/no-unresolved, import/extensions */

const initialState = {}
const history = createHistory()
const store = configureStore(initialState, history)
const MOUNT_NODE = document.getElementById('app')
Modal.setAppElement(MOUNT_NODE)

const publicStaticVoidMain = (Component, messages) => {
  ReactDOM.render(
    <AppContainer>
      <AppProviders store={store} messages={messages} history={history}>
        <Component />
      </AppProviders>
    </AppContainer>,
    MOUNT_NODE,
  )
}

// Chunked polyfill for browsers without Intl support
const intlPolyfill = () =>
  Promise.resolve(import('intl')).then(() =>
    Promise.all([
      import('intl/locale-data/jsonp/en.js'),
      import('intl/locale-data/jsonp/de.js'),
      import('intl/locale-data/jsonp/es.js'),
      import('intl/locale-data/jsonp/fr.js'),
    ]),
  )

if (!window.Intl) {
  intlPolyfill().then(() => publicStaticVoidMain(App, translationMessages))
} else {
  publicStaticVoidMain(App, translationMessages)
}

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept('./App', () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE)
    const NextApp = require('./App').default // eslint-disable-line global-require
    publicStaticVoidMain(NextApp, translationMessages)
  })
}
