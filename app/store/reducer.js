import { Record } from 'immutable'
import { combineReducers } from 'redux-immutable'
import { reducer as form } from 'redux-form/immutable'
import always from 'lodash/fp/constant'
import mapValues from 'lodash/fp/mapValues'

import app from 'store/app/app.reducer'
import errors from 'store/errors/errors.reducer'
import races from 'store/races/races.reducer'
import dndClasses from 'store/dndClasses/dndClasses.reducer'
import messages from 'store/messages/messages.reducer'
import route from 'store/route/route.reducer'
import filters from 'store/filters/filters.reducer'

const reducers = {
  form,
  app,
  races,
  dndClasses,
  errors,
  messages,
  route,
  filters,
}

export const GlobalStateRecord = new Record(
  mapValues(always(undefined), reducers),
)

export default combineReducers(reducers)
