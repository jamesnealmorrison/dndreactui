// Actions
import {
  ADD_FILTER,
  APPLY_FILTERS,
  CLEAR_FILTERS,
  EDIT_FILTER,
  REMOVE_FILTER,
  REVERT_FILTERS,
  SET_IS_SHOWING_FILTERS,
} from 'store/filters/filters.types'

export const addFilter = () => ({
  type: ADD_FILTER,
})

export const editFilter = (index, payload) => ({
  type: EDIT_FILTER,
  payload,
  meta: { index },
})

export const removeFilter = payload => ({
  type: REMOVE_FILTER,
  payload,
})

export const clearFilters = () => ({
  type: CLEAR_FILTERS,
})

export const applyFilters = () => ({
  type: APPLY_FILTERS,
})

export const revertFilters = () => ({
  type: REVERT_FILTERS,
})

export const showFilters = () => ({
  type: SET_IS_SHOWING_FILTERS,
  payload: true,
})

export const hideFilters = () => ({
  type: SET_IS_SHOWING_FILTERS,
  payload: false,
})
