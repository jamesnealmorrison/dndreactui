import moment from 'moment'

import { filterOperators } from 'store/filters/filters.records'

export const selectFilters = state => state.filters.records
export const selectPendingFilters = state => state.filters.pending
export const selectIsShowingFilters = state => state.filters.isShowingFilters
export const selectFiltersCount = state => selectFilters(state).size

const endOfDay = date =>
  moment(date)
    .endOf('day')
    .toISOString()
const startOfDay = date =>
  moment(date)
    .startOf('day')
    .toISOString()

export const computeMergedFilters = state =>
  selectFilters(state)
    .map(filter => {
      const { operator, value } = filter
      switch (operator) {
        case filterOperators.on.value:
          return filter
            .set('operator', filterOperators.between.value)
            .set('value', `${startOfDay(value)}/${endOfDay(value)}`)
        case filterOperators.before.value:
          return filter.set('value', startOfDay(value))
        case filterOperators.after.value:
          return filter.set('value', endOfDay(value))
        default:
          return filter
      }
    })
    .groupBy(f => f.field + f.operator)
    .toList()
    .toJS()
    .map(filters => ({
      field: filters[0].field,
      operator: filters[0].operator,
      values: filters.map(({ value }) => value),
    }))
