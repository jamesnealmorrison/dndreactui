import { defineMessages } from 'react-intl'

export default defineMessages({
  installedInventoryItem: {
    id: 'store.inventoryItems.createdInventoryItem',
    defaultMessage: 'Installed new {equipmentCode}',
  },
  updatedStatusCode: {
    id: 'store.inventoryItems.updatedStatusCode',
    defaultMessage: 'Updated status to {statusCode}',
  },
})
