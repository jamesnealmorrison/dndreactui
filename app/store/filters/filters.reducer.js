import { LOCATION_CHANGE } from 'react-router-redux'
import { List } from 'immutable'
import allPass from 'lodash/fp/overEvery'
import anyPass from 'lodash/fp/overSome'
import compose from 'lodash/fp/flowRight'
import isEmpty from 'lodash/fp/isEmpty'
import isNil from 'lodash/fp/isNil'
import isString from 'lodash/fp/isString'
import negate from 'lodash/fp/negate'
import trim from 'lodash/fp/trim'

import {
  ADD_FILTER,
  APPLY_FILTERS,
  CLEAR_FILTERS,
  EDIT_FILTER,
  REMOVE_FILTER,
  REVERT_FILTERS,
  SET_IS_SHOWING_FILTERS,
} from 'store/filters/filters.types'
import { FilterRecord, FiltersStateRecord } from 'store/filters/filters.records'

const isBlank = compose(
  isEmpty,
  trim,
)
const isBlankString = allPass([isBlank, isString])
const isFilled = negate(anyPass([isNil, isBlankString]))
const hasValueFor = k => o => isFilled(o[k])

export default function filtersReducer(state = FiltersStateRecord(), action) {
  switch (action.type) {
    case ADD_FILTER:
      return state.update('pending', filters => filters.push(FilterRecord()))
    case EDIT_FILTER:
      return state.setIn(
        ['pending', action.meta.index],
        FilterRecord(action.payload),
      )
    case REMOVE_FILTER:
      return state.deleteIn(['pending', action.payload])
    case CLEAR_FILTERS:
      return state.set('pending', List())
    case APPLY_FILTERS:
      return state.withMutations(mut => {
        mut.update('pending', filters =>
          filters.filter(
            allPass([
              hasValueFor('field'),
              hasValueFor('operator'),
              hasValueFor('value'),
            ]),
          ),
        )
        mut.set('records', mut.pending)
      })
    case REVERT_FILTERS:
      return state.set('pending', state.records)
    case SET_IS_SHOWING_FILTERS:
      return state.set('isShowingFilters', action.payload)
    case LOCATION_CHANGE:
      return state.set('isShowingFilters', false)
    default:
      return state
  }
}
