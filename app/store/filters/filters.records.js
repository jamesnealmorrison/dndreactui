import { List, Record } from 'immutable'
import { defineMessages } from 'react-intl'

export const FilterRecord = new Record({
  field: '',
  operator: '',
  value: '',
})

export const FiltersStateRecord = new Record({
  records: List(),
  pending: List(),
  isShowingFilters: false,
})

export const operatorLabels = defineMessages({
  on: {
    id: 'store.filters.opeators.on',
    defaultMessage: 'on',
  },
  between: {
    id: 'store.filters.opeators.between',
    defaultMessage: 'between',
  },
  before: {
    id: 'store.filters.opeatorsLables.before',
    defaultMessage: 'before',
  },
  after: {
    id: 'store.filters.opeatorsLables.after',
    defaultMessage: 'after',
  },
  is: {
    id: 'store.filters.opeatorsLables.is',
    defaultMessage: 'is',
  },
  isNot: {
    id: 'store.filters.opeatorsLables.isNot',
    defaultMessage: 'is not',
  },
})

export const filterOperators = {
  on: {
    value: 'on',
    label: operatorLabels.on,
  },
  between: {
    value: 'between',
    label: operatorLabels.between,
  },
  before: {
    value: 'lessThan',
    label: operatorLabels.before,
  },
  after: {
    value: 'greaterThanOrEqual',
    label: operatorLabels.after,
  },
  is: {
    value: 'is',
    label: operatorLabels.is,
  },
  isNot: {
    value: 'isNot',
    label: operatorLabels.isNot,
  },
}
