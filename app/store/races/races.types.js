export const ADD_RACES = 'races/ADD_RACES'
export const SET_RACE_LOADING = 'races/SET_RACE_LOADING'
export const SET_RACE_LIST_FILTER = 'races/SET_RACE_LIST_FILTER'
