// Reducer
import { LOCATION_CHANGE } from 'react-router-redux'
import { OrderedMap } from 'immutable'

import {
  ADD_RACES,
  SET_RACE_LIST_FILTER,
  SET_RACE_LOADING,
} from 'store/races/races.types'
import {
  RaceRecord,
  RacesStateRecord,
} from 'store/races/races.records'

export default function racesReducer(
  state = RacesStateRecord(),
  action,
) {
  switch (action.type) {
    case ADD_RACES:
      return state.set(
        'records',
        OrderedMap(action.payload.map(r => [r.id, RaceRecord(r)])),
      )
    case SET_RACE_LOADING:
      return state.set('isLoading', action.payload)
    case SET_RACE_LIST_FILTER:
      return state.set('filter', action.payload)
    default:
      return state
  }
}
