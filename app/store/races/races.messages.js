import { defineMessages } from 'react-intl'

export default defineMessages({
  addedNewRace: {
    id: 'store.races.addedNewRace',
    defaultMessage: 'Added new race: {race}',
  },
})
