import { OrderedMap, Record } from 'immutable'

export const RacesStateRecord = new Record({
  records: OrderedMap(),
  isLoading: false,
  filter: '',
})

export const RaceRecord = new Record({
  id: null,
  name: '',
  raceChoices: null
})
