import get from 'lodash/fp/get'

import fuzzySearch from 'helpers/fuzzySearch'

export const selectRaces = state =>
  state.races.records
    .toList()
    .sort((a, b) => a.name.localeCompare(b.name))


export const selectIsLoadingRaces = state =>
  state.races.isLoading

export const selectRaceById = id => state =>
  state.races.records.get(Number(id))

export const selectRaceFilter = state => state.races.filter

export const computeRaceList = state =>
  fuzzySearch(
    ['name'],
    selectRaceFilter(state),
    selectRaces(state),
  )
