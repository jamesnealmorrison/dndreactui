import flatMap from 'lodash/fp/flatMap'

import {
  ADD_RACES,
  SET_RACE_LIST_FILTER,
  SET_RACE_LOADING,
} from 'store/races/races.types'

import { createMessage } from 'store/messages/messages.actions'
import { showAlertError } from 'store/errors/errors.actions'
import apiClient from 'apiClient'
import racesMessages from 'store/races/races.messages'
import swallowAsyncErrors from 'helpers/swallowAsyncErrors'

// Actions
export const addRaces = payload => ({
  type: ADD_RACES,
  payload,
})

export const setRaceListFilter = payload => ({
  type: SET_RACE_LIST_FILTER,
  payload,
})

export const setRacesLoading = payload => ({
  type: SET_RACE_LOADING,
  payload,
})

// Thunks
export const createRace = race => async dispatch => {
  await apiClient.post('/domain/race', race)
  await swallowAsyncErrors(dispatch(getRaces()))

  dispatch(
    createMessage({
      ...racesMessages.addedNewRace,
      values: { race: race.name },
    }),
  )
}

export const getRaces = () => async dispatch => {
  dispatch(setRacesLoading(true))
  // const races = await apiClient.get('/domain/race')
  const races = [
      {
        id: 1,
        name: "Dwarf",
        raceChoices: [
          {
            nameOfChoice: "Sub Race",
            validChoices: [
              {
                id: 1,
                name: "Hill Dwarf"
              },
              {
                id: 2,
                name: "Mountain Dwarf"
              }
            ]
          },
          {
            nameOfChoice: "Tool Proficiency",
            validChoices: [
              {
                id: 1,
                name: "Smith's Tools"
              },
              {
                id: 2,
                name: "Brewer's Tools"
              },
              {
                id: 3,
                name: "Mason's Tools"
              }
            ]
          }
        ]
      },
      {
        id: 2,
        name: "Elf",
        raceChoices: [
          {
            nameOfChoice: "Sub Race",
            validChoices: [
              {
                id: 1,
                name: "High Elf"
              },
              {
                id: 2,
                name: "Wood Elf"
              },
              {
                id: 3,
                name: "Dark Elf (Drow)"
              }
            ]
          },
          {
            nameOfChoice: "Cantrip",
            validChoices: [
              {
                id: 1,
                name: "Acid Splash"
              },
              {
                id: 2,
                name: "Blade Ward"
              },
              {
                id: 3,
                name: "Chill Touch"
              },
              {
                id: 4,
                name: "Dancing Lights"
              },
              {
                id: 5,
                name: "Fire Bolt"
              },
              {
                id: 6,
                name: "Friends"
              },
              {
                id: 7,
                name: "Light"
              },
              {
                id: 8,
                name: "Mage Hand"
              },
              {
                id: 9,
                name: "Mending"
              },
              {
                id: 10,
                name: "Message"
              },
              {
                id: 11,
                name: "Minor Illusion"
              },
              {
                id: 12,
                name: "Poison Spray"
              },
              {
                id: 13,
                name: "Prestidigitation"
              },
              {
                id: 14,
                name: "Ray of Frost"
              },
              {
                id: 15,
                name: "Shocking Grasp"
              },
              {
                id: 16,
                name: "True Strike"
              }
            ]
          },
          {
            nameOfChoice: "Extra Language",
            validChoices: [
              {
                id: 1,
                name: "Dwarvish"
              },
              {
                id: 2,
                name: "Giant"
              },
              {
                id: 3,
                name: "Gnomish"
              },
              {
                id: 4,
                name: "Goblin"
              },
              {
                id: 5,
                name: "Halfling"
              },
              {
                id: 6,
                name: "Orc"
              },
              {
                id: 7,
                name: "Abyssal"
              },
              {
                id: 8,
                name: "Celestial"
              },
              {
                id: 9,
                name: "Deep Speech"
              },
              {
                id: 10,
                name: "Draconic"
              },
              {
                id: 11,
                name: "Infernal"
              },
              {
                id: 12,
                name: "Primordial"
              },
              {
                id: 13,
                name: "Sylvan"
              },
              {
                id: 14,
                name: "Undercommon"
              }
            ]
          },
        ]
      },
      {
        id: 3,
        name: "Halfling",
        raceChoices: [
          {
            nameOfChoice: "Sub Race",
            validChoices: [
              {
                id: 1,
                name: "Lightfoot"
              },
              {
                id: 2,
                name: "Stout"
              }
            ]
          }
        ]
      },
      {
        id: 4,
        name: "Human",
      },
      {
        id: 5,
        name: "Dragonborn",
      },
      {
        id: 6,
        name: "Gnome",
        raceChoices: [
          {
            nameOfChoice: "Sub Race",
            validChoices: [
              {
                id: 1,
                name: "Forest Gnome"
              },
              {
                id: 2,
                name: "Rock Gnome"
              }
            ]
          }
        ]
      },
      {
        id: 7,
        name: "Half-Elf",
      },
      {
        id: 8,
        name: "Half-Orc",
      },
      {
        id: 9,
        name: "Tiefling",
      }
    ]

  dispatch(addRaces(races))
  dispatch(setRacesLoading(false))
}
