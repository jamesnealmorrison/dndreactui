import { LOCATION_CHANGE } from 'react-router-redux'

import { AppStateRecord } from 'store/app/app.records'
import {
  CHANGE_LOCALE,
  LOGIN,
  LOGOUT,
  SCREEN_FORM_CHANGE,
  SET_IS_EDITING_USER_LOCATION,
  SET_NAV_OPEN,
  SET_USER_LOCATION,
} from 'store/app/app.types'
import { WIDESCREEN } from 'helpers/screenForm'

export default function appReducer(state = AppStateRecord(), action) {
  switch (action.type) {
    case LOGIN:
      return state.set('apiToken', action.payload)
    case LOGOUT:
      return state.set('apiToken', null)
    case SET_NAV_OPEN:
      return state.set('isNavOpen', action.payload)
    case LOCATION_CHANGE:
      return state.screenForm !== WIDESCREEN
        ? state.set('isNavOpen', false)
        : state
    case SCREEN_FORM_CHANGE:
      return state
        .set('screenForm', action.payload)
        .set('isNavOpen', action.payload === WIDESCREEN)
    case CHANGE_LOCALE:
      return state.set('locale', action.payload)
    case SET_USER_LOCATION:
      return state.set('userLocation', action.payload)
    case SET_IS_EDITING_USER_LOCATION:
      return state.set('isEditingUserLocation', action.payload)
    default:
      return state
  }
}
