// Records
import { Record } from 'immutable'

import { getScreenForm } from 'helpers/screenForm'

export const DEFAULT_LOCALE = 'en'

export const AppStateRecord = new Record({
  isNavOpen: window.innerWidth > 960,
  screenForm: getScreenForm(window),
  userLocation: 'ABC123',
  isEditingUserLocation: false,
  locale: DEFAULT_LOCALE,
  apiToken: null,
})
