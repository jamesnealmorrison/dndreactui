// Selectors
import isNil from 'lodash/fp/isNil'

export const selectScreenForm = state => state.app.screenForm
export const selectIsNavOpen = state => state.app.isNavOpen
export const selectUserLocation = state => state.app.userLocation
export const selectLocale = state => state.app.locale
export const selectIsLoggedIn = state => !isNil(state.app.apiToken)
export const selectIsEditingUserLocation = state =>
  state.app.isEditingUserLocation
