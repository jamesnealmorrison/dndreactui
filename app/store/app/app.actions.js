import {
  CHANGE_LOCALE,
  LOGIN,
  LOGOUT,
  SCREEN_FORM_CHANGE,
  SET_IS_EDITING_USER_LOCATION,
  SET_NAV_OPEN,
  SET_USER_LOCATION,
} from 'store/app/app.types'
import apiClient from 'apiClient'

export const setNavOpen = payload => ({ type: SET_NAV_OPEN, payload })
export const openNav = () => setNavOpen(true)
export const closeNav = () => setNavOpen(false)
export const screenFormChange = payload => ({
  type: SCREEN_FORM_CHANGE,
  payload,
})

export const changeLocale = payload => ({
  type: CHANGE_LOCALE,
  payload,
})

export const setUserLocation = payload => async dispatch => {
  await apiClient.get(`/domain/location/${payload}`)
  dispatch({
    type: SET_USER_LOCATION,
    payload,
  })
}

export const setIsEditingUserLocation = payload => ({
  type: SET_IS_EDITING_USER_LOCATION,
  payload,
})

export const login = () => async dispatch => {
  const apiToken = 'token'
  await new Promise(resolve => setTimeout(resolve, 500))
  window.localStorage.setItem('apiToken', apiToken)
  dispatch({ type: LOGIN, payload: apiToken })
}

export const logout = () => async dispatch => {
  window.localStorage.removeItem('apiToken')
  dispatch({ type: LOGOUT })
}
