// Reducer
import { LOCATION_CHANGE } from 'react-router-redux'
import { OrderedMap } from 'immutable'

import {
  ADD_DND_CLASSES,
  SET_DND_CLASS_LIST_FILTER,
  SET_DND_CLASS_LOADING,
} from 'store/dndClasses/dndClasses.types'
import {
  DndClassRecord,
  DndClassesStateRecord,
} from 'store/dndClasses/dndClasses.records'

export default function dndClassesReducer(
  state = DndClassesStateRecord(),
  action,
) {
  switch (action.type) {
    case ADD_DND_CLASSES:
      return state.set(
        'records',
        OrderedMap(action.payload.map(r => [r.id, DndClassRecord(r)])),
      )
    case SET_DND_CLASS_LOADING:
      return state.set('isLoading', action.payload)
    case SET_DND_CLASS_LIST_FILTER:
      return state.set('filter', action.payload)
    default:
      return state
  }
}
