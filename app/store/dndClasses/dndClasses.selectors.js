import get from 'lodash/fp/get'

import fuzzySearch from 'helpers/fuzzySearch'

export const selectDndClasses = state =>
  state.dndClasses.records
    .toList()
    .sort((a, b) => a.name.localeCompare(b.name))


export const selectIsLoadingDndClasses = state =>
  state.dndClasses.isLoading

export const selectDndClassById = id => state =>
  state.dndClasses.records.get(Number(id))

export const selectDndClassFilter = state => state.dndClasses.filter

export const computeDndClassList = state =>
  fuzzySearch(
    ['name'],
    selectDndClassFilter(state),
    selectDndClasses(state),
  )
