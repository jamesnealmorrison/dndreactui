import { OrderedMap, Record } from 'immutable'

export const DndClassesStateRecord = new Record({
  records: OrderedMap(),
  isLoading: false,
  filter: '',
})

export const DndClassRecord = new Record({
  id: null,
  name: '',
})
