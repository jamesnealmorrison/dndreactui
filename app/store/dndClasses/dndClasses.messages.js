import { defineMessages } from 'react-intl'

export default defineMessages({
  addedNewDndClass: {
    id: 'store.dndClasses.addedNewDndClass',
    defaultMessage: 'Added new dndClass: {dndClass}',
  },
})
