import flatMap from 'lodash/fp/flatMap'

import {
  ADD_DND_CLASSES,
  SET_DND_CLASS_LIST_FILTER,
  SET_DND_CLASS_LOADING,
} from 'store/dndClasses/dndClasses.types'
import { createMessage } from 'store/messages/messages.actions'
import { showAlertError } from 'store/errors/errors.actions'
import apiClient from 'apiClient'
import dndClassesMessages from 'store/dndClasses/dndClasses.messages'
import swallowAsyncErrors from 'helpers/swallowAsyncErrors'

// Actions
export const addDndClasses = payload => ({
  type: ADD_DND_CLASSES,
  payload,
})

export const setDndClassListFilter = payload => ({
  type: SET_DND_CLASS_LIST_FILTER,
  payload,
})

export const setDndClassesLoading = payload => ({
  type: SET_DND_CLASS_LOADING,
  payload,
})

// Thunks
export const createDndClass = dndClass => async dispatch => {
  await apiClient.post('/domain/dndClass', dndClass)
  await swallowAsyncErrors(dispatch(getDndClasses()))

  dispatch(
    createMessage({
      ...dndClassesMessages.addedNewDndClass,
      values: { dndClass: dndClass.name },
    }),
  )
}

export const getDndClasses = () => async dispatch => {
  dispatch(setDndClassesLoading(true))
  // const dndClasses = await apiClient.get('/domain/class')
  const dndClasses = [
      {
        id: 1,
        name: "Barbarian",
      },
      {
        id: 2,
        name: "Bard",
      },
      {
        id: 3,
        name: "Cleric",
      },
      {
        id: 4,
        name: "Druid",
      },
      {
        id: 5,
        name: "Fighter",
      },
      {
        id: 6,
        name: "Monk",
      },
      {
        id: 7,
        name: "Paladin",
      },
      {
        id: 8,
        name: "Ranger",
      },
      {
        id: 9,
        name: "Rogue",
      },
      {
        id: 10,
        name: "Sorcerer",
      },
      {
        id: 11,
        name: "Warlock",
      },
      {
        id: 12,
        name: "Wizard",
      }
    ]

  dispatch(addDndClasses(dndClasses))
  dispatch(setDndClassesLoading(false))
}
