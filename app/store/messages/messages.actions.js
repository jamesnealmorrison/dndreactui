import { CREATE_MESSAGE, DELETE_MESSAGE } from 'store/messages/messages.types'

export const createMessage = (text, timeout = 3000) => ({
  type: CREATE_MESSAGE,
  payload: { text, timeout },
})

export const deleteMessage = payload => ({
  type: DELETE_MESSAGE,
  payload,
})
