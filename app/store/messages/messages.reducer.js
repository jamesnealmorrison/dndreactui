import { CREATE_MESSAGE, DELETE_MESSAGE } from 'store/messages/messages.types'
import {
  MessageRecord,
  MessagesStateRecord,
} from 'store/messages/messages.records'

export default function messagesReducer(state = MessagesStateRecord(), action) {
  switch (action.type) {
    case CREATE_MESSAGE:
      return state.update('messagesList', messages =>
        messages.push(
          new MessageRecord({
            text: action.payload.text,
            timeout: action.payload.timeout,
          }),
        ),
      )

    case DELETE_MESSAGE:
      return state.update('messagesList', messages =>
        messages.delete(messages.indexOf(action.payload)),
      )

    default:
      return state
  }
}
