import { List, Record } from 'immutable'

export const MessagesStateRecord = new Record({
  messagesList: List(),
})

export const MessageRecord = new Record({
  text: null,
  timeout: 3000,
})
