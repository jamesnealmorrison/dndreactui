export const selectMessagesList = state => state.messages.messagesList
export const selectCurrentMessage = state => state.messages.messagesList.first()
