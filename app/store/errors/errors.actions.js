import {
  CLEAR_ERROR,
  SHOW_ERROR,
  SHOW_PAGE_ERROR,
} from 'store/errors/errors.types'

export const showAlertError = ({ title = '', message = '' }) => ({
  type: SHOW_ERROR,
  payload: { title, message },
})

export const showPageError = ({ title = '', message = '' }) => ({
  type: SHOW_PAGE_ERROR,
  payload: { title, message, pageError: true },
})

export const clearError = () => ({ type: CLEAR_ERROR })
