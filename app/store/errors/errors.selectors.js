export const selectCurrentError = state => state.errors.records.first()
export const selectHasError = state => !state.errors.records.isEmpty()
