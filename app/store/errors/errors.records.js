// Records
import { List, Record } from 'immutable'

export const ErrorStateRecord = new Record({
  records: List(),
})

export const ErrorMessageRecord = new Record({
  title: '',
  message: '',
  pageError: false,
})
