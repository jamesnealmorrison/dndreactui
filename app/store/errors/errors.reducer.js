// Reducer
import { LOCATION_CHANGE } from 'react-router-redux'
import { List } from 'immutable'
import get from 'lodash/fp/get'

import {
  CLEAR_ERROR,
  SHOW_ERROR,
  SHOW_PAGE_ERROR,
} from 'store/errors/errors.types'
import {
  ErrorMessageRecord,
  ErrorStateRecord,
} from 'store/errors/errors.records'

export default function languageReducer(state = ErrorStateRecord(), action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return state.update('records', records =>
        records.filterNot(get('pageError')),
      )

    case SHOW_ERROR:
      return state.update('records', records =>
        records.push(ErrorMessageRecord(action.payload)),
      )

    case SHOW_PAGE_ERROR:
      return state.set('records', List([ErrorMessageRecord(action.payload)]))

    case CLEAR_ERROR:
      return state.update('records', records => records.shift())

    default:
      return state
  }
}
