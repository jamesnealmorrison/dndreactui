export const SHOW_ERROR = 'showError/SHOW_ERROR'
export const SHOW_PAGE_ERROR = 'errors/SHOW_PAGE_ERROR'
export const CLEAR_ERROR = 'errors/CLEAR_ERROR'
