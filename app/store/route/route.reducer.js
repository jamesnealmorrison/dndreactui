import { LOCATION_CHANGE } from 'react-router-redux'

import { RouteStateRecord } from 'store/route/route.records'

export default function routeReducer(state = RouteStateRecord(), action) {
  return action.type === LOCATION_CHANGE
    ? state.set('location', action.payload)
    : state
}
