import queryString from 'query-string'

export const selectLocation = state => state.route.location
export const selectLocationQuery = state =>
  queryString.parse(state.route.location.search)
