import { injectGlobal } from 'styled-components'

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
    font-size: 16px;
    background-color: #F5F5F5;
  }

  a {
    text-decoration: none;
  }

  p {
    margin: 0;
  }

  body {
    font-family: 'Roboto', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    letter-spacing: 0.04em;
  }

  body.fontLoaded {
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
  }

  #app {
    height: 100%;
    min-width: 100%;
  }

  ::-ms-clear {
    width : 0;
    height: 0;
  }

  h1, h2, h3, h4, h5, h6 {
    margin-top: 0;
  }
`
