import axios from 'axios'
import get from 'lodash/fp/get'
import isArray from 'lodash/fp/isArray'

import httpErrorCodes from 'helpers/httpErrorCodes'
import config from './config'

const apiClient = axios.create({
  baseURL: config.apiUrl,
  timeout: 100000,
  headers: { 'Content-Type': 'application/json' },
})

apiClient.interceptors.response.use(get('data'), err => {
  /* eslint-disable no-param-reassign */
  if (err.response) {
    err.status = err.response.status
    err.title = httpErrorCodes[err.response.status]
    if (isArray(err.response.data.errors)) {
      err.message = err.response.data.errors.join('\n')
    } else {
      err.message = 'Server Error'
    }
  } else if (err.code === 'ECONNABORTED') {
    err.status = 0
    err.title = 'Connection Issues'
    err.message =
      'Either something is wrong with your internet connection or MSE is having issues'
  } else {
    err.status = 0
    err.title = 'Error'
  }
  /* eslint-enable no-param-reassign */

  return Promise.reject(err)
})

export default apiClient
