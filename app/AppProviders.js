import { ConnectedRouter } from 'react-router-redux'
import { Provider } from 'react-redux'
import { translationMessages } from 'intl/i18n'
import PropTypes from 'prop-types'
import React from 'react'
import createHistory from 'history/createBrowserHistory'

import LanguageProvider from 'App/containers/LanguageProvider'
import configureStore from 'store/configureStore'
import LoginWrap from 'App/containers/LoginWrap'

const AppProviders = ({
  history = createHistory(),
  store = configureStore({}, history),
  messages = translationMessages,
  children,
}) => (
  <Provider store={store}>
    <LanguageProvider messages={messages}>
      <LoginWrap>
        <ConnectedRouter history={history}>{children}</ConnectedRouter>
      </LoginWrap>
    </LanguageProvider>
  </Provider>
)

AppProviders.propTypes = {
  history: PropTypes.any,
  store: PropTypes.any,
  messages: PropTypes.any,
  children: PropTypes.node,
}

export default AppProviders
